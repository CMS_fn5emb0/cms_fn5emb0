/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file ModuleTemplate.cpp
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "ModuleTemplate.h"

ModuleTemplate::ModuleTemplate()
	:FiniteStateMachineTemplate(){
}
ModuleTemplate::~ModuleTemplate() {
	// TODO Auto-generated destructor stub
}

ModuleTemplate::State ModuleTemplate::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
ModuleTemplate::State ModuleTemplate::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
ModuleTemplate::State ModuleTemplate::ST_Run(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
ModuleTemplate::State ModuleTemplate::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

ModuleTemplate::State ModuleTemplate::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
ModuleTemplate::State ModuleTemplate::ST_Initializing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
ModuleTemplate::State ModuleTemplate::ST_Running(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
ModuleTemplate::State ModuleTemplate::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
