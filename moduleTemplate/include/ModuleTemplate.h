/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file ModuleTemplate.h
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef ModuleTemplate_H_
#define ModuleTemplate_H_
#include "FiniteStateMachineTemplate.h"

/** @class ModuleTemplate
 *  @details This class incorporates the functionalities to
 *  		 release memory of the capture module
 */

class ModuleTemplate: public FiniteStateMachineTemplate {
public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	ModuleTemplate();

	/** @brief Free the resources generated for the object
	 */
	virtual ~ModuleTemplate();

private:

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state fail to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* MODULETEMPLATE_H_ */
