/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file DisplayFiniteStateMachine.h
*  @brief This file contains the definition of the class
*  		  DisplayFiniteStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef DISPLAYFINITESTATEMACHINE_H_
#define  DISPLAYFINITESTATEMACHINE_H_

#include "FiniteStateMachineTemplateWithAux.h"
#include "nvmedia_idp.h"
#include "capture2d.h"

/** @class DisplayFiniteStateMachine
 *  @details This class incorporates the functionalities to
 *  		 display images from the buffer.
 */
class DisplayFiniteStateMachine: public FiniteStateMachineTemplateWithAux {
public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	DisplayFiniteStateMachine(int16_t _displayId, int16_t _x0, int16_t _y0,int16_t _width, int16_t _height,
			int16_t _widthSensor, int16_t _heightSensor, NvMedia2D *_engine2d, NvMediaDevice *_hardwareHandle);

	/** @brief Free the resources generated for the object
	 */
	virtual ~DisplayFiniteStateMachine();

private:


	NvMedia2D *engine2d;
	NvMedia2DBlitParameters blitParams;

	/** @var hardwareHandle
	 *  	  Is used to manage the hardware.
	 */
	NvMediaDevice *hardwareHandle;
	//boost::circular_buffer<NvMediaImage*>::iterator videoAuxIt;
	NvMediaRect* dstRect;
	NvMediaRect* dstRectAux;

	BufferPool *displayAuxPool;

	/** @var displayId
	 *  	  Defines the id of the display we will use.
	 */
	int16_t displayId;

	/** @var dispResolution
	 *  	  Defines the resolution of the display.
	 */
	NvMediaIDPPreferences *dispResolution;

	/** @var display
	 *  	  Is used in order to manage the display.
	 */
	NvMediaIDP *display;


	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialized,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to failed,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* DISPLAYFINITESTATEMACHINE_H_ */
