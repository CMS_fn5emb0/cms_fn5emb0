/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file DisplayFiniteStateMachine.cpp
*  @brief This file contains the implementation of the class
*  		  DisplayFiniteStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "DisplayFiniteStateMachine.h"


DisplayFiniteStateMachine::DisplayFiniteStateMachine(int16_t _displayId, int16_t _x0, int16_t _y0,
													int16_t _width, int16_t _height, int16_t _widthSensor, int16_t _heightSensor,
													NvMedia2D *_engine2d, NvMediaDevice *_hardwareHandle)
	:FiniteStateMachineTemplateWithAux(){

	hardwareHandle=_hardwareHandle;
	engine2d=_engine2d;
	displayId = _displayId ;
	dispResolution = new NvMediaIDPPreferences;
	dispResolution->width = _width;
	dispResolution->height = _height;
	display = new NvMediaIDP;
	display = NvMediaIDPCreate(displayId, 0, NvMediaSurfaceType_Image_RGBA,
								   dispResolution, NVMEDIA_FALSE);

	dstRect = new NvMediaRect;
	dstRect->x0 = _x0;
	dstRect->y0 = _y0;
	dstRect->x1 = _width+_x0;
	dstRect->y1 = _height+_y0;

	dstRectAux = new NvMediaRect;
	dstRectAux->x0 = _x0;
	dstRectAux->y0 = _y0;
	dstRectAux->x1 = _width+_x0;
	dstRectAux->y1 = _height+_y0;

	ImageBufferPoolConfig configBufferPool;

	memset(&configBufferPool, 0, sizeof(ImageBufferPoolConfig));

	configBufferPool.width = _widthSensor;
	configBufferPool.height = _heightSensor;
	configBufferPool.surfType = NvMediaSurfaceType_Image_RGBA;
	configBufferPool.imageClass = NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE;
	configBufferPool.imagesCount = 1;
	configBufferPool.device = hardwareHandle;

	BufferPool_Create (&displayAuxPool, BUFFER_SIZE, QUEUE_TIMEOUT, IMAGE_BUFFER_POOL, (void *)&configBufferPool);

	memset(&blitParams, 0, sizeof(blitParams));
}
DisplayFiniteStateMachine::~DisplayFiniteStateMachine() {

	// TODO Auto-generated destructor stub
}

DisplayFiniteStateMachine::State DisplayFiniteStateMachine::ST_Stopped(){
	return State::STOPPED;
}
DisplayFiniteStateMachine::State DisplayFiniteStateMachine::ST_Initialized(){
	return State::INITIALIZED;
}
DisplayFiniteStateMachine::State DisplayFiniteStateMachine::ST_Run(){

	NvMediaImage *releaseFrames[BUFFER_SIZE]={0};
	NvMediaImage **releaseList=&releaseFrames[0];
	ImageBuffer *imBuffer;
	ImageBuffer *imBufferRelease;
	NvMediaImage *frame = nullptr;
	frame = getFrame();

	if (frame != nullptr){
		if (NvMediaIDPFlip(display, frame , nullptr, dstRect, releaseList, nullptr) == NVMEDIA_STATUS_OK ){
			if (BufferPool_AcquireBuffer(displayAuxPool, (void **)&imBuffer) != NVMEDIA_STATUS_OK ){
				fprintf(stderr, " %s: Problem acquiring new frame from buffer pool \n", __FILE__);
			}else{

				if(frame->width < dstRect->x1 || frame->height < dstRect->y1){
					dstRectAux->x1 = frame->width;
					dstRectAux->y1 = frame->height;
				}
				NvMedia2DBlit(engine2d, imBuffer->image, dstRectAux, frame, nullptr, &blitParams);
				linkAuxStages(imBuffer->image);
			}
			while(*releaseList) {
			  imBufferRelease = (ImageBuffer *)(*releaseList)->tag;
			  BufferPool_ReleaseBuffer(imBufferRelease->bufferPool, imBufferRelease);
			  releaseList++;
			}
		  return State::RUN;
		}else{
		  return State::FAILED;
		}
	}else{
		printf("%s : %s : Waiting new frame\n", __FILE__, __func__);
	}
	return State::RUN;
}
DisplayFiniteStateMachine::State DisplayFiniteStateMachine::ST_Failed(){
	return State::FAILED;
}

DisplayFiniteStateMachine::State DisplayFiniteStateMachine::ST_Stopping(){
	return State::STOPPED;
}
DisplayFiniteStateMachine::State DisplayFiniteStateMachine::ST_Initializing(){
	return State::INITIALIZED;
}
DisplayFiniteStateMachine::State DisplayFiniteStateMachine::ST_Running(){

	return State::RUN;
}
DisplayFiniteStateMachine::State DisplayFiniteStateMachine::ST_Failing(){
	return State::FAILED;
}

