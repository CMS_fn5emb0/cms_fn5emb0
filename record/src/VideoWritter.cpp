/*
 * VideoWritter.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: fn5amm0
 */

#include "VideoWritter.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

VideoWritter::VideoWritter(){


	frameEncodedBuffer = new FrameEncodedBuffer();

	for (int i = 0; i < RECORDING_BUFFER_SIZE; i++){

		frameEncodedBuffer->dataChunk[i]= new Frame();
		frameEncodedBuffer->dataChunk[i]->size = 1024 * 1024 * sizeof(char);
		frameEncodedBuffer->dataChunk[i]->validData = 0;
		frameEncodedBuffer->dataChunk[i]->data = new char[frameEncodedBuffer->dataChunk[i]->size+256];
	}
	frameEncodedBuffer->in=0;
	frameEncodedBuffer->out=0;

	sem_init(&frameEncodedBuffer->full, 0, 0);
	frameEncodedBuffer->empty = RECORDING_BUFFER_SIZE;
	closingFile = false;

	pthread_create(&videoWritterThread, nullptr, VideoWritter::writeVideoThread, this);

}

VideoWritter::~VideoWritter() {
	// TODO Auto-generated destructor stub
}


bool VideoWritter::initVideoFile(std::string fileName){

	if(closingFile == true) return false;

	videoFile = new std::ofstream(fileName, std::ofstream::binary);
	pthread_mutex_lock(&frameEncodedBuffer->inMutex);
	frameEncodedBuffer->in=0;
	pthread_mutex_unlock(&frameEncodedBuffer->inMutex);

	pthread_mutex_lock(&frameEncodedBuffer->outMutex);
	frameEncodedBuffer->out=0;
	pthread_mutex_unlock(&frameEncodedBuffer->outMutex);

	pthread_mutex_lock(&frameEncodedBuffer->emptyMutex);
	frameEncodedBuffer->empty = RECORDING_BUFFER_SIZE;
	pthread_mutex_unlock(&frameEncodedBuffer->emptyMutex);

	return true;

}


bool VideoWritter::closeVideoFile(){
	std::cout << "closing" << std::endl;
	closingFile = true;
	return true;
}


bool VideoWritter::putFrameToBuffer(Encode::DataChunk* _frameEncoded){


	if (frameEncodedBuffer->empty<=0) return false;
	else{

		memcpy(frameEncodedBuffer->dataChunk[frameEncodedBuffer->in]->data,_frameEncoded->data,_frameEncoded->validData);

		frameEncodedBuffer->dataChunk[frameEncodedBuffer->in]->validData = _frameEncoded->validData;

		pthread_mutex_lock(&frameEncodedBuffer->inMutex);
		if (frameEncodedBuffer->in < RECORDING_BUFFER_SIZE-1) frameEncodedBuffer->in = frameEncodedBuffer->in+1;
		else frameEncodedBuffer->in = 0;
		pthread_mutex_unlock(&frameEncodedBuffer->inMutex);

		pthread_mutex_lock(&frameEncodedBuffer->emptyMutex);
		frameEncodedBuffer->empty = frameEncodedBuffer->empty-1;
		pthread_mutex_unlock(&frameEncodedBuffer->emptyMutex);
		sem_post(&frameEncodedBuffer->full);
	}

return true;
}

void* VideoWritter::writeVideo(void){
	while(true){
		if (closingFile == true and frameEncodedBuffer->empty >= RECORDING_BUFFER_SIZE){
			videoFile->flush();
			videoFile->close();
			closingFile = false;
			std::cout << "file closed" << std::endl;
		}
		sem_wait(&frameEncodedBuffer->full);
		videoFile->write(frameEncodedBuffer->dataChunk[frameEncodedBuffer->out]->data,
						 frameEncodedBuffer->dataChunk[frameEncodedBuffer->out]->validData);
		videoFile->flush();

		pthread_mutex_lock(&frameEncodedBuffer->outMutex);
		if (frameEncodedBuffer->out < RECORDING_BUFFER_SIZE-1) frameEncodedBuffer->out = frameEncodedBuffer->out+1;
		else frameEncodedBuffer->out = 0;
		pthread_mutex_unlock(&frameEncodedBuffer->outMutex);

		pthread_mutex_lock(&frameEncodedBuffer->emptyMutex);
		if (frameEncodedBuffer->empty < RECORDING_BUFFER_SIZE) frameEncodedBuffer->empty = frameEncodedBuffer->empty+1;
		pthread_mutex_unlock(&frameEncodedBuffer->emptyMutex);

	}

}
