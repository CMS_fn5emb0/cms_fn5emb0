/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
 *  @par Language: MISRA C++ 2008 & C++11
 *******************************************************************
 *  @file RecordFiniteStateMachine.cpp
 *  @brief This file contains the implementation of the class
 *  		  RecordFiniteStateMachine
 *  @author  Albert Mosella Montoro
 *  @version 1.0
 *  @date 15/06/2016
 *******************************************************************/

#include "RecordFiniteStateMachine.h"
#include <vector>
#include "sys/statvfs.h"
#include <iostream>

using namespace std;

RecordFiniteStateMachine::RecordFiniteStateMachine(NvMediaDevice *device, int width, int height, NvMedia2D *_engine2d)
:FiniteStateMachineTemplate(){

	hardwareHandle = device;
	fileName = "testRecording";
	fileExtension = ".h264";
	recordActivated = false;
	_width = width;
	_height = height;
	frameRate = 60;
	syncFrame = nullptr;
	encoder = new Encode(hardwareHandle, _width, _height, 12 * 1000 * 1000, frameRate);
	videoWritter = new VideoWritter();
	memset(&blitParams, 0, sizeof(blitParams));
	engine2d = _engine2d;
	src.x0 = 0;
	src.x1 = _width;
	src.y0 = 0;
	src.y1 = _height;
	NvMediaImageAdvancedConfig *advConfig;
	memset(&advConfig, 0, sizeof(advConfig));
	conversion = NvMediaImageCreate(hardwareHandle, NvMediaSurfaceType_Image_YUV_420, NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE, 1,
									_width, _height,NVMEDIA_IMAGE_ATTRIBUTE_UNMAPPED | NVMEDIA_IMAGE_ATTRIBUTE_SEMI_PLANAR |
									NVMEDIA_IMAGE_ATTRIBUTE_EXTRA_LINES, advConfig);
}


RecordFiniteStateMachine::~RecordFiniteStateMachine() {
	// TODO Auto-generated destructor stub
}

void RecordFiniteStateMachine::activateDeactivate(std::string _fileName, NvMediaImage*& frame){

	if (!recordActivated){
		fileName = _fileName+fileExtension;
		if (videoWritter->initVideoFile(fileName)== false){
			std::cout << "previous file closing!" << std::endl;
		}
		else{
			std::cout << "Starting to record" << std::endl;
			recordActivated=true;
		}
	}else{
		videoWritter->closeVideoFile();
		recordActivated=false;
	}

}

RecordFiniteStateMachine::State RecordFiniteStateMachine::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
RecordFiniteStateMachine::State RecordFiniteStateMachine::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
RecordFiniteStateMachine::State RecordFiniteStateMachine::ST_Run(){
    NvMediaImage *frame = nullptr;
	Encode::DataChunk* frameEncoded = nullptr;
	frame = getFrame();

	if (frame != nullptr){
		if(recordActivated){
			struct statvfs info;
			statvfs ("/", &info);
			unsigned long long freeSpace = (unsigned long long)info.f_bsize * (unsigned long long)info.f_bfree;
				if (freeSpace > MINIUM_SPACE_FREE){
						NvMedia2DBlit(engine2d, conversion, nullptr, frame, &src, &blitParams);
						encoder->encode(conversion,0);
						frameEncoded = encoder->getData();
						if (videoWritter->putFrameToBuffer(frameEncoded) == false ) std::cout << "Discarted frame" << std::endl;
						encoder->returnData(frameEncoded);
				}else{
					recordActivated = false;
					std::cout <<  "Space on hard disk insufficient" << std::endl;
				}
		}else{
			// TODO[1] ELSE RECORD NOT ACTIVATED
		}

	ImageBuffer* imBuffer = (ImageBuffer*)frame->tag;
	BufferPool_ReleaseBuffer(imBuffer->bufferPool, imBuffer);
	}else{
		// TODO[1] ELSE frame null
	}
	return State::RUN;
}
RecordFiniteStateMachine::State RecordFiniteStateMachine::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

RecordFiniteStateMachine::State RecordFiniteStateMachine::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
RecordFiniteStateMachine::State RecordFiniteStateMachine::ST_Initializing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
RecordFiniteStateMachine::State RecordFiniteStateMachine::ST_Running(){

	return State::RUN;

}
RecordFiniteStateMachine::State RecordFiniteStateMachine::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
