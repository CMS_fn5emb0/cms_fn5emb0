/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file RecordFiniteStateMachine.h
*  @brief This file contains the definition of the class
*  		  RecordFiniteStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef RecordFiniteStateMachine_H_
#define RecordFiniteStateMachine_H_
#include "FiniteStateMachineTemplate.h"
#include "encode.h"
#include "VideoWritter.h"
#include "capture2d.h"
#include "nvmedia.h"
/** @class RecordFiniteStateMachine
 *  @details This class incorporates the functionalities to
 *  		 record video
 */

class RecordFiniteStateMachine: public FiniteStateMachineTemplate {
public:


	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	RecordFiniteStateMachine(NvMediaDevice *device, int width, int height, NvMedia2D *_engine2d);

	/** @brief Free the resources generated for the object
	 */
	virtual ~RecordFiniteStateMachine();

	void activateDeactivate(std::string _fileName, NvMediaImage*& frame);

private:

	VideoWritter *videoWritter;
	NvMediaImage *syncFrame;
	NvMedia2D *engine2d;
	bool syncRecorder;
	int partNumber;
	NvMedia2DBlitParameters blitParams;
	NvMediaEncodePicParamsH264 picParams;
	NvMediaRect src;
	NvMediaImage *conversion;
	/** @var fileName
	 *  	 Defines the name of the file.
	 */
	std::string fileName;

	/** @var fileExtension
	 *  	 Defines the extension of the file.
	 */
	std::string fileExtension;

	/** @var height
	 *  	 Defines the height region of the camera.
	 */
    int _height;

	/** @var width
	 *  	  Defines the width region of the camera.
	 */
	int _width;

	/** @var frameRate
	 *  	 Defines the frameRate of the camera.
	 */
    int frameRate;

	/** @var camerasHanndle
	 *  	  Is used to manage the hardware.
	 */
	NvMediaDevice *hardwareHandle;

	/** @var recordActivated
	 *  	 Is used to activate the video recording.
	 */
	bool recordActivated;

	/** @var recordActivated
	 *  	 Is used to encode the video output.
	 */
	Encode *encoder;

	/** @var encFile
	 *  	 Is used to write the video encoded.
	 */
	std::ofstream *encFile;


	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to fail,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* RECORDFINITESTATEMACHINE_H_ */
