/*
 * VideoWritter.h
 *
 *  Created on: Oct 4, 2016
 *      Author: fn5amm0
 */

#ifndef VIDEOWRITTER_H_
#define VIDEOWRITTER_H_

#include <iostream>
#include <list>
#include <pthread.h>
#include <semaphore.h>
#include <fstream>
#include "encode.h"
#define RECORDING_BUFFER_SIZE 180

typedef struct{
	unsigned int size; // buffer size
	unsigned int validData; // valid data in buffer
	char *data; // start of buffer
} Frame;

typedef struct
{
	Frame *dataChunk[RECORDING_BUFFER_SIZE];
	int in; // first empty slot
	int out; // first full slot
	sem_t full; // number  of full slots
	int empty; // number of empty slots
	pthread_mutex_t inMutex;
	pthread_mutex_t outMutex;
	pthread_mutex_t emptyMutex;

	}FrameEncodedBuffer;

class VideoWritter {
public:

	VideoWritter();
	virtual ~VideoWritter();
	bool putFrameToBuffer(Encode::DataChunk* _frameEncoded);
	bool initVideoFile(std::string fileName);
	bool closeVideoFile();
private:
	static void*  writeVideoThread(void *arg) { return ((VideoWritter*)arg)->writeVideo(); }
	void* writeVideo(void);
	bool getFrame(Encode::DataChunk* &_frameEncoded);
	bool closingFile;
	FrameEncodedBuffer *frameEncodedBuffer;

	pthread_t videoWritterThread;

	std::ofstream *videoFile;

};
#endif /* VIDEOWRITTER_H_ */
