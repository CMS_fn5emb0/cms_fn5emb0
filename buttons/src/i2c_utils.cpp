/*****************************************************************************
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************/

/*
 * i2c_utils.c
 *
 *  Created on: 25/08/2015
 *      Author: Galder Unibaso <galder.unibaso@idneo.es>
 *
 *  08/06/2016 Robert: 	I change the function ficoi2c_read_u8 no to do any
 *						malloc.
 */

#ifndef __I2C_UTILS_H__
#include "i2c_utils.h"
#endif // __I2C_UTILS_H__

#ifndef __COMMON_UTILS_H__
#include "common_utils.h"
#endif // __COMMON_UTILS_H__

#ifndef	_STDLIB_H
#include <stdlib.h>
#endif //_STDLIB_H
#ifndef	_FCNTL_H
#include <fcntl.h>   	// File control definitions
#endif	//_FCNTL_H
#ifndef	_STRING_H
#include <string.h>			// String management
#endif //_STRING_H
#ifndef	_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif //_SYS_IOCTL_H
#ifndef	_UNISTD_H
#include <unistd.h>
#endif //_UNISTD_H
#ifndef _LINUX_I2C_DEV_H
#include <linux/i2c-dev.h>
#endif //_LINUX_I2C_DEV_H

//----------------------------------------------------------------------------
// GLOBAL VARIABLES
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// FUNCTIONS
//----------------------------------------------------------------------------

// Connect to I2C bus
bool ficoi2c_connect(int *i2cfd, char *i2c_bus_str, FILE *log_fd)
{
	char dev_str[FICOI2C_DEV_NAMESIZE];
	
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (i2cfd == NULL) {
		retval = 0;
	}
	else if (i2c_bus_str == NULL) {
		retval = 0;
	}
	else {
		memset(dev_str, '0', FICOI2C_DEV_NAMESIZE);
		sprintf(dev_str, "/dev/i2c-%s", i2c_bus_str);
		if (!(*i2cfd < 0)) {
			//
			// Disconnect i2c bus
			//
			if (!ficoi2c_disconnect(i2cfd, dev_str, log_fd)) {
				fprintf(log_fd, "\n%s Error: Cannot disconnect from I2C bus %s\n\n", ficotime_now(), i2c_bus_str);
				fflush(log_fd);
				retval = 0;
			}
		}
		if (retval) {
			if ((*i2cfd = open(dev_str, O_RDWR)) < 0) {
				fprintf(log_fd, "\n%s Error: Failed to open the I2C bus %s\n\n", ficotime_now(), i2c_bus_str);
				fflush(log_fd);
				retval = 0;
			}
		}
	}
	return retval;
}

// Disconnect from the I2C bus
bool ficoi2c_disconnect(int *i2cfd, char *i2c_bus_str, FILE *log_fd)
{
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (i2c_bus_str == NULL) {
		retval = 0;
	}
	else if (i2cfd == NULL) {
		retval = 0;
	}
	else if (!(*i2cfd < 0)) {
		//
		// Close connection
		//
		if (close(*i2cfd) < 0) {
			fprintf(log_fd, "\n%s Error: Cannot close I2C bus %s\n\n", ficotime_now(), i2c_bus_str);
			fflush(log_fd);
			retval = 0;
		}
		else {
#ifdef DEBUG
			fprintf(log_fd, "%s I2C file descriptor <%d> has been closed\n",  ficotime_now(), *i2cfd);
			fflush(log_fd);
#endif //DEBUG
			*i2cfd = -1;
		}
	}
	return retval;
}

// Set the I2C slave address for all subsequent I2C device transfers
bool ficoi2c_set_address(int *i2cfd, char *i2c_bus_str, u_int8_t i2c_addr, FILE *log_fd)
{
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (i2cfd == NULL) {
		retval = 0;
	}
	else if (i2c_bus_str == NULL) {
		retval = 0;
	}
	else if (ioctl(*i2cfd, I2C_SLAVE, i2c_addr) < 0) {
		fprintf(log_fd, "\n%s Error: Failed to set ths I2C slave address 0x%02x in I2C bus %s\n\n",
			ficotime_now(),
			i2c_addr,
			i2c_bus_str);
		fflush(log_fd);
		retval = 0;
	}
	return retval;
}

// Read I2C data (1 byte)
bool ficoi2c_read_u8(int i2cfd, u_int8_t *data_read_retval, char *i2c_bus_str, u_int8_t i2c_addr, u_int8_t i2c_reg, FILE *log_fd)
{
	u_int8_t data_read_len = 1;
    
    bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (i2c_bus_str == NULL) {
		retval = 0;
	}
	else if (i2cfd < 0) {
		retval = 0;
	}
	else if (data_read_retval == NULL) {
		retval = 0;
	}
	else {
		// Set I2C address
		if (!ficoi2c_set_address(&i2cfd, i2c_bus_str, i2c_addr, log_fd)) {
			retval = 0;
		}
		// Send I2C read command
		else if (write(i2cfd, &i2c_reg, sizeof(i2c_reg)) != sizeof(i2c_reg)) {
			fprintf(log_fd, "\n%s Error: Failed to send read command to 0x%02x register of the 0x%02x address in I2C bus %s\n\n",
				ficotime_now(),
				i2c_reg,
				i2c_addr,
				i2c_bus_str);
			fflush(log_fd);
			retval = 0;
		}

		else if (read(i2cfd, data_read_retval, data_read_len) != data_read_len) {
			fprintf(log_fd, "\n%s Error: Failed to read the value of the 0x%02x register of the 0x%02x address in I2C bus %s\n\n",
				ficotime_now(),
				i2c_reg,
				i2c_addr,
				i2c_bus_str);
			fflush(log_fd);
			retval = 0;
		}
		else {
//#ifdef DEBUG
//			fprintf(log_fd, "%s Received: 0x%02x\n", __func__, data_read);
//			fflush(log_fd);
//#endif //DEBUG
			// Copy readed data
			/*
			if (((*data_read_retval) = (u_int8_t*)malloc(sizeof(u_int8_t))) != NULL) {
				memcpy((*data_read_retval), &data_read, sizeof(u_int8_t));
			}
			else {
				fprintf(log_fd, "\n%s Error: Cannot copy readed I2C value of the 0x%02x register of the 0x%02x address in I2C bus %s\n\n",
				ficotime_now(),
				i2c_reg,
				i2c_addr,
				i2c_bus_str);
				fflush(log_fd);
				retval = 0;
			}
			*/
		}
	}
	return retval;
}

bool ficoi2c_read_u16(int i2cfd, u_int16_t **data_read_retval, char *i2c_bus_str, u_int8_t i2c_addr, u_int8_t i2c_reg, FILE *log_fd)
{
	u_int8_t data_read[2];
	u_int8_t data_read_len = 2;
    
    bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (i2c_bus_str == NULL) {
		retval = 0;
	}
	else if (i2cfd < 0) {
		retval = 0;
	}
	else if (data_read_retval == NULL) {
		retval = 0;
	}
	else {
		// Set I2C address
		if (!ficoi2c_set_address(&i2cfd, i2c_bus_str, i2c_addr, log_fd)) {
			retval = 0;
		}
		// Send I2C read command
		else if (write(i2cfd, &i2c_reg, sizeof(i2c_reg)) != sizeof(i2c_reg)) {
			fprintf(log_fd, "\n%s Error: Failed to send read command to 0x%02x register of the 0x%02x address in I2C bus %s\n\n",
				ficotime_now(),
				i2c_reg,
				i2c_addr,
				i2c_bus_str);
			fflush(log_fd);
			retval = 0;
		}
		else if (read(i2cfd, &data_read, data_read_len) != data_read_len) {
			fprintf(log_fd, "\n%s Error: Failed to read the value of the 0x%02x register of the 0x%02x address in I2C bus %s\n\n",
				ficotime_now(),
				i2c_reg,
				i2c_addr,
				i2c_bus_str);
			fflush(log_fd);
			retval = 0;
		}
		else {
//#ifdef DEBUG
//			fprintf(log_fd, "Received: [0x%02x 0x%02x]\n", data_read[0], data_read[1]);
//			fflush(log_fd);
//#endif //DEBUG
			// Copy readed data
			if (((*data_read_retval) = (u_int16_t*)malloc(sizeof(u_int16_t))) != NULL) {
				//memcpy((*data_read_retval), &data_read, sizeof(u_int8_t));
				**data_read_retval = (data_read[0] | (data_read[1] << 8));
//#ifdef DEBUG
//				fprintf(log_fd, "Generated: 0x%04x from [0x%02x 0x%02x]\n", **data_read_retval, data_read[0], data_read[1]);
//				fflush(log_fd);
//#endif //DEBUG
			}
			else {
				fprintf(log_fd, "\n%s Error: Cannot copy readed I2C value of the 0x%02x register of the 0x%02x address in I2C bus %s\n\n",
					ficotime_now(),
					i2c_reg,
					i2c_addr,
					i2c_bus_str);
				fflush(log_fd);
				retval = 0;
			}
		}
	}
	return retval;
}

// Write I2C data (1 byte)
bool ficoi2c_write_u8(int i2cfd, u_int8_t *data_write, char *i2c_bus_str, u_int8_t i2c_addr, u_int8_t i2c_reg, FILE *log_fd)
{
	u_int8_t i2c_data[2];
	u_int8_t i2c_data_length = 2;
	
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}
	
	if (i2c_bus_str == NULL) {
		retval = 0;
	}
	else if (data_write == NULL) {
		retval = 0;
	}
	else if (i2cfd < 0) {
		retval = 0;
	}
	else {
		// Set I2C address
		if (!ficoi2c_set_address(&i2cfd, i2c_bus_str, i2c_addr, log_fd)) {
			retval = 0;
		}
		// Write I2C data
		else {
			memset(&i2c_data, 0, i2c_data_length); 
			i2c_data[0] = i2c_reg;
			i2c_data[1] = *data_write;
			if (write(i2cfd, i2c_data, i2c_data_length) != i2c_data_length) {
				fprintf(log_fd, "\n%s Error: Failed to write on the I2C bus %s\n\n", ficotime_now(), i2c_bus_str);
				fflush(log_fd);
				retval = 0;
			}
		}
	}
	return retval;
}

// TO BE TESTED
bool ficoi2c_write_u16(int i2cfd, u_int16_t *data_write, char *i2c_bus_str, u_int8_t i2c_addr, u_int8_t i2c_reg, FILE *log_fd)
{
	u_int8_t i2c_data[3];
	u_int8_t i2c_data_length = 3;
	
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}
	
	if (i2c_bus_str == NULL) {
		retval = 0;
	}
	else if (data_write == NULL) {
		retval = 0;
	}
	else if (i2cfd < 0) {
		retval = 0;
	}
	else {
		// Set I2C address
		if (!ficoi2c_set_address(&i2cfd, i2c_bus_str, i2c_addr, log_fd)) {
			retval = 0;
		}
		// Write I2C data
		else {
			memset(&i2c_data, 0, i2c_data_length); 
			i2c_data[0] = i2c_reg;
			i2c_data[1] = *data_write;
			i2c_data[2] = (*data_write >> 8) & 0xff;
			if (write(i2cfd, i2c_data, i2c_data_length) != i2c_data_length) {
				fprintf(log_fd, "\n%s Error: Failed to write on the I2C bus %s\n\n", ficotime_now(), i2c_bus_str);
				fflush(log_fd);
				retval = 0;
			}
		}
	}
	return retval;
}
