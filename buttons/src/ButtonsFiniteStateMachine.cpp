/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file ModuleTemplate.cpp
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "ButtonsFiniteStateMachine.h"

ButtonsFiniteStateMachine::ButtonsFiniteStateMachine(int _maxBacklightValue, int _minBacklightValue,int _dayBacklightValue, int _nightBacklightValue ,int &_backlightLevel, int _stepBacklight, std::string &_scriptSetBacklightPath, std::string &_scriptDayNightPath)
	:FiniteStateMachineTemplate(){

	maxBacklightValue = _maxBacklightValue;
	minBacklightValue = _minBacklightValue;
	nightBacklightValue = _nightBacklightValue;
	dayBacklightValue= _dayBacklightValue;
	backlightLevel = _backlightLevel;
	stepBacklight = _stepBacklight;
	scriptSetBacklightPath=_scriptSetBacklightPath;
	scriptDayNightPath=_scriptDayNightPath;

}
ButtonsFiniteStateMachine::~ButtonsFiniteStateMachine() {
	// TODO Auto-generated destructor stub
}

ButtonsFiniteStateMachine::State ButtonsFiniteStateMachine::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
ButtonsFiniteStateMachine::State ButtonsFiniteStateMachine::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
ButtonsFiniteStateMachine::State ButtonsFiniteStateMachine::ST_Run(){

	processSwitchOne(dayBacklightValue, nightBacklightValue, backlightLevel, scriptSetBacklightPath, scriptDayNightPath);
	processButtonOne(maxBacklightValue, stepBacklight, backlightLevel, scriptSetBacklightPath);
	processButtonTwo(minBacklightValue, stepBacklight, backlightLevel, scriptSetBacklightPath);
	usleep(1600);

	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
ButtonsFiniteStateMachine::State ButtonsFiniteStateMachine::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

ButtonsFiniteStateMachine::State ButtonsFiniteStateMachine::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
ButtonsFiniteStateMachine::State ButtonsFiniteStateMachine::ST_Initializing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
ButtonsFiniteStateMachine::State ButtonsFiniteStateMachine::ST_Running(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
ButtonsFiniteStateMachine::State ButtonsFiniteStateMachine::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
