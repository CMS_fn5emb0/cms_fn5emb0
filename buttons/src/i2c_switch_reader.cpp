#include "i2c_switch_reader.h"
#include "common_utils.h"
#include "i2c_utils.h"
#include <string.h>
#include <iostream>
#include <unistd.h> // sleep
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <errno.h>
#include <stdlib.h> // system
#include <sstream>

//#define MAX_BACKLIGHT_VALUE 252
//#define MIN_BACKLIGHT_VALUE 0
//#define STEP_BACKLIGHT 5
//
// I2C PUSH INTERFACE
//
#define CMS_LUMINANCE_CTRL_DEFAULT_I2C_PUSH_BUS_STR "2"	// Default I2C bus for push button
#define CMS_LUMINANCE_CTRL_DEFAULT_I2C_PUSH_ADDR 0x20 // Default I2C address for push button
#define CMS_LUMINANCE_CTRL_DEFAULT_I2C_PUSH_REG 0x09 // Default I2C register for push buttonr

static char i2c_push_bus_str[FICOI2C_BUS_MAXSTRLEN]; // I2C push button bus
static u_int8_t i2c_push_addr; // I2C push button address
static u_int8_t i2c_push_reg; // I2C push button register


//static int count = MAX_BACKLIGHT_VALUE; //start counter Backlight

static bool initializedG = false;

// The initialization does this.
static int initI2C(FILE *log_fd){
	int retval = 1;
	if (log_fd == NULL) {
		log_fd = stderr;
	}
	//retval = system("i2cset -y 2 0x20 0x09 0xf0");
	//retval = system("i2cset -y 2 0x20 0x00 0x80");
	if(retval == -1){
		perror("");
		return 1;
	}
	return 0;
}

static void init(FILE *log_fd){
	// I2C bus: push button
	int ret;
	initializedG = true;
	strcpy(i2c_push_bus_str, CMS_LUMINANCE_CTRL_DEFAULT_I2C_PUSH_BUS_STR);
	i2c_push_addr = CMS_LUMINANCE_CTRL_DEFAULT_I2C_PUSH_ADDR;
	i2c_push_reg = CMS_LUMINANCE_CTRL_DEFAULT_I2C_PUSH_REG;
	ret = initI2C(log_fd);
	if(ret){
		initializedG = false;
	}
}

static int get_position(u_int8_t *position, FILE *log_fd,
	char *busStrP, u_int8_t addrP, u_int8_t regP)
{
	int retval = 1;
	int i2cfd_push = -1;
	char *busStr;
	u_int8_t addr;
	u_int8_t reg;

	if(!initializedG){
		init(log_fd);
	}
	// Set the defaults.
	busStr = i2c_push_bus_str;
	addr = i2c_push_addr;
	reg = i2c_push_reg;

	// Use user params only if they are set.
	if(busStrP != NULL){
		busStr = busStrP;
	}
	if(addrP && regP){
		addr = addrP;
		reg = regP;
	}
	if (log_fd == NULL) {
		log_fd = stderr;
	}
	do {
		if (ficoi2c_connect(&(i2cfd_push), busStr, log_fd) == 0) {
			fprintf(log_fd,
				"\n%s Error: Cannot connect to I2C PUSH bus %s\n\n",
				ficotime_now(),
				busStr);
			fflush(log_fd);
			break;
		}
		if (ficoi2c_read_u8(i2cfd_push, position,
			busStr, addr,
			reg, log_fd) == 0) {
			fprintf(log_fd,
					"\n%s Error: Cannot read I2C PUSH data from [%s:0x%02x:0x%02x]\n\n",
					ficotime_now(),
					busStr,
					addr,
					reg);
			fflush(log_fd);
			break;
		}
	}while(0);
	// Close connection
	if(i2cfd_push > 0 ){
		if (ficoi2c_disconnect(&(i2cfd_push),
				busStr,
				log_fd) == 0) {
			fprintf(log_fd,
				"\n%s Error: Cannot close I2C PUSH connection on bus %s\n\n",
				ficotime_now(),
				busStr);
			fflush(log_fd);
			i2cfd_push = -1;
		}
		retval = 0;
	}
	return retval;
}



int SREADER_get_switch_position(
	int *switchOneBool,
	int *switchTwoBool,
	FILE *log_fd){

	u_int8_t switch_position;
	int ret;
	ret = get_position(
		&switch_position
		, log_fd //FILE *log_fd
		, NULL //char *busStr
		, 0 //u_int8_t addr
		, 0 //u_int8_t reg
		);
	if(ret){
		return ret;
	}
	//printf("Switch pos: 0x%x\n", switch_position);
	switch_position = switch_position & 0xf0;
	*switchOneBool = (switch_position & 0x40) ? 0 : 1;
	*switchTwoBool = (switch_position & 0x10) ? 0 : 1;
	return ret;
}

int SREADER_get_buttonOne_position(
	int *buttonOneBool,
	int *buttonTwoBool,
	FILE *log_fd){

	u_int8_t button_position;
	int ret;
	ret = get_position(
		&button_position
		, log_fd //FILE *log_fd
		, NULL //char *busStr
		, 0 //u_int8_t addr
		, 0 //u_int8_t reg
		);
	if(ret){
		return ret;
	}
	//printf("Switch pos: 0x%x\n", switch_position);
	button_position = button_position & 0xc0;
	*buttonOneBool = (button_position & 0x40) ? 0 : 1;
	*buttonTwoBool = (button_position & 0x80) ? 0 : 1;
	return ret;
}

int SREADER_get_buttonTwo_position(
	int *buttonOneBool,
	int *buttonTwoBool,
	FILE *log_fd){

	u_int8_t button_position;
	int ret;
	ret = get_position(
		&button_position
		, log_fd //FILE *log_fd
		, NULL //char *busStr
		, 0 //u_int8_t addr
		, 0 //u_int8_t reg
		);
	if(ret){
		return ret;
	}
	//printf("Switch pos: 0x%x\n", switch_position);
	button_position = button_position & 0xc0;
	*buttonOneBool = (button_position & 0x80) ? 0 : 1;
	*buttonTwoBool = (button_position & 0x40) ? 0 : 1;
	return ret;
}

static int runProgram(char *program){
	int sysRet;
	sysRet = system(program);
	// system() ignores SIGINT and SIGQUIT. 
	// This may make programs that call it from a loop uninterruptible, 
	// unless this: 
	/*
	if (WIFSIGNALED(sysRet) &&
	(WTERMSIG(sysRet) == SIGINT || WTERMSIG(sysRet) == SIGQUIT)){
	   return -1;
	}
	*/
	if(sysRet == -1){
		fprintf(stderr, "%s\n", strerror(errno));
	}
	return 0;
}

void processSwitchOne(int MAX_BACKLIGHT_VALUE, int MIN_BACKLIGHT_VALUE, int &backlight_level, std::string &scriptSetBacklightPath, std::string &scriptDayNightPath){

	std::ostringstream backlight;

	int ret;
	int s1, notUsed;
	static int lastS1 = -1;
	FILE *logFile =  stderr;
	ret = SREADER_get_switch_position(&notUsed, &s1, logFile);
	if(ret){
		fprintf(logFile, "Error by SREADER_get_switch_position\n");
	}
	if(s1 != lastS1){
		lastS1 = s1;
		if(s1){

			backlight_level = MAX_BACKLIGHT_VALUE;
			backlight << scriptSetBacklightPath << " " <<backlight_level;
			system((backlight.str()).c_str());
			system((scriptDayNightPath+ " DAY").c_str());


		} else {

			backlight_level = MIN_BACKLIGHT_VALUE;
			backlight << scriptSetBacklightPath << " " <<backlight_level;
			system((backlight.str()).c_str());
			system((scriptDayNightPath+ " NIGHT").c_str());
		}
	}
}

void processButtonOne(int MAX_BACKLIGHT_VALUE, int STEP_BACKLIGHT, int &backlight_level, std::string &scriptSetBacklightPath){
	std::ostringstream backlight;
	int ret;
	int s3, notUsed;
	FILE *logFile =  stderr;
	ret = SREADER_get_buttonOne_position(&notUsed, &s3, logFile);
	if(ret){
		fprintf(logFile, "Error by SREADER_get_switch_position\n");
	}
	if(s3){
		if(backlight_level < MAX_BACKLIGHT_VALUE){
			backlight_level += STEP_BACKLIGHT;
			if(backlight_level > MAX_BACKLIGHT_VALUE)
			{
				backlight_level = MAX_BACKLIGHT_VALUE;
			}
			backlight << scriptSetBacklightPath << " " <<backlight_level;
			system((backlight.str()).c_str());
			usleep(1000);
		}
	}
}

void processButtonTwo(int MIN_BACKLIGHT_VALUE, int STEP_BACKLIGHT, int &backlight_level, std::string &scriptSetBacklightPath){
	std::ostringstream backlight;
	int ret;
	int s4, notUsed;
	FILE *logFile =  stderr;
	ret = SREADER_get_buttonTwo_position(&notUsed, &s4, logFile);
	if(ret){
		fprintf(logFile, "Error by SREADER_get_switch_position\n");
	}
	if(s4){
		if(backlight_level > MIN_BACKLIGHT_VALUE){
			backlight_level -= STEP_BACKLIGHT;
			if(backlight_level < MIN_BACKLIGHT_VALUE){
				backlight_level = MIN_BACKLIGHT_VALUE;
			}
			backlight << scriptSetBacklightPath << " " <<backlight_level;
			system((backlight.str()).c_str());
			usleep(1000);
		}
	}
}

