/*****************************************************************************
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************/

/*
 * i2c_utils.h
 *
 *  Created on: 30/09/2015
 *      Author: Galder Unibaso <galder.unibaso@idneo.es>
 */

#ifndef __I2C_UTILS_H__
#define __I2C_UTILS_H__

#ifndef	_STDIO_H
#include <stdio.h>
#endif //_STDIO_H
#ifndef _STDBOOL_H
#include <stdbool.h>		// Boolean type and values
#endif //_STDBOOL_H
#ifndef	_SYS_TYPES_H
#include <sys/types.h>
#endif	//_SYS_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------
// DEFINITONS
//----------------------------------------------------------------------------
#define FICOI2C_BUS_MAXSTRLEN			2
#define FICOI2C_DEV_NAMESIZE			40
#define FICOI2C_ADDR_MIN				0x03
#define FICOI2C_ADDR_MAX				0x77

//----------------------------------------------------------------------------
// DATA TYPES PROTOTYPES
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// FUNCTION PROTOTYPES
//----------------------------------------------------------------------------
bool ficoi2c_connect(int *i2cfd, char *i2c_bus_str, FILE *log_fd);
bool ficoi2c_disconnect(int *i2cfd, char *i2c_bus_str, FILE *log_fd);
bool ficoi2c_set_address(int *i2cfd, char *i2c_bus_str, u_int8_t i2c_addr, FILE *log_fd);
bool ficoi2c_read_u8(int i2cfd, u_int8_t *data_read_retval, char *i2c_bus_str, u_int8_t i2c_addr, u_int8_t i2c_reg, FILE *log_fd);
bool ficoi2c_read_u16(int i2cfd, u_int16_t **data_read_retval, char *i2c_bus_str, u_int8_t i2c_addr, u_int8_t i2c_reg, FILE *log_fd);
bool ficoi2c_write_u8(int i2cfd, u_int8_t *data_write, char *i2c_bus_str, u_int8_t i2c_addr, u_int8_t i2c_reg, FILE *log_fd);
bool ficoi2c_write_u16(int i2cfd, u_int16_t *data_write, char *i2c_bus_str, u_int8_t i2c_addr, u_int8_t i2c_reg, FILE *log_fd);

#ifdef __cplusplus
}
#endif

#else
#error "Multiple inclusions of i2c_utils.h"
#endif //__I2C_UTILS_H__
