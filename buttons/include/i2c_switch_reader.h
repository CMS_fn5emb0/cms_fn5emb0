#ifndef i2c_switch_reader
#define i2c_switch_reader


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> // sleep
#include <pthread.h>
#include <errno.h>
#include <string>
#include <iostream>

// Returns 0 on success
int SREADER_get_switch_position(int *switchOneBool,int *switchTwoBool);
int SREADER_get_button_position(int *buttonOneBool,int *buttonTwoBool);

void processSwitchOne(int MAX_BACKLIGHT_VALUE, int MIN_BACKLIGHT_VALUE, int &backlight_level, std::string &scriptSetBacklightPath, std::string &scriptDayNightPath);
void processButtonOne(int MAX_BACKLIGHT_VALUE, int STEP_BACKLIGHT, int &backlight_level, std::string &scriptSetBacklightPath);
void processButtonTwo(int MIN_BACKLIGHT_VALUE, int STEP_BACKLIGHT, int &backlight_level, std::string &scriptSetBacklightPath);

#endif

