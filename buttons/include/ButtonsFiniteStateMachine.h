/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file ModuleTemplate.h
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef BUTTONSFINTIESTATEMACHINE_H_
#define BUTTONSFINTIESTATEMACHINE_H_
#include "FiniteStateMachineTemplate.h"
#include "common_utils.h"
#include "i2c_switch_reader.h"
#include "i2c_utils.h"
#include <unistd.h>
#include <string>

/** @class ModuleTemplate
 *  @details This class incorporates the functionalities to
 *  		 release memory of the capture module
 */

class ButtonsFiniteStateMachine: public FiniteStateMachineTemplate {
public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	ButtonsFiniteStateMachine(int _maxBacklightValue, int _minBacklightValue, int _dayBacklightValue, int _nightBacklightValue,int &_backlightLevel, int _stepBacklight, std::string &_scriptSetBacklightPath, std::string &_scriptDayNightPath);

	/** @brief Free the resources generated for the object
	 */
	virtual ~ButtonsFiniteStateMachine();

private:

	int maxBacklightValue;
    int minBacklightValue;
	int backlightLevel;
	int stepBacklight;
	int nightBacklightValue;
	int dayBacklightValue;
	std::string scriptSetBacklightPath;
	std::string scriptDayNightPath;

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state fail to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* BUTTONSFINTIESTATEMACHINE_H_ */
