#include "FishEyeFiniteStateMachine.h"

#define IMAGE_BUFFERS_POOL_SIZE      4

//##############################################################################
// print GL error with location
static void getGLError(int line, const char *file, const char *function)
{
    GLenum error = glGetError();
    if(error != GL_NO_ERROR)
    {
        std::cout << file << " in function " << function << " in line " << line
                  << ": glError: 0x" << std::hex << error << std::dec << std::endl;
    }
}

#define glError() getGLError(__LINE__, __FILE__, __PRETTY_FUNCTION__);



FishEyeFiniteStateMachine::FishEyeFiniteStateMachine(NvMediaDevice* _device, const FISHEYEPARAMS &fep)
	:FiniteStateMachineTemplate()
{

	printf("-----------creating FishEyeFiniteStateMachine------------\n");

	_fep.xc=fep.xc;
	_fep.yc=fep.yc;
	_fep.c=fep.c;
	_fep.d=fep.d;
	_fep.e=fep.e;
	_fep.width=fep.width;
	_fep.height=fep.height;
	_fep.length_invpol=fep.length_invpol;

	for(int i=0; i<_fep.length_invpol; i++)
	{
		_fep.invpol[i]=fep.invpol[i];
	}

	device = _device;

	/*
	 *
		BUFFER POOL
	 *
	 */
	/* :-) */printf("%s: Create Buffer Pools\n", __func__);
	NvU32 inputSurfAttributes = 0;
	NvMediaImageAdvancedConfig  inputSurfAdvConfig;
	ImageBufferPoolConfig poolConfig;
	memset(&poolConfig, 0, sizeof(ImageBufferPoolConfig));
	memset(&inputSurfAdvConfig, 0, sizeof(NvMediaImageAdvancedConfig));

	poolConfig.width = _fep.width;
	poolConfig.height = _fep.height;
	poolConfig.imagesCount = 1;
	poolConfig.surfType = NvMediaSurfaceType_Image_RGBA;
	poolConfig.surfAttributes = inputSurfAttributes;
	poolConfig.surfAdvConfig = inputSurfAdvConfig;
	poolConfig.imageClass = NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE;
	poolConfig.device = device;

	poolConfig.surfAdvConfig = inputSurfAdvConfig;
	NvMediaStatus nvmst=BufferPool_Create(&bufferPool, IMAGE_BUFFERS_POOL_SIZE, QUEUE_TIMEOUT , IMAGE_BUFFER_POOL, &poolConfig);
	if(IsFailed(nvmst)) {
		printf("%s: BufferPool_Create failed\n", __func__);
		//return NVMEDIA_STATUS_ERROR;
	} else {
		printf("%s: BufferPool_Create succeed\n", __func__);
	}

	printf("-----------created FishEyeFiniteStateMachine------------\n");

}
FishEyeFiniteStateMachine::~FishEyeFiniteStateMachine()
{
	// TODO Auto-generated destructor stub
}

FishEyeFiniteStateMachine::State FishEyeFiniteStateMachine::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
FishEyeFiniteStateMachine::State FishEyeFiniteStateMachine::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
FishEyeFiniteStateMachine::State FishEyeFiniteStateMachine::ST_Run(){
	//TODO[1] TO BE IMPLEMENTED
	NvMediaImage *frame = nullptr;
	NvMediaImage *image = nullptr;

	frame = getFrame();
	/*linkStages(frame);
	return State::RUN;*/
	if (frame != nullptr)
	{

		//NvRect src;
		// Acquire processed image buffer
		ImageBuffer *buffer = nullptr;
		if( IsFailed( BufferPool_AcquireBuffer(bufferPool, (void **)&buffer) ) )
		{
			printf("%s: BufferPool : Not Aquired\n", __func__);
		}
		else
		{
			if(frame != nullptr)
			{

				NvMediaImageSurfaceMap mapping;
				//lock the Nvmedia image
				NvMediaStatus status = NvMediaImageLock(frame, NVMEDIA_IMAGE_ACCESS_READ_WRITE, &mapping);

				scaramuzza.undistort(mapping.surface[0].mapping,mapping.surface[0].mapping);


				NvMediaImageUnlock(frame);

				status = NvMedia2DBlit(engine2d, buffer->image, nullptr, frame, nullptr,  &blitParams);
				if (status != NVMEDIA_STATUS_OK)
				{
					fprintf(stdout, "Function Name:%s: File:%s: NvMedia2DBlit failed ,status:%d\n", __func__, __FILE__, status);
				}
			}
			else
				fprintf(stdout, "Function Name:%s: File:%s: NullPtr image\n", __func__, __FILE__);
		}

		linkStages(buffer->image);


		ImageBuffer* releaseBuffer = (ImageBuffer *)frame->tag;
		BufferPool_ReleaseBuffer(releaseBuffer->bufferPool, releaseBuffer);

	} else {
		fprintf(stdout, "%s : %s : Waiting new frame\n", __FILE__, __func__);
	}

	return State::RUN;
}
FishEyeFiniteStateMachine::State FishEyeFiniteStateMachine::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

FishEyeFiniteStateMachine::State FishEyeFiniteStateMachine::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
FishEyeFiniteStateMachine::State FishEyeFiniteStateMachine::ST_Initializing()
{
	//TODO[1] TO BE IMPLEMENTED

	printf("-----------initializing FishEyeFiniteStateMachine------------\n");


	// Engine 2D
	memset(&blitParams, 0, sizeof(blitParams));

	engine2d = NvMedia2DCreate(device);

	// Scaramuzza init

	if(!scaramuzza.initialize(_fep))
	{
		return State::FAILED;
	}

	printf("-----------initialized FishEyeFiniteStateMachine------------\n");

	return State::INITIALIZED;
}
FishEyeFiniteStateMachine::State FishEyeFiniteStateMachine::ST_Running(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
FishEyeFiniteStateMachine::State FishEyeFiniteStateMachine::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
