/*
 * Scaramuzza.cpp
 *
 *  Created on: May 10, 2017
 *      Author: fn5rap0
 */

#include "Scaramuzza.h"

#define imin(a,b) (a<b?a:b)

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

void world2camCPU(float point2D[2], float point3D[3], float* invpol, int length_invpol, float c, float d, float e, float xc, float yc)
{

 float norm        = sqrt(point3D[0]*point3D[0] + point3D[1]*point3D[1]);
 float theta       = atan(point3D[2]/norm);
 float t, t_i;
 float rho, x, y;
 float invnorm;
 int i;

  if (norm != 0)
  {
    invnorm = 1/norm;
    t  = theta;
    rho = invpol[0];
    t_i = 1;

    for (i = 1; i < length_invpol; i++)
    {
      t_i *= t;
      rho += t_i*invpol[i];
    }

    x = point3D[0]*invnorm*rho;
    y = point3D[1]*invnorm*rho;

    point2D[0] = x*c + y*d + xc;
    point2D[1] = x*e + y   + yc;
  }
  else
  {
    point2D[0] = xc;
    point2D[1] = yc;
  }
}

void create_perspecive_undistortion_LUTCPU( const cv::Mat& mapx, const cv::Mat& mapy, float sf, uint16_t width, uint16_t height, float* invpol, int length_invpol, float c, float d, float e, float xc, float yc)
{
     int i, j;
     float *data_mapx = (float*)mapx.data;
     float *data_mapy = (float*)mapy.data;
     float Nxc = height/2.0;
     float Nyc = width/2.0;
     float Nz  = -width/sf;
     float M[3];	//3D
     float m[2];	//2D

     for (i=0; i<height; i++)
         for (j=0; j<width; j++)
         {
             M[0] = (i - Nxc);
             M[1] = (j - Nyc);
             M[2] = Nz;
             world2camCPU(m, M, invpol, length_invpol, c, d, e, xc, yc);
             *( data_mapx + i*width+j ) = (float) m[1];
             *( data_mapy + i*width+j ) = (float) m[0];
         }
}


__device__ void world2cam(float2 &point2D, float3 point3D, float* invpol, int length_invpol, float c, float d, float e, float xc, float yc)
{

	float norm        = sqrtf(point3D.x*point3D.x + point3D.y*point3D.y);
	float theta       = atanf(point3D.z/norm);
	float t, t_i;
	float rho, x, y;
	float invnorm;
	int i;

	if (norm != 0)
	{
		invnorm = 1/norm;
		t  = theta;
		rho = invpol[0];
		t_i = 1;

		for (i = 1; i < length_invpol; i++)
		{
		  t_i *= t;
		  rho += t_i*invpol[i];
		}

		x = point3D.x*invnorm*rho;
		y = point3D.y*invnorm*rho;

		point2D.x = x*c + y*d + xc;
		point2D.y = x*e + y   + yc;
	}
	else
	{
		point2D.x = xc;
		point2D.y = yc;
	}
}

__global__ void create_perspecive_undistortion_LUT( void* __restrict__ mapx, void* __restrict__ mapy,  uint16_t width, uint16_t height, float* __restrict__ invpol, int length_invpol, float c, float d, float e, float xc, float yc, float Nxc, float Nyc, float Nz)
{
	float *data_mapx = (float*)mapx;
	float *data_mapy = (float*)mapy;
	float3 M;
	float2 m;

	int i= threadIdx.x + blockIdx.x*blockDim.x;
	int j= threadIdx.y + blockIdx.y*blockDim.y;

	if(i<height && j<width)
	{
		M.x = (i - Nxc);
		M.y = (j - Nyc);
		M.z = Nz;

		world2cam(m, M, invpol, length_invpol, c,d,e,xc,yc);
		*( data_mapx + i*width+j ) = (float) m.y;
		*( data_mapy + i*width+j ) = (float) m.x;

	}
}

__device__ uchar4 getColor(float x, float y, void* __restrict__ img, uint16_t width, uint16_t height )
{
	uchar4* image=(uchar4*)img;
	//uchar4 res={255, 255, 255, 1};
	//return res;
	return *(image + ((int)x)*width+((int)y));
}

__global__ void undistortGPU(void* __restrict__ imgIn, void* __restrict__ imgOut,  uint16_t width, uint16_t height, float* __restrict__ invpol, int length_invpol, float c, float d, float e, float xc, float yc, float Nxc, float Nyc, float Nz)
{
	//float *data_mapx = (float*)mapx;
	//float *data_mapy = (float*)mapy;
	uchar4* img=(uchar4*)imgOut;
	uchar4 color;
	float3 M;
	float2 m;

	int i= threadIdx.x + blockIdx.x*blockDim.x;
	int j= threadIdx.y + blockIdx.y*blockDim.y;

	if(i<height && j<width)
	{
		M.x = (i - Nxc);
		M.y = (j - Nyc);
		M.z = Nz;

		world2cam(m, M, invpol, length_invpol, c,d,e,xc,yc);
		//*( data_mapx + i*width+j ) = (float) m.y;
		//*( data_mapy + i*width+j ) = (float) m.x;

		float x=m.x;
		float y=m.y;

		float x1=floorf(x);
		float y1=floorf(y);

		float x2=ceilf(x);
		float y2=ceilf(y);
		uchar4 fQ11=getColor(x1,y1, imgIn, width, height);
		uchar4 fQ12=getColor(x1,y2, imgIn, width, height);
		uchar4 fQ21=getColor(x2,y1, imgIn, width, height);
		uchar4 fQ22=getColor(x2,y2, imgIn, width, height);


		color.x=(1/((x2-x1)*(y2-y1)))*
				(fQ11.x*(x2-x)*(y2-y)+
				fQ21.x*(x-x1)*(y2-y)+
				fQ12.x*(x2-x)*(y-y1)+
				fQ22.x*(x-x1)*(y-y1));

		color.y=(1/((x2-x1)*(y2-y1)))*
				(fQ11.y*(x2-x)*(y2-y)+
				fQ21.y*(x-x1)*(y2-y)+
				fQ12.y*(x2-x)*(y-y1)+
				fQ22.y*(x-x1)*(y-y1));

		color.z=(1/((x2-x1)*(y2-y1)))*
				(fQ11.z*(x2-x)*(y2-y)+
				fQ21.z*(x-x1)*(y2-y)+
				fQ12.z*(x2-x)*(y-y1)+
				fQ22.z*(x-x1)*(y-y1));

		color.w=(1/((x2-x1)*(y2-y1)))*
				(fQ11.w*(x2-x)*(y2-y)+
				fQ21.w*(x-x1)*(y2-y)+
				fQ12.w*(x2-x)*(y-y1)+
				fQ22.w*(x-x1)*(y-y1));
		*(img+i*width+j)=color;

	}
}



__global__ void remap(void* __restrict__ imgIn, void* __restrict__ imgOut, uint16_t width, uint16_t height, void* __restrict__ mapx, void* __restrict__ mapy)
{
	int i= threadIdx.x + blockIdx.x*blockDim.x;
	int j= threadIdx.y + blockIdx.y*blockDim.y;
	uchar4* img=(uchar4*)imgOut;
	uchar4 color;

	if(i<height && j<width)
	{
		//*(img+i*width+j)={255,255,255,0};
		//return;


		float *data_mapx = (float*)mapx;
		float *data_mapy = (float*)mapy;
		float x=*(data_mapx + i*width+j);
		float y=*(data_mapy + i*width+j);

		float x1=floorf(x);
		float y1=floorf(y);

		float x2=ceilf(x);
		float y2=ceilf(y);
		uchar4 fQ11=getColor(x1,y1, imgIn, width, height);
		uchar4 fQ12=getColor(x1,y2, imgIn, width, height);
		uchar4 fQ21=getColor(x2,y1, imgIn, width, height);
		uchar4 fQ22=getColor(x2,y2, imgIn, width, height);


		color.x=(1/((x2-x1)*(y2-y1)))*
				(fQ11.x*(x2-x)*(y2-y)+
				fQ21.x*(x-x1)*(y2-y)+
				fQ12.x*(x2-x)*(y-y1)+
				fQ22.x*(x-x1)*(y-y1));

		color.y=(1/((x2-x1)*(y2-y1)))*
				(fQ11.y*(x2-x)*(y2-y)+
				fQ21.y*(x-x1)*(y2-y)+
				fQ12.y*(x2-x)*(y-y1)+
				fQ22.y*(x-x1)*(y-y1));

		color.z=(1/((x2-x1)*(y2-y1)))*
				(fQ11.z*(x2-x)*(y2-y)+
				fQ21.z*(x-x1)*(y2-y)+
				fQ12.z*(x2-x)*(y-y1)+
				fQ22.z*(x-x1)*(y-y1));

		color.w=(1/((x2-x1)*(y2-y1)))*
				(fQ11.w*(x2-x)*(y2-y)+
				fQ21.w*(x-x1)*(y2-y)+
				fQ12.w*(x2-x)*(y-y1)+
				fQ22.w*(x-x1)*(y-y1));
		*(img+i*width+j)=color;
	}
}



Scaramuzza::Scaramuzza()
{
	cudaFree(mapx);
	cudaFree(mapy);
	cudaFree(invpol);
}

Scaramuzza::Scaramuzza(const Scaramuzza& sc):xc(sc.xc), yc(sc.yc), c(sc.c), d(sc.d), e(sc.e), width(sc.width), height(sc.height), length_invpol(sc.length_invpol), invpol(NULL),mapx(NULL), mapy(NULL), d_imageIn(NULL), d_imageOut(NULL)
{

	if(sc.invpol)
	{
		cudaMalloc((void **)&invpol, MAX_POL_LENGTH*sizeof(float));
		cudaMemcpy(invpol, sc.invpol, MAX_POL_LENGTH*sizeof(float), cudaMemcpyDeviceToDevice);
	}
	if(sc.mapx)
	{
		cudaMalloc((void **)&mapx, sc.width*sc.height*sizeof(float));
		cudaMemcpy(mapx, sc.mapx, sc.width*sc.height*sizeof(float), cudaMemcpyDeviceToDevice);
	}
	if(sc.mapy)
	{
		cudaMalloc((void **)&mapy, sc.width*sc.height*sizeof(float));
		cudaMemcpy(mapy, sc.mapy, sc.width*sc.height*sizeof(float), cudaMemcpyDeviceToDevice);
	}

	if(sc.d_imageIn)
	{
		cudaMalloc((void **)&d_imageIn, sc.width*sc.height*4);
	}

	if(sc.d_imageOut)
	{
		cudaMalloc((void **)&d_imageOut, sc.width*sc.height*4);
	}
}

Scaramuzza::~Scaramuzza()
{

}

const Scaramuzza& Scaramuzza::operator=(const Scaramuzza sc)
{
	xc=sc.xc;
	yc=sc.yc;
	c=sc.c;
	d=sc.d;
	e=sc.e;

	if(width!=sc.width || height!=sc.height)
	{
		cudaFree(d_imageIn);
		cudaFree(d_imageOut);
		cudaMalloc((void **)&d_imageIn, sc.width*sc.height*4);
		cudaMalloc((void **)&d_imageOut, sc.width*sc.height*4);
	}

	width=sc.width;
	height=sc.height;
	length_invpol=sc.length_invpol;


	if(sc.invpol)
	{
		if(invpol)cudaFree(invpol);
		cudaMalloc((void **)&invpol, MAX_POL_LENGTH*sizeof(float));
		cudaMemcpy(invpol, sc.invpol, MAX_POL_LENGTH*sizeof(float), cudaMemcpyDeviceToDevice);
	}
	if(sc.mapx)
	{
		if(mapx)cudaFree(mapx);
		cudaMalloc((void **)&mapx, sc.width*sc.height*sizeof(float));
		cudaMemcpy(mapx, sc.mapx, sc.width*sc.height*sizeof(float), cudaMemcpyDeviceToDevice);
	}
	if(sc.mapy)
	{
		if(mapy)cudaFree(mapy);
		cudaMalloc((void **)&mapy, sc.width*sc.height*sizeof(float));
		cudaMemcpy(mapy, sc.mapy, sc.width*sc.height*sizeof(float), cudaMemcpyDeviceToDevice);
	}
	return *this;
}

void Scaramuzza::undistort(void* imageIn, void* imageOut)const
{
	dim3 threads(32, 32);
	dim3 blocks(((height+threads.y-1)/threads.y ),((width+threads.x-1)/threads.x ));

	float Nxc = height/2.0;
	float Nyc = width/2.0;
	float Nz  = -width/2;

	cudaMemcpy(d_imageIn, imageIn, width*height*4, cudaMemcpyHostToDevice);
	//cudaMemcpy(d_imageOut, imageOut, width*height*4, cudaMemcpyHostToDevice);

	undistortGPU<<<blocks, threads>>>(d_imageIn, d_imageOut, width, height, invpol, length_invpol, c, d, e, xc, yc, Nxc, Nyc, Nz);

	/*create_perspecive_undistortion_LUT<<<blocks, threads>>>(mapx, mapy, width, height, invpol, length_invpol, c, d, e, xc, yc, Nxc, Nyc, Nz);

	cudaMemcpy(d_imageIn, imageIn, width*height*4, cudaMemcpyHostToDevice);
	cudaMemcpy(d_imageOut, imageOut, width*height*4, cudaMemcpyHostToDevice);


	remap<<<blocks, threads>>>(d_imageIn, d_imageOut, width, height, mapy,  mapx);
	//gpuErrchk( cudaPeekAtLastError() );
	//gpuErrchk( cudaDeviceSynchronize() );*/

	cudaMemcpy(imageOut, d_imageOut, width*height*4, cudaMemcpyDeviceToHost);
}

void Scaramuzza::undistortCPU(void* imageIn, void* imageOut)const
{


	cv::Mat src1 = cv::Mat(height,width,CV_8UC4);
	src1.data = reinterpret_cast<unsigned char*>(imageIn);

	cv::Mat dst = cv::Mat(height,width,CV_8UC4);
	dst.data = reinterpret_cast<unsigned char*>(imageOut);

	cv::Mat dst_persp = cv::Mat(height,width,CV_8UC4);

	cv::Mat mapx_persp = cv::Mat(height, width, CV_32FC1);
	cv::Mat mapy_persp = cv::Mat(height, width, CV_32FC1);

	float sf = 4;
	create_perspecive_undistortion_LUTCPU( mapx_persp, mapy_persp, sf,width, height, invpol, length_invpol, c, d, e, xc, yc);
	cv::remap( src1, dst_persp, mapx_persp, mapy_persp, CV_INTER_LINEAR);
	memcpy(imageOut, dst_persp.data, width*height*sizeof(char)*4);
}



bool Scaramuzza::initialize(const FISHEYEPARAMS& fep)
{
	xc=fep.xc;
	yc=fep.yc;
	c=fep.c;
	d=fep.d;
	e=fep.e;
	width=fep.width;
	height=fep.height;
	length_invpol=fep.length_invpol;


	// CUDA Buffers

		memset(&cdProp, 0, sizeof(cudaDeviceProp));
		cdProp.major=1;
		cdProp.minor=0;

		cdError=cudaChooseDevice(&cudaDev, &cdProp);

		if(cdError==cudaError_t::cudaErrorInvalidDevice)
		{
			printf("ERROR: The CUDA Device has no minimum requirements for this operation");
			return false;
		}
		else if(cdError==cudaError_t::cudaErrorDeviceAlreadyInUse)
		{
			printf("ERROR: The CUDA Device is in use!, please release it first");
			return false;
		}

		/*cudaHostAlloc((void **)&h_imgIn,  _fep.width*_fep.height*4, cudaHostAllocMapped);
		cudaHostAlloc((void **)&h_imgOut, _fep.width*_fep.height*4, cudaHostAllocMapped);

		cudaHostGetDevicePointer((void **)&d_imgIn,  (void *) h_imgIn , 0);
		cudaHostGetDevicePointer((void **)&d_imgOut, (void *) h_imgOut, 0);*/

		cudaMalloc((void **)&mapx, fep.width*fep.height*sizeof(float));
		cudaMalloc((void **)&mapy, fep.width*fep.height*sizeof(float));
		cudaMalloc((void **)&invpol, MAX_POL_LENGTH*sizeof(float));
		cudaMalloc((void **)&d_imageIn, fep.width*fep.height*4);
		cudaMalloc((void **)&d_imageOut, fep.width*fep.height*4);

		cudaMemcpy(invpol, fep.invpol, MAX_POL_LENGTH*sizeof(float), cudaMemcpyHostToDevice);


		return true;
}

