/*
 * Scaramuzza.h
 *
 *  Created on: May 10, 2017
 *      Author: fn5rap0
 */

#ifndef SCARAMUZZA_H_
#define SCARAMUZZA_H_

#include <cmath>
#include <cstdint>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <cstdio>
#include <cstring>
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"



#define MAX_POL_LENGTH 20

typedef struct
{
	float xc=0.0;         // row coordinate of the center
	float yc=0.0;         // column coordinate of the center
	float c=0.0;          // affine parameter
	float d=0.0;          // affine parameter
	float e=0.0;          // affine parameter
	uint16_t width=0;         // image width
	uint16_t height=0;        // image height
	float invpol[MAX_POL_LENGTH]={0}; // the coefficients of the inverse polynomial
	uint16_t length_invpol=0;             // length of inverse polynomial
}FISHEYEPARAMS;

class Scaramuzza
{
private:

	float xc=0.0;         // row coordinate of the center
	float yc=0.0;         // column coordinate of the center
	float c=0.0;          // affine parameter
	float d=0.0;          // affine parameter
	float e=0.0;          // affine parameter
	uint16_t width=0;         // image width
	uint16_t height=0;        // image height

	uint16_t length_invpol=0;             // length of inverse polynomial

	//CUDA Vars
	cudaDeviceProp cdProp;

	int cudaDev;
	cudaError_t cdError;
	float *invpol; // the coefficients of the inverse polynomial
	float *mapx , *mapy;
	void *d_imageIn, *d_imageOut;

public:
	Scaramuzza();
	Scaramuzza(const Scaramuzza& sc);
	~Scaramuzza();
	const Scaramuzza& operator=(const Scaramuzza sc);


	void undistort(void* imageIn, void* imageOut)const;
	void undistortCPU(void* imageIn, void* imageOut)const;
	bool initialize(const FISHEYEPARAMS& fep);

private:
	//void world2cam(double point2D[2], double point3D[3])const;
	//void create_perspecive_undistortion_LUT( CvMat *mapx, CvMat *mapy,  float sf)const;


};


#endif /* SCARAMUZZA_H_ */
