/*
 * FishEyeFiniteStateMachine.h
 *
 *  Created on: May 8, 2017
 *      Author: Rubén Abad
 */

#ifndef FISHEYEFINITESTATEMACHINE_H_
#define FISHEYEFINITESTATEMACHINE_H_
#include "FiniteStateMachineTemplate.h"

#include "Scaramuzza.h"

#include "shader.h"
#include "vbo.h"
#include "window.h"

#include "eglstream.h"
#include "streamconsumernvmediaimage.h"
#include "streamproducernvmediaimage.h"
#include "streamconsumeropengl.h"
#include "streamproduceropengl.h"

#include "nvmedia.h"
#include "nvmedia_2d.h"

extern "C" {
	#include "thread_utils.h"
	#include "surf_utils.h"
	#include "buffer_utils.h"
}


#include <pthread.h>
#include <iostream>
#include <stdio.h>
#include <execinfo.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstring>

#include <sys/time.h>

/** @class ModuleTemplate
 *  @details This class incorporates the functionalities to
 *  		 release memory of the capture module
 */

class FishEyeFiniteStateMachine: public FiniteStateMachineTemplate {
public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	FishEyeFiniteStateMachine(NvMediaDevice* _device, const FISHEYEPARAMS &fep);

	/** @brief Free the resources generated for the object
	 */
	virtual ~FishEyeFiniteStateMachine();

private:

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state fail to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();


private:
	BufferPool* bufferPool;

	NvMediaDevice* device;
	EGLStream* eglNv2Gl = nullptr;
	EGLStream* eglGl2Nv = nullptr;
	StreamConsumerNvMediaImage* nvConsumer = nullptr;
	StreamProducerNvMediaImage* nvProducer = nullptr;
	StreamConsumerOpenGL* glConsumer = nullptr;
	StreamProducerOpenGL* glProducer = nullptr;

	AbstractWindow* window;
	int cameras;


	int tex_width = 0, tex_height = 0;

	GLuint index;

	VBO* quadVBO;


	GLuint locations[2];

	struct timeval start_t;
	struct timeval current_t;
	struct timeval past_t;
	float interval_elapsed = 0.0;
	float total_elapsed = 0.0;
	unsigned int num_frames = 0;

	NvMedia2D* engine2d;
	NvMedia2DBlitParameters blitParams;

	FISHEYEPARAMS _fep;
	Scaramuzza scaramuzza;

};


#endif /* FISHEYEFINITESTATEMACHINE_H_ */
