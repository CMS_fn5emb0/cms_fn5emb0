/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed
// under the Mutual Non-Disclosure Agreement.
//
// Notice
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
//
// NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless
// expressly authorized by NVIDIA.  Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright © 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////


#include "rectifier.h"

#include <string.h>
#include <GLES2/gl2ext.h>
#include <iostream>

using namespace std;
#define SCALE_SEP

//#######################################################################################
// Initialize the class and required variables
Rectifier::Rectifier()
{
    memset(&m_programMesh, 0, sizeof(PROGRAM_TEXQUAD));

    // Rectified mesh data structures
    m_rectMesh        = NULL;
    m_rectIndices     = NULL;

    m_rectVBO         = 0;
    m_rectVAO         = 0;
    m_rectIND         = 0;
    m_rectNumVerts    = 0;
    m_rectNumIndices  = 0;
    m_scaling         = 0;
    m_dotNumIndices   = 0;

    m_initialized = false;
}

//#######################################################################################
// destroy the class and variables
Rectifier::~Rectifier()
{
    release();
}

void Rectifier::initialize(CMSPARAMETERS &parameters)
{
	int scaling = 1;
	initialize(parameters, scaling);
}

//#######################################################################################
// contruct an undistort program and initialize all the required varibles
void Rectifier::initialize(CMSPARAMETERS &parameters, int scaling)
{
    if(m_initialized) return;
    m_scaling = scaling;
    m_rectifiedFBO.createFBO(parameters.DST_ROI_WIDTH, parameters.DST_ROI_HEIGHT, GL_RGBA8); CHECK_GL_ERROR;

	/*	Aspherical::horizontal)	*/
    m_programMesh.program   = buildAndLinkProgramFromMemory(get2DTexQuadVS().c_str(), get2DTexQuadPS().c_str()); CHECK_GL_ERROR;
    m_programMesh.program1   = buildAndLinkProgramFromMemory(get2DDotLineVS().c_str(), get2DDotLinePS().c_str()); CHECK_GL_ERROR;
    m_programMesh.aPosition = glGetAttribLocation  (m_programMesh.program, "position");CHECK_GL_ERROR;
    m_programMesh.aTexcoord = glGetAttribLocation  (m_programMesh.program, "texcoord");CHECK_GL_ERROR;
    m_programMesh.uTex0     = glGetUniformLocation (m_programMesh.program, "tex");CHECK_GL_ERROR;
    m_programMesh.myUniformLocation = glGetUniformLocation(m_programMesh.program, "myUniform");
    m_programMesh.myUniformLocationh = glGetUniformLocation(m_programMesh.program, "myUniformh");

    // Passthrough quad
    generatePassthroughMesh(/*parameters*/);
    generateRectificationMesh(parameters);
    generatePassthroughMeshDotLine(parameters.DOTTED_LINES_NUM, parameters.DOTTED_LINES_THICKNESS);

    m_rectifyImage = false;
    m_initialized = true;
}

//#######################################################################################
// release all buffers, textures and programs
void Rectifier::release()
{
	//fprintf(stdout, "\nrelease rectifier\n");
    if(!m_initialized) return;

    // Perspective rectification mesh
    m_rectNumIndices      = 0;
    m_rectNumVerts        = 0;

    SAFE_DELETE_ARRAY(m_rectMesh);
    SAFE_DELETE_ARRAY(m_rectIndices );

    glDeleteBuffers(1, &m_rectVBO);
    glDeleteBuffers(1, &m_rectIND);
    glDeleteVertexArrays(1, &m_rectVAO);

    glUseProgram(0);
    glDeleteProgram(m_programMesh.program);
    glDeleteProgram(m_programMesh.program1);

    m_rectifiedFBO.releaseFBO();

    m_initialized = false;
}

void Rectifier::generatePassthroughMeshDotLine(int dashCount, int linethickness) {

    int pointsPerSingleLine = dashCount * 2;
    int singleLineVertices =  pointsPerSingleLine * 2;

    hVertices = new float[pointsPerSingleLine];

	float offset = 1.0f * 2.0/(pointsPerSingleLine);
    for(int i=0;i<pointsPerSingleLine;i++){
    	hVertices[i]=(1.0) - (offset*i);
    }

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);

    m_dotNumIndices = singleLineVertices * linethickness;
    GLfloat vertices[m_dotNumIndices];

	/*	Aspherical::horizontal	*/
	int k = 0;
	for(int i = 0; i < singleLineVertices; i++){
		vertices[i]=hVertex;
		if(i % 2 == 1){
			vertices[i]=hVertices[k];
			k++;
		}
	}
	for (int j = 1;j < (linethickness);j++){
		for(int i = (singleLineVertices*j); i < (singleLineVertices*(j+1)); i++){
			vertices[i]=hVertex+(0.001*j);
			if(i % 2 == 1)
				vertices[i]=(vertices[i-singleLineVertices]);
		}
	}

    glBindBuffer(GL_ARRAY_BUFFER, vbo);CHECK_GL_ERROR;
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);CHECK_GL_ERROR;
    // Specify the layout of the vertex data
    GLint posAttrib = glGetAttribLocation(m_programMesh.program1, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
}

//#######################################################################################
void Rectifier::generateRectificationMesh(CMSPARAMETERS &parameters)
{
    int i,j;
    GLfloat* ptr_fl;
    float    point2D[2];

	/*	Aspherical::horizontal)	*/
	float meshsrcdeno = 0;//(parameters.SRC_ROI_WIDTH - parameters.SRC_ROI_SCALE_SEP) / (meshResX - 1);
	int meshsrcResX = 0;//1 + (parameters.SRC_ROI_WIDTH - parameters.SRC_ROI_SCALE_SEP) / (meshsrcdeno);
	int meshResYX;

	meshResX = 1 + (parameters.DST_ROI_WIDTH - parameters.DST_ROI_SCALE_SEP) / parameters.PIXELS_PER_PATCHX;
	meshResY = parameters.DST_ROI_HEIGHT / parameters.PIXELS_PER_PATCHY;
	meshsrcdeno = (parameters.SRC_ROI_WIDTH - parameters.SRC_ROI_SCALE_SEP) / (meshResX - 1);
	meshsrcResX = 1 + (parameters.SRC_ROI_WIDTH - parameters.SRC_ROI_SCALE_SEP) / (meshsrcdeno);

	meshResYX = parameters.SRC_ROI_HEIGHT / parameters.PIXELS_PER_PATCHY;

	float stride = 1.0f * ((parameters.SRC_HEIGHT - parameters.SRC_ROI_HEIGHT)) / parameters.SRC_HEIGHT;
	stride = 1.0f * stride ;

	float u1[2];
	float* u_src1 = new float[meshResX - 1];

	// Dimension mesh arrays
	m_rectNumVerts  = (meshResX+1) * (meshResY+1);
	m_rectNumIndices=  meshResX * 2 * meshResY * 3;

	m_rectMesh      = new GLfloat[m_rectNumVerts * 4];
	m_rectIndices   = new GLuint[m_rectNumIndices];

	// Create the vertices
	// Verts will be x,y and s,t
	// Vertices will be in the -1 to 1 range

	float u,v,v1;
	// First part is just a large quad (NON ASPHERICAL REGION)


	hVertex = 1.0f * 2 * parameters.DST_ROI_SCALE_SEP / parameters.DST_ROI_WIDTH - 1.0f;
	if(parameters.SCALING_DIRECTION == -1)
	hVertex = -1.0f * hVertex;

	// First part is just a large quad
	for(i = 0; i < meshResY+1; i++){
		for(j = 0; j < 2; j++){
			ptr_fl = &m_rectMesh[(i * (meshResX+1) + j)*4];

			// Unitary coords [0...1]
			u =  1.0f * j * parameters.DST_ROI_SCALE_SEP / parameters.DST_ROI_WIDTH;
			v = (1.0f * i * parameters.DST_ROI_HEIGHT / meshResY) / parameters.DST_ROI_HEIGHT;

			v1 = (1.0f * i * parameters.SRC_ROI_HEIGHT / meshResYX) / parameters.SRC_ROI_HEIGHT;
			//v1 = v1 + stride;
			u1[j] = 1.0f * j * parameters.SRC_ROI_SCALE_SEP / parameters.SRC_ROI_WIDTH;
			// XY coordinates are computet in NDC space
			if(parameters.SCALING_DIRECTION ==  1) ptr_fl[0] = (2.0f * (u * parameters.DST_ROI_WIDTH  /*+ parameters.DST_ROI_X*/) / parameters.DST_ROI_WIDTH - 1.0f);;
			//if(parameters.SCALING_DIRECTION ==  1) ptr_fl[0] = (2.0f * (u * parameters.DST_ROI_WIDTH  + parameters.DST_ROI_X) / parameters.DST_ROI_WIDTH - 1.0f);;
			if(parameters.SCALING_DIRECTION == -1) ptr_fl[0] = -1.0f * (2.0f * (u * parameters.DST_ROI_WIDTH  /*+ parameters.DST_ROI_X*/) / parameters.DST_ROI_WIDTH - 1.0f);;
			//if(parameters.SCALING_DIRECTION == -1) ptr_fl[0] = -1.0f * (2.0f * (u * parameters.DST_ROI_WIDTH  + parameters.DST_ROI_X) / parameters.DST_ROI_WIDTH - 1.0f);;
			ptr_fl[1] = 2.0f * (v * parameters.DST_ROI_HEIGHT /*+ parameters.DST_ROI_Y*/) / parameters.DST_ROI_HEIGHT - 1.0f;
			//ptr_fl[1] = 2.0f * (v * parameters.DST_ROI_HEIGHT + parameters.DST_ROI_Y) / parameters.DST_ROI_HEIGHT - 1.0f;

			computeRemapping(point2D, u, v, parameters, 0, u1[j]);
			point2D[1] = point2D[1] + stride;

			// UV coordinates are computed based on the mappings
			if(parameters.SCALING_DIRECTION ==  1)  ptr_fl[2] = point2D[0];
			if(parameters.SCALING_DIRECTION == -1)  ptr_fl[2] = 1.0f - point2D[0];
			ptr_fl[3] = 1.0f - point2D[1];

		}
	}

	// Second part is a finer grid
	float startX = 1.0f * parameters.DST_ROI_SCALE_SEP / parameters.DST_ROI_WIDTH;
	float deltaX = 1.0f * ((parameters.DST_ROI_WIDTH - parameters.DST_ROI_SCALE_SEP) / (meshResX-1) ) / parameters.DST_ROI_WIDTH;


	float startsrcX = 1.0f * parameters.SRC_ROI_SCALE_SEP / parameters.SRC_ROI_WIDTH;
	float deltasrcX;
	int mode = 1;

	if((parameters.SRC_ROI_WIDTH - parameters.SRC_ROI_SCALE_SEP) == 0) {
		deltasrcX = 0.1f;   //Adding 0.1 so that vertex will go beyond the boundary and in fragment shader it will fill black color
		mode = 0;
	}
	else {
		deltasrcX = 1.0f * ((parameters.SRC_ROI_WIDTH - parameters.SRC_ROI_SCALE_SEP) / ((float)meshsrcResX-1) ) / parameters.SRC_ROI_WIDTH;
		mode = 1;
	}

	for(int l = 2; l < meshResX+1; l++){
		u_src1[l-2] = (1.0f * (l-1) * deltasrcX + startsrcX);
	}


	for(i = 0; i < meshResY+1; i++){
		for(j = 2; j < meshResX+1; j++){
			ptr_fl = &m_rectMesh[(i * (meshResX+1) + j)*4];

			// Unitary coords [0...1]
			u = 1.0f * (j-1) * deltaX + startX;
			v = (1.0f * i * parameters.DST_ROI_HEIGHT / meshResY) / parameters.DST_ROI_HEIGHT;

			v1 = (1.0f * i * parameters.SRC_ROI_HEIGHT / meshResYX) / parameters.SRC_ROI_HEIGHT;
			//v1 = v1 + stride;
			// XY coordinates are computet in NDC space
			if(parameters.SCALING_DIRECTION ==  1) ptr_fl[0] = (2.0f * (u * parameters.DST_ROI_WIDTH  /*+ parameters.DST_ROI_X*/) / parameters.DST_ROI_WIDTH - 1.0f);
			if(parameters.SCALING_DIRECTION == -1) ptr_fl[0] = -1.0f * (2.0f * (u * parameters.DST_ROI_WIDTH  /*+ parameters.DST_ROI_X*/) / parameters.DST_ROI_WIDTH - 1.0f);
			ptr_fl[1] = 2.0f * (v * parameters.DST_ROI_HEIGHT /*+ parameters.DST_ROI_Y*/) / parameters.DST_ROI_HEIGHT - 1.0f;

			computeRemapping(point2D, u,v, parameters, mode, u_src1[j-2]);

			point2D[1] = point2D[1] + stride;
			// UV coordinates are computed based on the mappings
			if(parameters.SCALING_DIRECTION ==  1)  ptr_fl[2] = point2D[0];
			if(parameters.SCALING_DIRECTION == -1)  ptr_fl[2] = 1.0f - point2D[0];
			ptr_fl[3] = 1.0f - point2D[1];
		}
	}
	// Create the indices
	// We are rendering 2 triangles per quad
	//   (i,j) --- (i, j + 1)
	//         | /
	//          /
	//         / |
	// (i+1,j) --- (i+1, j+1)
	//
	// Triangle 1 is: (i,j)->(i+1,j)->(i, j + 1)
	// Triangle 2 is: (i, j + 1)->(i+1,j)->(i+1, j+1)
	//
	// This code could be further optimized to use a triangle strip.
	//

	int counter = 0;
	for(i = 0; i < meshResY; i++){
	for(j = 0; j < meshResX; j++){
		m_rectIndices[counter++] = (i + 0) * (meshResX+1) + (j + 0);
		m_rectIndices[counter++] = (i + 1) * (meshResX+1) + (j + 0);
		m_rectIndices[counter++] = (i + 0) * (meshResX+1) + (j + 1);
		m_rectIndices[counter++] = (i + 0) * (meshResX+1) + (j + 1);
		m_rectIndices[counter++] = (i + 1) * (meshResX+1) + (j + 0);
		m_rectIndices[counter++] = (i + 1) * (meshResX+1) + (j + 1);
	}
	}

    // WARPING GRID
    // Create and bind the VAO
    glGenVertexArrays(1, &m_rectVAO);CHECK_GL_ERROR;
    glBindVertexArray(m_rectVAO);CHECK_GL_ERROR;

    // Create and fill the VBO
    glGenBuffers(1, &m_rectVBO);CHECK_GL_ERROR;
    glBindBuffer(GL_ARRAY_BUFFER, m_rectVBO);CHECK_GL_ERROR;
    glBufferData(GL_ARRAY_BUFFER, m_rectNumVerts * 4 *sizeof(GLfloat), m_rectMesh, GL_STATIC_DRAW);CHECK_GL_ERROR;

    // set up vertex attributes
    GLuint positionLoc = glGetAttribLocation (m_programMesh.program,"position");CHECK_GL_ERROR;
    glEnableVertexAttribArray(positionLoc);CHECK_GL_ERROR;
    glVertexAttribPointer(positionLoc, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);CHECK_GL_ERROR;

    GLuint texcoordLoc = glGetAttribLocation (m_programMesh.program,"texcoord");CHECK_GL_ERROR;
    glEnableVertexAttribArray(texcoordLoc);CHECK_GL_ERROR;
    glVertexAttribPointer(texcoordLoc, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*) (2 * sizeof(GLfloat)));CHECK_GL_ERROR;

    // Create and bind a VBO for index data
    glGenBuffers(1, &m_rectIND);CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_rectIND);CHECK_GL_ERROR;

    // copy data into the buffer object
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_rectNumIndices * sizeof(GLuint), m_rectIndices, GL_STATIC_DRAW); CHECK_GL_ERROR;
    glBindVertexArray(0);CHECK_GL_ERROR;

}

//#######################################################################################
void Rectifier::computeRemapping(float outPoint[2], const float inX, const float inY,  CMSPARAMETERS &parameters, int mode, float u_src)
{

   // int dst_scale_seperator = parameters.DST_ROI_HEIGHT - parameters.DST_ROI_SCALE_SEP;
    int src_scale_seperator = parameters.SRC_ROI_SCALE_SEP;
#ifdef SCALE_SEP
    ;
#else
    u_src = inX;
#endif
    if(mode == 0)
    {
		/*	Aspherical::horizontal)	*/

		// We are in the straight section of the curve
		outPoint[0] = (parameters.SRC_ROI_WIDTH * u_src + parameters.SRC_ROI_X) / parameters.SRC_WIDTH;

		// Y coordinate remains unchnaged
		outPoint[1] = (parameters.SRC_ROI_HEIGHT * inY + parameters.SRC_ROI_Y) / parameters.SRC_HEIGHT;

        return;
    }

    if( mode == 1)
    {
		/*	Aspherical::horizontal)	*/

		// Polynomial section
		// This curve shoudl be a 0..1 to 0..1 mapping
		float coeffs[10];

		//float relU = (parameters.DST_ROI_WIDTH  * inX - parameters.DST_ROI_SCALE_SEP) / (parameters.DST_ROI_WIDTH - parameters.DST_ROI_SCALE_SEP);
		float relU = (parameters.SRC_ROI_WIDTH  * u_src - parameters.SRC_ROI_SCALE_SEP) / (parameters.SRC_ROI_WIDTH - parameters.SRC_ROI_SCALE_SEP);
		float relV = 1.0;//inY;

		coeffs[0] = 1;
		coeffs[1] = relU;
		coeffs[2] = relV;
		coeffs[3] = relU * relV;
		coeffs[4] = relU * relU;
		coeffs[5] = relV * relV;
		coeffs[6] = relV * coeffs[4];
		coeffs[7] = relU * coeffs[5];
		coeffs[8] = relU * coeffs[4];
		coeffs[9] = relV * coeffs[5];

		outPoint[0] = outPoint[1] = 0.0f;

		for(int i = 0; i < 10; ++i)
		{
			outPoint[0] = outPoint[0] + coeffs[i] * parameters.polyU[i];
			outPoint[1] = outPoint[1] + coeffs[i] * parameters.polyV[i];
		}

		outPoint[1] = inY;
		// Transform to 0...1 in SRC space
		//float newX = ((parameters.DST_ROI_WIDTH - parameters.DST_ROI_SCALE_SEP) * outPoint[0] + parameters.DST_ROI_SCALE_SEP) / parameters.DST_ROI_WIDTH;
		float newX = ((parameters.SRC_ROI_WIDTH - parameters.SRC_ROI_SCALE_SEP) * outPoint[0] + parameters.SRC_ROI_SCALE_SEP) / parameters.SRC_ROI_WIDTH;

		outPoint[0] = (parameters.SRC_ROI_WIDTH  * newX + parameters.SRC_ROI_X) / parameters.SRC_WIDTH;
		outPoint[1] = (parameters.SRC_ROI_HEIGHT * outPoint[1] + parameters.SRC_ROI_Y) / parameters.SRC_HEIGHT;

		float x_clamp = (float)parameters.SRC_ROI_WIDTH / (float)parameters.SRC_WIDTH;

		if(parameters.SCALING_DIRECTION ==  1 && outPoint[0] > x_clamp) {
			 outPoint[0] = x_clamp + 0.1;
		}

		/* Black area is working if we don't clamp the edge */
		if(parameters.SCALING_DIRECTION ==  -1 && outPoint[0] < 1.0 - x_clamp) {
			//outPoint[0] = x_clamp - 0.1;
		}


		return ;

    }

    outPoint[0] = outPoint[1] = -1;
}

//#######################################################################################
void Rectifier::generatePassthroughMesh()
{
    GLfloat coords[3][4];

    // Verts will be x,y and s,t
    // Vertices will be in the -1 to 1 range
    // X is horizontal, Y is vertical
    coords[0][0] = -1.0f;
    coords[0][1] = -1.0f;
    coords[1][0] =  3.0f;
    coords[1][1] = -1.0f;
    coords[2][0] = -1.0f;
    coords[2][1] =  3.0f;

    coords[0][2] =  0.0;
    coords[0][3] =  1.0f;
    coords[1][2] =  2.0f;
    coords[1][3] =  1.0f;
    coords[2][2] =  0.0f;
    coords[2][3] = -1.0f;

    // Create and bind the VAO
    glGenVertexArrays(1, &m_quadVAO);CHECK_GL_ERROR;
    glBindVertexArray(m_quadVAO);CHECK_GL_ERROR;

    // Create and fill the VBO
    glGenBuffers(1, &m_quadVBO);CHECK_GL_ERROR;
    glBindBuffer(GL_ARRAY_BUFFER, m_quadVBO);CHECK_GL_ERROR;
    glBufferData(GL_ARRAY_BUFFER, 3 * 4 *sizeof(GLfloat), coords, GL_STATIC_DRAW);CHECK_GL_ERROR;

    // set up vertex attributes
    GLuint positionLoc = glGetAttribLocation (m_programMesh.program,"position");CHECK_GL_ERROR;
    glEnableVertexAttribArray(positionLoc);CHECK_GL_ERROR;
    glVertexAttribPointer(positionLoc, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);CHECK_GL_ERROR;

    GLuint texcoordLoc = glGetAttribLocation (m_programMesh.program,"texcoord");CHECK_GL_ERROR;
    glEnableVertexAttribArray(texcoordLoc);CHECK_GL_ERROR;
    glVertexAttribPointer(texcoordLoc, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*) (2 * sizeof(GLfloat)));CHECK_GL_ERROR;

    glBindVertexArray(0);CHECK_GL_ERROR;
}


//#######################################################################################
void Rectifier::renderImage(const GLuint inputTexture, bool distortImage, CMSPARAMETERS* cmsParameters)
{
    // Draw rectified image

	GLenum error;
    glDisable(GL_DEPTH_TEST);

    glBindFramebuffer(GL_FRAMEBUFFER, m_rectifiedFBO.fbo); CHECK_GL_ERROR;

    glViewport(0, 0, m_rectifiedFBO.width, m_rectifiedFBO.height);CHECK_GL_ERROR;
    glScissor(0, 0, m_rectifiedFBO.width, m_rectifiedFBO.height);CHECK_GL_ERROR;
    glEnable(GL_SCISSOR_TEST);CHECK_GL_ERROR;

    glClearColor(1.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(m_programMesh.program);CHECK_GL_ERROR;

    glUniform1i(m_programMesh.uTex0, 0);CHECK_GL_ERROR;
    //glUniform1i(m_programMesh.uTex1, 1);CHECK_GL_ERROR;

    if(distortImage)
    {
    	/*	Aspherical::horizontal)	*/
		glUniform1f(m_programMesh.myUniformLocation, ((float)cmsParameters->SRC_ROI_WIDTH / (float)cmsParameters->SRC_WIDTH));
		glUniform1f(m_programMesh.myUniformLocationh, 0.0f);

    }else
    {
        glUniform1f(m_programMesh.myUniformLocation, 1.0f);
    }

    glActiveTexture(GL_TEXTURE0); CHECK_GL_ERROR;
    glBindTexture(GL_TEXTURE_EXTERNAL_OES, inputTexture);CHECK_GL_ERROR;

    glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
    glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
    glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR); CHECK_GL_ERROR;
    glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR); CHECK_GL_ERROR;

    if(distortImage)
    {
        glBindVertexArray(m_rectVAO); CHECK_GL_ERROR;
        glDrawElements(GL_TRIANGLES, m_rectNumIndices, GL_UNSIGNED_INT, 0);CHECK_GL_ERROR;
    	error = glGetError();
    	if (error != 0) {
    		  std::cout <<" glDrawElements Err " << error<<std::endl;
    	}
        glBindVertexArray(0); CHECK_GL_ERROR;
    }
    else
    {
        glBindVertexArray(m_quadVAO); CHECK_GL_ERROR;
        glDrawArrays(GL_TRIANGLES, 0, 3); CHECK_GL_ERROR;
        glBindVertexArray(0); CHECK_GL_ERROR;
    }

    glBindTexture  (GL_TEXTURE_EXTERNAL_OES, 0);CHECK_GL_ERROR;
    glUseProgram(0);CHECK_GL_ERROR;
    glBindFramebuffer(GL_FRAMEBUFFER, 0); CHECK_GL_ERROR;
    glDisable(GL_SCISSOR_TEST);CHECK_GL_ERROR;

	/* RENDER LINE */
    renderLine();
}

void Rectifier::renderLine() {
    glDisable(GL_DEPTH_TEST);
	glBindFramebuffer(GL_FRAMEBUFFER, m_rectifiedFBO.fbo); CHECK_GL_ERROR;

	glUseProgram(m_programMesh.program1);CHECK_GL_ERROR;

    // Draw a triangle from the 3 vertices
    glBindVertexArray(vao); CHECK_GL_ERROR;
    glDrawArrays(GL_LINES, 0, m_dotNumIndices);
    glBindVertexArray(0); CHECK_GL_ERROR;

    glUseProgram(0);CHECK_GL_ERROR;
    glBindFramebuffer(GL_FRAMEBUFFER, 0); CHECK_GL_ERROR;
}

//#######################################################################################
std::string Rectifier::get2DTexQuadVS()
{
    std::string shaderProgram = STRINGIFY(

        \n#version 300 es\n

        layout(location = 0) in vec2 position;
        layout(location = 1) in vec2 texcoord;

        out vec2 TexCoord0;

        void main()
        {
            TexCoord0   = vec2(texcoord.x, texcoord.y);
            vec2 pos    = vec2(position.x, position.y);
            //gl_Position = vec4( pos.x, -pos.y, 0.0, 1.0);
            gl_Position = vec4( pos.x, pos.y, 0.0, 1.0);
        }

    );
    return shaderProgram;
}

//#######################################################################################
std::string Rectifier::get2DTexQuadVSH()
{
    std::string shaderProgram = STRINGIFY(

        \n#version 300 es\n

        layout(location = 0) in vec2 position;
        layout(location = 1) in vec2 texcoord;

        out vec2 TexCoord0;

        void main()
        {
            TexCoord0   = vec2(texcoord.x, texcoord.y);
            vec2 pos    = vec2(position.x, position.y);
            gl_Position = vec4( pos.x, pos.y, 0.0, 1.0);
        }

    );
    return shaderProgram;
}
//#######################################################################################
std::string Rectifier::get2DTexQuadPS()
{
     std::string shaderProgram = STRINGIFY(

        \n#version 300 es\n
        \n#extension GL_NV_EGL_stream_consumer_external: enable\n
	    \n#extension GL_OES_EGL_image_external : enable\n

        precision highp float;

	    uniform samplerExternalOES tex;
	    uniform float myUniform;
	    uniform float myUniformh;

        in vec2 TexCoord0;

	    layout(location = 0) out vec4 fragColor;

        void main()
        {
            vec4 texel = texture2D(tex, TexCoord0.st);
            if((TexCoord0.s > myUniform)||(TexCoord0.s < myUniformh))
                fragColor  = vec4 (0.0, 0.0, 0.0, texel.a);
            else
                fragColor  = vec4 (texel.r, texel.g, texel.b, texel.a);
        }
    );
    return shaderProgram;
}

std::string Rectifier::get2DTexQuadPSH()
{
     std::string shaderProgram = STRINGIFY(

        \n#version 300 es\n
        \n#extension GL_NV_EGL_stream_consumer_external: enable\n
        \n#extension GL_OES_EGL_image_external : enable\n

        precision highp float;

        uniform samplerExternalOES tex;
        uniform float myUniform;
        uniform float myUniformh;

        in vec2 TexCoord0;

        layout(location = 0) out vec4 fragColor;

        void main()
        {
            vec4 texel = texture2D(tex, TexCoord0.st);
            if((TexCoord0.t > myUniform)||(TexCoord0.t < myUniformh))
                fragColor  = vec4 (0.0, 0.0, 0.0, texel.a);
            else
                fragColor  = vec4 (texel.r, texel.g, texel.b, texel.a);
        }
    );
    return shaderProgram;
}
//#######################################################################################
std::string Rectifier::get2DDotLineVS()
{
    std::string shaderProgram = STRINGIFY(

        \n#version 300 es\n

        in vec2 position;
        void main()
        {
            gl_Position = vec4(position, 0.0, 1.0);
        };

    );
    return shaderProgram;
}
//#######################################################################################
std::string Rectifier::get2DDotLinePS()
{
     std::string shaderProgram = STRINGIFY(

        \n#version 300 es\n

        precision highp float;

     	 out vec4 outColor;
         void main()
         {
             outColor = vec4(0.0, 0.0, 0.0, 0.0);
         };
    );
    return shaderProgram;
}
