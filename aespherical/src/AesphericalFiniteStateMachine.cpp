/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file AesphericalFiniteStateMachine.cpp
*  @brief AesphericalFiniteStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "AesphericalFiniteStateMachine.h"


#define BUFFER_POOL_TIMEOUT          2000//100
#define DEQUEUE_TIMEOUT              16000
#define ENQUEUE_TIMEOUT              0
#define GET_FRAME_TIMEOUT            500
#define FEED_FRAME_TIMEOUT           100
#define WAIT_FOR_IDLE_TIMEOUT        100
#define IMAGE_BUFFERS_POOL_SIZE      4
#define MAX_CAPTURE_BUFFERS          2

//##############################################################################
// print GL error with location
static void getGLError(int line, const char *file, const char *function)
{
    GLenum error = glGetError();
    if(error != GL_NO_ERROR)
    {
        std::cout << file << " in function " << function << " in line " << line
                  << ": glError: 0x" << std::hex << error << std::dec << std::endl;
    }
}
// macro to have a simple glError function
#define glError() getGLError(__LINE__, __FILE__, __PRETTY_FUNCTION__);

float ren_timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

float ren_time_msec(struct timeval t)
{
    return ( t.tv_sec * 1000.0f + t.tv_usec/ 1000.0f );
}

AesphericalFiniteStateMachine::AesphericalFiniteStateMachine(NvMediaDevice* _device, CMSPARAMETERS* _cmsParameters)
	:FiniteStateMachineTemplate(){

	cmsParameters = _cmsParameters;
	device = _device;

    /*
     *
        BUFFER POOL
     *
     */
    /* :-) */printf("%s: Create Buffer Pools\n", __func__);
    NvU32 inputSurfAttributes = 0;
    NvMediaImageAdvancedConfig  inputSurfAdvConfig;
    ImageBufferPoolConfig poolConfig;
    memset(&poolConfig, 0, sizeof(ImageBufferPoolConfig));
    memset(&inputSurfAdvConfig, 0, sizeof(NvMediaImageAdvancedConfig));
    poolConfig.width = _cmsParameters->DST_WIDTH;
    poolConfig.height = _cmsParameters->DST_HEIGHT;
    poolConfig.imagesCount = 1;
    poolConfig.surfType = NvMediaSurfaceType_Image_RGBA;
    poolConfig.surfAttributes = inputSurfAttributes;
    poolConfig.surfAdvConfig = inputSurfAdvConfig;
    poolConfig.imageClass = NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE;
    poolConfig.device = device;

    poolConfig.surfAdvConfig = inputSurfAdvConfig;
    if(IsFailed(BufferPool_Create(&bufferPool, IMAGE_BUFFERS_POOL_SIZE, QUEUE_TIMEOUT , IMAGE_BUFFER_POOL, &poolConfig))) {
        printf("%s: BufferPool_Create failed\n", __func__);
        //return NVMEDIA_STATUS_ERROR;
    } else {
        printf("%s: BufferPool_Create succeed\n", __func__);
    }
}

AesphericalFiniteStateMachine::~AesphericalFiniteStateMachine() {
	// TODO Auto-generated destructor stub
}

AesphericalFiniteStateMachine::State AesphericalFiniteStateMachine::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
AesphericalFiniteStateMachine::State AesphericalFiniteStateMachine::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
AesphericalFiniteStateMachine::State AesphericalFiniteStateMachine::ST_Run(){

	NvMediaImage *frame = nullptr;
	NvMediaImage *image = nullptr;

	frame = getFrame();

	if (frame != nullptr){
		window->makeCurrent();

		/* NV -> GL */
		// if unable to post a frame, then return it back to buffer pool
		nvProducer->postFrame(frame);

		/* Process image through GL and send over EGL stream */
		// get image from stream and distort it
		glConsumer->acquireAndBind(0);


		rectifier->renderImage(glConsumer->getTexID(), true, cmsParameters);
		glConsumer->releaseAndUnbind(0);

		// render result image into EGL surface

		glProducer->enableCurrent();
		{
			glClear(GL_COLOR_BUFFER_BIT);

			// Display the output
			glViewport(0, 0, cmsParameters->DST_ROI_WIDTH, cmsParameters->DST_ROI_HEIGHT);
			texturedprog->bind();
			glUniform1i(index, 0);
			quadVBO->setPointerAndBind(locations, 2);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, rectifier->getOutputTexture());
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			glBindTexture(GL_TEXTURE_2D,0);

			texturedprog->unbind();
			quadVBO->unsetPointerAndUnbind(locations, 2);
		}

		glProducer->postTexture();
		glError();
		glProducer->disableCurrent();
		glError();

		/* GL -> NV */
		image = nullptr;
		image = nvConsumer->acquire(4000);

		//NvRect src;
		// Acquire processed image buffer
		ImageBuffer *buffer = nullptr;
		if( IsFailed( BufferPool_AcquireBuffer(bufferPool, (void **)&buffer) ) ) {
			printf("%s: BufferPool : Not Aquired\n", __func__);
		} else {
			if(image != nullptr){
				NvMediaStatus status = NvMedia2DBlit(engine2d, buffer->image, nullptr, image, nullptr,  &blitParams);
				if (status != NVMEDIA_STATUS_OK) {
					fprintf(stdout, "Function Name:%s: File:%s: NvMedia2DBlit failed ,status:%d\n", __func__, __FILE__, status);
				}
			}
			else
				fprintf(stdout, "Function Name:%s: File:%s: NullPtr image\n", __func__, __FILE__);
		}

		if(image) {
			nvConsumer->release(image);
		}

		/* NV -> GL */
		NvMediaImage *release = nullptr;
		release = nvProducer->getFrame(0);
		window->swapBuffers();


		/* NV -> NV */
		linkStages(buffer->image);

		ImageBuffer* releaseBuffer = (ImageBuffer *)frame->tag;
		BufferPool_ReleaseBuffer(releaseBuffer->bufferPool, releaseBuffer);

	} else {
		fprintf(stdout, "%s : %s : Waiting new frame\n", __FILE__, __func__);
	}

	return State::RUN;
}
AesphericalFiniteStateMachine::State AesphericalFiniteStateMachine::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

AesphericalFiniteStateMachine::State AesphericalFiniteStateMachine::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}

AesphericalFiniteStateMachine::State AesphericalFiniteStateMachine::ST_Initializing(){

    // create an WM output
    window = AbstractWindow::create(AbstractWindow::EGLOUTPUT, 0, 0, cmsParameters->DST_WIDTH, cmsParameters->DST_HEIGHT);

    if(!window->isInitialized())
    {
        fprintf(stderr, "Could not create window!\n");

    }

	EGLDisplay eglDisplay = window->getEGLDisplay();
	EGLConfig eglConfig = window->getEGLConfig();

	// give away rendering context
	// rendering thread will take over and perform rendering into that context
	eglMakeCurrent(eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);


	// -------------------------------------------
	// Create two egl streams for each camera
	//  1st stream: nvmedia->opengl (camera image to gl processing)
	//  2nd stream: opengl->nvmedia (gl processed image back to nvmedia)
	// -------------------------------------------
	GLint nv2glAcquireTimeout = 0;
	GLint nv2glLatency = 16000;
	GLint gl2nvAcquireTimeout = 0;
	GLint gl2nvLatency = 16000;

	eglNv2Gl  = new EGLStream(eglDisplay, EGLStream::FIFO, 4);
	//	eglNv2Gl->setConsumerLatency(nv2glLatency);
	//	eglNv2Gl->setConsumerAcquireTimeout(nv2glAcquireTimeout);
	eglGl2Nv = new EGLStream(eglDisplay, EGLStream::FIFO, 4);
	//	eglGl2Nv->setConsumerLatency(gl2nvLatency);
	//	eglGl2Nv->setConsumerAcquireTimeout(gl2nvAcquireTimeout);

	nvConsumer = new StreamConsumerNvMediaImage(eglDisplay, device, *eglGl2Nv);
	window->makeCurrent();
	glProducer = new StreamProducerOpenGL(eglDisplay, eglConfig, *eglGl2Nv, cmsParameters->DST_ROI_WIDTH, cmsParameters->DST_ROI_HEIGHT);


	glConsumer = new StreamConsumerOpenGL(eglDisplay, *eglNv2Gl);
	nvProducer = new StreamProducerNvMediaImage(eglDisplay, device, *eglNv2Gl, NvMediaSurfaceType_Image_RGBA, cmsParameters->DST_WIDTH, cmsParameters->DST_HEIGHT);

	eglMakeCurrent(eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
	glError();


    // -------------------------------------------
    // initialize OpenGL (ES) for rendering
    // setup GL states as required
    // -------------------------------------------
	window->makeCurrent();
    //glViewport(0, 0, cmsParameters->DST_WIDTH, cmsParameters->DST_HEIGHT);
    glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

	// create some quad to render to screen
	GLfloat vQuadVertices[] = {
		-1.0f, -1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, -1.0f, 0.0f, 1.0f, 1.0f,
		-1.0f,  1.0f, 0.0f, 0.0f, 0.0f,
		1.0f,  1.0f, 0.0f, 1.0f, 0.0f,
	};

	// upload quads to a vertex buffer object
	int quadStripe[] = {3, 2};
	quadVBO = new VBO(vQuadVertices, 4, quadStripe, 2, GL_STATIC_DRAW);

	// create a gpu program with a simple vertex and fragment shader
	texturedvs = new Shader(GL_VERTEX_SHADER, "/root/texture_vs.glsl", Shader::File);
	texturedfs = new Shader(GL_FRAGMENT_SHADER, "/root/texture2d_fs.glsl", Shader::File);
	texturedprog = new GPUProgram(*texturedvs, *texturedfs);
	glError();

	//GLuint locations[2];
	index = texturedprog->getUniformLocation("tex");

	locations[0] = texturedprog->getAttribLocation("vPosition");
	locations[1] = texturedprog->getAttribLocation("vTexCoord");
	quadVBO->setPointerAndBind(locations, 2);

	// Initialize the rectifier
	rectifier = new Rectifier();
	rectifier->initialize(*cmsParameters);

	eglMakeCurrent(eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

	struct timeval start_t;
	struct timeval current_t;
	struct timeval past_t;
	float interval_elapsed = 0.0;
	float total_elapsed = 0.0;
	unsigned int num_frames = 0;
	gettimeofday(&start_t, 0);

	// Engine 2D
	memset(&blitParams, 0, sizeof(blitParams));

	engine2d = NvMedia2DCreate(device);

	return State::INITIALIZED;
}

AesphericalFiniteStateMachine::State AesphericalFiniteStateMachine::ST_Running(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
AesphericalFiniteStateMachine::State AesphericalFiniteStateMachine::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
