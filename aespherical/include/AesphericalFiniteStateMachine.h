/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file AesphericalFiniteStateMachine.h
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef AesphericalFiniteStateMachine_H_
#define AesphericalFiniteStateMachine_H_
#include "FiniteStateMachineTemplate.h"

#include "rectifier.h"
#include "gpuprogram.h"
#include "shader.h"
#include "vbo.h"
#include "window.h"

#include "eglstream.h"
#include "streamconsumernvmediaimage.h"
#include "streamproducernvmediaimage.h"
#include "streamconsumeropengl.h"
#include "streamproduceropengl.h"

#include "nvmedia.h"
#include "nvmedia_2d.h"

extern "C" {
	#include "thread_utils.h"
	#include "surf_utils.h"
	#include "buffer_utils.h"
}

#include <pthread.h>
#include <iostream>
#include <stdio.h>
#include <execinfo.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstring>

#include <sys/time.h>



/** @class AesphericalFiniteStateMachine
 *  @details This class incorporates the functionalities to
 *  		 release memory of the capture module
 */

class AesphericalFiniteStateMachine: public FiniteStateMachineTemplate {


public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	AesphericalFiniteStateMachine(NvMediaDevice* device, CMSPARAMETERS* _cmsParameters);

	/** @brief Free the resources generated for the object
	 */
	virtual ~AesphericalFiniteStateMachine();

private:

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state fail to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();


private:


	BufferPool* bufferPool;

	NvMediaDevice* device;
	EGLStream* eglNv2Gl = nullptr;
	EGLStream* eglGl2Nv = nullptr;
	StreamConsumerNvMediaImage* nvConsumer = nullptr;
	StreamProducerNvMediaImage* nvProducer = nullptr;
    StreamConsumerOpenGL* glConsumer = nullptr;
	StreamProducerOpenGL* glProducer = nullptr;

	Rectifier* rectifier;
    AbstractWindow* window;
    int cameras;
    CMSPARAMETERS* cmsParameters;

	int tex_width = 0, tex_height = 0;

	GLuint index;

	VBO* quadVBO;

	// create a gpu program with a simple vertex and fragment shader
	Shader* texturedvs;
	Shader* texturedfs;
	GPUProgram* texturedprog;

	GLuint locations[2];

	struct timeval start_t;
	struct timeval current_t;
	struct timeval past_t;
	float interval_elapsed = 0.0;
	float total_elapsed = 0.0;
	unsigned int num_frames = 0;

	NvMedia2D* engine2d;
	NvMedia2DBlitParameters blitParams;
};

#endif /* AesphericalFiniteStateMachine_H_ */
