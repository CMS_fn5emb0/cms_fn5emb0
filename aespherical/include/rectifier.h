/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed
// under the Mutual Non-Disclosure Agreement.
//
// Notice
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT,
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
//
// NVIDIA Corporation assumes no responsibility for the consequences of use of such
// information or for any infringement of patents or other rights of third parties that may
// result from its use. No license is granted by implication or otherwise under any patent
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless
// expressly authorized by NVIDIA.  Details are subject to change without notice.
// This code supersedes and replaces all information previously supplied.
// NVIDIA Corporation products are not authorized for use as critical
// components in life support devices or systems without express written approval of
// NVIDIA Corporation.
//
// Copyright © 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef __RECTIFIER_H_
#define __RECTIFIER_H_

#include <string>
#include "fbo.h"
#include "util_GL.h"
#include "util_Matrix.h"
#include "streamconsumeropengl.h"
#include "streamproduceropengl.h"


typedef struct
{
	int 	SRC_WIDTH;
	int 	SRC_HEIGHT;
	int 	SRC_ROI_X;
	int 	SRC_ROI_Y;
	int 	SRC_ROI_HEIGHT;
	int 	SRC_ROI_WIDTH;
	int 	SRC_ROI_SCALE_SEP;

	int 	DST_WIDTH;
	int 	DST_HEIGHT;
	int 	DST_ROI_X;
	int 	DST_ROI_Y;
	int 	DST_ROI_HEIGHT;
	int 	DST_ROI_WIDTH;
	int 	DST_ROI_SCALE_SEP;

	int 	SCALING_DIRECTION;

	int 	PIXELS_PER_PATCHX;
	int 	PIXELS_PER_PATCHY;

	float polyU[10];        // 3rd degree polynomal
	float polyV[10];        // 3rd degree polynomal

	int 	DOTTED_LINES_NUM;
	int 	DOTTED_LINES_THICKNESS;
	int 	NUM_CAMERAS;

} CMSPARAMETERS;


//#######################################################################################
class Rectifier
{
public:
    Rectifier();
    ~Rectifier();

	void initialize(CMSPARAMETERS &parameters);
    void initialize(CMSPARAMETERS &parameters, int scaling);
    void release();

    void renderImage(const GLuint inputTexture, bool distortImage, CMSPARAMETERS* cmsParameters);

    void renderLine();

    GLuint getOutputTexture() { return m_rectifiedFBO.tex; };
    GLuint getOutputFBO()     { return m_rectifiedFBO.fbo; };
    GLuint getOutputWidth()   { return m_rectifiedFBO.width; };
    GLuint getOutputHeight()  { return m_rectifiedFBO.height; };
    GLuint getOutputFormat()  { return m_rectifiedFBO.format; };

private:
    void generateRectificationMesh( CMSPARAMETERS &paramters);
    void generatePassthroughMesh();
    void generatePassthroughMeshDotLine(int dashCount, int linethickness);
    void computeRemapping(float outPoint[2], const float inX, const float inY, CMSPARAMETERS &parameters, int mode, float u_src);

private:

    typedef struct{ GLuint program; GLuint aPosition; GLuint aTexcoord; GLuint uTex0; GLint myUniformLocation; GLint myUniformLocationh; GLuint program1;}PROGRAM_TEXQUAD;

    // Vertex shaders
    std::string get2DTexQuadVS();

    std::string get2DTexQuadVSH();

    // Fragment shaders
    std::string get2DTexQuadPS();

    std::string get2DTexQuadPSH();

    std::string get2DDotLineVS();

    std::string get2DDotLinePS();

    PROGRAM_TEXQUAD m_programMesh;

private:
    int meshResX, meshResY;

    FBO m_rectifiedFBO;

    nv::matrix4<float> m_mvMatrix;
    nv::matrix4<float> m_projMatrix;

    // Rectified mesh data structures
    GLuint  m_rectNumVerts;
    GLuint  m_rectNumIndices;
    GLfloat *m_rectMesh;
    GLuint  *m_rectIndices;

    GLuint  m_rectVBO;
    GLuint  m_rectVAO;
    GLuint  m_rectIND;

    // Passthrough quad
    GLuint  m_quadVBO;
    GLuint  m_quadVAO;
    GLuint  m_quadIND;

    bool m_rectifyImage;

    bool m_initialized;

    int m_scaling;
    float hVertex;
    float vVertex;
    float *hVertices;
    GLuint  m_dotNumIndices;
    // Create Vertex Array Object
    GLuint vao;
    // Create a Vertex Buffer Object and copy the vertex data to it
    GLuint vbo;
};

#endif
