/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
 *  @par Language: MISRA C++ 2008 & C++11
 *******************************************************************
 *  @file HandleStateMachine.cpp
 *  @brief This file contains the implementation of the class
 *  		  HandleStateMachine
 *  @author  Albert Mosella Montoro
 *  @version 1.0
 *  @date 15/06/2016
 *******************************************************************/

#include "HandleStateMachine.h"
#include <ctime>
#include "CXmlParser.h"
#include <unistd.h>


//#define NULL nullptr

HandleStateMachine::HandleStateMachine(char* xmlconfig)
:FiniteStateMachineTemplate(){

	// Init conf variables
	CXmlParser* xml= CXmlParser::getInstance();
	xml->parseXml(xmlconfig);

	initialize = false;
	textureId = 0;

	hardwareHandle = NvMediaDeviceCreate();
	engine2d = NvMedia2DCreate(hardwareHandle);

	// capture conf
	cameraWidth = xml->getCameraWidth();
	cameraHeight = xml->getCameraHeight();
	nCameras = xml->getCameraNumber();
	cameraFrameRate = xml->getCameraFrameRate();
	camerasConnectionType = (Capture2d::Connection)xml->getCameraConnectionType();
	cameraBufferType = Capture2d::PITCH_LINEAR;
	cameraColorConv = Capture2d::RGB;

	// crop conf
	crop = xml->getisCropEnabled();
	cropROIX0 = xml->getCropROIX0();
	cropROIY0 = xml->getCropROIY0();
	cropHeight = xml->getCropHeight();
	cropWidth = xml->getCropWidth();

	// display conf
	displayId = xml->getDisplayId();
	displayX0 = xml->getDisplayX0();
	displayY0 = xml->getDisplayY0();
	displayWidth = xml->getDisplayWidth();
	displayHeight = xml->getDisplayHeight();

	// snapshot
	formatSnapshot = xml->getSnapshotFormat();
	nameSnapshot = xml->getSnapshotPrefix();
	nameRecord = xml->getRecordingPrefix();

	/*AbstractWindow* */
	win = AbstractWindow::create(AbstractWindow::EGLOUTPUT, displayId, 0, displayWidth, displayHeight);

	// buttons
	buttons = xml->getisButtonsEnabled();
	maxBacklightValue = xml->getButtonsMaxTresholdBacklight() > 252 ? 252 : xml->getButtonsMaxTresholdBacklight();
	minBacklightValue = xml->getButtonsMinTresholdBacklight() < 0 ? 0 : xml->getButtonsMinTresholdBacklight();
	dayBacklightValue = xml->getButtonsDayValueBacklight() > 252 ? 252 : xml->getButtonsDayValueBacklight();
	nightBacklightValue = xml->getButtonsNightValueBacklight() < 0 ? 0 : xml->getButtonsNightValueBacklight();
	backlightLevel = 140;
	stepBacklight = xml->getButtonsStepBacklight();
	scriptSetBacklightPath = (xml->getButtonsBacklightPath()).c_str();
	scriptDayNightPath = (xml->getButtonsDayNightPath()).c_str();



	// soil conf
	soil = xml->getisSoilEnabled();
	if (soil) {
		svmPath[0] = (xml->getsvmPath()).c_str();
		svmPath[1] = (xml->getsvmExtPath()).c_str();
		svmPath[2] = (xml->getsvmExtNoPartialPath()).c_str();
		soilFPS = (xml->getSoilFPS());
	}

	// Aespherical data
	aespherical = xml->getIsAesphericalEnabled();
	if(aespherical){
		cmsParameters = new CMSPARAMETERS;
		cmsParameters->DOTTED_LINES_NUM = xml->getdottedLinesNum();
		cmsParameters->DOTTED_LINES_THICKNESS = xml->getdottedLinesThickness();
		cmsParameters->NUM_CAMERAS = 1;
		cmsParameters->PIXELS_PER_PATCHX = xml->getpixelsPerPatchX();
		cmsParameters->PIXELS_PER_PATCHY = xml->getpixelsPerPatchY();
		cmsParameters->SCALING_DIRECTION = xml->getscalingDirection();

		// SRC
		cmsParameters->SRC_WIDTH = xml->getSrcWidth();
		cmsParameters->SRC_HEIGHT = xml->getSrcHeight();
		cmsParameters->SRC_ROI_WIDTH = xml->getsrcROIWidth();
		cmsParameters->SRC_ROI_HEIGHT = xml->getsrcROIHeight();
		cmsParameters->SRC_ROI_X = xml->getsrcROIX();
		cmsParameters->SRC_ROI_Y = xml->getsrcROIY();
		cmsParameters->SRC_ROI_SCALE_SEP = xml->getsrcROIScaleSEP();

		// DST
		cmsParameters->DST_WIDTH = xml->getdstWidth();
		cmsParameters->DST_HEIGHT = xml->getdstHeight();
		cmsParameters->DST_ROI_X = xml->getdstROIX();
		cmsParameters->DST_ROI_Y = xml->getdstROIY();
		cmsParameters->DST_ROI_WIDTH = xml->getdstROIWidth();
		cmsParameters->DST_ROI_HEIGHT = xml->getdstROIHeight();
		cmsParameters->DST_ROI_SCALE_SEP = xml->getdstROIScaleSEP();

		polyU = xml->getPolyU();
		polyV = xml->getPolyV();

		for(int i = 0; i < 10; i++)
		{
			cmsParameters->polyU[i] = *(polyU+i);
			cmsParameters->polyV[i] = *(polyV+i);
		}
	}
	//FishEye data
	fisheye = xml->getIsFishEyeCameraEnabled();
	if(fisheye)
	{
		feParameters= new FISHEYEPARAMS;
		feParameters->xc=xml->getCenterX();
		feParameters->yc=xml->getCenterY();
		feParameters->c=xml->getC();
		feParameters->d=xml->getD();
		feParameters->e=xml->getE();
		feParameters->length_invpol=xml->getNumScaramuzzaCoeff();

		feParameters->width=displayWidth;
		feParameters->height=displayHeight;

		for(int i=0; i<feParameters->length_invpol; i++)
		{
			feParameters->invpol[i]=xml->getScaramuzzaCoeff()[i];
		}
	}

	// overlay conf
	overlay = xml->getisOverlayEnabled();
	Path.push_back((xml->getoverlayPath()).c_str());
	Path.push_back((xml->getCleanPath().c_str()));
	Path.push_back((xml->getDirtyPath().c_str()));
	Path.push_back((xml->getCleanOverlayPath().c_str()));
	Path.push_back((xml->getDirtyOverlayPath().c_str()));

	vertexshaderPath = (xml->getVertexshaderPath()).c_str();
	fragmentshaderPath = (xml->getFragmentshaderPath()).c_str();

	if (buttons && displayId == 0) buttonsFiniteStateMachine = new ButtonsFiniteStateMachine(maxBacklightValue, minBacklightValue, dayBacklightValue, nightBacklightValue, backlightLevel, stepBacklight, scriptSetBacklightPath, scriptDayNightPath);

	FiniteStateMachineTemplate *lastFiniteStateMachine;
	captureFiniteStateMachine = new CaptureFiniteStateMachine(cameraWidth, cameraHeight, nCameras, camerasConnectionType, cameraBufferType, cameraColorConv, engine2d, hardwareHandle);
	lastFiniteStateMachine = captureFiniteStateMachine;

	if(soil) {
		soilFiniteStateMachine = new SoilFiniteStateMachine(cameraWidth, cameraHeight, nCameras, soilFPS, cameraFrameRate, svmPath);
		lastFiniteStateMachine->addNextStage(soilFiniteStateMachine);
		lastFiniteStateMachine = soilFiniteStateMachine;
	}


	if(aespherical and crop){
		std::cout << "Error: Aespherical and Crop cannot be enabled together" << std::endl;
		exit(1);
	}


	if(aespherical and fisheye)
	{
		std::cout << "Error: Aespherical and Fish Eye cannot be enabled together" << std::endl;
		exit(1);
	}


	if(crop and fisheye)
	{
		std::cout << "Error: crop and Fish Eye cannot be enabled together" << std::endl;
		exit(1);
	}



	if(aespherical){
		aesphericalFiniteStateMachine = new AesphericalFiniteStateMachine(hardwareHandle, cmsParameters);
		lastFiniteStateMachine->addNextStage(aesphericalFiniteStateMachine);
		lastFiniteStateMachine = aesphericalFiniteStateMachine;
	}



	if (crop){
		cropFiniteStateMachine = new CropFiniteStateMachine(cropROIX0, cropROIY0, cropHeight, cropWidth, engine2d, hardwareHandle);
		lastFiniteStateMachine->addNextStage(cropFiniteStateMachine);
		lastFiniteStateMachine = cropFiniteStateMachine;
	}


	if(fisheye)
	{
		fishEyeFiniteStateMachine = new FishEyeFiniteStateMachine(hardwareHandle, *feParameters);
		lastFiniteStateMachine->addNextStage(fishEyeFiniteStateMachine);
		lastFiniteStateMachine = fishEyeFiniteStateMachine;
	}


	if(overlay || soil){
		overlayFiniteStateMachine = new OverlayFiniteStateMachine(captureFiniteStateMachine->getDevice(), cameraWidth, cameraHeight, nCameras, Path, overlay, win, vertexshaderPath, fragmentshaderPath);
		lastFiniteStateMachine->addNextStage(overlayFiniteStateMachine);
		lastFiniteStateMachine = overlayFiniteStateMachine;
	}


	inputSnapshotFiniteStateMachine = new SnapshotFiniteStateMachine(nameSnapshot,formatSnapshot);
	inputRecordFiniteStateMachine = new RecordFiniteStateMachine(captureFiniteStateMachine->getDevice(),cameraWidth, cameraHeight, engine2d);

	captureFiniteStateMachine->addNextAuxStage(inputSnapshotFiniteStateMachine);
	inputSnapshotFiniteStateMachine->addNextStage(inputRecordFiniteStateMachine);

	displayFiniteStateMachine = new DisplayFiniteStateMachine(displayId, displayX0, displayY0,displayWidth, displayHeight, cameraWidth, cameraHeight, engine2d, hardwareHandle);

	lastFiniteStateMachine->addNextStage(displayFiniteStateMachine);

	outputSnapshotFiniteStateMachine = new SnapshotFiniteStateMachine(nameSnapshot,formatSnapshot);
	outputRecordFiniteStateMachine = new RecordFiniteStateMachine(captureFiniteStateMachine->getDevice(),cropWidth, cropHeight, engine2d);

	displayFiniteStateMachine->addNextAuxStage(outputSnapshotFiniteStateMachine);
	outputSnapshotFiniteStateMachine->addNextStage(outputRecordFiniteStateMachine);


	if (soil){
		soilFiniteStateMachine->initMsgQueue(soilFiniteStateMachine);
		soilFiniteStateMachine->linkQueues(this, soilFiniteStateMachine); //Assigns OutMsgQ value from soil to Handle inMsgQ
		overlayFiniteStateMachine->linkQueues(overlayFiniteStateMachine, this); //Assigns OutMsgQ value from Handle to overlay inMsgQ
	}

	else if(!soil and overlay){
		this->initMsgQueue(this);
		overlayFiniteStateMachine->linkQueues(overlayFiniteStateMachine, this); //Assigns OutMsgQ value from Handle to overlay inMsgQ
	}

}

HandleStateMachine::~HandleStateMachine(){
	// TODO Auto-generated destructor stub
}

HandleStateMachine::State HandleStateMachine::ST_Stopped(){
	return State::STOPPED;
}
HandleStateMachine::State HandleStateMachine::ST_Initialized(){
	return State::INITIALIZED;
}


void HandleStateMachine::addMessage(std::queue <MSG> *outMsgQ, MSG message) {
	// POLICY
	if (outMsgQ->size() > 0)
		outMsgQ->pop();

	outMsgQ->push(message);

	if(inMsgQ) {
		if(inMsgQ->size() > 0)
			inMsgQ->pop();
	}
}

HandleStateMachine::MSG HandleStateMachine::getMessage(std::queue <MSG> *inMsgQ) {
	return inMsgQ->front();
}

bool HandleStateMachine::CheckMessage(std::queue <MSG> *inMsgQ){
	if(inMsgQ) {
		if(!inMsgQ->empty()){
			//std::cout << "queue message: " << inMsgQ->front().msg << std::endl;
			return true;
		}

		//std::cout << "queue message is empty" <<  std::endl;
		return false;
	}

	//std::cout << "queue message does not exist" <<  std::endl;
	return false;
}

void HandleStateMachine::MessageQueue() {


	MSG msgHandle;

	if(CheckMessage(inMsgQ)){
		MSG msg = getMessage(inMsgQ);
		FSM fsmSender = msg.sender;
		if(fsmSender == FSM_SOILING){

			if(msg.msg == reinterpret_cast<void*>(SoilFiniteStateMachine::STATE_PARTIALLY_DIRTY)||
			   msg.msg == reinterpret_cast<void*>(SoilFiniteStateMachine::STATE_OCCLUDED)||
			   msg.msg == reinterpret_cast<void*>(SoilFiniteStateMachine::STATE_DIRTY)){

				if(overlay)
					msgHandle.msg = reinterpret_cast<void*>(OVERLAY_DIRTY_LOGO);
				else
					msgHandle.msg = reinterpret_cast<void*>(OVERLAY_DIRTY);

				addMessage(outMsgQ, msgHandle);

			} else if (msg.msg == reinterpret_cast<void*>(SoilFiniteStateMachine::STATE_CLEAN)) {

				if(overlay)
					msgHandle.msg = reinterpret_cast<void*>(OVERLAY_CLEAN_LOGO);
				else
					msgHandle.msg = reinterpret_cast<void*>(OVERLAY_CLEAN);

				addMessage(outMsgQ, msgHandle);

			} else if(msg.msg == reinterpret_cast<void*>(SoilFiniteStateMachine::STATE_UNKNOWN)) {
				if(overlay) {
					msgHandle.msg = reinterpret_cast<void*>(OVERLAY_EMPTY);
					addMessage(outMsgQ, msgHandle);
				}
				else{
					msgHandle.msg = reinterpret_cast<void*>(OVERLAY_INIT);
					addMessage(outMsgQ, msgHandle);
				}
			}
		}

	} else {
		if(overlay and !soil) {
			msgHandle.msg = reinterpret_cast<void*>(OVERLAY_EMPTY);
			addMessage(outMsgQ, msgHandle);
		}
	}

}

HandleStateMachine::State HandleStateMachine::ST_Run(){
	struct pollfd fds;
	char input;
	bool ret = 0;
	fds.fd = 0; // this is STDIN
	fds.events = POLLIN;
	ret = poll(&fds, 1, 0);
	NvMediaImage *syncFrame;

	time_t rawtime;
	struct tm * timeinfo;
	char timeStamp[20];

	if (ret == 1) {

		input = getc(stdin);
		time (&rawtime);
		timeinfo = localtime(&rawtime);
		strftime(timeStamp,20,"%Y%m%dT%H%M%S",timeinfo);
		syncFrame = nullptr;
		switch (tolower(input)) {
		case 'r':
				inputRecordFiniteStateMachine->activateDeactivate(nameRecord+std::string(timeStamp)+"_sync_input", syncFrame);
				outputRecordFiniteStateMachine->activateDeactivate(nameRecord+std::string(timeStamp)+"_sync_output",syncFrame);

			break;
		case 'i':
				inputRecordFiniteStateMachine->activateDeactivate(nameRecord+std::string(timeStamp)+"_input", syncFrame);

			break;
		case 'o':
				outputRecordFiniteStateMachine->activateDeactivate(nameRecord+std::string(timeStamp)+"_sync_output",syncFrame);

			break;
		case 's':
				inputSnapshotFiniteStateMachine->takeSnapshot(nameSnapshot+"_"+std::string(timeStamp)+"_sync_input."+formatSnapshot, syncFrame);
				outputSnapshotFiniteStateMachine->takeSnapshot(nameSnapshot+"_"+std::string(timeStamp)+"_sync_output."+formatSnapshot, syncFrame);

			break;
		default:

			break;
		}

	}else{
		// TODO[3] : for the moment nothing
	}

	if (soil || overlay)
		MessageQueue();

	usleep(16000);
	return State::RUN;
}
HandleStateMachine::State HandleStateMachine::ST_Failed(){
	return State::FAILED;
}
HandleStateMachine::State HandleStateMachine::ST_Stopping(){
	return State::STOPPED;
}

HandleStateMachine::State HandleStateMachine::ST_Initializing(){

	if(buttons && displayId == 0) buttonsFiniteStateMachine->changeState(State::INITIALIZED);
	captureFiniteStateMachine->changeState(State::INITIALIZED);
	if(soil){
		soilFiniteStateMachine->changeState(State::INITIALIZED);
		initialize = true;
	}
	if(overlay || initialize) overlayFiniteStateMachine->changeState(State::INITIALIZED);
	if(crop and !aespherical and !fisheye) cropFiniteStateMachine->changeState(State::INITIALIZED);
	if(aespherical and !crop and !fisheye) aesphericalFiniteStateMachine->changeState(State::INITIALIZED);
	if(fisheye and !aespherical and !crop) fishEyeFiniteStateMachine->changeState(State::INITIALIZED);
	inputRecordFiniteStateMachine->changeState(State::INITIALIZED);
	inputSnapshotFiniteStateMachine->changeState(State::INITIALIZED);
	outputRecordFiniteStateMachine->changeState(State::INITIALIZED);
	outputSnapshotFiniteStateMachine->changeState(State::INITIALIZED);
	displayFiniteStateMachine->changeState(State::INITIALIZED);

	return State::INITIALIZED;
}


HandleStateMachine::State HandleStateMachine::ST_Running(){

	// We use while loop to force the syncronization between threads.

	if(buttons && displayId == 0){
		while (buttonsFiniteStateMachine->getCurrentState() != buttonsFiniteStateMachine->RUN &&
				buttonsFiniteStateMachine->getCurrentState() != buttonsFiniteStateMachine->FAILED){
			buttonsFiniteStateMachine->changeState(State::RUN);
		}
	}

	while (captureFiniteStateMachine->getCurrentState() != captureFiniteStateMachine->RUN &&
			captureFiniteStateMachine->getCurrentState() != captureFiniteStateMachine->FAILED){
		captureFiniteStateMachine->changeState(State::RUN);
	}

	if(soil){
		while (soilFiniteStateMachine->getCurrentState() != soilFiniteStateMachine->RUN &&
				soilFiniteStateMachine->getCurrentState() != soilFiniteStateMachine->FAILED){
			soilFiniteStateMachine->changeState(State::RUN);
		}
	}

	if(aespherical and !crop and !fisheye){
		while (aesphericalFiniteStateMachine->getCurrentState() != aesphericalFiniteStateMachine->RUN &&
				aesphericalFiniteStateMachine->getCurrentState() != aesphericalFiniteStateMachine->FAILED){
			aesphericalFiniteStateMachine->changeState(State::RUN);
		}
	}

	if(crop and !aespherical and !fisheye){
		while (cropFiniteStateMachine->getCurrentState() != cropFiniteStateMachine->RUN &&
				cropFiniteStateMachine->getCurrentState() != cropFiniteStateMachine->FAILED){
			cropFiniteStateMachine->changeState(State::RUN);
		}
	}

	if(fisheye and !aespherical and !crop){
		while (fishEyeFiniteStateMachine->getCurrentState() != fishEyeFiniteStateMachine->RUN &&
				fishEyeFiniteStateMachine->getCurrentState() != fishEyeFiniteStateMachine->FAILED){
			fishEyeFiniteStateMachine->changeState(State::RUN);
		}
	}

	if(overlay || initialize){
		while (overlayFiniteStateMachine->getCurrentState() != overlayFiniteStateMachine->RUN &&
				overlayFiniteStateMachine->getCurrentState() != overlayFiniteStateMachine->FAILED){
			overlayFiniteStateMachine->changeState(State::RUN);
		}
	}

	while (displayFiniteStateMachine->getCurrentState() != displayFiniteStateMachine->RUN &&
			displayFiniteStateMachine->getCurrentState() != displayFiniteStateMachine->FAILED){
		displayFiniteStateMachine->changeState(State::RUN);
	}

	while (inputSnapshotFiniteStateMachine->getCurrentState() != inputSnapshotFiniteStateMachine->RUN &&
			inputSnapshotFiniteStateMachine->getCurrentState() != inputSnapshotFiniteStateMachine->FAILED){
		inputSnapshotFiniteStateMachine->changeState(State::RUN);
	}

	while (inputRecordFiniteStateMachine->getCurrentState() != inputRecordFiniteStateMachine->RUN &&
			inputRecordFiniteStateMachine->getCurrentState() != inputRecordFiniteStateMachine->FAILED){
		inputRecordFiniteStateMachine->changeState(State::RUN);
	}

	while (outputSnapshotFiniteStateMachine->getCurrentState() != outputSnapshotFiniteStateMachine->RUN &&
			outputSnapshotFiniteStateMachine->getCurrentState() != outputSnapshotFiniteStateMachine->FAILED){
		outputSnapshotFiniteStateMachine->changeState(State::RUN);
	}

	while (outputRecordFiniteStateMachine->getCurrentState() != outputRecordFiniteStateMachine->RUN &&
			outputRecordFiniteStateMachine->getCurrentState() != outputRecordFiniteStateMachine->FAILED){
		outputRecordFiniteStateMachine->changeState(State::RUN);
	}

	// execution control
	std::cout << std::endl << "****************** OPTIONS ******************" << std::endl;
	std::cout << " 1.Record both Input/Output Video (ON/OFF) Press r " << std::endl;
	std::cout << " 2.Record Only Input Video (ON/OFF) Press i " << std::endl;
	std::cout << " 3.Record Only Output Video (ON/OFF) Press o " << std::endl;
	std::cout << " 4.Snapshot Input/Output Frame Press s " << std::endl;
	return State::RUN;
}

HandleStateMachine::State HandleStateMachine::ST_Failing(){
	return State::FAILED;
}
