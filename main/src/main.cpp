/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
 *  @par Language: MISRA C++ 2008 & C++11
 *******************************************************************
 *  @file main.cpp
 *  @brief This file contains the implementation of the main
 *  @author  Albert Mosella Montoro
 *  @version 1.0
 *  @date 15/06/2016
 *******************************************************************/

#include <iostream>
#include <unistd.h>
#include <signal.h>
#include "HandleStateMachine.h"

#define CMS_VERSION "1.0.0"

using namespace std;




static void show_usage(string name)
{
	std::cout << "How to use: " << name << std::endl
			<< "Options:\n" << std::endl
			<< "\t -v \t [--config] or [--cfg] \t: path for configuration file" << std::endl
			<< std::endl;
}

static void show_version()
{
	std::cout << "CMS VERSION: " << CMS_VERSION << std::endl;
}



int main(int argc, char *argv[])
{
	char* xmlFile;
	xmlFile = "";

	if((argc == 2) and string(argv[1]) == "-v"){
		show_version();
		return 1;
	}

	else if (argc < 3) {
		show_usage(argv[0]);
		return 1;
	}

	for (int i = 1; i < argc; ++i) {
		if(std::string(argv[i]) == "-v")
			show_version();
		if (std::string(argv[i]) == "--config" || std::string(argv[i]) == "--cfg"){
			if(i +1 < argc){
				xmlFile = argv[i+1];
			}else{
				return 1;
			}
		}else{
		}

	}
	HandleStateMachine *mainMachine = new HandleStateMachine(xmlFile);
	mainMachine->changeState(mainMachine->INITIALIZED);

	while (mainMachine->getCurrentState() != mainMachine->RUN &&
			mainMachine->getCurrentState() != mainMachine->FAILED){
		mainMachine->changeState(mainMachine->RUN);
	}

	if (mainMachine->getCurrentState() == mainMachine->FAILED){
		return -1;
	}else while(mainMachine->getCurrentState() != mainMachine->STOPPED){
		sleep(6000);
	}
}


