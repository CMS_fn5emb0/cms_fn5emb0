/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file HandleStateMachine.h
*  @brief This file contains the definition of the class
*  		  HandleStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/
#ifndef HANDLESTATEMACHINE_H_
#define HANDLESTATEMACHINE

#include "FiniteStateMachineTemplate.h"
#include "CaptureFiniteStateMachine.h"
#include "RecordFiniteStateMachine.h"
#include "DisplayFiniteStateMachine.h"
#include "SnapshotFiniteStateMachine.h"
#include "OverlayFiniteStateMachine.h"
#include "CropFiniteStateMachine.h"
#include "ButtonsFiniteStateMachine.h"
#include "SoilFiniteStateMachine.h"
#include "AesphericalFiniteStateMachine.h"
#include "FishEyeFiniteStateMachine.h"

#include <stdio.h>
#include <sys/poll.h>
#include "capture2d.h"
#include "nvmedia.h"
#include <iostream>

#define OVERLAY_INIT -1
#define OVERLAY_EMPTY 0
#define OVERLAY_CLEAN 1
#define OVERLAY_DIRTY 2
#define OVERLAY_CLEAN_LOGO 3
#define OVERLAY_DIRTY_LOGO 4

/** @class HandleStateMachine
 *  @details This class incorporates the functionalities to
 *  		 handle the state machines of our program.
 */

class HandleStateMachine: public FiniteStateMachineTemplate {

public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	HandleStateMachine(char* xmlconfig);

	/** @brief Free the resources generated for the object
	 */
	virtual ~HandleStateMachine();

	/** @brief This method adds the states of the camera to a queue.
	 *
	 * @return void
	 */
	void addMessage(std::queue <MSG> *outMsgQ, MSG message);
	/** @brief This method gets the message stored in inMsgQ from the queue of Handle
	 *
	 * @returns the value of the message from the front of the queue
	 */
	MSG getMessage(std::queue <MSG> *inMsgQ);
	/** @brief This method check if there is a message stored in the queue
	 *
	 * @returns true when there is a message stored in the queue of handle
	 * @returns false when there is not message stored in the queue of Handle or the queue does not exist.
	 */
	bool CheckMessage(std::queue <MSG> *inMsgQ);
	/** @brief This method manages the messages that comes from the different modules
	 *
	 * @return void
	 */
	void MessageQueue();

private:
	ButtonsFiniteStateMachine *buttonsFiniteStateMachine;

	bool initialize;
	int textureId;

	bool buttons;
	int maxBacklightValue;
	int minBacklightValue;
	int backlightLevel;
	int stepBacklight;
	int nightBacklightValue;
	int dayBacklightValue;
	std::string scriptSetBacklightPath;
	std::string scriptDayNightPath;

	NvMediaDevice *hardwareHandle;
	NvMedia2D *engine2d;

	/** @var capture2d
	 *  	Is used to manage the camera capture.
	 */
	Capture2d *capture2d;

	AbstractWindow* win;
	AbstractWindow::WindowManager wm;
	EGLDisplay displayOvl;
	AbstractWindow* winOvl;

	CMSPARAMETERS* cmsParameters;

	FISHEYEPARAMS* feParameters;

	const float *polyU;
	const float *polyV;

	const char* svmPath[3];
	int soilFPS;

	std::list<const char*> Path;

	const char* vertexshaderPath;
	const char* fragmentshaderPath;

	bool aespherical = false;
	bool overlay = false;
	bool crop = false;
	bool soil = false;
	bool fisheye = false;

	int16_t cropROIX0;
	int16_t cropROIY0;
	int16_t cropHeight;
	int16_t cropWidth;

	/** @var cameraWidth
	 *  	  Defines the width region of the camera.
	 */
	int cameraWidth;

	/** @var cameraHeight
	 *  	 Defines the height region of the camera.
	 */
	int cameraHeight;

	/** @var nCameras
	 *  	 Defines the number of cameras to manage.
	 */
	int nCameras;

	/** @var cameraFrameRate
	 *  	Defines the camera frame rate.
	 */
	int cameraFrameRate;

	/** @var camerasConnectionType
	 *  	 Defines the type of the connection.
	 */
	int16_t camerasConnectionType;

	/** @var cameraBufferType
	 *  	  Defines the type of the buffer used to capture.
	 */
	Capture2d::BufferType cameraBufferType;

	/** @var cameraColorConv
	 *  	Defines the color space.
	 */
	Capture2d::ColorConversion cameraColorConv;


	/** @var displayId
	 *  	  Defines the id of the display we will use.
	 */
	int16_t displayId;

	int16_t displayX0;
	int16_t displayY0;

	/** @var displayWidth
	 *  	  Defines the width resolution of the display.
	 */
	int16_t displayWidth;

	/** @var displayHeight
	 *  	  Defines the height resolution of the display.
	 */
	int16_t displayHeight;

	std::string formatSnapshot;
	std::string nameSnapshot;

	std::string nameRecord;

	/** @var soilFiniteStateMachine
	 *  	 Is used to manage the soil state machine.
	 */
	SoilFiniteStateMachine *soilFiniteStateMachine;

	/** @var cropFiniteStateMachine
	 *  	 Is used to manage the crop state machine.
	 */
	CropFiniteStateMachine *cropFiniteStateMachine;

	/** @var overFiniteStateMachine
	 *  	 Is used to manage the overlay state machine.
	 */
	OverlayFiniteStateMachine *overlayFiniteStateMachine;

	/** @var displayFiniteStateMachine
	 *  	 Is used to manage the display state machine.
	 */
	DisplayFiniteStateMachine *displayFiniteStateMachine;

	/** @var recordFiniteStateMachine
	 *  	 Is used to manage the record state machine.
	 */
	RecordFiniteStateMachine *inputRecordFiniteStateMachine;

	RecordFiniteStateMachine *outputRecordFiniteStateMachine;

	/** @var captureFiniteStateMachine
	 *  	 Is used to manage the capture state machine.
	 */
    CaptureFiniteStateMachine *captureFiniteStateMachine;

    /** @var inputSnapshotFiniteStateMachine
	 *  	 Is used to manage the  input snapshot state machine.
	 */
    SnapshotFiniteStateMachine *inputSnapshotFiniteStateMachine;

    /** @var outputSnapshotFiniteStateMachine
	 *  	 Is used to manage the  output snapshot state machine.
	 */
    SnapshotFiniteStateMachine *outputSnapshotFiniteStateMachine;

    /** @var aesphericalFiniteStateMachine
	 *  	 Is used to manage the  aespherical state machine.
	 */
    AesphericalFiniteStateMachine* aesphericalFiniteStateMachine;

/** @var aesphericalFiniteStateMachine
	 *  	 Is used to manage the  fish eye state machine.
	 */
	FishEyeFiniteStateMachine* fishEyeFiniteStateMachine;

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to fail,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* HANDLESTATEMACHINE_H_ */
