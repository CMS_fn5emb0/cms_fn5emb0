/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file CaptureFiniteStateMachine.h
*  @brief This file contains the definition of the class
*  		  CaptureFiniteStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef CAPTUREFINITESTATEMACHINE_H_
#define CAPTUREFINITESTATEMACHINE_H_
#include "FiniteStateMachineTemplateWithAux.h"
#include "capture2d.h"
#include "nvmedia.h"


/** @class CaptureFiniteStateMachine
 *  @details This class incorporates the functionalities to
 *  		 capture image from the sensor.
 */

class CaptureFiniteStateMachine: public FiniteStateMachineTemplateWithAux {
public:
	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	CaptureFiniteStateMachine(int _width,  int _height, int _nCameras, int16_t _camerasConnectionType,
							  Capture2d::BufferType _bufferType, Capture2d::ColorConversion _colorConv,
							  NvMedia2D *_engine2d, NvMediaDevice *_hardwareHandle);

	/** @brief Free the resources generated for the object
	 */
	virtual ~CaptureFiniteStateMachine();

	/** @brief This method return the capture2d object.
	 *
	 * @return Capture2d*
	 */
	inline Capture2d* getCapture(){return capture2d;}

	/** @brief This method return the hardwareHanndle object.
	 *
	 * @return NvMediaDevice*
	 */
	inline NvMediaDevice* getDevice(){return hardwareHandle;}

private:

	//boost::circular_buffer<NvMediaImage*>::iterator videoBufferIt;

	//boost::circular_buffer<NvMediaImage*>::iterator videoAuxIt;

	BufferPool *capturePool;
	BufferPool *captureAuxPool;


	NvMedia2D *engine2d;
	NvMedia2DBlitParameters blitParams;

	/** @var camerasHanndle
	 *  	  Is used to manage the hardware.
	 */
	NvMediaDevice *hardwareHandle;

	/** @var capture2d
	 *  	 Is used to manage the camera capture.
	 */
	Capture2d *capture2d;

	/** @var width
	 *  	  Defines the width region of the camera.
	 */
	int width;

	/** @var height
	 *  	 Defines the height region of the camera.
	 */
    int height;

	/** @var nCameras
	 *  	 Defines the number of cameras to manage.
	 */
	int nCameras;

	/** @var camerasConnectionType
	 *  	 Defines the type of the connection.
	 */
	int16_t camerasConnectionType;

	/** @var bufferType
	 *  	  Defines the type of the buffer used to capture.
	 */
	Capture2d::BufferType bufferType;

	/** @var colorConv
	 *  	Defines the color space.
	 */
	Capture2d::ColorConversion colorConv;

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to fail,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* CAPTUREFINITESTATEMACHINE_H_ */
