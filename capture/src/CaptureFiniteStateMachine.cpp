/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file CaptureFiniteStateMachine.cpp
*  @brief This file contains the implementation of the class
*  		  CaptureFiniteStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "CaptureFiniteStateMachine.h"
#include <iostream>
#include <unistd.h>
CaptureFiniteStateMachine::CaptureFiniteStateMachine(int _width,  int _height, int _nCameras, int16_t _camerasConnectionType,
		Capture2d::BufferType _bufferType, Capture2d::ColorConversion _colorConv,
		NvMedia2D *_engine2d, NvMediaDevice *_hardwareHandle)
:FiniteStateMachineTemplateWithAux(){
	//hardwareHandle = NvMediaDeviceCreate();
	//engine2d = NvMedia2DCreate(hardwareHandle);

	hardwareHandle = _hardwareHandle;
	engine2d = _engine2d;
	width = _width;
	height = _height;
	nCameras = _nCameras;
	camerasConnectionType = _camerasConnectionType;
	bufferType = _bufferType;
	colorConv = _colorConv;
	capture2d = new Capture2d(hardwareHandle, (Capture2d::Connection) camerasConnectionType, nCameras,
			BUFFER_SIZE, width, height, bufferType, colorConv);

	ImageBufferPoolConfig configBufferPool;

	memset(&configBufferPool, 0, sizeof(ImageBufferPoolConfig));
	configBufferPool.width = _width;
	configBufferPool.height = _height;
	configBufferPool.surfType = NvMediaSurfaceType_Image_RGBA;
	configBufferPool.imageClass = NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE;
	configBufferPool.imagesCount = 1;
	configBufferPool.device = hardwareHandle;

	BufferPool_Create (&capturePool, BUFFER_SIZE, QUEUE_TIMEOUT, IMAGE_BUFFER_POOL, (void *)&configBufferPool);
	BufferPool_Create (&captureAuxPool, 8, QUEUE_TIMEOUT, IMAGE_BUFFER_POOL, (void *)&configBufferPool);
	memset(&blitParams, 0, sizeof(blitParams));
}
CaptureFiniteStateMachine::~CaptureFiniteStateMachine() {
	// TODO Auto-generated destructor stub
}

CaptureFiniteStateMachine::State CaptureFiniteStateMachine::ST_Stopped(){
	return State::STOPPED;
}
CaptureFiniteStateMachine::State CaptureFiniteStateMachine::ST_Initialized(){
	return State::INITIALIZED;
}
CaptureFiniteStateMachine::State CaptureFiniteStateMachine::ST_Run(){

	NvMediaImage *frame = nullptr;
	ImageBuffer *captureBuffer = nullptr;
	ImageBuffer *captureAuxBuffer = nullptr;

	frame = capture2d->getSingleFrame(capture2d->getNextFrame(),0);

	if (BufferPool_AcquireBuffer(capturePool, (void **)&captureBuffer) != NVMEDIA_STATUS_OK ) {
		fprintf(stdout, " %s: Problem acquiring new frame from buffer pool \n", __FILE__);
	} else {
		// copy the image from camera videobuffer to application buffer
		NvMedia2DBlit(engine2d, captureBuffer->image, nullptr, frame, nullptr, &blitParams);
		linkStages(captureBuffer->image);
	}

	if (BufferPool_AcquireBuffer(captureAuxPool, (void **)&captureAuxBuffer) != NVMEDIA_STATUS_OK ) {
			fprintf(stdout, "Problem acquiring new frame from buffer pool \n");
	} else {
		// copy the image from camera videobuffer to application buffer
		NvMedia2DBlit(engine2d, captureAuxBuffer->image, nullptr, frame, nullptr, &blitParams);
		linkAuxStages(captureAuxBuffer->image);
	}

	// Return the frame to the camera buffer pool
	capture2d->returnSingleFrame(frame);
	return State::RUN;
}
CaptureFiniteStateMachine::State CaptureFiniteStateMachine::ST_Failed(){
	return State::FAILED;
}

CaptureFiniteStateMachine::State CaptureFiniteStateMachine::ST_Stopping(){
	return State::STOPPED;
}
CaptureFiniteStateMachine::State CaptureFiniteStateMachine::ST_Initializing(){
	// TODO[2] include cam config
	return State::INITIALIZED;
}
CaptureFiniteStateMachine::State CaptureFiniteStateMachine::ST_Running(){
	return State::RUN;
}
CaptureFiniteStateMachine::State CaptureFiniteStateMachine::ST_Failing(){
	return State::FAILED;
}
