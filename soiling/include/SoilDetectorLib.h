/*
 * SoilDetectorLib.h
 *
 *  Created on: Nov 10, 2015
 *      Author: Adasens
 */

#ifndef SOILDETECTORLIB_H_
#define SOILDETECTORLIB_H_

#include "SoilDetectionParameters.h"

/** @breif This function Init the soil detector and must be called
 *         before call other methods of this library.
 * @param  params: Structure where are described all the parameters.
 * @param  camWidth: With of the image.
 * @param  camHeight: Height of the image.
 * @param  numCameras: The number of the cameras that will be used
 *         to process the Soil Detector Method.
 * @param  svmPath: the path of the methodtype
 * @return void
 */
void InitSoilDetector(SoilDetectionParameters params,unsigned int camWidth, unsigned int camHeight,unsigned int numCameras, const char** svmPath);

/** @breif This method update the soil detector state.
 * @param  image: The image to be processed by the algorithm.
 * @param  CameraIndex: The camera index number.
 * @return void
 */
void UpdateSoilDetector(const unsigned char* image,unsigned int cameraIndex);

/** @breif This method obtain the global state of the whole ROI.
 * @param  CameraIndex: The camera index number.
 * @return global state: ( Clean = 0, Partially = 1, Dirty = 2,
 * 		   Unknown = 3, Occluded= 4).
 */
unsigned int GetGlobalStateSoilDetector(unsigned int cameraIndex);

/** @breif This method obtain the local state of the whole ROI.
 * @param  idx: The cell index number in the grid.
 * @param  CameraIndex: The camera index number.
 * @return local state: ( Clean = 0, Partially = 1, Dirty = 2, Unknown = 3).
 */
unsigned int GetLocalStateSoilDetector(unsigned int idx,unsigned int cameraIndex);

/** @breif This method obtain the number of cells in the X axis.
 * @param  CameraIndex: The camera index number.
 * @return number of cells in the X axis.
 */
unsigned int GetNumCellsXSoilDetector(unsigned int cameraIndex);

/** @breif This method obtain the number of cells in the Y axis.
 * @param  CameraIndex: The camera index number.
 * @return number of cells in the Y axis.
 */
unsigned int GetNumCellsYSoilDetector(unsigned int cameraIndex);

/** @breif This method obtain the width of the ROI.
 * @param  CameraIndex: The camera index number.
 * @return width of the ROI.
 */
unsigned int GetROIWidthSoilDetector(unsigned int cameraIndex);

/** @breif This method obtain the height of the ROI.
 * @param  CameraIndex: The camera index number.
 * @return height of the ROI.
 */
unsigned int GetROIHeightSoilDetector(unsigned int cameraIndex);

#endif /* SOILDETECTORLIB_H_ */
