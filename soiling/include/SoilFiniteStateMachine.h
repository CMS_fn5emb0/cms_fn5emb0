/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file SoilFiniteStateMachine.h
*  @brief This file contains the definition of the class
*  		  SoilFiniteStateMachine
*  @author  ficoadas
*  @version 1.0
*  @date 18/08/2016
*******************************************************************/

#ifndef SOILFINITESTATEMACHINE_H_
#define SOILFINITESTATEMACHINE_H_


// include files
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <nvmedia.h>
#include <nvmedia_2d.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <serialmessage.h>

#include "FiniteStateMachineTemplate.h"
#include "SoilDetectionParameters.h"
#include "SoilDetectorLib.h"

// namespaces
using namespace std;
using namespace cv;
using namespace SerialMsgHandler;

#define POLICY 5


class SoilFiniteStateMachine: public FiniteStateMachineTemplate {
public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	SoilFiniteStateMachine(int imgWidth, int imgHeight, int nCameras, int fps, int camFrameRate, const char** svmPath);

	/** @brief Enum class. Indicates the kind of states of Soil Detector.
	 *
	 */
	enum Soil_State {STATE_UNKNOWN, STATE_CLEAN, STATE_DIRTY, STATE_PARTIALLY_DIRTY, STATE_OCCLUDED};

	/** @brief Free the resources generated for the object
	 */
	virtual ~SoilFiniteStateMachine();

	/** @brief Function that adds the message to the queue.
	 * Only push a message when the message is different from the previous one
	 * It also has a Policy to limit the size of the queue.
	*/
	void SoilMessage();

private:

	/** @var soilFps
	 *  	 Define the number frames per second.
	 */
	int soilFps;

	/** @var framesAcquired
	 *  	 Counter of number of frames acquired.
	 */
	int framesAcquired;

	/** @var cameraFrameRate
	 *  	 Defines the camera frame rate.
	 */
	int cameraFrameRate;

	/** @var _imgWidth
	 *		Defines the image width.
	*/
	unsigned int imgWidth;

	/** @var imgHeight
	 *		Defines the image height.
	*/
	unsigned int imgHeight;

	/** @var _camIndex
	 *		Defines the camera index.
	*/
	unsigned int camIndex;

	/** @var numCameras
	 *		Defines the number of cameras used.
	*/
	unsigned int numCameras;

	/** @var svmPath
	 *		Defines the list of SVM Classifier.
	*/
	const char* svmPath[3];

	/* @var lastsoilState
	 *      Indicates the last state of soil Detector.
	 */
	Soil_State lastsoilState;

	/* @var soilState
	 *      Indicates the current state of soil Detector.
	 */
	Soil_State soilState;

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state fail to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* SOILFINITESTATEMACHINE_H_ */
