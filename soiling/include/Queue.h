/**
* @file Queue.h
*
* @brief
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#pragma once
#include<iostream>
#include<cstdlib>

#define default_value 16
using namespace std;

template< class T > class Queue
{
    public:
        Queue(int = default_value);//default constructor
        ~Queue()//destructor
        {
            if (values!=nullptr)
               delete[] values;
        }
        bool push(T);
        T pop();
        bool isEmpty();
        bool isFull();
        void empty();
        void setSize(int x);
        T front();
    private:
        int size;
        //TODO: Avoid pointers
        T *values;
        int _front;
        int _back;
};
//-------------------------------------------------------------
template< class T > Queue<T>::Queue(int x)
{ 
    size = x;
    values = new T[size];
    _front = 0;
    _back = 0;
}

template< class T > void Queue<T>::setSize(int x)
{
    if (size > 0 && values != nullptr)
    {
        delete[] values;
    }
    size = x;
    
    values = new T[size];
    _front = 0;
    _back = 0;
}

template< class T > void Queue<T>::empty()
{
    delete[] values;
}

template< class T > bool Queue<T>::isFull()
{
    if ((_back + 1) % size == _front)
        return 1;
    else
        return 0;
}

template< class T > bool Queue<T>::push(T x)
{
    bool b = 0;
    if (!Queue<T>::isFull())
    {
        values[_back] = x;
        _back = (_back + 1) % size;
        b = 1;
    }
    return b;
}

template< class T > bool Queue<T>::isEmpty()
{
    if (_back == _front)//is empty
        return 1;
    else
        return 0; //is not empty
}

template< class T > T Queue<T>::pop()
{
    T val = (T)0;
    if (!Queue<T>::isEmpty())
    {
        val = values[_front];
        _front = (_front + 1) % size;
    }
    else
    {
        cerr << "Queue is Empty : ";
    }
    return val;

}

template< class T > T Queue<T>::front()
{
    T val= (T)0;
    if (!Queue<T>::isEmpty())
    {
        val = values[_front];
    }
    else
    {
        cerr << "Queue is Empty : ";
    }
    return val;

}


