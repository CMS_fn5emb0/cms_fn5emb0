/**
* @file TemporalFilter.h
*
* @brief Adds hysteresis and temporal smoothing to the algorithm output.
* This is achieved by building a histogram of the last N output states. The histogram bin with the
* largest state dictates the output state, however, the output state is only allowed to change if the
* bin has a size of greater than _stateChangeThreshold * N. _stateChangeThreshold = 0.80 by default
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#pragma once
#include <stdio.h>
#include <iostream>
#include "defines.h"
#include "CellClassification.h"
#include <queue>

class TemporalFilter
{

public:
    /** @brief Constructor.
     *
     */
    TemporalFilter();

    /** @brief Initializes the temporal filter.
     * @param temporalSize this is the window size for the filter.
     */
    void Init(unsigned int temporalSize,unsigned int histogramSize, double hysteresisStateChangeThreshold);

    /** @brief This updates the temporal filter with the new input state.
     *         This is called whenever new state data are available.
     * @param state the new state.
     */
    void UpdateCellFilter(DiscreteROIState state);

    /** @brief This returns the output of the filter. It is the best candidate for the
     *         output state calculated using the largest histogram bin with hysteresis added.
     */
    DiscreteROIState BestCellCandidate();

    /** @brief Resets the filter state. */
    void Clean();

    /** @brief Destructor class. */
    ~TemporalFilter();

private:

    unsigned int _temporalSize;
    unsigned int _temporalCnt;
    unsigned int* _histogram;
    unsigned int _histogramSize;

    std::queue <DiscreteROIState> _queueCellState;

    bool _queueCellFilled;
    DiscreteROIState _oldCellState;

    double _stateChangeThreshold;
};

