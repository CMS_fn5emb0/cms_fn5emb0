/*
 * Wrapper.h
 *
 *  Created on: Jul 7, 2016
 *      Author: ficoadas
 */

#ifndef CWRAPPER_H_
#define CWRAPPER_H_

// include files
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <nvmedia.h>
#include <nvmedia_2d.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <serialmessage.h>

#include "SoilDetectionParameters.h"
#include "SoilDetectorLib.h"

// namespaces
using namespace std;
using namespace cv;
using namespace SerialMsgHandler;

// configuretion class
// for the time being parameters this calls are hard coded and made as public
// todo: need to write get, set functions for all the parameters or one for all
//       parameters of the class and also need to made them private or protected
//       as per the requirement

class CConfigLoader
{
public:
    // Soil Detector Parameters

    /* @var classifier
	 *      SVM type.
	 */
    std::string classifier;


    /* @var roiX
	 *      x value of roi.
	 */
    unsigned int roiX;

    /* @var roiY
	 *      y value of roi.
	 */
    unsigned int roiY;

    /* @var roiwidth
	 *      image width.
	 */
    unsigned int roiWidth;

    /* @var roiHeight
	 *      image height.
	 */
    unsigned int roiHeight;

    /* @var rows
	 *      num rows.
	 */
    unsigned int rows;

    /* @var cols
	 *      num col.
	 */
    unsigned int cols;

    /* @var temporalFilter
	 *      temporal filter.
	 */
    unsigned int temporalFilter;

    /* @var overallTemporalFilter
	 *      overall temporal filter.
	 */
    unsigned int overallTemporalFilter;

    /* @var downsampleFactor
	 *      down sample rate.
	 */
    unsigned int downsampleFactor;

    /* @var hysteresisThreshold
	 *      hyteresis threshold value.
	 */
    float hysteresisThreshold;

    /** @brief constructor */
    CConfigLoader()
    {
    	// Soil Detector Default Parameters
        classifier="SVMExtNoPartial";
        roiX=192;
        roiY=71;
        roiWidth=800;
        roiHeight=400;
        rows=5;
        cols=5;
        temporalFilter=60;
        overallTemporalFilter=40;
        downsampleFactor=1;
        hysteresisThreshold=0.8f;
    }

    /** @brief destructor */
    ~CConfigLoader()
    {
    }

};/*end of class CConfigLoader */

#endif /* CWRAPPER_H_ */
