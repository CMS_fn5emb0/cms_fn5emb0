/**
* @file SoilDetectionParameters.h
*
* @brief        A structure to hold the parameters for the soil detection application.
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
* $Id: SoilDetectionParameters.h 281 2015-08-14 09:17:40Z lovellsmithc $
*/

#ifndef SOILDETECTIONPARAMETERS_H_
#define SOILDETECTIONPARAMETERS_H_
#pragma once

/** @brief Indicates the kind of classification to use. There are three available solutions:
 * SVM - Support vector machine based. In this configuration, it is only taking into account
 *       individual cells features. The state of each cell can be Clean, Partial,Dirty or Unknown.
 * SVMExt - Support vector machine based using neighboring ROIs.
 * SVMExtNoPartial - Support vector machine using neighboring ROIs without partial dirty state.
 *
 */
enum class MethodType { SVM, SVMExt, SVMExtNoPartial };
//States
enum class DiscreteROIState { Clean, PartiallyDirty, Dirty, Unknown, Occluded };
struct SoilDetectionParameters
{
    MethodType ClassifierType;

    /* @var NumCols
	 *      Indicate the number of cell in the Y axis.
	 */
    unsigned int NumRows;

    /* @var NumCols
     *      Indicate the number of cell in the X axis.
     */
    unsigned int NumCols;

    /* @var ROIX
	 *      This parameter indicate the X coordinate where start the
	 *      Region of Interest (ROI).
	 */
    unsigned int ROIX;

    /* @var ROIY
     *      This parameter indicate the Y coordinate where start the
     *      Region of Interest.
     */
    unsigned int ROIY;

    /* @var ROIWidth
     *      This parameter defines the width of the ROI.
     */
    unsigned int ROIWidth;

    /** @var ROIHeight
     *       This parameter defines the height of the ROI.
     */
    unsigned int ROIHeight;

    /** @var TemporalFilterSize
     *       This parameter control the state over the time of each cell (local state),
     * 		 as much longer it is, more robust is the method but it takes more time
     * 		 to give an answer. The recommended values are between100-2000.
	 */
    unsigned int TemporalFilterSize;

    /** @var OverallTemporalFilterSize
     *       This parameter apply a over all temporal filter of the
     * 		 global state. The recommended values are between 100-1000.
     * 		 As much bigger is the value, the robustness increase
     * 		 but the delay also is increased.
     */
    unsigned int OverallTemporalFilterSize;

    /** @var ImageDownsampleFactor
     * 		 This parameter is for internal purpose only. It is not
     *       recommended modify this value.
     */
    unsigned int ImageDownsampleFactor;

    /** @var HysteresisStateChangeThreshold
     *       This parameter is for internal purpose only. It is not
     *       recommended modify this value.
     */
    double HysteresisStateChangeThreshold;
};
#endif
