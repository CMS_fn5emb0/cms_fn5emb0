/**
* @file FeatureExtraction.h
*
* @brief Calculates features for use with the CellClassification class.
* These features are used to classify the cell states using a support vector machine.
* The features are the 1st to 3rd order moments of the Laplacian of the image.
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#pragma once

typedef unsigned int uint;

#include "opencv2/core/core.hpp"
#include "defines.h"
#include <stdio.h>
#include <cstdint>
#include <stdio.h>
#include <iostream>
#include <vector>

#ifdef _WIN32
#include <cstdint>
#endif

enum { FEATURESNUM = 3, MEANIDX = 0, VARIANCEIDX = 1, KURTOSISIDX = 2 };

class FeatureExtraction
{
public:
    /** @brief Default constructor
     */
    FeatureExtraction();

    /** @brief Extract the features moments from the image
     *  @param src: the pointer to the image to be processed
     *  @param[out] mean: the mean value of the Laplacian
     *  @param[out] deviation: the deviation value of the Laplacian
     *  @param[out] kurtosis: the kurtosis value of the Laplacian
     *  @param width: the width of the image
     *  @param height: the height of the image
     *  @param roiX: the X value of the beginning of the grid of ROIs measured from the top left of the video.
     *  @param roiY: the Y value of the beginning of the grid of ROIs measured from the top left of the video.
     *  @param roiWidth: the width of each ROI.
     *  @param roiHeight: the height of each ROI.
     *  @return void
     */
    static void ExtractMomentsFeatures(const unsigned char *src, double &mean, double &deviation, double &kurtosis, uint width, uint height, uint roiX, uint roiY, uint roiWidth, uint roiHeight, uint downsampleFactor);

    /** @brief Compute the Laplacian value from the image
     *  @param src: the pointer to the image to be processed
     *  @param[out] dst: the pointer destination of the processed image
     *  @param width: the width of the image
     *  @param height: the height of the image
     *  @return void
     */
    static void Laplacian(const unsigned char *src, int *dst, uint width, uint height);

    /** @brief Update the features vector of all the cells
     *  @param image: the pointer to the image to be processed
     *  @param width: the width of the image
     *  @param height: the height of the image
     *  @param cellIdx: the index of the cell to be processed
     *  @param numCellsX: the number of Cells in the X axis
     *  @param numCellsY: the number of Cells in the Y axis
     *  @param roiWidth: the width of each ROI.
     *  @param roiHeight: the height of each ROI.
     *  @param roiX: the X value of the beginning of the grid of ROIs measured from the top left of the video.
     *  @param roiY: the Y value of the beginning of the grid of ROIs measured from the top left of the video.
     *  @return void
     */
    void UpdateMomentsFeatures(const unsigned char * image, uint width, uint height, uint cellIdx, uint numCellsX, uint numCellsY, uint roiWidth, uint roiHeight, uint roiX, uint roiY, uint downsampleFactor);

    /** @brief Return the Mean value of the Laplacian
     *  @return the Mean value of the Laplacian
     */
    double Mean() const { return _cellsFeatures[MEANIDX]; };

    /** @brief Return the Variance value of the Laplacian
     *  @return the Variance value of the Laplacian
     */
    double Variance() const { return _cellsFeatures[VARIANCEIDX]; };

    /** @brief Return the Kurtosis value of the Laplacian
     *  @return the Kurtosis value of the Laplacian
     */
    double Kurtosis() const { return _cellsFeatures[KURTOSISIDX]; };

    /** @brief Return the computed feature vector
     *  @return the computed feature vector
     */
    cv::Mat GetFeatures();

private:
    double _cellsFeatures[FEATURESNUM];
    int _featuresSize;
};


