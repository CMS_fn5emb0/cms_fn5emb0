/**
* @file CellStates.h
*
* @brief A class that is composed of the CellClassification class, FeatureExtraction, and TemporalFilter classes. It is responsible for
* determining the output state of the cells using help from the other classes.
* The cell state is determined by calculating the 1st to 3rd moments of the Laplacian of the image contained within each cell. These
* values are then passed as a vector to a support vector machine that classifies the cell state. Depending on which SVM method is used
* the feature vector passed to the SVM may be composed of the moments from neighbouring cells as well as the cell that is being classified.
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#pragma once

typedef unsigned int uint;

#include <stdio.h>
#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include <thread>
#include "defines.h"
#include <cstdint>

#include "TemporalFilter.h"
//-----------------------------------------------------------------------------------------------------
class CellsStates
{
public:

    /** @brief Initialize the class with the default values.
     *  @param svmFile: path to the svm configuration file
     *  @param width: the image width.
     *  @param height: the image height.
     *  @param params: the algorithm parameters.
     *  @return void
     */
    void Init(std::string svmFile, uint width, uint height, SoilDetectionParameters params);
    
    /** @brief Process the incoming image and update the cell states.
     *  @param image: the image to be processed
     *  @return void
     */
    void UpdateCellStates(const uint8_t* image);

    /** @brief Obtain the state of the given index.
     *  @param idx: the index of the cell
     *  @return Class state
     */
    DiscreteROIState GetCellState(int idx) const;
    /** @brief Obtain the value of the given index.
     *  @param idx: the index of the cell
     *  @return Class state
     */
    DiscreteROIState GetOverAllState() const{return _overallState;};

    /** @brief Clean the temporal buffer
     *  @return void
     */
    void CleanTemporalFilter();

    /** @brief Get the Region of Interest X value of the beginning of the grid of ROIs measured from the top left of the video.
     *  @return Region of Interest X value of the beginning of the grid of ROIs measured from the top left of the video
    */
    uint GetROIx()const{ return _params.ROIX; }

    /** @brief Get the Region of Interest Y value of the beginning of the grid of ROIs measured from the top left of the video.
     *  @return Region of Interest Y value of the beginning of the grid of ROIs measured from the top left of the video
     */
    uint GetROIy()const{ return _params.ROIY; }

    /** @brief Get the Region of Interest width value
     *  @return Region of Interest width value
     */
    uint GetROIwidth()const{ return _params.ROIWidth; }

    /** @brief Get the Region of Interest height value
     *  @return Region of Interest height value
     */
    uint GetROIheight()const { return _params.ROIHeight; }

    /** @brief Get number of cells in the X axis
     *  @return the number of cells in the X axis
     */
    uint GetNumCellsX()const{ return _params.NumCols; }

    /** @brief Get number of cells in the Y axis
     *  @return the number of cells in the Y axis
     */
    uint GetNumCellsY()const{ return _params.NumRows; }

    CellsStates() : _initalised(false) {}
    ~CellsStates();
private:
    /** @brief Obtain the value of the given index.
     *  @param idx: the index of the cell
     *  @return Class state
     */
    DiscreteROIState estimateOverallState() const;

    DiscreteROIState* _cellStates; // Cells State
    DiscreteROIState _overallState;
    FeatureExtraction* _features;
    CellClassification _cellClassification;
    TemporalFilter* _temporalStateFilter;
    TemporalFilter _temporalOverallFilter;
    SoilDetectionParameters _params;
    uint _numCells;
    uint _width;
    uint _height;

    bool _initalised;
};

