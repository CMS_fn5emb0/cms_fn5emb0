/**
* @file CellClassification.h
*
* @brief        Classifies ROI states using a support vector machine, and features returned from the FeatureExtraction class.
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#pragma once
#include "opencv2/ml/ml.hpp"
#include <stdio.h>
#include <iostream>
#include <assert.h>
#include "SoilDetectionParameters.h"

#include "FeatureExtraction.h"
//-----------------------------------------------------------------------------------------------------
//States
//enum class DiscreteROIState { Clean, PartiallyDirty, Dirty, Unknown, Occluded };

//-----------------------------------------------------------------------------------------------------
class CellClassification
{
public:
    /** @brief Initialises the class and loads the SVM configuration file.
    */
    void Init(unsigned int numRows, unsigned int numCols, std::string svmFile);

    /** @brief Classify a pattern vector .
     *  @param pattern: the feature vector to be classified
    *  return the class that belong the pattern
    */
    DiscreteROIState Classify(cv::Mat pattern)const;
    
    /** @brief Generate new pattern vector with the given method.
     *  @param features: the basic pattern vector
     *  @param numCell: the index of the cell.
     *  @param methodType: the type of generated pattern
     *  return the new generated pattern
     */
    cv::Mat GeneratePattern(FeatureExtraction* features, uint numCell, MethodType methodType) const;

private:
    unsigned int _numRows;
    unsigned int _numCols;
    CvSVM _svm;
    bool _initialised;

    cv::Mat generateSVMExtPattern(FeatureExtraction* _features, uint numCell)const;
};
//-----------------------------------------------------------------------------------------------------
