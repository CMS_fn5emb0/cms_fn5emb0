/**
* @file defines.h
*
* @brief        Compile time defines.
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#pragma once

// Parameters that define the overall state
#define MAXCLEANVALUE 23 
#define MAXPARTIALDIRTYVALUE 40 
#define MAXDIRTYVALUE 90
#define MINUNKNOWNVALUE 75



