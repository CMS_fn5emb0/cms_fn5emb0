/**
* @file FeatureExtraction.cpp
*
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#include "FeatureExtraction.h"

//-----------------------------------------------------------------------------------------------------
FeatureExtraction::FeatureExtraction()
{
    _featuresSize = FEATURESNUM;
}
//-----------------------------------------------------------------------------------------------------
void FeatureExtraction::ExtractMomentsFeatures(const unsigned char *src, double &mean, double &variance, double &kurtosis, uint width, uint height, uint roiX, uint roiY, uint roiWidth, uint roiHeight, uint downsampleFactor)
{
    assert(src != nullptr);


    (void)height;

    int a0, a2, a4, a6, a8, a0_;

    long long n = 0;
    int sum = 0;
    long long sq_sum2 = 0;
    long long sq_sum3 = 0;
    long long sq_sum4 = 0;
    int value = 0;
    int value2 = 0;

    // Laplacian Convolution
    // 0  1  0
    // 1 -4  1ExtractMomentsFeatures
    // 0  1  0
    unsigned int x=0;
    unsigned int y=0;
    unsigned int yshift=0;
    unsigned int yshift_plus=0;
    unsigned int yshift_minus=0;
    unsigned cnt=0;

    for (y = roiY + 1; y < roiY + roiHeight - 1; y+= downsampleFactor)
    {
        yshift=(y)*width;
        yshift_plus=(y + 1)*width;
        yshift_minus=(y - 1)*width;

        for (x = roiX + 1; x < roiX + roiWidth - 1; x+= downsampleFactor)
        {
            #ifdef _TEGRA
              asm ("PLD [%0, #0x80]"::"r" (&src[x + yshift]));
            #endif
            a0 = src[x + yshift];//y*width

            a2 = src[x+yshift_minus];//(y-1)*width

            a4 = src[(x + 1) + yshift];//(y)*width

            a6 = src[x + yshift_plus];//(y + 1)*width

            a8 = src[(x - 1) + yshift];//y*width
            a0_ = -(a0 << 2);

            value = (a0_ + a2 + a4 + a6 + a8);

            //---------------------------------------------------------
            //Values to estimate the moments
            value2 = value*value;
            sum += value;
            sq_sum2 += value2;
            sq_sum3 += value2*value;
            sq_sum4 += value2*value2;

            //---------------------------------------------------------
            cnt++;
        }
    }


    //The following formulae are for one pass calculation (reduction) of the
    //moments of the Laplacian. The formulae come from a Russian book
    //whose English translation is:
    //Mama, Papa I am a Microcalculator
    //Финк Л. Папа, мама, я и микрокалькулятор.
    n = cnt;
    mean = (double)sum / (double)n;
    double mean2 = mean*mean;
    double std = sqrt((double)sq_sum2 / (double)n - mean2);

    double inv_std = 1.0 / (std+0.000000001); //Avoid divison by zero without affecting results
    double inv_std4 = inv_std*inv_std*inv_std*inv_std;
   
    double mean3 = mean2*mean;
    double mean4 = mean2*mean2;

    //(c^-4)*((6 * ((a*b) ^ 2)) + (b ^ 4) + (a ^ 4)) + (-4 * (a ^ 3)*b*(c^-4)) + (-4 * a*(b ^ 3)*(c^-4))
    kurtosis = ((inv_std4*(6.0 * mean2*sq_sum2 + mean4 + sq_sum4))) + ((-4.0*inv_std4 * mean))*sq_sum3 + ((-4.0 * mean3*inv_std4))*sum;
    kurtosis = (kurtosis / (double)n) - 3;

    variance = ((double)sq_sum2 / (double)n - mean2);
}

//-----------------------------------------------------------------------------------------------------
void FeatureExtraction::Laplacian(const unsigned char *src, int *dst, uint width, uint height)
{
    int a0, a2, a4, a6, a8, a0_,value;

    for (uint y = 1; y < height - 1; y++)
    {
        for (uint x = 1; x < width - 1; x++)
        {
            a0 = src[x + y*width];
            a4 = src[(x + 1) + (y)*width];
            a8 = src[(x - 1) + (y)*width];
            a2 = src[(x)+(y - 1)*width];
            a6 = src[(x)+(y + 1)*width];

            a0_ = -(a0 << 2);
            value = (a0_ + a2 + a4 + a6 + a8);
            dst[x + y*width] = value;
        }
    }
}
//-----------------------------------------------------------------------------------------------------
void FeatureExtraction::UpdateMomentsFeatures(const unsigned char * image, uint width, uint height, uint cellIdx, uint numCellsX, uint numCellsY, uint roiWidth, uint roiHeight, uint roiX, uint roiY, uint downsampleFactor)
{
    uint cellxSize, cellySize;
    cellxSize = roiWidth / numCellsX;
    cellySize = roiHeight / numCellsY;

    double mean = 0.0;
    double variance = 0.0;
    double kurtosis = 0.0;

    int cellx = cellIdx % numCellsX;
    int celly = (cellIdx / numCellsX);
    roiX = cellx*cellxSize + roiX;
    roiY = celly*cellySize + roiY;

    ExtractMomentsFeatures(image, mean, variance, kurtosis, width, height, roiX, roiY, cellxSize, cellySize, downsampleFactor);

    _cellsFeatures[MEANIDX] = mean;
    _cellsFeatures[VARIANCEIDX] = variance;
    _cellsFeatures[KURTOSISIDX] = kurtosis;
}
//-----------------------------------------------------------------------------------------------------
cv::Mat FeatureExtraction::GetFeatures()
{
    cv::Mat pattern(1, _featuresSize, CV_32FC1);
    for (int i = 0; i < _featuresSize; i++)
    {
        pattern.at <float>(0, i) = (float)_cellsFeatures[i];
    }

    return pattern;
}
