/**
* @file CellStates.cpp
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

//#include "../XMLConfigSoilDetection.h"

#ifdef _USE_OPENMP
#include "omp.h"
#endif

#include <iostream>
#include <fstream>
#include "CellsStates.h"
#include <CXmlParser.h>
using namespace xmlparser;
using namespace std;
enum { CELL_HISTOGRAM_SIZE = 3, OVERALL_HISTOGRAM_SIZE = 5, DIRTYHISTIDX = 2};

//-----------------------------------------------------------------------------------------------------
void CellsStates::Init(std::string svmFile, uint width, uint height, SoilDetectionParameters params)
{
    _params = params;
    _width = width;
    _height = height;
    _numCells = _params.NumCols * _params.NumRows;

    uint numCells = _params.NumRows * _params.NumCols;

    _cellStates = new DiscreteROIState[numCells];
    _features = new FeatureExtraction[numCells];
    _temporalStateFilter = new TemporalFilter[numCells];

    for (uint i = 0; i < numCells; i++)
    	_temporalStateFilter[i].Init(params.TemporalFilterSize, OVERALL_HISTOGRAM_SIZE, params.HysteresisStateChangeThreshold);


    _temporalOverallFilter.Init(params.OverallTemporalFilterSize, OVERALL_HISTOGRAM_SIZE, params.HysteresisStateChangeThreshold);

    //Initialise Load the SVM Configuration File
   _cellClassification.Init(_params.NumRows, _params.NumCols, svmFile);


    _initalised = true;
}
//-----------------------------------------------------------------------------------------------------
void CellsStates::UpdateCellStates(const uint8_t* image)
{
    cv::Mat pattern;
    DiscreteROIState state = DiscreteROIState::Clean;
    //SVM Methods
    if (_params.ClassifierType == MethodType::SVM || _params.ClassifierType == MethodType::SVMExt || _params.ClassifierType == MethodType::SVMExtNoPartial)
    {
         #ifndef _USE_OPENMP

         for(unsigned int i = 0;i < _params.NumCols * _params.NumRows; i++)
         {
             _features[i].UpdateMomentsFeatures(image, _width, _height, i, _params.NumCols, _params.NumRows, _params.ROIWidth, _params.ROIHeight, _params.ROIX, _params.ROIY, _params.ImageDownsampleFactor);
         }
         #else

            #ifdef _LINUX
            //Microsoft Visual Studio 2013 only supports OMP up to version 2.0
            //We use version 3.0 features below therefore we only allow compilation under Linux
            //Update all the cells
            //OMP Version. It is not possible to insert this into a loop (for) because the compiler
            //throw an error. This version is only for 3x3 Cells version
            int i=0;

            #pragma omp parallel num_threads(2)
            #pragma omp single
            {
                for(i=0;i<this->_numCells;i++)
                {
                    #pragma omp task firstprivate(i)
                    {
                        _features[i].UpdateMomentsFeatures(image, _width, _height, i, _params.NumCols, _params.NumRows, _params.ROIWidth, _params.ROIHeight, _params.ROIX, _params.ROIY, _params.ImageDownsampleFactor);
                    }
                }
            }
            //#else
            //#error "OpenMP not supported with MS compiler."
            #endif
            
        #endif

        //Generate Pattern & Classify    
        for (unsigned int i = 0; i < _numCells; i++)
        {

            pattern = _cellClassification.GeneratePattern(_features, i, _params.ClassifierType);
            state = _cellClassification.Classify(pattern);

            //Temporal cell filter
            _temporalStateFilter[i].UpdateCellFilter(state);
            _cellStates[i] = _temporalStateFilter[i].BestCellCandidate();

        }

        //Temporal overall filter
        state=estimateOverallState();
        _temporalOverallFilter.UpdateCellFilter(state);
        _overallState=_temporalOverallFilter.BestCellCandidate();
    }
}
//-----------------------------------------------------------------------------------------------------
DiscreteROIState CellsStates::GetCellState(int idx) const
{
    assert(idx < (int)_numCells);
    return _cellStates[idx];
}

CellsStates::~CellsStates()
{
    if (_initalised)
    {
        delete[] _cellStates;
        delete[] _features;
        delete[] _temporalStateFilter;
    }
}

//-----------------------------------------------------------------------------------------------------
DiscreteROIState CellsStates::estimateOverallState() const
{

    DiscreteROIState state;
    unsigned int histogram[OVERALL_HISTOGRAM_SIZE];
    unsigned int unknownCnt = 0;
    float percentageDirty;
    float percentageUnknown;
    for (int i = 0; i < OVERALL_HISTOGRAM_SIZE; i++)
        histogram[i] = 0;

    for (unsigned int idx = 0; idx < _numCells; idx++)
    {
        state = GetCellState(idx);
        if (state != DiscreteROIState::Unknown)
            histogram[(int)state]++;

        else 
            unknownCnt++;
    }

    if (unknownCnt != _numCells)
        percentageDirty = (float)(100U * histogram[DIRTYHISTIDX]) / _numCells;

    else
        percentageDirty = 0.0f;

    percentageUnknown = (float)(100U * unknownCnt) / _numCells;


    if (percentageDirty < MAXCLEANVALUE)
        state = DiscreteROIState::Clean;
    else if ((percentageDirty >= MAXCLEANVALUE) && (percentageDirty < MAXPARTIALDIRTYVALUE))
        state = DiscreteROIState::PartiallyDirty;
    else if ((percentageDirty >= MAXPARTIALDIRTYVALUE) && (percentageDirty < MAXDIRTYVALUE))
        state = DiscreteROIState::Dirty;
    else if (percentageDirty >= MAXDIRTYVALUE)
        state = DiscreteROIState::Occluded;

    if (percentageUnknown > MINUNKNOWNVALUE)
        return DiscreteROIState::Unknown;
    else
       return state;
}
//-----------------------------------------------------------------------------------------------------
void CellsStates::CleanTemporalFilter()
{
    for (uint i = 0; i < _numCells; i++)
    {
        _temporalStateFilter[i].Init(_params.TemporalFilterSize, CELL_HISTOGRAM_SIZE, _params.HysteresisStateChangeThreshold);
    }
    _temporalOverallFilter.Init(_params.OverallTemporalFilterSize, OVERALL_HISTOGRAM_SIZE, _params.HysteresisStateChangeThreshold);
}

