/*
 * SoilDetectorLib.c
 *
 *  Created on: Nov 10, 2015
 *      Author: Adasens
 */
#include "SoilDetectorLib.h"
#include "CellsStates.h"

bool _created=false;
unsigned int _numCameras;
CellsStates * _cellStates; //Soil Detector Class
//--------------------------------------------------------
void InitSoilDetector(SoilDetectionParameters params,unsigned int camWidth, unsigned int camHeight,unsigned int numCameras, const char** svmPath)
{
	  std::string svmFile;

	  //Load the training file
	  if (params.ClassifierType==MethodType::SVMExt)
	  {
	      svmFile = svmPath[1];

	  }
	  else if (params.ClassifierType==MethodType::SVMExtNoPartial)
	  {
	      svmFile = svmPath[2];

	  }
	  else if (params.ClassifierType==MethodType::SVM)
	  {
	      svmFile = svmPath[0];

	  }
	  else
	  {
	      svmFile = svmPath[2];

	  }

	  if (!_created)
	  {
		  _cellStates = new CellsStates[numCameras];
	  }
	  else
	  {
		  delete [] _cellStates;
		  _cellStates = new CellsStates[numCameras];
	  }
	  for (unsigned int i=0;i<numCameras;i++)
	  	  _cellStates[i].Init(svmFile, camWidth, camHeight, params);

	  _numCameras=numCameras;
	  _created=true;

}
//--------------------------------------------------------
void UpdateSoilDetector(const uint8_t* image,unsigned int cameraIndex)
{
	if (cameraIndex<_numCameras)
    	_cellStates[cameraIndex].UpdateCellStates(image);
}
//--------------------------------------------------------
unsigned int GetGlobalStateSoilDetector(unsigned int cameraIndex)
{
	if (cameraIndex<_numCameras)
	    return (unsigned int)_cellStates[cameraIndex].GetOverAllState();
	else return (unsigned int)DiscreteROIState::Unknown;
}
//--------------------------------------------------------
unsigned int GetLocalStateSoilDetector(unsigned int idx,unsigned int cameraIndex)
{
	if (cameraIndex<_numCameras)
	{
		if (idx<_cellStates[cameraIndex].GetNumCellsX()*_cellStates[cameraIndex].GetNumCellsY())
			return (unsigned int)_cellStates[cameraIndex].GetCellState(idx);
		else return (unsigned int)DiscreteROIState::Unknown;
	}
	else return (unsigned int)DiscreteROIState::Unknown;
}
//--------------------------------------------------------
unsigned int GetNumCellsXSoilDetector(unsigned int cameraIndex)
{
	if (cameraIndex<_numCameras)
		return _cellStates[cameraIndex].GetNumCellsX();
	else return 0;
}
//--------------------------------------------------------
unsigned int GetNumCellsYSoilDetector(unsigned int cameraIndex)
{
	if (cameraIndex<_numCameras)
		return _cellStates[cameraIndex].GetNumCellsY();
	else return 0;
}
//--------------------------------------------------------
unsigned int GetROIWidthSoilDetector(unsigned int cameraIndex)
{
	if (cameraIndex<_numCameras)
		return _cellStates[cameraIndex].GetROIwidth();
	else return 0;
}
//--------------------------------------------------------
unsigned int GetROIHeightSoilDetector(unsigned int cameraIndex)
{
	if (cameraIndex<_numCameras)
    	return _cellStates[cameraIndex].GetROIheight();
	else return 0;
}
//--------------------------------------------------------
