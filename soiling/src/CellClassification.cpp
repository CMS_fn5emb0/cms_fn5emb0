/**
* @file CellClassification.cpp
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#include "CellClassification.h"

enum { NUMNEIGHBORS = 4};

void CellClassification::Init(unsigned int numRows, unsigned int numCols, std::string svmFile)
{
    _numRows = numRows;
    _numCols = numCols;
    _svm.load(svmFile.c_str());
    _initialised = true;
}

//-----------------------------------------------------------------------------------------------------
cv::Mat CellClassification::GeneratePattern(FeatureExtraction* features, uint numCell, MethodType methodType) const
{
    cv::Mat pattern;
    switch (methodType) //SVM, SVMExt, SVMExtNoPartial
    {
    case MethodType::SVM: 
        pattern = features[numCell].GetFeatures();
        break;
    case MethodType::SVMExt: 
        pattern = generateSVMExtPattern(features, numCell);
        break;
    case MethodType::SVMExtNoPartial: 
        pattern = generateSVMExtPattern(features, numCell);
        break;
    default:
        break;
    }
    return pattern;

}
//-----------------------------------------------------------------------------------------------------
cv::Mat CellClassification::generateSVMExtPattern(FeatureExtraction* features, uint numCell) const
{
    assert(features != nullptr);
    assert(_initialised);

    cv::Mat localpattern = features[numCell].GetFeatures();
    cv::Mat rowPatternNeighbor(1, localpattern.cols*NUMNEIGHBORS, CV_32FC1);

    if (!_initialised)
        return rowPatternNeighbor;

    cv::Mat rowPattern[NUMNEIGHBORS];
    int cnt = 0;

    localpattern.copyTo(rowPattern[0]);

    unsigned int cellx = numCell % _numRows;
    unsigned int celly = (numCell / _numCols);

    //Cells Index
    // 0 1 2
    // 3 4 5
    // 6 7 8
    if ((cellx==0) && (celly==0)) //(0)
    {
        rowPattern[1] = features[numCell + 1].GetFeatures();
        rowPattern[2] = features[numCell + _numCols].GetFeatures();
        rowPattern[3] = features[numCell + _numCols+1].GetFeatures();
    }
    else if ((cellx>0) && (celly==0) && (cellx<_numCols-1)) //(1)
    {
        rowPattern[1] = features[numCell - 1].GetFeatures();
        rowPattern[3] = features[numCell + _numCols].GetFeatures();
        rowPattern[2] = features[numCell + 1].GetFeatures();
    }
    else if ((cellx==_numCols-1) && (celly==0))//(2)
    {
        rowPattern[1] = features[numCell - 1].GetFeatures();
        rowPattern[2] = features[numCell + _numCols-1].GetFeatures();
        rowPattern[3] = features[numCell + _numCols].GetFeatures();
    }
    else if ((cellx==0) && (celly>0) && (celly<_numRows - 1))//(3)
    {
        rowPattern[1] = features[numCell - _numCols].GetFeatures();
        rowPattern[2] = features[numCell + 1].GetFeatures();
        rowPattern[3] = features[numCell + _numCols].GetFeatures();
    }
    else if ((cellx==_numCols-1) && (celly>0) && (celly<_numRows -1))//(5)
    {
        rowPattern[1] = features[numCell - _numCols].GetFeatures();
        rowPattern[2] = features[numCell - 1].GetFeatures();
        rowPattern[3] = features[numCell + _numCols].GetFeatures();
    }
    else if ((cellx==0) && (celly==_numRows-1))//(6)
    {
        rowPattern[1] = features[numCell - _numCols].GetFeatures();
        rowPattern[2] = features[numCell - _numCols+1].GetFeatures();
        rowPattern[3] = features[numCell + 1].GetFeatures();
    }
    else if ((celly==_numRows-1) && (cellx>0) && (cellx<_numCols-1))//(7)
    {
        rowPattern[1] = features[numCell - 1].GetFeatures();
        rowPattern[2] = features[numCell - _numCols].GetFeatures();
        rowPattern[3] = features[numCell + 1].GetFeatures();
    }
    else if((cellx==_numCols-1) && (celly==_numRows-1))//(8)
    {
        rowPattern[1] = features[numCell - 1].GetFeatures();
        rowPattern[2] = features[numCell - _numCols-1].GetFeatures();
        rowPattern[3] = features[numCell - _numCols].GetFeatures();
    }
    else //(4)
    {
        rowPattern[1] = features[numCell - 1].GetFeatures();
        rowPattern[2] = features[numCell - _numCols].GetFeatures();
        rowPattern[3] = features[numCell + 1].GetFeatures();
    }

    for (int i = 0; i < NUMNEIGHBORS; i++)
    {
        rowPatternNeighbor.at<float>(0, cnt) = rowPattern[i].at<float>(0, 0);
        rowPatternNeighbor.at<float>(0, cnt + 1) = rowPattern[i].at<float>(0, 1);
        rowPatternNeighbor.at<float>(0, cnt + 2) = rowPattern[i].at<float>(0, 2);
        cnt += 3;
    }

    return rowPatternNeighbor;
}
//-----------------------------------------------------------------------------------------------------
DiscreteROIState CellClassification::Classify(cv::Mat pattern) const
{
    DiscreteROIState result;

    result = static_cast<DiscreteROIState>((int)_svm.predict(pattern));

    return result;
}
