/**
* @file TemporalFilter.cpp
*
* @par          Project: FICOCCLUV2
* @copyright    2015 ADASENS Automotive Innovation GmbH
* @par          Language: C++11
*
*/

#include "TemporalFilter.h"
#include "defines.h"

//--------------------------------------------------------------
TemporalFilter::TemporalFilter() : _stateChangeThreshold(0.8), _oldCellState(DiscreteROIState::Unknown)
{
    _queueCellFilled = false;
    _queueCellState.empty();
    _temporalSize = 0U;
    _temporalCnt = 0U;
}
//--------------------------------------------------------------
void TemporalFilter::Init(unsigned int temporalSize, unsigned int histogramSize, double hysteresisStateChangeThreshold)
{
    _histogramSize=histogramSize;
    _temporalCnt = 0U;
    _temporalSize = temporalSize;
    _stateChangeThreshold = hysteresisStateChangeThreshold;

    _histogram=new unsigned int[_histogramSize];

    for (unsigned int i = 0; i < _histogramSize; i++)
    {
        _histogram[i] = 0U;
    }
    _queueCellFilled = false;
}
//--------------------------------------------------------------
void TemporalFilter::UpdateCellFilter(DiscreteROIState state)
{
    if (state != DiscreteROIState::Unknown)
    {
        if (_temporalSize > _temporalCnt)
        {
            _queueCellState.push(state);
            _temporalCnt++;
            _histogram[(int)state]++;
        }
        else
        {
            DiscreteROIState oldState = _queueCellState.front();
            _queueCellState.pop();
            _queueCellState.push(state);
            _histogram[(int)oldState]--;
            _histogram[(int)state]++;
        }
    }
}
//--------------------------------------------------------------
DiscreteROIState TemporalFilter::BestCellCandidate()
{
    int idxMax = 0;
    int maxi = -1;
    int value;

    DiscreteROIState newState;
    if (_temporalSize <= _temporalCnt)
    {
        for (unsigned int i = 0; i < _histogramSize; i++)
        {
            value = _histogram[i];
            if (value > maxi)
            {
                idxMax = i;
                maxi = value;
            }
        }

        //Only change state if the state bin with the largest count has
        //at least a certain percentage of the total number of samples
        if (maxi > (_temporalSize * _stateChangeThreshold))
        {
            //Change state
            newState = (DiscreteROIState)idxMax;
            _oldCellState = newState;
        }
        else
        {
            newState = _oldCellState;
        }

        return newState;
    }
    else 
        return DiscreteROIState::Unknown;

}
//--------------------------------------------------------------
void TemporalFilter::Clean()
{
    _queueCellFilled = false;
    _queueCellState.empty();
    _oldCellState = DiscreteROIState::Unknown;
}
//--------------------------------------------------------------
TemporalFilter::~TemporalFilter()
{
    delete [] _histogram;
}
//--------------------------------------------------------------
