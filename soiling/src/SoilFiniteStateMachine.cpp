/*
 * SoilFiniteStateMachine.c
 *
 *  Created on: Aug 18, 2016
 *      Author: ficoadas
 */

#include "SoilFiniteStateMachine.h"
#include "CWrapper.h"
#include <CXmlParser.h>
using namespace xmlparser;


SoilFiniteStateMachine::SoilFiniteStateMachine(int _imgWidth, int _imgHeight, int nCameras, int fps, int camFrameRate, const char** _svmPath)
	:FiniteStateMachineTemplate(){

	imgWidth = _imgWidth;
	imgHeight = _imgHeight;
	camIndex = 0;
	numCameras = nCameras;

	soilFps = fps;
	framesAcquired = 0;
	cameraFrameRate = camFrameRate;

	for (int i = 0; i < 3; i++){
		svmPath[i] = _svmPath[i];
	}

	lastsoilState = Soil_State::STATE_CLEAN;

	//Create objs to set the configurations
	SoilDetectionParameters params;
	CConfigLoader appConfig;

	//Set the soil detection parameters
	params.ImageDownsampleFactor=appConfig.downsampleFactor;
	params.NumCols=appConfig.cols;
	params.NumRows=appConfig.rows;
	params.OverallTemporalFilterSize=appConfig.overallTemporalFilter;
	params.ROIHeight=appConfig.roiHeight;
	params.ROIWidth=appConfig.roiWidth;
	params.ROIX=appConfig.roiX;
	params.ROIY=appConfig.roiY;
	params.TemporalFilterSize=appConfig.temporalFilter;
	params.HysteresisStateChangeThreshold=appConfig.hysteresisThreshold;

	//Load the training file, check the classifier format
	if (appConfig.classifier=="SVMExt")
	{
	  params.ClassifierType=MethodType::SVMExt;
	}
	else if (appConfig.classifier=="SVMExtNoPartial")
	{
	  params.ClassifierType=MethodType::SVMExtNoPartial;
	  cout << "SVMExtNoPartial matches" << endl;
	}
	else if (appConfig.classifier=="SVM")
	{
	  params.ClassifierType=MethodType::SVM;
	}
	else
	{
	  params.ClassifierType = MethodType::SVMExtNoPartial;
	  cout << "SVMExtNoPartial default" << endl;
	}

	//camera FPS is set for 60
	InitSoilDetector(params,imgWidth, imgHeight, numCameras, svmPath);
}

SoilFiniteStateMachine::~SoilFiniteStateMachine() {
	// TODO Auto-generated destructor stub
}

void SoilFiniteStateMachine::SoilMessage() {

	MSG msgSoil;

	if(lastsoilState != soilState){
		msgSoil.sender = FSM::FSM_SOILING;
		msgSoil.receiver = FSM::FSM_HANDLE;
		msgSoil.msg = reinterpret_cast<void*>(soilState);

		if (outMsgQ->size() < POLICY){
			outMsgQ->push(msgSoil);
		}
		else{
			std::cout << "Queue Full" << std::endl;
		}
		lastsoilState = soilState;
	}

}

SoilFiniteStateMachine::State SoilFiniteStateMachine::ST_Stopped(){
	return State::STOPPED;
}

SoilFiniteStateMachine::State SoilFiniteStateMachine::ST_Initialized(){
	return State::INITIALIZED;
}

SoilFiniteStateMachine::State SoilFiniteStateMachine::ST_Run(){

	NvMediaImage *frame = nullptr;
	frame = getFrame();
	int mod;

	if (frame != nullptr){

		mod = framesAcquired%cameraFrameRate;
		if(mod < soilFps) {

			//Create matrix for the frame
			cv::Mat var = cv::Mat(4,5,CV_8UC1);

			//Global State of the camera
			DiscreteROIState state=DiscreteROIState::Unknown;

			// map captured frame to CPU space
			NvMediaImageSurfaceMap mapping;

			//lock the Nvmedia image
			NvMediaStatus status = NvMediaImageLock(frame, NVMEDIA_IMAGE_ACCESS_READ, &mapping);

			// Verify the input status and check the image format
			if(NVMEDIA_STATUS_OK == status)
			{
				//cout << "\tSize " << mapping.width << "X" << mapping.height << endl;
				// TYPE of IMAGE
				switch(mapping.type) {
				   case NvMediaSurfaceType_Image_RGBA:
						//cout << "\tType (RGBA)" << endl;
						break;
				   case NvMediaSurfaceType_Image_YUV_420:
						//cout << "\tType (YUV420)" << endl;
						break;
				   case NvMediaSurfaceType_Image_YUV_422:
						//cout << "\tType (YUV422)" << endl;
						break;
				   default:
						//cout << "\tType (XXXX)" << endl;
						break;
				}

				//creating  matrix for RGB image
				cv::Mat test = cv::Mat(mapping.height,mapping.width, CV_8UC4);
				test.data = reinterpret_cast<unsigned char*>(mapping.surface[0].mapping);

				//creating  matrix for GRAY image
				cv::Mat grayIm = cv::Mat(mapping.height,mapping.width, CV_8UC1);
				cv::cvtColor(test, grayIm, CV_RGBA2GRAY);

				//Send gray image data to update the soil information
				UpdateSoilDetector(grayIm.data,camIndex);

				// Done with the image, unmap it
				NvMediaImageUnlock(frame);

				//Get the state of camera state once the soil detection state is updated
				state = (DiscreteROIState)GetGlobalStateSoilDetector(camIndex);

				switch (state) {
					case DiscreteROIState::Clean:
						soilState = Soil_State::STATE_CLEAN;
						break;
					case DiscreteROIState::Dirty:
						soilState = Soil_State::STATE_DIRTY;
						break;
					case DiscreteROIState::Occluded:
						soilState = Soil_State::STATE_OCCLUDED;
						break;
					case DiscreteROIState::PartiallyDirty:
						soilState = Soil_State::STATE_PARTIALLY_DIRTY;
						break;
					case DiscreteROIState::Unknown:
						soilState = Soil_State::STATE_UNKNOWN;
						break;
					default:
						 break;
				}

				SoilMessage();

			}
		}

		framesAcquired++;
		if(framesAcquired >= cameraFrameRate)
			framesAcquired = 0;

		linkStages(frame);
	}else{
		//printf("%s : %s : Waiting new frame\n", __FILE__, __func__);
	}

	return State::RUN;
}

SoilFiniteStateMachine::State SoilFiniteStateMachine::ST_Failed(){
	return State::FAILED;
}

SoilFiniteStateMachine::State SoilFiniteStateMachine::ST_Stopping(){
	return State::STOPPED;
}

SoilFiniteStateMachine::State SoilFiniteStateMachine::ST_Initializing(){
	// TODO[2] include cam config
	return State::INITIALIZED;
}

SoilFiniteStateMachine::State SoilFiniteStateMachine::ST_Running(){
	return State::RUN;
}

SoilFiniteStateMachine::State SoilFiniteStateMachine::ST_Failing(){
	return State::FAILED;
}
