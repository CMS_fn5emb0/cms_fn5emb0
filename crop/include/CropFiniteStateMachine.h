/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file CropFiniteStateMachine.h
*  @brief Crop Module
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef CROPFINTIESTATEMACHINE_H_
#define CROPFINTIESTATEMACHINE_H_
#include "FiniteStateMachineTemplate.h"
#include "nvmedia.h"
#include "capture2d.h"
#include <iostream>

/** @class ModuleTemplate
 *  @details This class incorporates the functionalities to
 *  		 release memory of the capture module
 */

class CropFiniteStateMachine: public FiniteStateMachineTemplate {
public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	CropFiniteStateMachine(int16_t _cropROIX0, int16_t _cropROIY0, int16_t _cropHeight, int16_t _cropWidth,
							NvMedia2D *_engine2d, NvMediaDevice *_hardwareHandle);

	/** @brief Free the resources generated for the object
	 */
	virtual ~CropFiniteStateMachine();

private:


	int cropROIX0;
	int cropROIY0;
	int cropHeight;
	int cropWidth;
	//boost::circular_buffer<NvMediaImage*> *videoOut;
	// boost::circular_buffer<NvMediaImage*>::iterator videoOutIt;

	NvMediaDevice *hardwareHandle;
	NvMedia2D *engine2d;
	NvMedia2DBlitParameters blitParams;
	NvMediaRect * srcRect;

	/** \brief This method enqueue the image to the video output buffer.
	 *
	 * @param NvMediaImage: Image we want to enqueue
	 *
	 * @return void
	 */
	//inline void enqueueOut(NvMediaImage* frame){videoOut->push_back(frame);}
	BufferPool *cropPool;


	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state fail to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* CROPFINTIESTATEMACHINE_H_ */
