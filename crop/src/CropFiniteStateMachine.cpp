/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file ModuleTemplate.cpp
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "CropFiniteStateMachine.h"

CropFiniteStateMachine::CropFiniteStateMachine(int16_t _cropROIX0, int16_t _cropROIY0, int16_t _cropHeight, int16_t _cropWidth,
						NvMedia2D *_engine2d, NvMediaDevice *_hardwareHandle)
	:FiniteStateMachineTemplate(){

	cropROIX0 = _cropROIX0;
	cropROIY0 = _cropROIY0;
	cropHeight = _cropHeight;
	cropWidth = _cropWidth;

	srcRect = new NvMediaRect;
	srcRect->x0 = cropROIX0;
	srcRect->y0 = cropROIY0;
	srcRect->x1 = cropWidth+cropROIX0;
	srcRect->y1 = cropHeight+cropROIY0;

	hardwareHandle=_hardwareHandle;
	engine2d=_engine2d;


	ImageBufferPoolConfig configBufferPool;

	memset(&configBufferPool, 0, sizeof(ImageBufferPoolConfig));
	configBufferPool.width = _cropWidth;
	configBufferPool.height = _cropHeight;
	configBufferPool.surfType = NvMediaSurfaceType_Image_RGBA;
	configBufferPool.surfAttributes = 0;
	configBufferPool.imageClass = NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE;
	configBufferPool.imagesCount = 1;
	configBufferPool.device = hardwareHandle;

	BufferPool_Create (&cropPool, BUFFER_SIZE, QUEUE_TIMEOUT, IMAGE_BUFFER_POOL, (void *)&configBufferPool);

	memset(&blitParams, 0, sizeof(blitParams));

	
}
CropFiniteStateMachine::~CropFiniteStateMachine() {
	// TODO Auto-generated destructor stub
}

CropFiniteStateMachine::State CropFiniteStateMachine::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
CropFiniteStateMachine::State CropFiniteStateMachine::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
CropFiniteStateMachine::State CropFiniteStateMachine::ST_Run(){
	//TODO[1] TO BE IMPLEMENTED
	NvMediaImage* frame = nullptr;
	ImageBuffer *cropBuffer = nullptr;
	frame = getFrame();
	if (frame != nullptr){
		BufferPool_AcquireBuffer(cropPool, (void **)&cropBuffer);
		NvMedia2DBlit(engine2d, cropBuffer->image, nullptr, frame, srcRect, &blitParams);
		linkStages(cropBuffer->image);
		ImageBuffer* imBuffer = (ImageBuffer*)frame->tag;
		BufferPool_ReleaseBuffer(imBuffer->bufferPool, imBuffer);
	}else{}
	return State::RUN;
}
CropFiniteStateMachine::State CropFiniteStateMachine::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

CropFiniteStateMachine::State CropFiniteStateMachine::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
CropFiniteStateMachine::State CropFiniteStateMachine::ST_Initializing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
CropFiniteStateMachine::State CropFiniteStateMachine::ST_Running(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
CropFiniteStateMachine::State CropFiniteStateMachine::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
