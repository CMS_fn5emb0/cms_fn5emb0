#ifndef WINDOW_X11_H
#define WINDOW_X11_H

#include "window.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include  <X11/Xlib.h>
#include  <X11/Xatom.h>
/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////
#include  <X11/Xutil.h>

class WindowX11 : public AbstractWindow
{
public:
    // create an X11 window
    //   width: width of window
    //   height: height of window
    //   robust: enable robustness mode of EGL
    WindowX11(int width, int height, bool robust = false);
    // release window
    ~WindowX11(void);
    // swap back and front buffers
    virtual void swapBuffers(void);
    // check if window was initialized successfully
    virtual bool isInitialized(void);
    // reset EGL context
    virtual void resetContext(void);
    // make calling thread current for the rendering context of this window
    virtual void makeCurrent(void);
    // get EGL display
    virtual EGLDisplay getEGLDisplay(void) { return display; }
    virtual EGLContext getEGLContext (void) { return context; }
    virtual EGLConfig getEGLConfig(void) { return config; }

private:
    // create X11 window
    //   width: width of window
    //   height: height of window
    void createWindow(int width, int height);
    // create EGL context
    //   robust: enable robustness mode
    void createEGLContext(bool robust);
    
    // X11
    Display *xDisplay;
    Window xWin;
    
    // EGL
    EGLDisplay display;
    EGLContext context;
    EGLSurface surface;
    EGLConfig config;
    
    // internal flags
    bool xInitialized;
    bool eglInitialized;
    bool robust;
};

#endif // WINDOW_X11_H
