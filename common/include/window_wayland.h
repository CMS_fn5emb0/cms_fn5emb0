/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_WAYLAND_H
#define WINDOW_WAYLAND_H
#include "window.h"

#include <wayland-client.h>
#include <wayland-egl.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>

class WindowWayland : public AbstractWindow
{
public:
    // create a wayland window
    //   width: width of window
    //   height: height of window
    //   robust: enable robustness mode of EGL
    WindowWayland(int width, int height, bool robust = false);
    // release window
    ~WindowWayland(void);
    // swap back and front buffers
    virtual void swapBuffers(void);
    // check if window was initialized successfully
    virtual bool isInitialized(void);
    // reset EGL context
    virtual void resetContext(void);
    // make calling thread current for the rendering context of this window
    virtual void makeCurrent(void);
    // get EGL display
    virtual EGLDisplay getEGLDisplay(void) { return display; }
    virtual EGLContext getEGLContext (void) { return context; }
    virtual EGLConfig getEGLConfig(void) { return config; }

private:
    // create wayland window
    //   width: width of window
    //   height: height of window
    void createWindow(int width, int height);
    // create EGL context
    //   robust: enable robustness mode
    void createEGLContext(bool robust);
    
    // EGL
    EGLDisplay display;
    EGLContext context;
    EGLSurface surface;
    EGLConfig config;
    
    // Wayland
    // required callbacks
    static void registryHandleGlobal(void *data, struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version);
    void registryHandleGlobal(struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version);
    
    static void registryHandleGlobalRemove(void *data, struct wl_registry *registry, uint32_t name);
    void registryHandleGlobalRemove(struct wl_registry *registry, uint32_t name);
    
    static void handlePing(void *data, struct wl_shell_surface *shell_surface, uint32_t serial);
    void handlePing(struct wl_shell_surface *shell_surface, uint32_t serial);
    
    static void handleConfigure(void *data, struct wl_shell_surface *shell_surface, uint32_t edges, int32_t width, int32_t height);
    void handleConfigure(struct wl_shell_surface *shell_surface, uint32_t edges, int32_t width, int32_t height);
    
    static void handlePopupDone(void *data, struct wl_shell_surface *shell_surface);
    void handlePopupDone(struct wl_shell_surface *shell_surface);
    
    wl_display* wDisplay;
    wl_registry* wRegistry;
    wl_compositor* wCompositor;
    wl_shell* wShell;
    wl_surface* wSurface;
    wl_shell_surface* wShellSurface;
    wl_egl_window* wEglWin;
    
    wl_registry_listener wRegistryListener;
    wl_shell_surface_listener wShellSurfaceListener;
    
    bool waylandInitialized;
    bool eglInitialized;
    bool robust;
};

#endif // WINDOW_WAYLAND_H
