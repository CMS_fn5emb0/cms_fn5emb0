/*****************************************************************************
 *
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 ****************************************************************************/

/*
 * capture2d.h
 *
 *  Last modification on: 17/08/2016
 *      Author: Miguel H. Rib <miguel.rib@ficosa.com>
 */


#ifndef CAPTURE2D_H
#define CAPTURE2D_H

#include <nvmedia_icp.h>
#include <nvmedia_isc.h>
#include <config_isc.h>
#include <nvmedia_2d.h>

class Capture2d
{
    public:
        enum Connection
        {
            CSI_AB,
            CSI_CD,
            CSI_EF
        };
        enum Board
        {
            JETSON_PRO_TK1,
            DRIVE_PX_TEGRA_A,
            DRIVE_PX_TEGRA_B,
        };
        enum BufferType
        {
            PITCH_LINEAR,
            BLOCK_LINEAR
        };
        enum ColorConversion
        {
            NONE,
            RGB
        };
        
        // create new instance
        //   device: NvMediaDevice to use
        //   connection: Which CSI port to use
        //   cameras: number of cameras
        //   buffers: how many capture buffers to use
        //   width: camera width
        //   height: camera height
        Capture2d(NvMediaDevice *device, Connection connection, 
            int cameras, int buffers, int width, int height,
            BufferType bufferType, ColorConversion colorConversion
            );
        // release instance
        ~Capture2d();
        // get next captured frame
        NvMediaImage* getNextFrame();
        // returns an image to buffer queue
        //   frame: image to return
        void returnFrame(NvMediaImage *frame);
        
        // get a single camera image
        //   img: aggregated frame
        //   index: camera to provide
        NvMediaImage* getSingleFrame(NvMediaImage* img, int index);
        // return a single camera image and last subimage returns whole frame, too
        //   img: image to return
        void returnSingleFrame(NvMediaImage *img);
        // get the parent frame of a single camera image
        //   img: frame to get parent image
        NvMediaImage* getParentFrame(NvMediaImage* img);

        ColorConversion getColorConversion() const { return colorConversion; }
        BufferType getBufferType() const { return bufferType; }
        NvMedia2D* getMediaEngine() { return engine2d; }

        // if enabled the timestamp for each frame retrieved through getNextFrame() will be overwritten by a system time.
        // @note/@todo - Please note that a timestamp retrieved through this API does not represent
        //               the actual timestamp when the frame was captured, i.e. retrieved from image sensor.
        //               If you are using the timestamp to synchronize the frames with some external event,
        //               you would need to calibrate for the temporal difference between these timestamps and the time source of your events.
        void enableOverwriteTimestamps() { overwriteTimestamps = true; }

        // if disabled - the timestamp provided by NvMedia will be used
        void disableOverwriteTimestamps() { overwriteTimestamps = false; }

        NvMediaGlobalTime getFirstFrameTimestamp() const { return firstFrameTimestamp; }

        // detect which board is used
        static enum Board getBoard();

    private:
        struct ImageSiblings
        {
            NvMediaImage** images;
            int references;
        };
        
        // initialise cameras
        //   connection: which CSI port to use
        //   cameras: number of cameras
        bool initialiseSensor(Connection connection, int cameras);
        // release cameras
        void deinitialiseSensors(void);

        // capture
        NvMediaICP *captureDevice;
        int cameras;
        int buffers;
        Connection connection;

        // sensors
        ConfigISCInfo iscConfigInfo;
        ConfigISCDevices isc;
        
        int width;
        int height;

        ColorConversion colorConversion;
        BufferType bufferType;

        NvMedia2D *engine2d;
        NvMedia2DBlitParameters blitParams;

        bool overwriteTimestamps;
        NvMediaGlobalTime firstFrameTimestamp;
        bool hasFirstFrame;
};

#endif // CAPTURE2D_H
