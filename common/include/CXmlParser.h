#ifndef _XMLPARSER_H_
#define _XMLPARSER_H_

#include "pugixml.hpp"
#include <iostream>

using namespace std;
namespace xmlparser {

    enum CameraConnection {
        CSI_AB = 0, CSI_CD, CSI_EF
    };
    //const int16_t SPEED_STEP_SIZE = 5;
}

using namespace pugi;

class CXmlParser {
	private:
		CXmlParser() :
			_configurationName(), _cameraConnectionName(),
			_connectionType(0), _cameraNumber(0), _cameraFrameRate(), _cropROIX0(0), _cropROIY0(0), _cropWidth(0), _cropHeight(0), _displayId(0), _displayWindow(0),
			_cameraHeight(0), _cameraWidth(0), _displayHeight(0), _displayWidth(0), _displayX0(0), _displayY0(0),
			_snapshotPrefix(),_snapshotFormat(), _recordingPrefix(), _isAesphericalEnabled(), _isOverlayEnabled(), _isSoilEnabled(), _soilFPS(), _SVM(), _SVMExt(), _SVMExtNoPartial(),
			_cleanPath(), _cleanOverlayPath(), _dirtyPath(), _dirtyOverlayPath(), _vertexshaderPath(), _fragmentshaderPath(), _isCropEnabled(), _overlayPath(),_isButtonsEnabled(), _backlightPath(), _daynightPath(),
			_srcWidth(0), _srcHeight(0), _srcROIX(0),_srcROIY(0), _srcROIWidth(0), _buttonsMin(0), _buttonsMax(0), _buttonsDay(0),_buttonsNight(0), _buttonsStep(0),
			_srcROIHeight(0), _srcROIScaleSEP(0), _dstWidth(0), _dstHeight(0), _dstROIX(0), _idoverlayPath(),_idcleanPath(), _iddirtyPath(), _idoverlayCleanPath(), _idoverlayDirtyPath(),
			_dstROIY(0), _dstROIWidth(0), _dstROIHeight(0), _dstROIScaleSEP(0),
			_scalingDirection(0), _pixelsPerPatchX(0),_pixelsPerPatchY(), _dottedLinesNum(), _dottedLinesThickness(), _isFishEyeCameraEnabled(), _numScaramuzzaCoeff(0), _centerX(), _centerY(), _c(), _d(), _e()// polyU(), polyV()
		{
		};

		CXmlParser(CXmlParser const& xmlParserInstance):
			_configurationName(), _cameraConnectionName(),
			_connectionType(0), _cameraNumber(0),_cameraFrameRate(), _cropROIX0(0), _cropROIY0(0), _cropWidth(0), _cropHeight(0), _displayId(0), _displayWindow	(0),
			_cameraHeight(0), _cameraWidth(0), _displayHeight(0), _displayWidth(0), _displayX0(0), _displayY0(0),
			_snapshotPrefix(), _snapshotFormat(), _recordingPrefix(),_isAesphericalEnabled(), _isOverlayEnabled(), _isSoilEnabled(), _soilFPS() , _SVM(), _SVMExt(), _SVMExtNoPartial(),
			_cleanPath(), _cleanOverlayPath(), _dirtyPath(), _dirtyOverlayPath(), _vertexshaderPath(), _fragmentshaderPath(), _isCropEnabled() , _overlayPath(),_isButtonsEnabled(), _backlightPath(), _daynightPath(),
			_srcWidth(0), _srcHeight(0), _srcROIX(0),_srcROIY(0), _srcROIWidth(0), _buttonsMin(0), _buttonsMax(0), _buttonsDay(0),_buttonsNight(0), _buttonsStep(0),
			_srcROIHeight(0), _srcROIScaleSEP(0), _dstWidth(0), _dstHeight(0), _dstROIX(0), _idoverlayPath(),_idcleanPath(), _iddirtyPath(), _idoverlayCleanPath(), _idoverlayDirtyPath(),
			_dstROIY(0), _dstROIWidth(0), _dstROIHeight(0), _dstROIScaleSEP(0),
			_scalingDirection(0), _pixelsPerPatchX(0),_pixelsPerPatchY(), _dottedLinesNum(), _dottedLinesThickness(), _isFishEyeCameraEnabled(), _numScaramuzzaCoeff(0), _centerX(), _centerY(), _c(), _d(), _e()//polyU(), polyV()
		{
		};

		//CXmlParser operator=(CXmlParser const& xmlParserInstance){};
		static CXmlParser* mptrXmlParserInstance;
		static bool mXmlParserInstanceFlag;

		char _configurationName;
		char* _cameraConnectionName;

		int16_t _connectionType;
		int16_t _cameraNumber;
		int16_t _cameraWidth;
		int16_t _cameraHeight;
		int16_t _cameraFrameRate;

		int16_t _cropROIX0;
		int16_t _cropROIY0;
		int16_t _cropWidth;
		int16_t _cropHeight;

		int16_t _displayId;
		int16_t _displayWindow;
		int16_t _displayHeight;
		int16_t _displayWidth;
		int16_t _displayX0;
		int16_t _displayY0;

		string _snapshotPrefix;
		string _snapshotFormat;
		string _recordingPrefix;

		bool _isButtonsEnabled;
		string _backlightPath;
		string _daynightPath;
		int16_t  _buttonsMax;
		int16_t  _buttonsMin;
		int16_t  _buttonsDay;
		int16_t  _buttonsNight;
		int16_t  _buttonsStep;

		int16_t _idoverlayPath;
		int16_t _idcleanPath;
		int16_t _iddirtyPath;
		int16_t _idoverlayCleanPath;
		int16_t _idoverlayDirtyPath;

		//soilin params
		bool _isSoilEnabled;
		string _SVM;
		string _SVMExt;
		string _SVMExtNoPartial;
		int16_t _soilFPS;

		bool _isAesphericalEnabled;
		bool _isOverlayEnabled;
	    bool  _isCropEnabled;
		string _overlayPath;
		string _cleanPath;
		string _dirtyPath;
		string _dirtyOverlayPath;
		string _cleanOverlayPath;
		string _vertexshaderPath;
		string _fragmentshaderPath;
		int16_t _srcWidth;
		int16_t _srcHeight;
		int16_t _srcROIX;
		int16_t _srcROIY;
		int16_t _srcROIWidth;
		int16_t _srcROIHeight;
		int16_t _srcROIScaleSEP;

		int16_t _dstWidth;
		int16_t _dstHeight;
		int16_t _dstROIX;
		int16_t _dstROIY;
		int16_t _dstROIWidth;
		int16_t _dstROIHeight;
		int16_t _dstROIScaleSEP;
		int16_t _scalingDirection;
		int16_t _pixelsPerPatchX;
		int16_t _pixelsPerPatchY;
		int16_t _dottedLinesNum;
		int16_t _dottedLinesThickness;

		float _polyU[10];
		float _polyV[10];


		bool 	_isFishEyeCameraEnabled;
		float 	_scaramuzzaCoeff[50];
		uint16_t _numScaramuzzaCoeff;
		float 	_centerX, _centerY, _c ,_d, _e;


		void deserializesubChildnodeVal(char* srcBuffer, float *value, uint16_t &count, const char* delim=",");
	public:

		static CXmlParser* getInstance(void);
		~CXmlParser(void);
		void parseXml(const char* xmlFile);

		static bool recordCommand;
		static bool isEnablekeyboard;
		static bool isEnableCan;
		static bool printSoilStatus;
		static bool changeOverlay;
		static const char* overlayxmlpath;
		bool getRecordCommand();
		void setRecordCommand(bool f);

		bool getRecordInputImg();
		void setRecordInputImg(bool f);

		char getConfigurationName(void) const;
		void setConfigurationName(char);

		int16_t getCameraConnectionType(void) const;
		int16_t getCameraNumber (void ) const;
		int16_t getCameraWidth(void) const;
		int16_t getCameraHeight(void) const;
		int16_t getCameraFrameRate(void) const;

		int16_t getOverlayPathId(void) const;
		int16_t getCleanPathId(void) const;
		int16_t getDirtyPathId(void) const;
		int16_t getOverlayCleanPathId(void) const;
		int16_t getOverlayDirtyPathId(void) const;

		bool getisSoilEnabled(void) const;

		string getsvmPath(void) const;
		string getsvmExtPath(void) const;
		string getsvmExtNoPartialPath(void) const;
		int16_t getSoilFPS(void) const;

		bool getisCropEnabled(void) const;
		int16_t getCropROIX0(void) const;
		int16_t getCropROIY0 (void ) const;
		int16_t getCropWidth(void) const;
		int16_t getCropHeight(void) const;

		int16_t getDisplayId(void) const;
		int16_t getDisplayWindow(void) const;
		int16_t getDisplayX0(void) const;
		int16_t getDisplayY0(void) const;
		int16_t getDisplayHeight(void) const;
		int16_t getDisplayWidth(void) const;

		bool getisButtonsEnabled(void) const;
		int16_t getButtonsMinTresholdBacklight(void) const;
		int16_t getButtonsMaxTresholdBacklight(void) const;
		int16_t getButtonsDayValueBacklight(void) const;
		int16_t getButtonsNightValueBacklight(void) const;
		int16_t getButtonsStepBacklight(void) const;
		string getButtonsBacklightPath(void) const;
		string getButtonsDayNightPath(void) const;

		string getSnapshotPrefix(void) const;
		string getSnapshotFormat(void) const;
		string getRecordingPrefix(void) const;

		bool getisOverlayEnabled(void) const;
		string getoverlayPath(void) const;
		string getCleanPath(void) const;
		string getDirtyPath(void) const;
		string getOccludedPath(void) const;
		string getPartiallyPath(void) const;
		string getDirtyOverlayPath(void) const;
		string getCleanOverlayPath(void) const;
		string getVertexshaderPath(void) const;
		string getFragmentshaderPath(void) const;

		bool getIsAesphericalEnabled(void) const;
		int16_t getSrcWidth(void) const;
		int16_t getSrcHeight(void) const;
		int16_t getsrcROIX(void) const;
		int16_t getsrcROIY(void) const;
		int16_t getsrcROIWidth(void) const;
		int16_t getsrcROIHeight(void) const;
		int16_t getsrcROIScaleSEP(void)const;

		int16_t getdstWidth(void) const;
		int16_t getdstHeight(void) const;
		int16_t getdstROIX(void) const;
		int16_t getdstROIY(void) const;
		int16_t getdstROIWidth(void) const;
		int16_t getdstROIHeight(void) const;
		int16_t getdstROIScaleSEP(void) const;
		int16_t getscalingDirection(void) const;

		int16_t getpixelsPerPatchX(void) const;
		int16_t getpixelsPerPatchY(void) const;

		int16_t getdottedLinesNum(void) const;
		int16_t getdottedLinesThickness(void) const;


		const float* getPolyU(void) const;
		const float* getPolyV(void) const;

		bool getIsFishEyeCameraEnabled(void) const;
		const float* getScaramuzzaCoeff(void) const;
		uint16_t getNumScaramuzzaCoeff(void) const;
		float getCenterX(void)const;
		float getCenterY(void)const;
		float getC(void)const;
		float getD(void)const;
		float getE(void)const;
	};

#endif
