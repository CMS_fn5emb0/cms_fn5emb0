/*****************************************************************************
 *
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 ****************************************************************************/

/*
 * capture2d.h
 *
 *  Last modification on: 17/08/2016
 *      Author: Miguel H. Rib <miguel.rib@ficosa.com>
 */


#ifndef WINDOW_H
#define WINDOW_H

#include <EGL/egl.h>

class AbstractWindow
{
public:
    enum WindowManager {
        X11,
        WAYLAND,
        EGLOUTPUT
    };
    
    static AbstractWindow* create(WindowManager wm, int width, int height, bool robust = false);
	static AbstractWindow* create(WindowManager wm, int displayId, int windowId, int width, int height, bool robust = false);
	
    // swap back and front buffers
    virtual void swapBuffers(void) = 0;
    // check if window was initialized successfully
    virtual bool isInitialized(void) = 0;
    // reset EGL context
    virtual void resetContext(void) = 0;
    // make calling thread current for the rendering context of this window
    virtual void makeCurrent(void) = 0;
    // get EGL display
    virtual EGLDisplay getEGLDisplay(void) = 0;
    virtual EGLContext getEGLContext(void) = 0;
    virtual EGLConfig getEGLConfig(void) = 0;

};

#endif // WINDOW_H
