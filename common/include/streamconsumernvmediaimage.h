/*****************************************************************************
 *
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 ****************************************************************************/

/*
 * capture2d.h
 *
 *  Last modification on: 17/08/2016
 *      Author: Miguel H. Rib <miguel.rib@ficosa.com>
 */


#ifndef STREAMCONSUMERNVMEDIA_H
#define STREAMCONSUMERNVMEDIA_H

#include "eglstream.h"
#include <nvmedia.h>
#include <nvmedia_eglstream.h>

class StreamConsumerNvMediaImage
{
public:
    // Create an NvMediaImage stream consumer
    //   display: EGL display to provide context
    //   device: NvMedia device to use
    //   stream: stream that the consumer should attach too
    StreamConsumerNvMediaImage(EGLDisplay display, NvMediaDevice* device, EGLStream &stream);

    // release consumer
    ~StreamConsumerNvMediaImage();
    
    // acquire image from the EGL stream. Return NULL if image was not acquired
    // Any acquired image has to be released through release() method.
    NvMediaImage* acquire();
    
    // @fv4mrp0 : acquire image from the EGL stream. Return NULL if image was not acquired
    // Any acquired image has to be released through release() method.
    NvMediaImage* acquire(unsigned int timeoutms);

    // release previously acquired image.
    void release(NvMediaImage* image);

private:

    EGLDisplay display;
    EGLStreamKHR stream;

    NvMediaEGLStreamConsumer *consumer;
};

#endif // STREAMCONSUMEROPENGL_H
