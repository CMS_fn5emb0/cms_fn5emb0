/********************************************************************
 *@file Logutils.hpp
 *@brief The file contains declaration of Logutils class
 *@author FICOSA-ADAS
 *@version 1.0
 *@date created 03-Jun-2016
 *******************************************************************/

#ifndef _LOG_UTILS_H_
#define _LOG_UTILS_H_

#include <stdarg.h>
#include <stdio.h>
#include<iostream>
using namespace std;
#include<string.h>





#define LINE_INFO       __FUNCTION__, __LINE__
#define LOG_DBG_(...)   Logutils::LogLevelMessage_(LEVEL_DBG_, LINE_INFO, __VA_ARGS__)
#define LOG_INFO_(...)   Logutils::LogLevelMessage_(LEVEL_INFO_, LINE_INFO, __VA_ARGS__)
#define LOG_WARN_(...)  Logutils::LogLevelMessage_(LEVEL_WARN_, LINE_INFO, __VA_ARGS__)
#define LOG_ERR_(...)   Logutils::LogLevelMessage_(LEVEL_ERR_, LINE_INFO, __VA_ARGS__)
#define LOG_MSG_(...)    Logutils::LogMessage_(__VA_ARGS__)

#define LOG_DISPLAY(...) Logutils::Log(__VA_ARGS__)
/*
 #define LOG(...) {\
    char str[100];\
    sprintf(str, __VA_ARGS__);\
    std::cout << "[" << __FILE__ << "][" << __FUNCTION__ << "][Line " << __LINE__ << "] " << str << std::endl;\
    }
 */
namespace logutils {
enum LogLevel_ {
	LEVEL_ERR_ = 0, LEVEL_WARN_ = 1, LEVEL_INFO_ = 2, LEVEL_DBG_ = 3,
};

enum LogStyle_ {
	LOG_STYLE_NORMAL_ = 0, LOG_STYLE_FUNCTION_LINE_
};

class Logutils {
public:
	Logutils() {
	}
	void SetLogLevel_(enum LogLevel_ level);
	void SetLogStyle_(enum LogStyle_ style);
	void SetLogFile_(FILE *logFileHandle);
	void static LogLevelMessage_(enum LogLevel_ level, const char *functionName,
			int lineNumber, const char *format, ...);
	void static LogMessage_(const char *format, ...);
	void static Log(const char *format, ...);

};
}
#endif /* _LOG_UTILS_H_ */

