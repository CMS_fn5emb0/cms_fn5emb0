/*****************************************************************************
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************/

/*
 * common_utils.h
 *
 *  Created on: 17/08/2015
 *      Author: Galder Unibaso <galder.unibaso@idneo.es>
 */

#ifndef __COMMON_UTILS_H__
#define __COMMON_UTILS_H__


#ifndef	_STDIO_H
#include <stdio.h>
#endif //_STDIO_H
#ifndef _STDBOOL_H
#include <stdbool.h>		// Boolean type and values
#endif //_STDBOOL_H
#ifndef _PTHREAD_H
#include <pthread.h>		// Thread management
#endif //_PTHREAD_H
#ifndef	_STDLIB_H
#include <stdlib.h>
#endif //_STDLIB_H

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------
// DEFINITONS
//----------------------------------------------------------------------------

#define FICOCOMMON_FILENAME_MAXSTRLEN			128		// Maximum name length (127+\0)
#define FICOCOMMON_CMD_LINE_MAXSTRLEN			512		// Maximum command length (511+\0)

#define FICOLINE_MAXSTRLEN						128		// Maximum line length
#define FICOLINE_EQUAL_DELIMITER				"="		// Equal delimiter
#define FICOLINE_SPACE_DELIMITER				" "		// White space delimiter
#define FICOLINE_RETURN_DELIMITER				"\n"	// Return delimiter

#define FICOTIME_MS_MAXSTRLEN					13		//HH:mm:ss.mmm\0
#define FICOTIME_US_MAXSTRLEN					16		//HH:mm:ss.uuuuuu\0
#define FICOTIME_NS_MAXSTRLEN					19		//HH:mm:ss.nnnnnnnnn\0

//----------------------------------------------------------------------------
// DATA TYPES PROTOTYPES
//----------------------------------------------------------------------------

//
// Mutexed boolean
//
typedef struct _ficopthread_mutexed_bool_t {
	bool value;				// Value
	pthread_mutex_t mutex;	// Mutex
} ficopthread_mutexed_bool_t;

//
// Mutexed u_int8_t
//
typedef struct _ficopthread_mutexed_uint8_t {
	u_int8_t value;			// Value
	pthread_mutex_t mutex;	// Mutex
} ficopthread_mutexed_uint8_t;

/*
//
// Mutexed double
//
typedef struct _ficopthread_mutexed_double_t {
	double value;			// Value
	pthread_mutex_t mutex;	// Mutex
} ficopthread_mutexed_double_t;
*/

/*
//
// One way list
//
typedef struct _ficolist_oneway_element_t {
	void *element;
	struct _ficolist_oneway_element_t *next;
} ficolist_oneway_element_t;

typedef struct _ficolist_mutexed_oneway_t {
	int num;
	ficolist_oneway_element_t *head;
	ficolist_oneway_element_t *tail;
	pthread_mutex_t mutex;
} ficolist_mutexed_oneway_t;
*/
//----------------------------------------------------------------------------
// FUNCTION PROTOTYPES
//----------------------------------------------------------------------------
void ficoprintf_uint8_hex(u_int8_t *frame, u_int8_t frame_len, FILE *log_fd);
void ficoprintf_uint_hex(u_int *frame, u_int frame_len, FILE *log_fd);
char *ficotime_now(void);
bool ficotime_now_ms(char **now_time_str);
bool ficotime_now_us(char **now_time_str);
bool ficotime_now_ns(char **now_time_str);
void ficostring_free(char **str);
bool ficostring_copy(char **to, char *from);
bool ficoparse_toupper(char **input);
bool ficoparse_tolower(char **input);
bool ficoparse_conf_param_value(char *ps_name, char *param, char *value, const char **params, char ***values, bool **is_set_values, bool **is_available_params, int max_param_num, FILE *log_fd);
int ficosocket_set_nonblocking(int sockfd);
bool ficosocket_close(int *sockfd, FILE *log_fd);
bool ficofile_exists(char *file_str);
bool ficofile_close(FILE **fd);
FILE *ficofile_open_read(char *file_str);
FILE *ficofile_open_write(char *file_str);
char *ficoline_get_param_with_delim(char *line, char *delim);
char *ficoline_get_value_with_delim(char *line, char *delim);
bool ficopthread_trylock_mutex(pthread_mutex_t *mutex);
bool ficopthread_trylock_mutex_block(pthread_mutex_t *mutex);
bool ficopthread_unlock_mutex(pthread_mutex_t *mutex);
bool ficopthread_unlock_mutex_block(pthread_mutex_t *mutex);
bool ficopthread_init_mutexed_bool_block(bool value, bool *mutexed_var, pthread_mutex_t *mutex);
bool ficopthread_set_mutexed_bool_block(bool value, bool *mutexed_var, pthread_mutex_t *mutex);
bool ficopthread_set_mutexed_bool_unblock(bool value, bool *mutexed_var, pthread_mutex_t *mutex);
//bool ficopthread_get_mutexed_bool_block(bool *value, bool *mutexed_var, pthread_mutex_t *mutex);
bool ficopthread_get_mutexed_bool_block(bool *value, bool mutexed_var, pthread_mutex_t *mutex);
bool ficopthread_wait_finish(pthread_t thread_id, void **thread_retval);
/*
void ficolist_oneway_list_element_free(ficolist_oneway_element_t **list_element);
bool ficolist_mutexed_oneway_list_clean_start(ficolist_mutexed_oneway_t **list, FILE *log_fd);
bool ficolist_mutexed_oneway_list_init(ficolist_mutexed_oneway_t **list, FILE *log_fd);
bool ficolist_mutexed_oneway_list_extract_head(void **element, ficolist_mutexed_oneway_t **list, FILE *log_fd);
int ficolist_mutexed_oneway_list_check_empty(ficolist_mutexed_oneway_t *list, FILE *log_fd);
bool ficolist_mutexed_oneway_list_append(void *element, ficolist_mutexed_oneway_t **list, FILE *log_fd);
bool ficolist_mutexed_oneway_list_delete_head(ficolist_mutexed_oneway_t **list, FILE *log_fd);
bool ficolist_mutexed_oneway_list_clean(ficolist_mutexed_oneway_t **list, FILE *log_fd);
bool ficolist_mutexed_oneway_list_free(ficolist_mutexed_oneway_t **list, FILE *log_fd);
*/
bool ficotimerfd_create(int *timer_fd);
bool ficotimerfd_destroy(int *timer_fd);
bool ficotimerfd_start(time_t sec, long nsec, time_t ival_sec, long ival_nsec, int timer_fd);
bool ficotimerfd_wait(int timer_fd);
bool ficotimerfd_stop(int timer_fd);

#ifdef __cplusplus
}
#endif

#else
#error "Multiple inclusions of common_utils.h"
#endif //__COMMON_UTILS_H__
