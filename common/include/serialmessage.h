/*
*  serialmessage.h
*
*  Created on: March 2, 2016
*      Author: FICOSA ADAS
*/

#ifndef SERIAL_MSG_HANDLER_H_
#define SERIAL_MSG_HANDLER_H_

// include files
#include <iostream>
#include <string.h>
#include <sstream>

// namespaces
using namespace std;

 /*
    Byte 1: 0x55 --> PRV Roof
            0xAA --> PRV Door

    Byte 2: 0x00 --> Soiling OFF
            0x01 --> Soiling ON

    Byte 3: 0x00 --> Camara Limpia
            0x01 --> Camara Sucia

    Byte 4: XOR of previous 3 bytes
*/

// message handler namespace
namespace SerialMsgHandler 
{

  // enum for all parameter that needs to be included in 
  // message string that needs to passed over CAN bus
  enum MSG_TYPE
  {
      PVR_POS=0,
      CAMTYPE,
      SOILSTATE,
      XOR,
      NONE
  };

  // enum for camera mount position
  enum PRV_POSITION
  {
      PRV_ROOF=0,
      PRV_DOOR
  };

  // enum for soil state, present or not present
  enum SOIL_STATE
  {
      SOIL_OFF=0,
      SOIL_ON
  };

  // camera steat dirty / clean
  enum CAMERA_TYPE
  {
      CAM_LIMPIA=0,
      CAM_SUCIA,
      CAM_UNKNOWN
  };

  /**
   * @brief 
   * @class CSerialMsgHandler
   *
  */
  class CSerialMsgHandler
  {

    private:
        std::stringstream _msgstring; //msg string
        unsigned int _prvPos;         //prv position
        unsigned int _camType;        //cam state
        unsigned int  _soilState;     //soil present / or not
        unsigned char _sendBytes[4];

    public:

        /**
        * @brief constructor initilises all the members to the default values
        *
        */
        CSerialMsgHandler();

        /**
        * @brief destructor 
        */
        ~CSerialMsgHandler()
        {
             ;
        }

        /**
        * @brief 
        *
        * @param  prvpos      This is camera mounting position either Roof or Door
        * @param  soilstate   This will intimate the soil state present on camera zero/one
        * @param  camType     This will gives the camera state clean or dirty
        *
        * @return nothing
        */
        void setSerialMsgHandlerParams( unsigned int prvpos, bool soilstate, unsigned int camType);

        /**
        * @brief 
        *
        * @param  prvpos      This is camera mounting position either Roof or Door
        *
        * @return nothing
        */
        void setPRVposition(unsigned int prvpos);

        /**
        * @brief 
        *
        * @param  soilstate This will intimate the soil state present on camera zero/one
        *
        * @return nothing
        */
        void setSoilStatus( bool soilstate);

        /**
        * @brief 
        *
        * @param  camType This will gives the camera state clean or dirty
        *
        * @return nothing
        */
        void setCameraType(unsigned int camType);

        /**
        * @brief 
        * @return nothing
        */
        void sendMessage();

        /**
        * @brief 
        * @return nothing
        */
        void printMessage();
  };

}/*end namespace SerialMsgHandler*/

#endif //SERIAL_MSG_HANDLER_H_
