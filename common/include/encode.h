#ifndef ENCODE_H
#define ENCODE_H

#include <nvmedia_image.h> //iep is not self contained...
#include <nvmedia_iep.h>
#include <nvmedia_2d.h>


class Encode
{
    public:
        struct DataChunk
        {
            unsigned int size; // buffer size
            unsigned int validData; // valid data in buffer
            char *data; // start of buffer
        };

        // Create an encoder object and configure it
        //   device: NvMedia device to create encoder instance
        //   width: width of video
        //   height: height of video
        //   bitrate: average bitrate of the video stream
        //   framerate: target framerate of the video stream
        Encode(NvMediaDevice *device, int width, int height, unsigned int bitrate = 12 * 1000 * 1000, unsigned int framerate = 30);
        // Release encoder object
        ~Encode();
        // encode a video frame
        //   frame: video frame to encode
        //   offset: which image to use in an aggregated image; for non-aggregated images set to 0
        void encode(NvMediaImage *frame, int offset);
        // get buffer with encoded video data
        DataChunk* getData();
        // return video data buffer
        void returnData(DataChunk *data);

        void createEncoder();
        void destroyEncoder();
        NvMediaIEP* getEncoder(){
        	return encoder;
        };

    private:
        NvMediaIEP *encoder;
        NvMedia2D *engine2d;
        NvMediaImage *conversion;
        NvMediaEncodeConfigH264 encodeConfig;
        NvMediaEncodeConfigH264VUIParams vuiParams;
        NvMediaEncodePicParamsH264 picParams;
        NvMedia2DBlitParameters blitParams;
        
        DataChunk *chunk;

        unsigned int bitrate;
        int width;
        int height;
};

#endif // ENCODE_H
