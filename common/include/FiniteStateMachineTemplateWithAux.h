/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file FiniteStateMachineTemplateWithAux.h
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef FiniteStateMachineTemplateWithAux_H_
#define FiniteStateMachineTemplateWithAux_H_
#include "FiniteStateMachineTemplate.h"
#include <iostream>

/** @class FiniteStateMachineTemplateWithAux
 *  @details This class incorporates the functionalities to
 *  		 release memory of the capture module
 */

class FiniteStateMachineTemplateWithAux: public FiniteStateMachineTemplate {

protected:

	/** @var auxVideoBuffer
	 *  	 Contains the auxiliar video frames that we will processed
	 *  	 in the state machine.
	 */
	//boost::circular_buffer<NvMediaImage*> *auxVideoBuffer;
	NvQueue *auxVideoBuffer;
	/** @brief This method links buffer with
	 *   the others buffers belonging to the following stages.
	 *
	 * @return void
	 */
	//void linkAuxStages();
	void linkAuxStages(NvMediaImage* frame);
public:

	/** @var msg
	 *  	 This struct type is used in order to know
	 *  	 which is the stored message by the sender.
	 */

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	FiniteStateMachineTemplateWithAux();

	/** @brief Free the resources generated for the object
	 */
	virtual ~FiniteStateMachineTemplateWithAux();

	/** @brief This method add the following stages for
	 * 		   to do interaction in the future.
	 * @param FiniteStateMachineTemplate : The new stage to add.
	 *
	 * @return void
	 */
	void addNextAuxStage(FiniteStateMachineTemplate* stage);

	/** \brief This method enqueue the image to the video buffer.
	 *
	 * @param NvMediaImage: Image we want to enqueue
	 *
	 * @return void
	 */
	void auxEnqueue(NvMediaImage* frame);

	/** @brief This method dequeues the first element of the buffer.
	 *
	 * @return void
	 */
//	inline void auxDequeue(){if (!auxVideoBuffer->empty()) auxVideoBuffer->pop_front();}

	/** @brief This method get the current frame to be processed.
	 *
	 * @return NvMediaImage
	 */
	NvMediaImage* getAuxFrame();
	inline int getNAuxStages(){return nAuxStages;}
private:

	int nAuxStages;
	bool trobat;

	/** @var nextStages
	 *  	 Contains all the next stages with
	 *  	 we will interact in the run time.
	 */
	std::list<FiniteStateMachineTemplate*> *nextAuxStages;



	/** @brief This method is the template to describe the behavior
	 *		   of the state stopped, after execution returns the
	 *		   current state.
	 *
	 * @return State
	 */
	virtual State ST_Stopped();

	/** @brief This method is the template to describe the behavior
	 *		   of the state initialized, after execution returns the
	 *		   current state.
	 *
	 * @return State
	 */
	virtual State ST_Initialized();

	/** @brief This method is the template to describe the behavior
	 *		   of the state run, after execution returns the
	 *		   current state.
	 *
	 * @return State
	 */
	virtual State ST_Run();

	/** @brief This method is the template to describe the behavior
	 *		   of the state failed, after execution returns the
	 *		   current state.
	 *
	 * @return State
	 */
	virtual State ST_Failed();

	/** @brief This method is the template to describe the behavior
	 *		   of the state transition to stop, after execution returns the
	 *		   current state.
	 *
	 * @return State
	*/
	virtual State ST_Stopping();

	/** @brief This method is the template to describe the behavior
	 *		   of the state transition to initialize, after execution returns the
	 *		   current state.
	 *
	 * @return State
	*/
	virtual State ST_Initializing();

	/** @brief This method is the template to describe the behavior
	 *		   of the state transition to run, after execution returns the
	 *		   current state.
	 *
	 * @return State
	*/
	virtual State ST_Running();

	/** @brief This method is the template to describe the behavior
	 *		   of the state transition to fail, after execution returns the
	 *		   current state.
	 *
	 * @return State
	*/
	virtual State ST_Failing();
};

#endif /* MODULETEMPLATE_H_ */
