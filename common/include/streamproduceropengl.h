/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////
#ifndef STREAMPRODUCEROPENGL_H
#define STREAMPRODUCEROPENGL_H

#include "eglstream.h"
#include <EGL/egl.h>
#include <GLES3/gl31.h>

#include <Logutils.hpp>
using namespace logutils;

class StreamProducerOpenGL
{
public:
    // create an OpenGL stream producer
    //   display: EGLDisplay to use
    //   stream: EGLStream to attach to
    //   tex: GL texture to be posted to the EGL stream
    //   target: Target type of the texture, i.e. GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_2D, etc.
    StreamProducerOpenGL(EGLDisplay display, EGLConfig config, EGLStream &stream, int width = 0, int height = 0);

    // make this producer the current buffer.
    // any consecutive rendering will render into the internal surface, which then can be posted to stream
    void enableCurrent();

    // deactivate this surface as being current one.
    // this will restore the old surface
    void disableCurrent();

    // post texture to the stream
    void postTexture();

    // release producer
    ~StreamProducerOpenGL();
    
private:
    // load required extensions
    void loadExtensions();

    EGLDisplay display;
    EGLStreamKHR stream;
    EGLSurface surface;
    EGLConfig config;
    //EGLContext context;
    int width;
    int height;
    //GLuint tex;
    //GLenum target;

    EGLContext currentContext;
    EGLSurface currentDrawSurface;
    EGLSurface currentReadSurface;

};

#endif // STREAMPRODUCERNVMEDIAIMAGE_H
