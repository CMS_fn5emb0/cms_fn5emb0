
#ifndef __COMMON_UTILS_H__
#include "common_utils.h"
#endif // __COMMON_UTILS_H__
#ifndef _SERIAL_UTILS_H_
#include "serial_utils.h"
#endif //_SERIAL_UTILS_H_

#ifndef _NET_IF_H
#include <net/if.h>
#endif //_NET_IF_H

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------
// DEFINITONS
//----------------------------------------------------------------------------


//
// SERIAL INTERFACE
//
#define SERIAL_CTRL_DEFAULT_SERIAL_DEV_STR				"/dev/ttyUSB0"						// Default serial device
#define SERIAL_CTRL_DEFAULT_SERIAL_TYPE_STR				"USB-RS232"							// Default serial type as sting
#define SERIAL_CTRL_DEFAULT_SERIAL_TYPE					FICOSERIAL_TYPE_USB_RS232			// Default serial type
#define SERIAL_CTRL_DEFAULT_SERIAL_BAUDRATE_STR			"57600"								// Default serial baud rate string
#define SERIAL_CTRL_DEFAULT_SERIAL_DATAPARITYSTOP_STR	"8N1"								// Default serial data/parity/stop as string
#define SERIAL_CTRL_DEFAULT_SERIAL_DATAPARITYSTOP		FICOSERIAL_DATAPARITYDATA_8N1		// Default serial data/parity/stop 
#define SERIAL_CTRL_DEFAULT_SERIAL_HW_FLOW_CTRL_STR		"No"								// Default serial HW flow control as string
#define SERIAL_CTRL_DEFAULT_SERIAL_SW_FLOW_CTRL_STR		"No"								// Default serial SW flow control as string
#define SERIAL_CTRL_DEFAULT_SERIAL_MAX_RTX_STR			"0"									// Default serial max. retransmissions as string
#define SERIAL_CTRL_DEFAULT_SERIAL_MAX_RTX				0									// Default serial max. retransmissions
	    
#define SERIAL_CTRL_CMP_PREFIX					"@ce61"								// Serial command fixed prefix
#define SERIAL_CTRL_CMD_MAXSTRLEN					8									// "@ce61XX\0"

#define SERIAL_APP_DEFAULT_CONF_FILE_STR	"serial_ctrl.conf"	// Default configuration file
#define SERIAL_APP_DEFAULT_LOG_FILE_STR		"serial_ctrl.log"	// Default log file

int ficosa_send_message(unsigned char * send_bytes, unsigned int length);

#ifdef __cplusplus
}
#endif
