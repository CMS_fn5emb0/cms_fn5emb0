/*****************************************************************************
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************/

/*
 * serial_utils.h
 *
 *  Created on: 25/08/2015
 *      Author: Galder Unibaso <galder.unibaso@idneo.es>
 */

#ifndef __SERIAL_UTILS_H__
#define __SERIAL_UTILS_H__

#ifndef	_STDIO_H
#include <stdio.h>
#endif //_STDIO_H
#ifndef _STDBOOL_H
#include <stdbool.h>		// Boolean type and values
#endif //_STDBOOL_H
#ifndef	_SYS_TYPES_H
#include <sys/types.h>
#endif	//_SYS_TYPES_H
#ifndef	_TERMIOS_H
#include <termios.h> 	// POSIX terminal control definitions
#endif //_TERMIOS_H

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------
// DEFINITONS
//----------------------------------------------------------------------------

#define FICOSERIAL_DEV_NAMESIZE					13		// Serial device name (12+\0)
#define FICOSERIAL_TYPE_MAXSTRLEN				10		// Serial type (9+\0)
#define FICOSERIAL_BAUDRATE_MAXSTRLEN			8		// Serial baud rate (7+\0)
#define FICOSERIAL_DATAPARITYSTOP_MAXSTRLEN		4		// Serial data/parity/stop (3+\0)

#define FICOSERIAL_FLOW_CTRL_OFF			0			// Serial HW/SW flow control: OFF
#define FICOSERIAL_FLOW_CTRL_ON				1			// Serial HW/SW flow control: ON

#define FICOSERIAL_RX_BUFFER_MAXSTRLEN		255			// Serial receive string buffer maximum length

#define FICOSERIAL_STX 		0x02
#define FICOSERIAL_ETX		0x03

#define FICOSERIAL_LF		0x0A
#define FICOSERIAL_CR		0x0D
#define FICOSERIAL_SPACE	0x20
#define FICOSERIAL_END		0x00

#define FICOSERIAL_DC1		0x11
#define FICOSERIAL_DC3 		0x13

//----------------------------------------------------------------------------
// DATA TYPES PROTOTYPES
//----------------------------------------------------------------------------

enum {
	FICOSERIAL_TYPE_RS232 = 0,
	FICOSERIAL_TYPE_USB,
	FICOSERIAL_TYPE_USB_RS232,
	FICOSERIAL_TYPE_END
};

enum {
	FICOSERIAL_DATAPARITYDATA_8N1 = 0,
	FICOSERIAL_DATAPARITYDATA_7E1,
	FICOSERIAL_DATAPARITYDATA_END
};

//----------------------------------------------------------------------------
// FUNCTION PROTOTYPES
//----------------------------------------------------------------------------
void ficoserial_fprintf_settings(char *dev_str, char *type_str, char *dataparitystop_str, bool flow_ctrl_hw, bool flow_ctrl_sw, struct termios *newtio, FILE *log_fd);
void ficoserial_fprintf_termios_hex(struct termios tio, FILE *log_fd);
bool ficoserial_check_type_str(char *type_str);
bool ficoserial_get_type_from_str(char *type_str, u_int8_t *type, FILE *log_fd);
bool ficoserial_check_baudrate_str(char *baudrate_str);
bool ficoserial_get_baudrate_from_str(char *baudrate_str, speed_t *baudrate, FILE *log_fd);
bool ficoserial_check_dataparitystop_str(char *dataparitystop_str);
bool ficoserial_get_dataparitystop_from_str(char *dataparitystop_str, u_int8_t *dataparitystop, FILE *log_fd);
bool ficoserial_set_config(int *serialfd, char *dev_str, char *type_str, char *baudrate_str, char *dataparitystop_str, bool flow_ctrl_hw, bool flow_ctrl_sw, struct termios *oldtio, struct termios *newtio, FILE *log_fd);
bool ficoserial_init_dev(char *dev_str, char *type_str, char *baudrate_str, char *dataparitystop_str, bool flow_ctrl_hw, bool flow_ctrl_sw, struct termios *oldtio, struct termios *newtio, FILE *log_fd);
bool ficoserial_connect(int *serialfd, char *dev_str, struct termios *currenttio, FILE *log_fd);
bool ficoserial_disconnect(int *serialfd, struct termios *oldtio, char *dev_str, FILE *log_fd);
bool ficoserial_write(int serialfd, u_int8_t *frame, u_int8_t frame_len, char *dev_str, FILE *log_fd);
bool ficoserial_write_str(int serialfd, char *frame_str, char *dev_str, FILE *log_fd);
bool ficoserial_read_nbytes(int serialfd, u_int8_t **read_frame, u_int8_t read_frame_len, char *dev_str,  FILE *log_fd);
bool ficoserial_read_check(int serialfd, u_int8_t *rep_frame, u_int8_t rep_frame_len, char *dev_str, FILE *log_fd);
bool ficoserial_read_line_str(int serialfd, char **read_line_str, char end_of_line, char *dev_str, FILE *log_fd);
bool ficoserial_read_check_str(int serialfd, char *rep_frame_str, char end_of_line, char *dev_str, FILE *log_fd);

#ifdef __cplusplus
}
#endif

#else
#error "Multiple inclusions of serial_utils.h"
#endif //__SERIAL_UTILS_H__
