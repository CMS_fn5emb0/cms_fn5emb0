/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef STREAMCONSUMEROPENGL_H
#define STREAMCONSUMEROPENGL_H

#include "eglstream.h"
#include "nvmedia_eglstream.h"

#include <EGL/egl.h>
#include <GLES3/gl31.h>

class StreamConsumerOpenGL
{
public:
    // Create an OpenGL stream consumer
    //   display: EGL display to provide context
    //   stream: stream that the consumer should attach too
    StreamConsumerOpenGL(EGLDisplay display, EGLStream &stream);
    // release consumer
    ~StreamConsumerOpenGL();
    // bind texture
    //   textureUnit: number of texture unit to use
    void bind(int textureUnit = 0);
    // remove binding of (any) texture
    //   textureUnit: texture unit to free (use -1 for current one)
    void unbind(int textureUnit = 0);
    // update texture
    //   textureUnit: texture unit to use
    bool acquire(int textureUnit = -1);
    // return texture
    //   textureUnit: texture unit to use
    bool release(int textureUnit = -1);
    // update and bind texture
    //   textureUnit: texture unit to use
    bool acquireAndBind(int textureUnit = 0);
    // return texture and remove texture binding
    //   textureUnit: texture unit to use
    bool releaseAndUnbind(int textureUnit = 0);

    GLuint getTexID() { return tex; };
    
private:
    // load required extensions
    void loadExtensions();

    EGLDisplay display;
    EGLStreamKHR stream;
    GLuint tex;
    GLenum type;
};

#endif // STREAMCONSUMEROPENGL_H
