/*****************************************************************************
 *
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 ****************************************************************************/

/*
 * capture2d.h
 *
 *  Last modification on: 17/08/2016
 *      Author: Miguel H. Rib <miguel.rib@ficosa.com>
 */


#ifndef WINDOW_EGLDEVICE_H
#define WINDOW_EGLDEVICE_H

#include "window.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>

class WindowEGL : public AbstractWindow
{
public:
    // create output based on EGLOutput
    // robust enables robustness extension of EGL
    WindowEGL(int width, int height, bool robust = false);
    WindowEGL(int displayId, int windowId, int width, int height, bool robust = false);
     
    // clean 
    ~WindowEGL(void);
    // present current buffer
    virtual void swapBuffers(void);
    // returns if window was initialized successfully
    virtual bool isInitialized(void);
    // recreate a valid EGL context
    virtual void resetContext(void);
    // make calling thread current for the rendering context of this window
    virtual void makeCurrent(void);
    // get EGL display
    virtual EGLDisplay getEGLDisplay(void) { return display; }
    virtual EGLContext getEGLContext (void) { return context; }
    virtual EGLConfig getEGLConfig(void) { return config; }

private:
    // initialize low-level aspects of display
    bool initDrm(const char *name, EGLAttrib *layerAttribs, int &dispWidth, int &dispHeight);
    bool initDrm(const char *name, int displayId, int windowId, EGLAttrib* layerAttribs, int &dispWidth, int &dispHeight);
    
    EGLDisplay display;
    EGLSurface surface;
    EGLContext context;
    EGLConfig config;
    EGLStreamKHR stream;
    bool initialized;
    bool robust;
};

#endif // WINDOW_EGLDEVICE_H
