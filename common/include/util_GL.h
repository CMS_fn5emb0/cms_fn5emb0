/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright © 2014-2015 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////


#ifndef __GLUTIL_H
#define __GLUTIL_H


#define SAFE_DELETE_ARRAY(a) if( (a) != NULL ) delete[] (a); (a) = NULL
#define STRINGIFY(s) #s

#define DEG2RAD(x)           ((x) * 0.01745329251994329575)
#define RAD2DEG(x)           ((x) * 57.29577951308232087721)

//#define _GLESMODE

// Include GL
#include <GLES3/gl3.h>
#define _GLESMODE

#ifdef _DEBUG
#define CHECK_GL_ERROR checkGLerror(__FILE__, __LINE__)
#else
#define CHECK_GL_ERROR
#endif

//#######################################################################################
// Error reporting
void checkGLerror(char* file, int line);

//#######################################################################################
// Shader compilation
void shaderAttachFromMemory(GLuint program, GLenum type, const char *source);
void shaderAttachFromFile (GLuint program, GLenum type, const char *filePath);

GLuint buildAndLinkProgramFromMemory(const char *VSfilePath, const char *PSfilePath);
GLuint buildAndLinkProgramFromFiles (const char *vsSource, const char *psSource);

#endif
