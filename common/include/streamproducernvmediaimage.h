/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2015 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////
#ifndef STREAMPRODUCERNVMEDIAIMAGE_H
#define STREAMPRODUCERNVMEDIAIMAGE_H

#include "eglstream.h"
#include <nvmedia.h>
#include <nvmedia_eglstream.h>

class StreamProducerNvMediaImage
{
public:
    // create an NvMediaImage stream producer
    //   display: EGLDisplay to use
    //   device: NvMedia device to use
    //   stream: EGLStream to attach to
    //   OPTIONAL: provide parameters to test support for format and size
    //   type: image format to use
    //   width: width of stream
    //   height: height of stream
    StreamProducerNvMediaImage(EGLDisplay display, NvMediaDevice* device, EGLStream &stream, 
        NvMediaSurfaceType type = NvMediaSurfaceType_Image_YUV_422, int width = 0, int height = 0);
    // release producer
    ~StreamProducerNvMediaImage();
    // post a frame to stream
    //   frame to post
    bool postFrame(NvMediaImage *frame);
    // get a returned frame from the stream
    //   timeoutms: maximal time in ms that function will for a frame to be returned
    NvMediaImage* getFrame(unsigned int timeoutms = NVMEDIA_EGL_STREAM_TIMEOUT_INFINITE);
    
private:
    NvMediaEGLStreamProducer *producer;
};

#endif // STREAMPRODUCERNVMEDIAIMAGE_H
