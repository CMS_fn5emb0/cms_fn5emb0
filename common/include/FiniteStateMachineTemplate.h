/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file FiniteStateMachineTemplate.h
*  @brief This file contains the definition of the class
*  		  FiniteStateMachineTemplate
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef FINITESTATEMACHINETEMPLATE_H_
#define FINITESTATEMACHINETEMPLATE_H_
#include <thread>
#include <list>
#include <nvmedia_image.h>
#include "buffer_utils.h"
#include "thread_utils.h"
#include <string.h>
#include <queue>
//#include <boost/circular_buffer.hpp>
extern "C" {
	#include "thread_utils.h"
}
/** @def BUFFER_SIZE
 *  	 Defines the maximum size of the buffers.
 *
*/

#define BUFFER_SIZE 4

/** @def BUFFER_SIZE
 *  	 Defines the sleep used to reduce the cpu usage
 *
*/
#define USSLEEPFRAMERATE 100
#define QUEUE_TIMEOUT 300

/** @def MINIUM_SPACE_FREE
 *  	 Defines the minimum space free to save information in the hard disk.
 *
*/
#define MINIUM_SPACE_FREE 104857600

/** @def TIME_OUT
 *  	 Defines the general time out.
 *
*/
#define TIME_OUT 100

/** @class FiniteStateMachineTemplate
 *  @details This abstract class incorporates
 *           properties common to all the finite state machines
 */

class FiniteStateMachineTemplate {

protected:
	/** @var videoBuffer
	 *  	 Contains the video frames that we will processed
	 *  	 in the state machine.
	 */
	//boost::circular_buffer<NvMediaImage*> *videoBuffer;
	NvQueue *videoBuffer;

	/** @enum TransitionMessage
	 *  This enumeration is used in order to handle the
	 *  transition between states.
	 *
	 */
	/** @var StateChanged
	 *  	 This enumeration type is used in order to communicate that the state
	 *  	 was changed correctly.
	 *
	 */
	/** @var StateIgnored
	 * 		 This enumeration type is used in order to communicate that the state
	 *  	 was not changed.
	 *
	 */


	enum TransitionMessage {StateChanged, StateIgnored};

	/** @brief This method links buffer with
	 *   the others buffers belonging to the following stages.
	 *
	 * @return void
	 */
	//void linkStages();

	/** @brief This method links buffer with
	 *   the others buffers belonging to the following stages.
	 *
	 * @return void
	 */
	void linkStages(NvMediaImage* frame);

public:
	/** @var FSM_BUTTONS
	 *  	 This enumeration type is used in order to represent
	 *  	 the Buttons module. Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	/** @var FSM_CAPTURE
	 *  	 This enumeration type is used in order to represent
	 *  	 the Capture module. Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	/** @var FSM_CROP
	 *  	 This enumeration type is used in order to represent
	 *  	 the Crop module. Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	/** @var FSM_DISPLAY
	 *  	 This enumeration type is used in order to represent
	 *  	 the Display module. Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	/** @var FSM_HANDLE
	 *  	 This enumeration type is used in order to represent
	 *  	 the Handle module. Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	/** @var FSM_OVERLAY
	 *  	 This enumeration type is used in order to represent
	 *  	 the Overlay module . Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	 /** @var FSM_RECORD
	 *  	 This enumeration type is used in order to represent
	 *  	 the Record module. Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	 /** @var FSM_SNAPSHOT
	 *  	 This enumeration type is used in order to represent
	 *  	 the Snapshot module. Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	/** @var FSM_SOILING
	 *  	 This enumeration type is used in order to represent
	 *  	 the Soiling module. Used in message Queues to identify
	 *  	 who is the sender and who is the receiver.
	 */
	typedef enum {
		FSM_BUTTONS,
		FSM_CAPTURE,
		FSM_CROP,
		FSM_DISPLAY,
		FSM_HANDLE,
		FSM_OVERLAY,
		FSM_RECORD,
		FSM_SNAPSHOT,
		FSM_SOILING,
		FSM_FPS
	}FSM;


	/** @var sender
	 *  	 This struct type is used in order to know
	 *  	 which module is sending the message in the queue.
	 */
	/** @var receiver
	 *  	This struct type is used in order to know
	 *  	 which module is receiving the message in the queue.
	 */
	/** @var msg
	 *  	 This struct type is used in order to know
	 *  	 which is the stored message by the sender.
	 */
	typedef struct{
		FSM sender;
		FSM receiver;
		void* msg;
	}MSG;

	typedef struct{
		FSM sender;
		FSM receiver;
		int FPS;
	}FPS_MSG;

	/** @var outMsgQ
	 *  	 Container adaptor, specifically designed to operate in a FIFO context.
	 *  	 Store the message outputs
	 *  	 Output Queue always will be linked to Input Queue.
	 */
	std::queue <MSG> *outMsgQ;
	/** @var inMsgQ
	 *  	 Container adaptor, specifically designed to operate in a FIFO context.
	 *  	 Stores the message inputs
	 */
	std::queue <MSG> *inMsgQ;

	/** @brief This method initializes queues
	 *
	 * @return void
	 */
	void initMsgQueue(FiniteStateMachineTemplate* fsm);

	/** @brief This method check if the queue is initialized
	 * Default state is false
	 * @return true when the queue is initialized
	 */
	bool returnMsgQueueInitialized();

	/** @brief This method link modules
	 * @param FiniteStateMachineTemplate : fsm1 always linked with fsm2
	 *
	 * @return void
	 */
	static void linkQueues(FiniteStateMachineTemplate* fsm1,FiniteStateMachineTemplate* fsm2);


	/** @var STOPPED
	 *  	 This enumeration type is used in order to represent
	 *  	 the stopped state. In this state the machine only
	 *  	 perform monitoring tasks.
	 */
	/** @var INITIALIZED
	 *  	 This enumeration type is used in order to represent
	 *  	 the initialized state.In this state the machine only
	 *  	 perform monitoring tasks.
	 */
	/** @var RUN
	 *  	 This enumeration type is used in order to represent
	 *  	 the run state. This state is used to do the tasks
	 *  	 for which it is designed this state machine.
	 */
	/** @var FAILED
	 *  	 This enumeration type is used in order to represent
	 *  	 the failed state.In this state the machine only
	 *  	 perform monitoring tasks.
	 */
	/** @var TRANSITION_TO_STOPPED
	 *  	 This enumeration type is used in order to represent
	 *  	 the transition to stopped state. In this state state
	 *  	 machine is doing the tasks to change the state to stopped.
	 */
	/** @var TRANSITION_TO_INITIALIZED
	 *  	 This enumeration type is used in order to represent
	 *  	 the transition to initialized state. In this state state
	 *  	 machine is doing the tasks to change the state to initialized.
	 */
	/** @var TRANSITION_TO_RUN
	 *  	 This enumeration type is used in order to represent
	 *  	 the transition to run state. In this state state
	 *  	 machine is doing the tasks to change the state to run.
	 */
	/** @var TRANSITION_TO_FAILED
	 *  	 This enumeration type is used in order to represent
	 *  	 the transition to failed state. In this state state
	 *  	 machine is doing the tasks to change the state to failed.
	 */
	enum State {STOPPED, INITIALIZED, RUN, FAILED,TRANSITION_TO_STOPPED,
				TRANSITION_TO_INITIALIZED, TRANSITION_TO_RUN, TRANSITION_TO_FAILED};

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	FiniteStateMachineTemplate();
	/** @brief Free the resources generated for the object
	 */
	virtual ~FiniteStateMachineTemplate();

	/** @brief This method add the following stages for
	 * 		   to do interaction in the future.
	 * @param FiniteStateMachineTemplate : The new stage to add.
	 *
	 * @return void
	 */
	void addNextStage(FiniteStateMachineTemplate* stage);

	/** @brief This method launch the signal to change
	 * 		   the current state.
	 * @param State : The State to which we want to change.
	 *
	 * @return TransitionMessage : If the change state was successful
	 * 		   the return value will be StateChanged in other case
	 * 		   will be StateIgnored.
	 */
	TransitionMessage changeState(State newState);

	/** @brief This method links buffer with
	 *   the others buffers belonging to the following stages.
	 *
	 * @return State
	 */
	inline State getCurrentState(){return currentState;}

	/** \brief This method enqueue the image to the video buffer.
	 *
	 * @param NvMediaImage: Image we want to enqueue
	 *
	 * @return void
	 */
	void enqueue(NvMediaImage* frame);

	/** @brief This method dequeues the first element of the buffer.
	 *
	 * @return void
	 */
	NvMediaImage* getFrame();

	/** @brief This method get the current frame to be processed.
	 *
	 * @return NvMediaImage
	 */
	//inline NvMediaImage* getFrame(){ return (!videoBuffer->empty()) ? videoBuffer->front() : nullptr;}


private:

	/** @var initialized
	 *  	 Contains the actual state of the queues.
	 */
	bool initialized;

	int nStages;

	/** @var currentState
	 *  	 Contains the actual state of the machine.
	 */
	State currentState;

	/** @var threadAction
	 *  	 Represents the thread that is running the
	 *  	 behaviour of the state machine
	 */
	std::thread *threadAction;

	/** @var nextStages
	 *  	 Contains all the next stages with
	 *  	 we will interact in the run time.
	 */
	std::list<FiniteStateMachineTemplate*> *nextStages;

	/** @brief This method describes the behavior of the
	 * 		  finite state machine, and launch an independent thread to do
	 * 		  the task associated with this behavior
	 *
	 * @return void
	 */
	void threadBehaviour();

	/** @brief This method receives the signal emitted
	 * 		   for #changeState()
	 *
	 * @return TransitionMessage : If the change state was successful
	 * 		   the return value will be StateChanged in other case
	 * 		   will be StateIgnored.
	 */
	TransitionMessage internalChangeState(State newState);

	/** @brief This method is the template to describe the behavior
	 *		   of the state stopped, after execution returns the
	 *		   current state.
	 *
	 * @return State
	 */
	virtual State ST_Stopped();

	/** @brief This method is the template to describe the behavior
	 *		   of the state initialized, after execution returns the
	 *		   current state.
	 *
	 * @return State
	 */
	virtual State ST_Initialized();

	/** @brief This method is the template to describe the behavior
	 *		   of the state run, after execution returns the
	 *		   current state.
	 *
	 * @return State
	 */
	virtual State ST_Run();

	/** @brief This method is the template to describe the behavior
	 *		   of the state failed, after execution returns the
	 *		   current state.
	 *
	 * @return State
	 */
	virtual State ST_Failed();

	/** @brief This method is the template to describe the behavior
	 *		   of the state transition to stop, after execution returns the
	 *		   current state.
	 *
	 * @return State
	*/
	virtual State ST_Stopping();

	/** @brief This method is the template to describe the behavior
	 *		   of the state transition to initialize, after execution returns the
	 *		   current state.
	 *
	 * @return State
	*/
	virtual State ST_Initializing();

	/** @brief This method is the template to describe the behavior
	 *		   of the state transition to run, after execution returns the
	 *		   current state.
	 *
	 * @return State
	*/
	virtual State ST_Running();

	/** @brief This method is the template to describe the behavior
	 *		   of the state transition to fail, after execution returns the
	 *		   current state.
	 *
	 * @return State
	*/
	virtual State ST_Failing();

};
#endif /* FINITESTATEMACHINETEMPLATE_H_ */
