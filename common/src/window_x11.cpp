/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "window_x11.h"
#include <iostream>
#include <cstring>

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

WindowX11::WindowX11(int width, int height, bool robust)
: xInitialized(false)
, eglInitialized(false)
, robust(robust)
{
    createWindow(width, height);
    createEGLContext(robust);
}

WindowX11::~WindowX11(void)
{
    eglDestroyContext(display, context);
    eglDestroySurface(display, surface);
    eglTerminate(display);
    eglReleaseThread();
    
    XDestroyWindow(xDisplay, xWin);
    XCloseDisplay(xDisplay);
}

void WindowX11::swapBuffers(void)
{
    if(!isInitialized())
    {
        return;
    }

    eglSwapBuffers(display, surface);
}

bool WindowX11::isInitialized(void)
{
    return (xInitialized && eglInitialized);
}

void WindowX11::resetContext()
{
    eglDestroyContext(display, context);
    
    EGLint contextAttribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 3,
        EGL_CONTEXT_OPENGL_ROBUST_ACCESS_EXT, robust ? EGL_TRUE : EGL_FALSE,
        EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_EXT, robust ? EGL_LOSE_CONTEXT_ON_RESET_EXT : EGL_NO_RESET_NOTIFICATION_EXT,
        EGL_NONE, EGL_NONE
    };
    
    context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs);
    if (context == EGL_NO_CONTEXT)
    {
        std::cout << "Could not create EGL context!" << std::endl;
        return;
    }
    
    // Make the context current
    EGLBoolean status = eglMakeCurrent(display, surface, surface, context);
    if (!status)
    {
        std::cout << "Could not activate context" << std::endl;
        return;
    }
}

void WindowX11::makeCurrent()
{
    EGLBoolean status = eglMakeCurrent(display, surface, surface, context);
    if (status == EGL_FALSE)
    {
        std::cout << "Could not make current context: error - " << eglGetError() << std::endl;
        return;
    }
}

void WindowX11::createWindow(int width, int height)
{
    Window root;
    XSetWindowAttributes swa;
    XSetWindowAttributes xattr;
    Atom wmState;
    XWMHints hints;
    XEvent xev;

    /*
     * X11 native display initialization
     */

    xDisplay = XOpenDisplay(nullptr);
    if (xDisplay == nullptr)
    {
        xDisplay = XOpenDisplay(":0.0");
        if (xDisplay == nullptr)
        {
            std::cout << "Could not open X display" << std::endl;
            return;
        }
    }
    root = DefaultRootWindow(xDisplay);

    swa.event_mask  =  ExposureMask | PointerMotionMask | KeyPressMask;
    xWin = XCreateWindow(xDisplay, root, 0, 0, width, height, 0, CopyFromParent,
                         InputOutput, CopyFromParent, CWEventMask, &swa);

    xattr.override_redirect = FALSE;
    XChangeWindowAttributes(xDisplay, xWin, CWOverrideRedirect, &xattr);

    hints.input = TRUE;
    hints.flags = InputHint;
    XSetWMHints(xDisplay, xWin, &hints);

    // make the window visible on the screen
    XMapWindow (xDisplay, xWin);
    XStoreName (xDisplay, xWin, "OpenGL ES playground");

    // get identifiers for the provided atom name strings
    wmState = XInternAtom(xDisplay, "_NET_WM_STATE", FALSE);

    memset (&xev, 0, sizeof(xev));
    xev.type                 = ClientMessage;
    xev.xclient.window       = xWin;
    xev.xclient.message_type = wmState;
    xev.xclient.format       = 32;
    xev.xclient.data.l[0]    = 1;
    xev.xclient.data.l[1]    = FALSE;
    XSendEvent (xDisplay, DefaultRootWindow (xDisplay), FALSE,
                SubstructureNotifyMask, &xev);
    xInitialized = true;
}

void WindowX11::createEGLContext(bool robust)
{
    EGLint contextAttribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 3,
        EGL_CONTEXT_OPENGL_ROBUST_ACCESS_EXT, robust ? EGL_TRUE : EGL_FALSE,
        EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_EXT, robust ? EGL_LOSE_CONTEXT_ON_RESET_EXT : EGL_NO_RESET_NOTIFICATION_EXT,
        EGL_NONE, EGL_NONE
    };
    
    // Get Display
    display = eglGetDisplay((EGLNativeDisplayType)xDisplay);
    if (display == EGL_NO_DISPLAY)
    {
        std::cout << "Could not init EGL display" << std::endl;
        return;
    }
    eglBindAPI(EGL_OPENGL_ES_API);
    
    // Initialize EGL
    
    EGLint majorVersion;
    EGLint minorVersion;
    EGLBoolean status = eglInitialize(display, &majorVersion, &minorVersion);
    
    if (!status)
    {
        std::cout << "Could not init EGL" << std::endl;
        return;
    }
    
    // Get configs
    EGLint numConfigs;
    status = eglGetConfigs(display, NULL, 0, &numConfigs);
    if (!status)
    {
        std::cout << "Could not get config count" << std::endl;
        return;
    }
    
    // Choose config
    EGLint attribList[] =
    {
       EGL_SURFACE_TYPE,    EGL_STREAM_BIT_KHR,
       EGL_RENDERABLE_TYPE, EGL_OPENGL_ES3_BIT,
       EGL_RED_SIZE,       8,
       EGL_GREEN_SIZE,     8,
       EGL_BLUE_SIZE,      8,
       EGL_ALPHA_SIZE,     8,
       EGL_DEPTH_SIZE,     8,
       EGL_STENCIL_SIZE,   8,
       EGL_SAMPLE_BUFFERS, 1,
       EGL_SAMPLES,        4,
       EGL_NONE
    };
    status = eglChooseConfig(display, attribList, &config, 1, &numConfigs);
    if (!status)
    {
        std::cout << "Failed to select config!" << std::endl;
        return;
    }

    /*for (int i=0; i < 9; i++)
    {
        EGLint val = attribList[i * 2 + 1];
        eglGetConfigAttrib(display, config, attribList[i * 2 + 0], &val);
        printf("CONFIG: %d = %d\n", attribList[i * 2 + 0], val);
    }*/

    // Create a surface
    surface = eglCreateWindowSurface(display, config, (EGLNativeWindowType)xWin, NULL);
    if (surface == EGL_NO_SURFACE)
    {
        std::cout << "Could not create EGL surface!" << std::endl;
        return;
    }
    
    // Create a GL context
    context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs);
    if (context == EGL_NO_CONTEXT)
    {
        std::cout << "Could not create EGL context!" << std::endl;
        return;
    }
    
    // Make the context current
    status = eglMakeCurrent(display, surface, surface, context);
    if (!status)
    {
        std::cout << "Could not activate context" << std::endl;
        return;
    }

    status = eglSwapInterval(display, 1);
    if(!status)
    {
        std::cout << "Could not enable VSYNC!" << std::endl;
        return;
    }
    
    eglInitialized = true;
}
