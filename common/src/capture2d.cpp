/*****************************************************************************
 *
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 ****************************************************************************/

/*
 * capture2d.h
 *
 *  Last modification on: 17/08/2016
 *      Author: Miguel H. Rib <miguel.rib@ficosa.com>
 */




#include "capture2d.h"
extern "C"
{
#include <isc_max9286.h>
#include <isc_max9271.h>
#include <isc_ov10635.h>
#include <isc_ov490.h>
#include <isc_pca9539.h>
}

#include <iostream>
#include <cstring>
#include <sstream>

#define STR_RESOLUTION( w, h ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << w << "x" << std::dec << h ) ).str()

// Only used for detecting Board. Not a crucial dependency and could be replaced
// by something else that does the job.
//#include <cuda_runtime.h>

enum Capture2d::Board Capture2d::getBoard() {

    enum Capture2d::Board detectedBoard = JETSON_PRO_TK1;
    return detectedBoard;
}

Capture2d::Capture2d(NvMediaDevice *device, Connection connection, int cameras, int buffers,
    int width, int height, BufferType bufferType, ColorConversion colorConversion)
: buffers(buffers)
, connection(connection)
, cameras(cameras)
, width(width)
, height(height)
, bufferType(bufferType)
, colorConversion(colorConversion)
{
    // initialize sensors, if selected port fails try the other one
    enum Board currentBoard = Capture2d::getBoard();

    // configure interfaces and addresses depending on the board and user selection
    switch(currentBoard)
    {
        case JETSON_PRO_TK1:
        	if(connection == CSI_AB) {

        		if(!initialiseSensor(connection, cameras)) {
        			std::cout << __func__ << " : Initializing CSI_AB : "<<"[FAILED]"<<std::endl;
        		} else {
        			std::cout << "Initializing CSI_AB : "<<"[OK]"<<std::endl;
        		}
        	}
        	else { // Connection == CSI_CD
				if(!initialiseSensor(connection, cameras)) {
					std::cout << __func__ << " : Initializing CSI_CD : "<<"[FAILED]"<<std::endl;
				} else {
					std::cout << __func__ << " : Initializing CSI_CD : "<<"[OK]"<<std::endl;
				}
             }
            break;
        default:
            break;
    }

    /***********/
    //width = 1280;
    //height = 480;
    /***********/
    // configure NvMedia ICP to read from the sensor
    NvMediaICPSettings settings;
    memset(&settings, 0, sizeof(settings));
    settings.interfaceType = (connection == CSI_AB) ? NVMEDIA_IMAGE_CAPTURE_CSI_INTERFACE_TYPE_CSI_AB : NVMEDIA_IMAGE_CAPTURE_CSI_INTERFACE_TYPE_CSI_CD;
    settings.inputFormat.inputFormatType = NVMEDIA_IMAGE_CAPTURE_INPUT_FORMAT_TYPE_YUV422;
    settings.surfaceFormat.surfaceFormatType = NVMEDIA_IMAGE_CAPTURE_SURFACE_FORMAT_TYPE_Y_V_U_422;
    settings.width = cameras * width;
    settings.height = height;
    settings.startX = 0;
    settings.startY = 0;
    settings.embeddedDataLines = 0;
    settings.interfaceLanes = 4;
    captureDevice = NvMediaICPCreate(NVMEDIA_IMAGE_CAPTURE_INTERFACE_FORMAT_CSI, buffers, &settings);

    // create a few buffers
    for(int i = 0; i < buffers; ++i)
    {
        // create a buffer for all cameras
        NvMediaImage *img = NvMediaImageCreate(device, NvMediaSurfaceType_Image_YUV_422,
                                               (cameras == 1) ? NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE : NVMEDIA_IMAGE_CLASS_AGGREGATE_IMAGES,
                                               cameras, width, height, 0, nullptr);

        // create access to subimages, put subimages into a reference list
        ImageSiblings *siblings = new ImageSiblings;
        siblings->images = new NvMediaImage*[cameras];
        siblings->references = 0;

        if( colorConversion == RGB) {
            // convert color to a different color space:
            // create an independent buffer for each camera
            NvMediaImageAdvancedConfig advConfig;
            memset(&advConfig, 0, sizeof(advConfig));
			advConfig.embeddedDataLinesTop = 3;
			advConfig.embeddedDataLinesBottom = 3;
            for(int j = 0; j < cameras; ++j)
            {
                if( bufferType == BLOCK_LINEAR) {
                    siblings->images[j] = NvMediaImageCreate(device, NvMediaSurfaceType_Image_RGBA, NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE, 1,
                                                         width, height, NVMEDIA_IMAGE_ATTRIBUTE_UNMAPPED | NVMEDIA_IMAGE_ATTRIBUTE_EXTRA_LINES, &advConfig);
                } else {
                    // Pitch-Linear Format
                    siblings->images[j] = NvMediaImageCreate(device, NvMediaSurfaceType_Image_RGBA, NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE, 1,
                                                         width, height, NVMEDIA_IMAGE_ATTRIBUTE_EXTRA_LINES, &advConfig);
                }

                if(siblings->images[j])
                    siblings->images[j]->tag = (void*)img; // store parent frame
                else
                    std::cout << "Could not create sibling " << j << std::endl;
            }
        } else {
            // provide access to each camera as a single image
            // this creates a simplified access without additional buffers (or copies)
            for(int j = 0; j < cameras; ++j)
            {
                siblings->images[j] = NvMediaImageSiblingCreate(img, j, 0);
                if(siblings->images[j])
                    siblings->images[j]->tag = (void*)img; // store parent frame
                else
                    std::cout << "Could not create sibling " << j << std::endl;
            }
        }

        img->tag = (void*)siblings;

        returnFrame(img);
    }

    if( colorConversion != NONE ) {
        // for color conversion we want to use the 2D HW engine
        engine2d = NvMedia2DCreate(device);
    }
}

Capture2d::~Capture2d()
{
    deinitialiseSensors();

    if( colorConversion != NONE ) {
        NvMedia2DDestroy(engine2d);
        engine2d = nullptr;
    }

    // free memory and hardware engines
    for(int i = 0; i < buffers; ++i)
    {
        NvMediaImage *frame = getNextFrame();
        if(frame && frame->tag)
        {
            ImageSiblings* siblings = (ImageSiblings*)frame->tag;
            for(int j = 0; j < cameras; ++j)
            {
                NvMediaImageDestroy(siblings->images[j]);
            }
            delete[] siblings->images;
            delete siblings;
        }
        NvMediaImageDestroy(frame);
    }

    NvMediaICPDestroy(captureDevice);
}

bool Capture2d::initialiseSensor(Connection connection, int cameras)
{
    memset(&iscConfigInfo, 0, sizeof(iscConfigInfo));
    memset(&isc, 0, sizeof(isc));

    enum Board currentBoard = Capture2d::getBoard();

    // configure interfaces and addresses depending on the board and user selection
    switch( currentBoard ) {

        case JETSON_PRO_TK1: {
            if(connection == CSI_AB)
            {
                iscConfigInfo.max9286_address = 0x6a;
                iscConfigInfo.csi_link = NVMEDIA_IMAGE_CAPTURE_CSI_INTERFACE_TYPE_CSI_AB;
            }
            else // connection == CSI_CD
            {
                iscConfigInfo.max9286_address = 0x6c;
                iscConfigInfo.csi_link = NVMEDIA_IMAGE_CAPTURE_CSI_INTERFACE_TYPE_CSI_CD;
            }

            iscConfigInfo.i2cDevice = 0;
            iscConfigInfo.board = "E1859-a02";

            break;
        }
    }
    
    std::string resolution = STR_RESOLUTION( width, height );
	char* c_resolution = strdup(resolution.c_str());
	
    iscConfigInfo.broadcast_max9271_address = 0x40;
    iscConfigInfo.broadcast_sensor_address = 0x24;
    iscConfigInfo.sensorType = ISC_OV491;//ISC_OV10635;
    iscConfigInfo.sensorsNum = cameras;
    iscConfigInfo.inputSurfType = NvMediaSurfaceType_Image_YUV_422;
    iscConfigInfo.resolution = c_resolution; //"1280x486";

	fprintf(stdout, "%s : IscConfigInfo : Resolution (%s)\n", __func__, c_resolution);

    // configure cameras
    NvMediaStatus status = ConfigISCCreateDevices(&iscConfigInfo, &isc, false, "ov491");
    // if it failed, try to reduce the number of cameras
    if(status != NVMEDIA_STATUS_OK)
    {
        for(int i = cameras - 1; i > 0 && status != NVMEDIA_STATUS_OK; --i)
        {
            iscConfigInfo.sensorsNum = i;
            status = ConfigISCCreateDevices(&iscConfigInfo, &isc, true, "ov491");
        }
        if(status != NVMEDIA_STATUS_OK)
        {
            std::cout << __func__ << " : Failed to configure ISC!" << std::endl;
            return false;
        }
        else
        {
            std::cout << __func__ << " : Failed to configure " << cameras << " cameras but succeeded to enable " << iscConfigInfo.sensorsNum << " cameras!" << std::endl;
            this->cameras = iscConfigInfo.sensorsNum;
            return true;
        }
    }
    return true;
} 

void Capture2d::deinitialiseSensors(void)
{

// free cameras
#if NVMEDIA_VERSION_MAJOR <= 2 && NVMEDIA_VERSION_MINOR <= 1
    ConfigISCDestoryDevices(&iscConfigInfo, &isc);
#else
    ConfigISCDestroyDevices(&iscConfigInfo, &isc);
#endif

}

NvMediaImage* Capture2d::getNextFrame()
{
    // get next captured frame
    NvMediaImage *frame = NvMediaICPGetFrame(captureDevice, NVMEDIA_IMAGE_CAPTURE_TIMEOUT_INFINITE);
    if(!frame)
    {
        std::cout << __func__ << " : Could not retrieve frame!" << std::endl;
    }
    // if color conversion is enabled, convert frame to new color space and split it into single buffers
    else if(colorConversion != NONE &&  engine2d != nullptr)
    {
        memset(&blitParams, 0, sizeof(blitParams));
        ImageSiblings* siblings = (ImageSiblings*)frame->tag;
        for(int i = 0; i < cameras; ++i)
        {
            NvMediaRect src;
            src.x0 = i * width;
            src.y0 = 0;
            src.x1 = (i + 1) * width;
            src.y1 = height;
            NvMedia2DBlit(engine2d, siblings->images[i], nullptr, frame, &src, &blitParams);
        }
    }

    return frame;
}

void Capture2d::returnFrame(NvMediaImage *frame)
{
    if(!frame)
    {
        return;
    }

    // feed frame back to capture queue
    NvMediaStatus status = NvMediaICPFeedFrame(captureDevice, frame, NVMEDIA_IMAGE_CAPTURE_TIMEOUT_INFINITE);
    if(status != NVMEDIA_STATUS_OK)
    {
        std::cout << __func__ << " : Error returning frame to Capture2D: " << status << std::endl;
    }
}

NvMediaImage *Capture2d::getSingleFrame(NvMediaImage *img, int index)
{
    if(index >= cameras || index < 0 || !img)
        return nullptr;

    // select a certain subframe
    // use reference counting to make sure that main frame is not freed too early
    if(img->tag)
    {
        ImageSiblings* siblings = (ImageSiblings*)img->tag;
        siblings->references++;
        return siblings->images[index];
    }
    return nullptr;
}

void Capture2d::returnSingleFrame(NvMediaImage *img)
{
    if(img && img->tag)
    {
        // get parent frame and decrease reference count
        // if reference count is 0 return frame to queue
        NvMediaImage *parent = getParentFrame(img);
        if(parent && parent->tag)
        {
            ImageSiblings* siblings = (ImageSiblings*)parent->tag;
            siblings->references--;
            if(siblings->references == 0)
            {
                returnFrame(parent);
            }
        }
    }
}

NvMediaImage *Capture2d::getParentFrame(NvMediaImage *img)
{
    // return pointer to parent frame
    if(!img)
        return nullptr;
    return (NvMediaImage*)(img->tag);
}

