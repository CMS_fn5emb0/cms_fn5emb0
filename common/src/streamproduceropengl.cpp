/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "streamproduceropengl.h"

#include <EGL/eglext.h>
#include <GLES2/gl2ext.h>
#include <iostream>

static PFNEGLCREATESTREAMPRODUCERSURFACEKHRPROC eglCreateStreamProducerSurfaceKHR = nullptr;

#define EGL_BAD_STREAM_KHR                          0x321B
#define EGL_BAD_STATE_KHR                           0x321C


StreamProducerOpenGL::StreamProducerOpenGL(EGLDisplay display, EGLConfig config, EGLStream& stream, int width, int height)
    : display(display)
    , stream(stream.getID())
    , config(config)
    , width(width)
    , height(height)
{
    loadExtensions();

    // Create EGL surface from stream
    const EGLint srfAttrs[] = {
        EGL_WIDTH,               width,
        EGL_HEIGHT,              height,
        EGL_NONE
    };

    surface = eglCreateStreamProducerSurfaceKHR(display, config, stream.getID(), srfAttrs);
    if (surface == EGL_NO_SURFACE)
    {
        LOG_ERR_( "Function Name: %s, Could not obtain EGL surface for GL producer error:%s\n", __func__,eglGetError());
    }

    currentContext = eglGetCurrentContext();
    if (currentContext == EGL_NO_CONTEXT)
    {
        LOG_ERR_( "Function Name: %s, eglGetCurrentContext return no context\n", __func__);
    }

    if (eglMakeCurrent(display, surface, surface, currentContext) == EGL_FALSE)
    {
        LOG_ERR_( "Function Name: %s, eglMakeCurrent failed with error:%s\n", __func__,eglGetError());
    }

}

StreamProducerOpenGL::~StreamProducerOpenGL()
{
    eglDestroySurface(display, surface);
}

void StreamProducerOpenGL::enableCurrent()
{
    currentContext = eglGetCurrentContext();
    if (currentContext == EGL_NO_CONTEXT)
    {
        LOG_ERR_( "Function Name: %s, eglGetCurrentContext return no context\n", __func__);
    }

    currentDrawSurface = eglGetCurrentSurface(EGL_DRAW);
    if (currentDrawSurface == EGL_NO_SURFACE)
    {
        LOG_ERR_( "Function Name: %s, eglMakeCurrent (EGL_DRAW) failed with error:%s\n", __func__,eglGetError());
    }

    currentReadSurface = eglGetCurrentSurface(EGL_READ);
    if (currentReadSurface == EGL_NO_SURFACE)
    {
        LOG_ERR_( "Function Name: %s, eglMakeCurrent (EGL_READ) failed with error:%s\n", __func__,eglGetError());
    }

    if (eglMakeCurrent(display, surface, surface, currentContext) == EGL_FALSE)
    {
        LOG_ERR_( "Function Name: %s, eglMakeCurrent failed with error:%s\n", __func__,eglGetError());
    }
}

void StreamProducerOpenGL::disableCurrent()
{
    if (eglMakeCurrent(display, currentDrawSurface, currentReadSurface, currentContext) == EGL_FALSE)
    {
        LOG_ERR_( "Function Name: %s, eglMakeCurrent failed with error:%s\n", __func__,eglGetError());
    }
}

void StreamProducerOpenGL::postTexture()
{
    if (eglSwapBuffers(display, surface) == EGL_FALSE)
    {
        LOG_ERR_( "Function Name: %s, Cannot post texture, EGL error:%s\n", __func__,eglGetError());
    }
}

void StreamProducerOpenGL::loadExtensions()
{
    // query EGL driver for extensions
    if(eglCreateStreamProducerSurfaceKHR == nullptr)
    {
        eglCreateStreamProducerSurfaceKHR = (PFNEGLCREATESTREAMPRODUCERSURFACEKHRPROC)eglGetProcAddress("eglCreateStreamProducerSurfaceKHR");
        if(eglCreateStreamProducerSurfaceKHR == nullptr)
        {
            LOG_ERR_( "Function Name: %s, Failed to load eglCreateStreamProducerSurfaceKHR\n", __func__);
        }
    }
}


