/********************************************************************
 *@file Logutils.cpp
 *@brief The file LogMessage() Prints message
 *@author FICOSA-ADAS
 *@version 1.0
 *@date created 03-Jun-2016
 *******************************************************************/
#include <stdio.h>
#include <string.h>
#include "Logutils.hpp"
using namespace logutils;
#include <syslog.h>

static enum LogLevel_ msg_level = LEVEL_ERR_;
static enum LogStyle_ msg_style = LOG_STYLE_NORMAL_;
static FILE *msg_file_ = NULL;
/*******************************************************************
 *@brief SetLogLevel_ function Set logging level
 *@param[in] : level Logging style
 *@return None
 *******************************************************************/
void Logutils::SetLogLevel_(enum LogLevel_ level) {

	if ((level < LEVEL_ERR_) || (level > LEVEL_DBG_))
		return;

	msg_level = level;

}

/*******************************************************************
 *@brief SetLogStyle_ function Set logging print slyle
 *@param[in] : level Logging style
 *@return None
 *******************************************************************/
void Logutils::SetLogStyle_(enum LogStyle_ style) {
	if ((style < LOG_STYLE_NORMAL_) || (style > LOG_STYLE_FUNCTION_LINE_))
		return;
	msg_style = style;
}

/*******************************************************************
 *@brief SetLogFile_ function Set logging file handle
 *@param[in] : file handle
 *@return None
 *******************************************************************/
void Logutils::SetLogFile_(FILE *logFileHandle) {
	if (!logFileHandle)
		return;

	msg_file_ = logFileHandle;
}

/*******************************************************************
 *@brief LogLevelMessage_ function  Print message if logging level is
 *       higher than message level
 *@param[in] : level
 *@param[in] : format
 *@param[in] : Parameters list
 *@return None
 *******************************************************************/
void Logutils::LogLevelMessage_(enum LogLevel_ level, const char *functionName,
		int lineNumber, const char *format, ...) {

	va_list ap;
	char str[256];
	//FILE *logFile = msg_file_ ? msg_file_ : stdout;

	int sysLevel = 3;
	if (level > msg_level)
		return;

	if (msg_level == level || msg_level == LEVEL_INFO_) {
		strcpy(str, "[nvidiaCMS] : ");
		switch (level) {
		case LEVEL_ERR_:
			strcat(str, "ERROR: ");
			sysLevel = LOG_ERR;
			break;
		case LEVEL_WARN_:
			strcat(str, "WARNING: ");
			sysLevel = LOG_WARNING;
			break;
		case LEVEL_INFO_:
		case LEVEL_DBG_:
			// Empty
			sysLevel = LOG_DEBUG;
			break;
		}va_start(ap, format);
		vsnprintf(str + strlen(str), sizeof(str) - strlen(str), format, ap);

		if (msg_style == LOG_STYLE_NORMAL_) {
			// Add trailing new line char
			if (strlen(str) && str[strlen(str) - 1] != '\n')
				strcat(str, "\n");

		} else if (msg_style == LOG_STYLE_FUNCTION_LINE_) {
			// Remove trailing new line char
			if (strlen(str) && str[strlen(str) - 1] == '\n')
				str[strlen(str) - 1] = 0;

			// Add function and line info
			snprintf(str + +strlen(str), sizeof(str) - strlen(str),
					" at %s():%d\n", functionName, lineNumber);
		}
		//fprintf(logFile, "%s", str);
		switch (sysLevel) {
		case LOG_ERR:
			setlogmask(LOG_MASK (LOG_ERR));
			openlog("", LOG_PID | LOG_CONS, LOG_USER);
			syslog(LOG_ERR, "%s", str);
			closelog();
			break;
		case LOG_DEBUG:
		case LOG_INFO:
			openlog("", LOG_PID | LOG_CONS, LOG_USER);
			syslog(LOG_DEBUG, "%s", str);
			syslog(LOG_INFO, "%s", str);
			closelog();
			break;
		case LOG_WARNING:
			setlogmask(LOG_MASK (LOG_WARNING));
			openlog("", LOG_PID | LOG_CONS, LOG_USER);
			syslog(LOG_WARNING, "%s", str);
			closelog();
			break;
		}

		va_end(ap);
	}
}

/*******************************************************************
 *@brief LogMessage function Prints message
 *@param[in] : format
 *@param[in] : Parameters list
 *@return None
 *******************************************************************/
void Logutils::LogMessage_(const char *format, ...) {
	va_list ap;
	char str[128];
	FILE *logFile = msg_file_ ? msg_file_ : stdout;

	va_start(ap, format);
	vsnprintf(str, sizeof(str), format, ap);
	fprintf(logFile, "%s", str);
	va_end(ap);
}

/*******************************************************************
 *@brief Log function Prints message
 *@param[in] : format
 *@param[in] : Parameters list
 *@return None
 *******************************************************************/
void Logutils::Log(const char *format, ...) {
	char str[1000];

	va_list ap;
	va_start(ap, format);
	vsnprintf(str + strlen(str), sizeof(str) - strlen(str), format, ap);
	//std::cout << "[" << __FILE__ << "][" << __FUNCTION__ << "][Line " << __LINE__ << "] " << str << std::endl;
	std::cout << str << std::endl << std::flush << cout.flush();
}
/* End of file */
