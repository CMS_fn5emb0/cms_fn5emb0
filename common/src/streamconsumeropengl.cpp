/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "streamconsumeropengl.h"
#include <EGL/eglext.h>
#include <GLES2/gl2ext.h>
#include <iostream>

static PFNEGLSTREAMCONSUMERGLTEXTUREEXTERNALKHRPROC eglStreamConsumerGLTextureExternalKHR = nullptr;
static PFNEGLSTREAMCONSUMERACQUIREKHRPROC eglStreamConsumerAcquireKHR = nullptr;
static PFNEGLSTREAMCONSUMERRELEASEKHRPROC eglStreamConsumerReleaseKHR = nullptr;
#include <Logutils.hpp>
using namespace logutils;

StreamConsumerOpenGL::StreamConsumerOpenGL(EGLDisplay display, EGLStream &stream)
: display(display)
, stream(stream.getID())
, type(GL_TEXTURE_EXTERNAL_OES)
{
    loadExtensions();
    
    // create texture
    glGenTextures(1, &tex);
    glBindTexture(type, tex);
    // bind stream to texture
    EGLBoolean status = eglStreamConsumerGLTextureExternalKHR(display, this->stream);
    if(status == EGL_FALSE)
    {
        LOG_DBG_( "Function Name: %s, Failed to connect OpenGL stream consumer %s:\n", __func__,stream.getID());
    }
    // do basic texture configuration
    glTexParameteri(type, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexParameterf(type, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(type, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

StreamConsumerOpenGL::~StreamConsumerOpenGL()
{
    // release all pending frames
    release();
    while(acquire()) { release(); }

    // free texture
    glDeleteTextures(1, &tex);
}

void StreamConsumerOpenGL::loadExtensions()
{
    // query EGL driver for extensions
    if(eglStreamConsumerGLTextureExternalKHR == nullptr)
    {
        eglStreamConsumerGLTextureExternalKHR = (PFNEGLSTREAMCONSUMERGLTEXTUREEXTERNALKHRPROC)eglGetProcAddress("eglStreamConsumerGLTextureExternalKHR");
        if(eglStreamConsumerGLTextureExternalKHR == nullptr)
        {
            LOG_DBG_( "Function Name: %s, Failed to load glMapBuffer \n", __func__);
        }
    }
    if(eglStreamConsumerAcquireKHR == nullptr)
    {
        eglStreamConsumerAcquireKHR = (PFNEGLSTREAMCONSUMERACQUIREKHRPROC)eglGetProcAddress("eglStreamConsumerAcquireKHR");
        if(eglStreamConsumerAcquireKHR == nullptr)
        {
        	 LOG_DBG_( "Function Name: %s, Failed to load eglStreamConsumerAcquireKHR \n", __func__);
        }
    }
    if(eglStreamConsumerReleaseKHR == nullptr)
    {
        eglStreamConsumerReleaseKHR = (PFNEGLSTREAMCONSUMERRELEASEKHRPROC)eglGetProcAddress("eglStreamConsumerReleaseKHR");
        if(eglStreamConsumerReleaseKHR == nullptr)
        {
            LOG_DBG_( "Function Name: %s, Failed to load eglStreamConsumerReleaseKHR \n", __func__);
        }
    }
}


void StreamConsumerOpenGL::bind(int textureUnit)
{
    if(textureUnit >= 0)
    {
        glActiveTexture(GL_TEXTURE0 + textureUnit);
    }
    glBindTexture(type, tex);
}

void StreamConsumerOpenGL::unbind(int textureUnit)
{
    if(textureUnit >= 0)
    {
        glActiveTexture(GL_TEXTURE0 + textureUnit);
    }
    glBindTexture(type, tex);
}

bool StreamConsumerOpenGL::acquire(int textureUnit)
{
    bind(textureUnit);
    bool status = eglStreamConsumerAcquireKHR(display, stream);
    unbind(textureUnit);
    
    return status;
}

bool StreamConsumerOpenGL::release(int textureUnit)
{
    bind(textureUnit);
    bool status = eglStreamConsumerReleaseKHR(display, stream);
    unbind(textureUnit);
    
    return status;
}

bool StreamConsumerOpenGL::acquireAndBind(int textureUnit)
{
    bind(textureUnit);
    return acquire(-1);
}

bool StreamConsumerOpenGL::releaseAndUnbind(int textureUnit)
{
    bind(textureUnit);
    bool status = release(-1);
    unbind(textureUnit);
    return status;
}
