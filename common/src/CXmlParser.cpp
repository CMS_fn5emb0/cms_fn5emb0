/*******************************************************************
 *@file CXmlParser.cpp
 *@brief The file contains implementation of Xmlparser class
 *@author FICOSA-ADAS
 *@version 1.0
 *@date created 16-Feb-2016
 *******************************************************************/
#include "CXmlParser.h"
#include <stdlib.h>
#include <string.h>

using namespace xmlparser;

//Instance flag for xmlparser, Initially it will be false since no instance is created yet
bool CXmlParser::mXmlParserInstanceFlag = false;
CXmlParser *CXmlParser::mptrXmlParserInstance = NULL;
bool CXmlParser::recordCommand = false;
bool CXmlParser::isEnablekeyboard = false;
bool CXmlParser::isEnableCan = false;
bool CXmlParser::printSoilStatus = false;

bool CXmlParser::changeOverlay = false;
const char* CXmlParser::overlayxmlpath = "";
#define TRUE 1
#define FALSE 0
/*******************************************************************
 *@brief Create instance of xmlParser
 *******************************************************************/
CXmlParser* CXmlParser::getInstance() {
    //If instance is not created , create instance of xmlparser
    if (!mXmlParserInstanceFlag) {
        mXmlParserInstanceFlag = true;
        mptrXmlParserInstance = new CXmlParser;
        return mptrXmlParserInstance;
    } else {
        return mptrXmlParserInstance;
    }
}

/*******************************************************************
 *@brief Default destructor of xmlParser class
 *******************************************************************/
CXmlParser::~CXmlParser(void) {
    if (mXmlParserInstanceFlag) {
        delete mptrXmlParserInstance;
    }
    mXmlParserInstanceFlag = false;
}

/*******************************************************************
 *@brief The method to used for parsing xml in child node
 *@param[in] srcBuffer : string buffer in file to parse
 *@param[in] value : data pointer
 *@param[in] count : number of elements in node
 *@param[in] delim : string delimitator for tokens (default=",")
 *@return None
 *******************************************************************/
void CXmlParser::deserializesubChildnodeVal(char* srcBuffer, float *value, uint16_t &count, const char* delim)
{
    uint16_t nElements = 0;
    char* token = srcBuffer;
    token = strtok(srcBuffer, delim);
    while (token != NULL)
    {
        sscanf(token, "%f", &value[nElements]);
        nElements++;
        token = strtok(NULL, ",");
    }
    count = nElements;
}

/*******************************************************************
 *@brief The method to used for parsing xml
 *@param[in] xmlFile : Input file to parse xml
 *@return None
 *******************************************************************/
void CXmlParser::parseXml(const char* xmlFile)
{
	xml_document xmlDoc;
    //load xml file
    xml_parse_result xmlParseResult = xmlDoc.load_file(xmlFile);

    if (xmlParseResult.status != pugi::status_ok)
    {
    	 printf("Configuration xml: File not found\n");
    	 exit(0);
    }
    int nodeNumber = 0;
    for (pugi::xml_node ch_node = xmlDoc.child("CONFIG_PARAMETERS").first_child(); ch_node; ch_node = ch_node.next_sibling())
    {
        if (strcmp(ch_node.name(),"CAMERA_INFO")==0)  // (nodeNumber==0)
        {
            const char* ConnectionType  =  (xmlDoc.child("CONFIG_PARAMETERS").child("CAMERA_INFO").child_value("CSI_PORT"));

            if (strcmp(ConnectionType, "CSI_AB") == 0 )
            {
                _connectionType = CSI_AB;
            }
            else if (strcmp(ConnectionType, "CSI_CD") == 0 )
            {
                 _connectionType =CSI_CD;
            }
            else if (strcmp(ConnectionType, "CSI_EF") == 0 )
            {
                 _connectionType = CSI_EF;
            }
            else
            {
                cout<<"Please ensure proper connection type is given in xml file"<<endl;
                exit(0);
            }
            _cameraHeight = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("CAMERA_INFO").child_value("CAMERA_HEIGHT"));
            _cameraWidth  = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("CAMERA_INFO").child_value("CAMERA_WIDTH"));
            _cameraNumber = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("CAMERA_INFO").child_value("CAMERA_NUMBER"));
            _cameraFrameRate = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("CAMERA_INFO").child_value("CAMERA_FRAME_RATE"));

            _cropROIX0  = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("CROP_INFO").child_value("ROI_X0"));
            _cropROIY0  = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("CROP_INFO").child_value("ROI_Y0"));
            _cropWidth  = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("CROP_INFO").child_value("ROI_WIDTH"));
            _cropHeight = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("CROP_INFO").child_value("ROI_HEIGHT"));

            _displayHeight 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("DISPLAY_INFO").child_value("VIEWPORT_HEIGHT"));
            _displayWidth 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("DISPLAY_INFO").child_value("VIEWPORT_WIDTH"));
            _displayX0 		= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("DISPLAY_INFO").child_value("VIEWPORT_X0"));
            _displayY0 		= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("DISPLAY_INFO").child_value("VIEWPORT_Y0"));
            _displayId 		= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("DISPLAY_INFO").child_value("DISPLAY_ID"));
            _displayWindow	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("DISPLAY_INFO").child_value("DISPLAY_WINDOW"));

            const char* isButtonsEnabledStr = (xmlDoc.child("CONFIG_PARAMETERS").child("BUTTONS_INFO").child_value("BUTTONS_ENABLED"));
							_isButtonsEnabled = (strcmp(isButtonsEnabledStr, "TRUE") == 0) ? TRUE : FALSE;

            _buttonsMax = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("BUTTONS_INFO").child_value("BUTTONS_MAX_THRESHOLD_BACKLIGHT"));
		    _buttonsMin	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("BUTTONS_INFO").child_value("BUTTONS_MIN_THRESHOLD_BACKLIGHT"));
		    _buttonsDay	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("BUTTONS_INFO").child_value("BUTTONS_DAY_VALUE_BACKLIGHT"));
		    _buttonsNight	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("BUTTONS_INFO").child_value("BUTTONS_NIGHT_VALUE_BACKLIGHT"));
		    _buttonsStep	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("BUTTONS_INFO").child_value("BUTTONS_STEP_BACKLIGHT"));
		    _backlightPath 	= (xmlDoc.child("CONFIG_PARAMETERS").child("BUTTONS_INFO").child_value("BUTTONS_BACKLIGHT_PATH"));
		    _daynightPath 	= (xmlDoc.child("CONFIG_PARAMETERS").child("BUTTONS_INFO").child_value("BUTTONS_DAY_NIGHT_PATH"));

            //const char* prvCamStr  =  (xmlDoc.child("CONFIG_PARAMETERS").child("CAMERA_INFO").child_value("PRV_CAMERA_USED"));
            _snapshotPrefix	  = (xmlDoc.child("CONFIG_PARAMETERS").child("SNAPSHOT_INFO").child_value("PREFIX"));
            _snapshotFormat	  = (xmlDoc.child("CONFIG_PARAMETERS").child("SNAPSHOT_INFO").child_value("FORMAT"));
            _recordingPrefix  = (xmlDoc.child("CONFIG_PARAMETERS").child("RECORDING_INFO").child_value("PREFIX"));
            _SVM = (xmlDoc.child("CONFIG_PARAMETERS").child("SOIL_INFO").child_value("SVM_PATH"));
            _SVMExt = (xmlDoc.child("CONFIG_PARAMETERS").child("SOIL_INFO").child_value("SVMEXT_PATH"));
            _SVMExtNoPartial = (xmlDoc.child("CONFIG_PARAMETERS").child("SOIL_INFO").child_value("SVMEXTNOPARTIAL_PATH"));
            _soilFPS = atoi(xmlDoc.child("CONFIG_PARAMETERS").child("SOIL_INFO").child_value("SOIL_FPS"));

            _overlayPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").find_child_by_attribute("IMAGE","id","1").child_value());
            _cleanPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").find_child_by_attribute("IMAGE","id","2").child_value());
            _dirtyPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").find_child_by_attribute("IMAGE","id","3").child_value());
            _cleanOverlayPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").find_child_by_attribute("IMAGE","id","4").child_value());
            _dirtyOverlayPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").find_child_by_attribute("IMAGE","id","5").child_value());

            //_idoverlayPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").child("IMAGE").child(_overlayPath.c_str()).attribute("id").as_int());
            //_idcleanPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").child("IMAGE").attribute("id").value());
            //_iddirtyPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").child("IMAGE").child(_dirtyPath.c_str()).attribute("id").as_int());
            //_idoverlayCleanPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").child("IMAGE").attribute("id").as_int());
            //_idoverlayDirtyPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").child("IMAGE").child(_dirtyOverlayPath.c_str()).attribute("id").as_int());
            //std::cout << "overlay clean id: " << _idcleanPath << std::endl;

            _vertexshaderPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").child_value("VERTEXSHADER_PATH"));
            _fragmentshaderPath = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").child_value("FRAGMENTSHADER_PATH"));

            for (pugi::xml_node tool = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").first_child()); tool; tool = tool.next_sibling())
            {
				for (pugi::xml_attribute attr = tool.first_attribute(); attr; attr = attr.next_attribute())
				{
					if(tool.child_value() == _overlayPath) {
						_idoverlayPath = attr.as_int();
					} else if(tool.child_value() == _cleanPath) {
						_idcleanPath = attr.as_int();
					} else if(tool.child_value() == _dirtyPath) {
						_iddirtyPath = attr.as_int();
					} else if(tool.child_value() == _cleanOverlayPath) {
						_idoverlayCleanPath = attr.as_int();
					} else if(tool.child_value() == _dirtyOverlayPath) {
						_idoverlayDirtyPath = attr.as_int();
					}
				}
            }

            const char* isOverlayEnabledStr = (xmlDoc.child("CONFIG_PARAMETERS").child("OVERLAY_INFO").child_value("OVERLAY_ENABLED"));
                        _isOverlayEnabled = (strcmp(isOverlayEnabledStr, "TRUE") == 0) ? TRUE : FALSE;
			const char* isAesphericalEnabledStr = (xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("AESPHERICAL_ENABLED"));
						_isAesphericalEnabled = (strcmp(isAesphericalEnabledStr, "TRUE") == 0) ? TRUE : FALSE;
			const char* isCropEnabledStr = (xmlDoc.child("CONFIG_PARAMETERS").child("CROP_INFO").child_value("CROP_ENABLED"));
									_isCropEnabled = (strcmp(isCropEnabledStr, "TRUE") == 0) ? TRUE : FALSE;
			const char* isSoilEnabledStr = (xmlDoc.child("CONFIG_PARAMETERS").child("SOIL_INFO").child_value("SOIL_ENABLED"));
									_isSoilEnabled = (strcmp(isSoilEnabledStr, "TRUE") == 0) ? TRUE : FALSE;
			const char* isFisEyeCameraEnabledStr = (xmlDoc.child("CONFIG_PARAMETERS").child("FISHEYE_INFO").child_value("FISHEYE_ENABLED"));
						_isFishEyeCameraEnabled= (strcmp(isFisEyeCameraEnabledStr, "TRUE") == 0) ? TRUE : FALSE;


            //_isAesphericalEnabled = (xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("AESPHERICAL_ENABLED"));

            _srcWidth 			= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("SRC_WIDTH"));
            _srcHeight 			= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("SRC_HEIGHT"));
            _srcROIX 			= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("SRC_ROI_X"));
            _srcROIY 			= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("SRC_ROI_Y"));
            _srcROIWidth 		= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("SRC_ROI_WIDTH"));
            _srcROIHeight 		= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("SRC_ROI_HEIGHT"));
            _srcROIScaleSEP 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("SRC_ROI_SCALE_SEP"));
         	_dstWidth 			= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DST_WIDTH"));
            _dstHeight 			= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DST_HEIGHT"));
            _dstROIX 			= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DST_ROI_X"));
            _dstROIY 			= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DST_ROI_Y"));
            _dstROIWidth 		= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DST_ROI_WIDTH"));
            _dstROIHeight 		= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DST_ROI_HEIGHT"));
            _dstROIScaleSEP 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DST_ROI_SCALE_SEP"));
            _scalingDirection 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("SCALING_DIRECTION"));
            _pixelsPerPatchX 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("PIXELS_PER_PATCHX"));
            _pixelsPerPatchY 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("PIXELS_PER_PATCHY"));
            _dottedLinesNum 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DOTTED_LINES_NUM"));
            _dottedLinesThickness 	= atoi(xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child_value("DOTTED_LINES_THICKNESS"));

            _centerX			= atof(xmlDoc.child("CONFIG_PARAMETERS").child("FISHEYE_INFO").child_value("CENTER_X"));
            _centerY			= atof(xmlDoc.child("CONFIG_PARAMETERS").child("FISHEYE_INFO").child_value("CENTER_Y"));

            _c			= atof(xmlDoc.child("CONFIG_PARAMETERS").child("FISHEYE_INFO").child_value("C"));
            _d			= atof(xmlDoc.child("CONFIG_PARAMETERS").child("FISHEYE_INFO").child_value("D"));
            _e			= atof(xmlDoc.child("CONFIG_PARAMETERS").child("FISHEYE_INFO").child_value("E"));

            deserializesubChildnodeVal((char*)xmlDoc.child("CONFIG_PARAMETERS").child("FISHEYE_INFO").child_value("SCARAMUZZA_COEFF"), _scaramuzzaCoeff, _numScaramuzzaCoeff);


	   }

		pugi::xml_node subChildnode1 = (xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child("PROPERTY"));
		if(strcmp(subChildnode1.name(), "PROPERTY")  == 0)
		{
			uint16_t coeffCount = 10;
			pugi::xml_attribute_iterator value = subChildnode1.attributes_begin();
			pugi::xml_attribute_iterator name  = value++;

			if(strcmp(name->value(),"polyU")== 0)
				deserializesubChildnodeVal((char*)value->value(), _polyU, coeffCount );
		}
		pugi::xml_node subChildnode2 = (xmlDoc.child("CONFIG_PARAMETERS").child("AESPHERICAL_INFO").child("PROPERTY").next_sibling());
		if(strcmp(subChildnode2.name(), "PROPERTY")  == 0)
		{
			uint16_t coeffCount = 10;
			pugi::xml_attribute_iterator value = subChildnode2.attributes_begin();
			pugi::xml_attribute_iterator name  = value++;
			if(strcmp(name->value(),"polyV")== 0)
				deserializesubChildnodeVal((char*)value->value(), _polyV, coeffCount );
		}
    }
    nodeNumber++;
}


/*******************************************************************
 *@brief get camera Count
 *@return cameraCount
 *******************************************************************/
int16_t CXmlParser::getCameraNumber (void ) const
{
	return _cameraNumber;
}
/*******************************************************************
 *@brief get camera Count
 *@return cameraCount
 *******************************************************************/
int16_t CXmlParser::getCameraConnectionType (void ) const
{
	return _connectionType;
}

/*******************************************************************
 *@brief get Image Height
 *@return imageHeight
 *******************************************************************/
int16_t CXmlParser::getCameraHeight(void) const
{
    return _cameraHeight;
}
/*******************************************************************
 *@brief get Image Width
 *@return imageWidth
 *******************************************************************/
int16_t CXmlParser::getCameraWidth(void) const
{
    return _cameraWidth;
}

/*******************************************************************
 *@brief get Image Width
 *@return imageWidth
 *******************************************************************/
int16_t CXmlParser::getCameraFrameRate(void) const
{
    return _cameraFrameRate;
}

/*******************************************************************
 *@brief get crop ROI X0
 *@return cropROIX0
 *******************************************************************/
int16_t CXmlParser::getCropROIX0 (void ) const
{
	return _cropROIX0;
}
/*******************************************************************
 *@brief get crop ROI Y0
 *@return cropROIY0
 *******************************************************************/
int16_t CXmlParser::getCropROIY0 (void ) const
{
	return _cropROIY0;
}

/*******************************************************************
 *@brief get crop Width
 *@return cropWidth
 *******************************************************************/
int16_t CXmlParser::getCropWidth(void) const
{
    return _cropWidth;
}
/*******************************************************************
 *@brief get cropHeight
 *@return crop Height
 *******************************************************************/
int16_t CXmlParser::getCropHeight(void) const
{
    return _cropHeight;
}
/*******************************************************************
 *@brief get display Id
 *@return displayId
 *******************************************************************/
int16_t CXmlParser::getDisplayId(void) const
{
    return _displayId;
}
/*******************************************************************
 *@brief get display window
 *@return displayWindow
 *******************************************************************/
int16_t CXmlParser::getDisplayWindow(void) const
{
    return _displayWindow;
}

/*******************************************************************
 *@brief get Display Height
 *@return displayHeight
 *******************************************************************/
int16_t CXmlParser::getDisplayHeight(void) const
{
    return _displayHeight;
}
/*******************************************************************
 *@brief get display Width
 *@return displayWidth
 *******************************************************************/
int16_t CXmlParser::getDisplayWidth(void) const
{
    return _displayWidth;
}
/*******************************************************************
 *@brief get display X0
 *@return displayX0
 *******************************************************************/
int16_t CXmlParser::getDisplayX0(void) const
{
    return _displayX0;
}
/*******************************************************************
 *@brief get display Y0
 *@return displayY0
 *******************************************************************/
int16_t CXmlParser::getDisplayY0(void) const
{
    return _displayY0;
}
/*******************************************************************
 *@brief get snapshot Prefix
 *@return snapshotPrefix
 *******************************************************************/
string CXmlParser::getSnapshotPrefix(void) const
{
    return _snapshotPrefix;
}

string CXmlParser::getSnapshotFormat(void) const
{
    return _snapshotFormat;
}
/*******************************************************************
 *@brief get recording Prefix
 *@return recordingPrefix
 *******************************************************************/
string CXmlParser::getRecordingPrefix(void) const
{
    return _recordingPrefix;
}
/*******************************************************************
 *@brief get src Width
 *@return srcWidth
 *******************************************************************/
int16_t CXmlParser::getSrcWidth(void) const
{
    return _srcWidth;
}
/*******************************************************************
 *@brief get src Height
 *@return srcHeight
 *******************************************************************/
int16_t CXmlParser::getSrcHeight(void) const
{
    return _srcHeight;
}
/*******************************************************************
 *@brief get src ROI X
 *@return srcROIX
 *******************************************************************/
int16_t CXmlParser::getsrcROIX(void) const
{
    return _srcROIX;
}
/*******************************************************************
 *@brief get src ROI Y
 *@return srcROIY
 *******************************************************************/
int16_t CXmlParser::getsrcROIY(void) const
{
    return _srcROIY;
}
/*******************************************************************
 *@brief get src ROI Width
 *@return srcROIWidth
 *******************************************************************/
int16_t CXmlParser::getsrcROIWidth(void) const
{
    return _srcROIWidth;
}
/*******************************************************************
 *@brief get src ROI Height
 *@return srcROIHeight
 *******************************************************************/
int16_t CXmlParser::getsrcROIHeight(void) const
{
    return _srcROIHeight;
}
/*******************************************************************
 *@brief get src ROI Scale SEP
 *@return srcROIScaleSEP
 *******************************************************************/
int16_t CXmlParser::getsrcROIScaleSEP(void) const
{
    return _srcROIScaleSEP;
}
/*******************************************************************
 *@brief get dst Width
 *@return dstWidth
 *******************************************************************/
int16_t CXmlParser::getdstWidth(void) const
{
    return _dstWidth;
}
/*******************************************************************
 *@brief get dst Height
 *@return dstHeight
 *******************************************************************/
int16_t CXmlParser::getdstHeight(void) const
{
    return _dstHeight;
}
/*******************************************************************
 *@brief get dst ROI X
 *@return dstROIX
 *******************************************************************/
int16_t CXmlParser::getdstROIX(void) const
{
    return _dstROIX;
}
/*******************************************************************
 *@brief get dst ROI Y
 *@return dstROIY
 *******************************************************************/
int16_t CXmlParser::getdstROIY(void) const
{
    return _dstROIY;
}
/*******************************************************************
 *@brief get dst ROI Width
 *@return dstROIWidth
 *******************************************************************/
int16_t CXmlParser::getdstROIWidth(void) const
{
    return _dstROIWidth;
}
/*******************************************************************
 *@brief get dst ROI Height
 *@return dstROIHeight
 *******************************************************************/
int16_t CXmlParser::getdstROIHeight(void) const
{
    return _dstROIHeight;
}
/*******************************************************************
 *@brief get dst ROI Scale SEP
 *@return dstROIScaleSEP
 *******************************************************************/
int16_t CXmlParser::getdstROIScaleSEP(void) const
{
    return _dstROIScaleSEP;
}
/*******************************************************************
 *@brief get scaling Direction
 *@return scalingDirection
 *******************************************************************/
int16_t CXmlParser::getscalingDirection(void) const
{
    return _scalingDirection;
}

/*******************************************************************
 *@brief get pixels Per PatchX
 *@return dottedLinesNum
 *******************************************************************/
int16_t CXmlParser::getdottedLinesNum(void) const
{
    return _dottedLinesNum;
}
/*******************************************************************
 *@brief get dotted lines thickness
 *@return dottedLinesThickness
 *******************************************************************/
int16_t CXmlParser::getdottedLinesThickness(void) const
{
    return _dottedLinesThickness;
}

/*******************************************************************
 *@brief get pixels Per PatchX
 *@return pixelsPerPatchX
 *******************************************************************/
int16_t CXmlParser::getpixelsPerPatchX(void) const
{
    return _pixelsPerPatchX;
}
/*******************************************************************
 *@brief get pixels Per PatchY
 *@return pixelsPerPatchY
 *******************************************************************/
int16_t CXmlParser::getpixelsPerPatchY(void) const
{
    return _pixelsPerPatchY;
}
/*******************************************************************
 *@brief get polyU
 *@return polyU
 *******************************************************************/
/*******************************************************************
 *@brief get PolyU
 *@return polyU
*******************************************************************/
const float* CXmlParser::getPolyU(void) const
{
	return _polyU;
}
/*******************************************************************
 *@brief get polyV
 *@return polyV
*******************************************************************/
const float* CXmlParser::getPolyV(void) const
{
	return _polyV;
}
 /*******************************************************************
  *@brief get IsOverlayEnabled
  *@return IsOverlayEnabled
 *******************************************************************/
 bool CXmlParser::getisOverlayEnabled(void) const
 {
    return _isOverlayEnabled;
}

 /*******************************************************************
   *@brief get IsAesphericalEnabled
   *@return IsAesphericalEnabled
  *******************************************************************/
bool CXmlParser::getIsAesphericalEnabled(void) const
{
 return _isAesphericalEnabled;
}

 /*******************************************************************
   *@brief get IssiolEnabled
   *@return IsSoilEnabled
  *******************************************************************/
bool CXmlParser::getisSoilEnabled(void) const
{
 return _isSoilEnabled;
}


string CXmlParser::getsvmPath(void) const
{
	return _SVM;
}

string CXmlParser::getsvmExtPath(void) const
{
	return _SVMExt;
}

string CXmlParser::getsvmExtNoPartialPath(void) const
{
	return _SVMExtNoPartial;
}

int16_t CXmlParser::getSoilFPS(void) const
{
	return _soilFPS;
}

string CXmlParser::getCleanPath(void) const
{
	return _cleanPath;
}

string CXmlParser::getCleanOverlayPath(void) const
{
	return _cleanOverlayPath;
}

string CXmlParser::getDirtyPath(void) const
{
	return _dirtyPath;
}

string CXmlParser::getDirtyOverlayPath(void) const
{
	return _dirtyOverlayPath;
}

string CXmlParser::getVertexshaderPath(void) const
{
	return _vertexshaderPath;
}

string CXmlParser::getFragmentshaderPath(void) const
{
	return _fragmentshaderPath;
}

int16_t CXmlParser::getOverlayPathId(void) const{
	return _idoverlayPath;
}

int16_t CXmlParser::getCleanPathId(void) const{
	return _idcleanPath;
}

int16_t CXmlParser::getDirtyPathId(void) const{
	return _iddirtyPath;
}

int16_t CXmlParser::getOverlayCleanPathId(void) const{
	return _idoverlayCleanPath;
}

int16_t CXmlParser::getOverlayDirtyPathId(void) const{
	return _idoverlayDirtyPath;
}

/*******************************************************************
  *@brief get IsCropEnabled
  *@return IsCropEnabled
*******************************************************************/
bool CXmlParser::getisCropEnabled(void) const
 {
    return _isCropEnabled;
}

/*******************************************************************
  *@brief get IsOverlayPath
  *@return IsOverlayPath
*******************************************************************/
string CXmlParser::getoverlayPath(void) const
 {
    return _overlayPath;
}
/*******************************************************************
 *@brief get IsButtonsEnabled
 *@return IsButtonsEnabled
*******************************************************************/
bool CXmlParser::getisButtonsEnabled(void) const
{
   return _isButtonsEnabled;
}
/*******************************************************************
 *@brief getButtonsMinTresholdBacklight
 *@return ButtonsMinTresholdBacklight
 *******************************************************************/
int16_t CXmlParser::getButtonsMinTresholdBacklight(void) const
{
    return _buttonsMin;
}
/*******************************************************************
 *@brief getButtonsMaxTresholdBacklight
 *@return ButtonsMaxTresholdBacklight
 *******************************************************************/
int16_t CXmlParser::getButtonsMaxTresholdBacklight(void) const
{
    return _buttonsMax;
}
/*******************************************************************
 *@brief getButtonsDayValueBacklight
 *@return ButtonsDayValueBacklight
 *******************************************************************/
int16_t CXmlParser::getButtonsDayValueBacklight(void) const
{
    return _buttonsDay;
}
/*******************************************************************
 *@brief getButtonsNightValueBacklight
 *@return ButtonsNightValueBacklight
 *******************************************************************/
int16_t CXmlParser::getButtonsNightValueBacklight(void) const
{
    return _buttonsNight;
}
/*******************************************************************
 *@brief getButtonsStepBacklight
 *@return ButtonsStepBacklight
 *******************************************************************/
int16_t CXmlParser::getButtonsStepBacklight(void) const
{
    return _buttonsStep;
}

string CXmlParser::getButtonsBacklightPath(void) const
{
    return _backlightPath;
}

string CXmlParser::getButtonsDayNightPath(void) const
{
    return _daynightPath;
}

/*******************************************************************
  *@brief get IsCropEnabled
  *@return _isFishEyeCameraEnabled
*******************************************************************/
bool CXmlParser::getIsFishEyeCameraEnabled(void)const
{
	return _isFishEyeCameraEnabled;
}

/*******************************************************************
  *@brief getScaramuzzaCoeff
  *@return _scaramuzzaCoeff
*******************************************************************/
const float* CXmlParser::getScaramuzzaCoeff(void) const
{
	return _scaramuzzaCoeff;
}

/*******************************************************************
  *@brief getNumScaramuzzaCoeff
  *@return _numScaramuzzaCoeff
*******************************************************************/
uint16_t CXmlParser::getNumScaramuzzaCoeff(void) const
{
	return _numScaramuzzaCoeff;
}
/*******************************************************************
  *@brief getCenterX
  *@return _centerX
*******************************************************************/
float CXmlParser::getCenterX(void) const
{
	return _centerX;
}
/*******************************************************************
  *@brief getCenterY
  *@return _centerY
*******************************************************************/
float CXmlParser::getCenterY(void) const
{
	return _centerY;
}
/*******************************************************************
  *@brief getC
  *@return _c
*******************************************************************/
float CXmlParser::getC(void) const
{
	return _c;
}
/*******************************************************************
  *@brief getD
  *@return _d
*******************************************************************/
float CXmlParser::getD(void) const
{
	return _d;
}
/*******************************************************************
  *@brief getE
  *@return _e
*******************************************************************/
float CXmlParser::getE(void) const
{
	return _e;
}

