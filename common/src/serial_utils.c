/*****************************************************************************
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************/

/*
 * serial_utils.c
 *
 *  Created on: 25/08/2015
 *      Author: Galder Unibaso <galder.unibaso@idneo.es>
 */

#ifndef __SERIAL_UTILS_H__
#include "serial_utils.h"
#endif //__SERIAL_UTILS_H__

#ifndef __COMMON_UTILS_H__
#include "common_utils.h"
#endif // __COMMON_UTILS_H__

#ifndef	_UNISTD_H
#include <unistd.h>
#endif //_UNISTD_H
#ifndef	_FCNTL_H
#include <fcntl.h>   	// File control definitions
#endif	//_FCNTL_H
#ifndef	_STRING_H
#include <string.h>		// String management
#endif //_STRING_H
#ifndef	_STDLIB_H
#include <stdlib.h>
#endif //_STDLIB_H
#ifndef	_ERRNO_H
#include <errno.h>
#endif //_ERRNO_H

//----------------------------------------------------------------------------
// GLOBAL VARIABLES
//----------------------------------------------------------------------------

const char *ficoserial_type_str_vector[] = {
	"RS232",
	"USB",
	"USB-RS232",
	"INVALID"
};

const char *ficoserial_dataparitystop_str_vector[] = {
	"8N1",
	"7E1",
	"INVALID"
};

//----------------------------------------------------------------------------
// FUNCTIONS
//----------------------------------------------------------------------------

// Prinf serial port configuration
void ficoserial_fprintf_settings(char *dev_str, char *type_str, char *dataparitystop_str, bool flow_ctrl_hw, bool flow_ctrl_sw, struct termios *newtio, FILE *log_fd)
{
	speed_t baudrate = B0;
	int input_baudrate = -1;
	int output_baudrate = -1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}
	
	if (dev_str == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s Error: Invalid device string address\n\n", ficotime_now());
		fflush(log_fd);
#endif //DEBUG
		return;
	}
	else if (type_str == NULL) {
#ifdef DEBUG		
		fprintf(log_fd, "\n%s Error: Invalid type address\n\n", ficotime_now());
		fflush(log_fd);
#endif //DEBUG
		return;
	}
	else if (dataparitystop_str == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s Error: Invalid data/parity/stop string address\n\n", ficotime_now());
		fflush(log_fd);
#endif //DEBUG
		return;
	}
	else if (newtio == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s Error: Invalid newtio address\n\n", ficotime_now());
		fflush(log_fd);
#endif //DEBUG
		return;
	}

	//
	// Input baud rate
	//
	baudrate = cfgetispeed(newtio);
	switch (baudrate) {
	case B0:      input_baudrate = 0; break;
	case B50:     input_baudrate = 50; break;
	case B110:    input_baudrate = 110; break;
	case B134:    input_baudrate = 134; break;
	case B150:    input_baudrate = 150; break;
	case B200:    input_baudrate = 200; break;
	case B300:    input_baudrate = 300; break;
	case B600:    input_baudrate = 600; break;
	case B1200:   input_baudrate = 1200; break;
	case B1800:   input_baudrate = 1800; break;
	case B2400:   input_baudrate = 2400; break;
	case B4800:   input_baudrate = 4800; break;
	case B9600:   input_baudrate = 9600; break;
	case B19200:  input_baudrate = 19200; break;
	case B38400:  input_baudrate = 38400; break;
	case B57600:  input_baudrate = 57600; break;
	case B115200:  input_baudrate = 115200; break;
	case B230400:  input_baudrate = 230400; break;
	case B460800:  input_baudrate = 460800; break;
	case B500000:  input_baudrate = 500000; break;
	case B576000:  input_baudrate = 576000; break;
	case B921600:  input_baudrate = 921600; break;
	case B1000000:  input_baudrate = 1000000; break;
	case B1152000:  input_baudrate = 1152000; break;
	case B1500000:  input_baudrate = 1500000; break;
	case B2000000:  input_baudrate = 2000000; break;
	case B2500000:  input_baudrate = 2500000; break;
	case B3000000:  input_baudrate = 3000000; break;
	case B3500000:  input_baudrate = 3500000; break;
	case B4000000:  input_baudrate = 4000000; break;
	default:
		fprintf(log_fd, "\n%s Error: Invalid input baud rate\n", ficotime_now());
		fflush(log_fd);
		break;
	}		

	//
	// Output baud rate
	//
	memset(&baudrate, 0, sizeof(speed_t)); 
	baudrate = cfgetospeed(newtio);
	switch (baudrate) {
	case B0:      output_baudrate = 0; break;
	case B50:     output_baudrate = 50; break;
	case B110:    output_baudrate = 110; break;
	case B134:    output_baudrate = 134; break;
	case B150:    output_baudrate = 150; break;
	case B200:    output_baudrate = 200; break;
	case B300:    output_baudrate = 300; break;
	case B600:    output_baudrate = 600; break;
	case B1200:   output_baudrate = 1200; break;
	case B1800:   output_baudrate = 1800; break;
	case B2400:   output_baudrate = 2400; break;
	case B4800:   output_baudrate = 4800; break;
	case B9600:   output_baudrate = 9600; break;
	case B19200:  output_baudrate = 19200; break;
	case B38400:  output_baudrate = 38400; break;
	case B57600:  output_baudrate = 57600; break;
	case B115200:  output_baudrate = 115200; break;
	case B230400:  output_baudrate = 230400; break;
	case B460800:  output_baudrate = 460800; break;
	case B500000:  output_baudrate = 500000; break;
	case B576000:  output_baudrate = 576000; break;
	case B921600:  output_baudrate = 921600; break;
	case B1000000:  output_baudrate = 1000000; break;
	case B1152000:  output_baudrate = 1152000; break;
	case B1500000:  output_baudrate = 1500000; break;
	case B2000000:  output_baudrate = 2000000; break;
	case B2500000:  output_baudrate = 2500000; break;
	case B3000000:  output_baudrate = 3000000; break;
	case B3500000:  output_baudrate = 3500000; break;
	case B4000000:  output_baudrate = 4000000; break;
	default:
		fprintf(log_fd, "\n%s Error: Invalid output baud rate\n", ficotime_now());
		fflush(log_fd);
		break;
	}

	fprintf(log_fd, "----------------------------------------------\n");
	fprintf(log_fd, "%s SERIAL PORT INFO:\n", ficotime_now());
	fprintf(log_fd, "\tDevice: %s\n", dev_str);
	fprintf(log_fd, "\tConnection type: %s\n", type_str);
	fprintf(log_fd, "\tData/Parity/Stop: %s\n", dataparitystop_str);
	fprintf(log_fd, "\tInput speed: %d Bps\n", input_baudrate);
	fprintf(log_fd, "\tOutput speed: %d Bps\n", output_baudrate);
	fprintf(log_fd, "\tHW flow control: %s\n", flow_ctrl_hw == 1 ? "ON" : "OFF");
	fprintf(log_fd, "\tSW flow control: %s\n", flow_ctrl_sw == 1 ? "ON" : "OFF");
	fprintf(log_fd, "----------------------------------------------\n");
	fflush(log_fd);
	return;
}

void ficoserial_fprintf_termios_hex(struct termios tio, FILE *log_fd)
{
	u_int8_t i = 0;
	
	if (log_fd == NULL) {
		log_fd = stdout;
	}
	
	fprintf(log_fd, "Input mode flags   (c_iflag): 0x%08x\n", tio.c_iflag);
	fprintf(log_fd, "Output mode flags  (c_oflag): 0x%08x\n", tio.c_oflag);
	fprintf(log_fd, "Control mode flags (c_cflag): 0x%08x\n", tio.c_cflag);
	fprintf(log_fd, "Local mode flags   (c_lflag): 0x%08x\n", tio.c_lflag);
	fprintf(log_fd, "Line discipline     (c_line): 0x%02x\n", tio.c_line);
	fprintf(log_fd, "Control characters:\n");
	for (i = 0; i < NCCS; i++) {
		fprintf(log_fd,"                   (c_cc[%i]): 0x%02x\n", i, tio.c_cc[i]);
	}
	fprintf(log_fd, "Input speed       (c_ispeed): 0x%08x\n", tio.c_ispeed);
	fprintf(log_fd, "Output speed      (c_ospeed): 0x%08x\n", tio.c_ospeed);
	return;
}

// Check serial type
bool ficoserial_check_type_str(char *type_str)
{
	bool retval = 0;
	
	u_int8_t i = 0;
	
	if ((type_str != NULL)
		&& (ficoparse_toupper(&type_str))) {
		for (i = FICOSERIAL_TYPE_RS232; i < FICOSERIAL_TYPE_END; i++) {
			if (strcmp(type_str, ficoserial_type_str_vector[i]) == 0) {
				retval = 1;
				break;
			}
		}
	} 
	return retval;
}

// Get serial type from string
bool ficoserial_get_type_from_str(char *type_str, u_int8_t *type, FILE *log_fd)
{
	bool retval = 0;
	
	u_int8_t i = 0;
	printf("%s\n", __func__);
	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (type_str == NULL) {
		retval = 0;
	}
	else if (!ficoparse_toupper(&type_str)) {
		retval = 0;
	}
	else if (type == NULL) {
		retval = 0;
	}
	else {
		for (i = FICOSERIAL_TYPE_RS232; i < FICOSERIAL_TYPE_END; i++) {
			if (strcmp(type_str, ficoserial_type_str_vector[i]) == 0) {
				*type = i;
				retval = 1;
				break;
			}
		}
	}
	
	printf("%s : retVal=%d\n", __func__, retval);
	return retval;
}

// Check serial baud rate
bool ficoserial_check_baudrate_str(char *baudrate_str)
{
	bool retval = 1;
	
	u_int32_t u32_baudrate = 0;

	if (baudrate_str == NULL) {
		retval = 0;
	}
	else {
		u32_baudrate = strtoul(baudrate_str, NULL, 10);
		switch (u32_baudrate) {
		case 0:
		case 50:
		case 110:
		case 134:
		case 150:
		case 200:
		case 300:
		case 600:
		case 1200:
		case 1800:
		case 2400:
		case 4800:
		case 9600:
		case 19200:
		case 38400:
		case 57600:
		case 115200:
		case 230400:
		case 460800:
		case 500000:
		case 576000:
		case 921600:
		case 1000000:
		case 1152000:
		case 1500000:
		case 2000000:
		case 2500000:
		case 3000000:
		case 3500000:
		case 4000000:
			break;
		default:
			retval = 0;
			break;
		}	
	}
	return retval;
}

// Check serial baud rate
bool ficoserial_get_baudrate_from_str(char *baudrate_str, speed_t *baudrate, FILE *log_fd)
{
	bool retval = 1;
	
	u_int32_t u32_baudrate = 0;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (baudrate_str == NULL) {
		retval = 0;
	}
	else if (baudrate == NULL) {
		retval = 0;
	}
	else {
		u32_baudrate = strtoul(baudrate_str, NULL, 10);
		switch (u32_baudrate) {
		case 0:      *baudrate = B0; break;
		case 50:     *baudrate = B50; break;
		case 110:    *baudrate = B110; break;
		case 134:    *baudrate = B134; break;
		case 150:    *baudrate = B150; break;
		case 200:    *baudrate = B200; break;
		case 300:    *baudrate = B300; break;
		case 600:    *baudrate = B600; break;
		case 1200:   *baudrate = B1200; break;
		case 1800:   *baudrate = B1800; break;
		case 2400:   *baudrate = B2400; break;
		case 4800:   *baudrate = B4800; break;
		case 9600:   *baudrate = B9600; break;
		case 19200:  *baudrate = B19200; break;
		case 38400:  *baudrate = B38400; break;
		case 57600:  *baudrate = B57600; break;
		case 115200:  *baudrate = B115200; break;
		case 230400:  *baudrate = B230400; break;
		case 460800:  *baudrate = B460800; break;
		case 500000:  *baudrate = B500000; break;
		case 576000:  *baudrate = B576000; break;
		case 921600:  *baudrate = B921600; break;
		case 1000000:  *baudrate = B1000000; break;
		case 1152000:  *baudrate = B1152000; break;
		case 1500000:  *baudrate = B1500000; break;
		case 2000000:  *baudrate = B2000000; break;
		case 2500000:  *baudrate = B2500000; break;
		case 3000000:  *baudrate = B3000000; break;
		case 3500000:  *baudrate = B3500000; break;
		case 4000000:  *baudrate = B4000000; break;
		default:
			fprintf(log_fd, "\n%s Error: Invalid baud rate %s\n", ficotime_now(), baudrate_str);
			fflush(log_fd);
			retval = 0;
			break;
		}	
	}
	return retval;
}

// Check serial data/parity/stop
bool ficoserial_check_dataparitystop_str(char *dataparitystop_str)
{
	bool retval = 0;
	
	u_int8_t i = 0;
	
	if ((dataparitystop_str != NULL)
		&& (ficoparse_toupper(&dataparitystop_str))) {
		for (i = FICOSERIAL_DATAPARITYDATA_8N1; i < FICOSERIAL_DATAPARITYDATA_END; i++) {
			if (strcmp(dataparitystop_str, ficoserial_dataparitystop_str_vector[i]) == 0) {
				retval = 1;
				break;
			}
		}
	} 
	return retval;
}

// Get data/parity/stop configuration from string
bool ficoserial_get_dataparitystop_from_str(char *dataparitystop_str, u_int8_t *dataparitystop, FILE *log_fd)
{
	bool retval = 0;
	
	u_int8_t i = 0;
	
	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (dataparitystop_str == NULL) {
		retval = 0;
	}
	else if (!ficoparse_toupper(&dataparitystop_str)) {
		retval = 0;
	}
	else if (dataparitystop == NULL) {
		retval = 0;
	}
	else {
		for (i = FICOSERIAL_DATAPARITYDATA_8N1; i < FICOSERIAL_DATAPARITYDATA_END; i++) {
			if (strcmp(dataparitystop_str, ficoserial_dataparitystop_str_vector[i]) == 0) {
				*dataparitystop = i;
				retval = 1;
				break;
			}
		}
	}
	return retval;
}

// Set serial port configuration
bool ficoserial_set_config(int *serialfd, char *dev_str, char *type_str, char *baudrate_str, char *dataparitystop_str, bool flow_ctrl_hw, bool flow_ctrl_sw, struct termios *oldtio, struct termios *newtio, FILE *log_fd)
{
	u_int8_t type = FICOSERIAL_TYPE_END;
	u_int8_t dataparitystop = FICOSERIAL_DATAPARITYDATA_8N1;
//	speed_t baudrate = B0;
	printf("%s\n", __func__);
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if ((serialfd == NULL)
		|| (!(*serialfd > 0))) {
		retval = 0;
	}
	else if (dev_str == NULL) {
		retval = 0;
	}
	else if (type_str == NULL) {
		retval = 0;
	}
	else if (baudrate_str == NULL) {
		retval = 0;
	}
	else if (dataparitystop_str == NULL) {
		retval = 0;
	}
	else if (oldtio == NULL) {
		retval = 0;
	}
	else if (newtio == NULL) {
		retval = 0;
	}
	else if (!ficoserial_get_type_from_str(type_str, &type, log_fd)) {
		retval = 0;
	}
	else if (!ficoserial_get_dataparitystop_from_str(dataparitystop_str, &dataparitystop, log_fd)) {
		retval = 0;
	}
	else {
		//
		// Clean struct for default port settings
		//
		printf("%s : Clean struct for default port settings\n", __func__);
		memset(oldtio, 0, sizeof(struct termios));
		memset(newtio, 0, sizeof(struct termios));
		//
		// Save current serial port settings
		//
		printf("%s : Save current serial port settings\n", __func__);
		if (tcgetattr(*serialfd, oldtio) != 0) {
			if ((type == FICOSERIAL_TYPE_USB)
				|| (type == FICOSERIAL_TYPE_USB_RS232)) {
				fprintf(log_fd, "\n%s Error: Cannot save current serial port setting (%s)\n\n",
					ficotime_now(),
					strerror(errno));
				fflush(log_fd);
				retval = 0;
			}
#ifdef DEBUG
			else {
				fprintf(log_fd, "%s Warning: Cannot save current serial port setting (%s)\n",
					ficotime_now(),
					strerror(errno));
				fflush(log_fd);
			}
#endif //DEBUG	
		}
#ifdef DEBUG
		fprintf(log_fd, "%s BEFORE: oldtio\n", ficotime_now());
		fflush(log_fd);
		ficoserial_fprintf_termios_hex(*oldtio, log_fd);
#endif //DEBUG		
		//
		// Check serial connection type
		//
#ifdef DEBUG
		fprintf(log_fd, "%s Checking serial connection type (%d)...\n", ficotime_now(), retval);
		fflush(log_fd);
#endif	//DEBUG
		if (retval) {
			switch (type) {
			case FICOSERIAL_TYPE_RS232:
				printf("%s : FICOSERIAL_TYPE_RS232",__func__);
				//
				// TO BE DONE
				//
				fprintf(log_fd, "%s TO BE DONE Configuring RS232 serial connection...\n", ficotime_now());
				fflush(log_fd);
				retval = 0;
//				newtio->c_cflag = CS8;
				break;
			case FICOSERIAL_TYPE_USB:
				printf("%s : FICOSERIAL_TYPE_USB",__func__);
				//
				// TO BE DONE
				//
				fprintf(log_fd, "%s TO BE DONE Configuring USB serial connection...\n", ficotime_now());
				fflush(log_fd);
				retval = 0;
//				// Use old tty parameters
//				newtio = oldtio;
//				// NON - CANONICAL
//				newtio->c_cflag |= (CLOCAL | CREAD);
//				newtio->c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
//				// Raw output
//				newtio->c_oflag &= ~OPOST;
//				//
//				// HW flow control
//				//
//				if (flow_ctrl_hw == 0) {
//					// Disable hardware control
//					newtio->c_iflag &= ~ CRTSCTS;
//				}
//				else {
//					// Enable hardware control
//					newtio->c_iflag |= CRTSCTS;					
//				}
				break;
			case FICOSERIAL_TYPE_USB_RS232:
				printf("%s : FICOSERIAL_TYPE_USB_RS232",__func__);
#ifdef DEBUG
				fprintf(log_fd, "%s Configuring USB-RS232 serial connection...\n", ficotime_now());
				fflush(log_fd);
#endif	//DEBUG

////////////////////////////////////////////////////////////////////////
///////////////				GUL				////////////////////////////
////////////////////////////////////////////////////////////////////////
				*newtio = *oldtio;
				// NON - CANONICAL
				newtio->c_cflag |= (CLOCAL | CREAD);
				newtio->c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
				// Raw output
				newtio->c_oflag &= ~OPOST;
				
				newtio->c_cflag &=  ~PARENB;            	// Make 8n1
				newtio->c_cflag &=  ~CSTOPB;
				newtio->c_cflag &=  ~CSIZE;
				newtio->c_cflag |=  CS8;

				newtio->c_iflag &= ~ CRTSCTS;			// Disable hardware control
				//newtio->c_iflag |= CRTSCTS;			// Enable hardware control
				
				newtio->c_iflag &= ~(IXON | IXOFF | IXANY);	// Disable software control
				//newtio->c_iflag |= (IXON | IXOFF | IXANY);	// Enable software control
				
				newtio->c_cc[VTIME] = 0;
				newtio->c_cc[VMIN] = 1;

				// Set speed
				cfsetispeed(newtio, (speed_t)B57600);
				cfsetospeed(newtio, (speed_t)B57600);
				
				// Clean the modem line and activate the settings for the port
				tcflush(*serialfd, TCIOFLUSH);
				tcsetattr(*serialfd, TCSANOW, newtio);
				break;
			default:
				fprintf(log_fd, "\n%s Error: Unknown serial connection type (id %d). It must be [RS232|USB|USB-RS232]\n",
					ficotime_now(),
					type);
				fflush(log_fd);
				retval = 0;
				break;
			}
		}
/*
		//
		// Check baudrate
		//
#ifdef DEBUG
		fprintf(log_fd, "%s Checking baud rate (%d)...\n", ficotime_now(), retval);
		fflush(log_fd);
#endif	//DEBUG
		if (retval) {
#ifdef DEBUG
			fprintf(log_fd, "%s Getting baud rate for \"%s\" bps...\n", ficotime_now(), baudrate_str);
			fflush(log_fd);
#endif //DEBUG
			if (!ficoserial_get_baudrate_from_str(baudrate_str, &baudrate, log_fd)) {
				fprintf(log_fd, "\n%s Error: Cannot get a valid baud rate for \"%s\" bps\n\n", ficotime_now(), baudrate_str);
				fflush(log_fd);				
				retval = 0;
			}
			else {
				//
				// Set baudrate
				//
#ifdef DEBUG
				fprintf(log_fd, "%s Setting baudrate for \"%s\" bps (0x%08x)...\n", ficotime_now(), baudrate_str, baudrate);
				fflush(log_fd);
#endif //DEBUG
				cfsetispeed(newtio, baudrate);
				cfsetospeed(newtio, baudrate);
			}
		}
		//
		// Check data/parity/stop
		//
#ifdef DEBUG
		fprintf(log_fd, "%s Checking data/parity/stop (%d)...\n", ficotime_now(), retval);
		fflush(log_fd);
#endif	//DEBUG
		if (retval) {
			switch (dataparitystop) {
			case FICOSERIAL_DATAPARITYDATA_8N1:
				//
				// 8N1
				//
#ifdef DEBUG
				fprintf(log_fd, "%s Setting %s serial configuration...\n", ficotime_now(), ficoserial_dataparitystop_str_vector[dataparitystop]);
				fflush(log_fd);
#endif	//DEBUG
				newtio->c_cflag &=  ~PARENB;
				newtio->c_cflag &=  ~CSTOPB;
				newtio->c_cflag &=  ~CSIZE;
				newtio->c_cflag |=  CS8;
				break;
			case FICOSERIAL_DATAPARITYDATA_7E1:
				//
				// TO BE DONE: 7E1
				//
				fprintf(log_fd, "%s TO BE DONE Setting %s serial configuration\n", ficotime_now(), ficoserial_dataparitystop_str_vector[dataparitystop]);
				fflush(log_fd);
				retval = 0;
				break;
			default:
				fprintf(log_fd, "\n%s Error: Unknown data/parity/stop serial configuration\n\n", ficotime_now());
				fflush(log_fd);
				retval = 0;
				break;
			}
		}
		//
		// Check HW flow control 
		//
#ifdef DEBUG
		fprintf(log_fd, "%s Checking HW flow control (%d)...\n", ficotime_now(), retval);
		fflush(log_fd);
#endif	//DEBUG
		if (retval) {
			if (flow_ctrl_hw == 0) {
				// Disable hardware control
				newtio->c_iflag &= ~ CRTSCTS;
				//
				// Seen in a web page: newtio->c_cflag &= ~CRTSCTS;
				//
			}
			else {
				// Enable hardware control
				newtio->c_iflag |= CRTSCTS;
				//
				// Seen in a web page: newtio->c_cflag |= CRTSCTS;
				//
			}
		}
		//
		// Check SW flow control
		//
#ifdef DEBUG
		fprintf(log_fd, "%s Checking SW flow control (%d)...\n", ficotime_now(), retval);
		fflush(log_fd);
#endif	//DEBUG
		if (retval) {
			if (flow_ctrl_sw == 0) {
				// Disable software control
				newtio->c_iflag &= ~(IXON | IXOFF | IXANY);
			}
			else {
				// Enable software control
				newtio->c_iflag |= (IXON | IXOFF | IXANY);
			}
		}
		//
		// Set rest of parameters
		//
#ifdef DEBUG
		fprintf(log_fd, "%s Set rest of params (%d)...\n", ficotime_now(), retval);
		fflush(log_fd);
#endif	//DEBUG
		if (retval) {
			// Set read timeout
			newtio->c_cc[VTIME] = 0;
			// Set read doesn't block
			newtio->c_cc[VMIN] = 1;
///////////////////////////////////////////////////////////////////
			// NON - CANONICAL
			newtio->c_cflag |= (CLOCAL | CREAD);
			//newtio->c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
			// Raw output
			//newtio->c_oflag &= ~OPOST;
			cfmakeraw(newtio);
///////////////////////////////////////////////////////////////////

			// Flush the port and applies attributes
			tcflush(*serialfd, TCIOFLUSH);
			if (tcsetattr(*serialfd, TCSANOW, newtio) != 0) {
				fprintf(log_fd, "\n%s Error: Cannot set new serial port setting (%s)\n\n",
					ficotime_now(),
					strerror(errno));
				fflush(log_fd);
				retval = 0;
			}
		}
*/
	}
	if (!retval) {
		// Restore old configuration
		if ((serialfd != NULL)
			&& (!(*serialfd > 0))
			&& (oldtio != NULL)) {
			// Flush the port and applies attributes
			tcflush(*serialfd, TCIOFLUSH);
			if (tcsetattr(*serialfd, TCSANOW, oldtio) != 0) {
				fprintf(log_fd, "\n%s Error: Cannot set new serial port setting (%s)\n\n",
					ficotime_now(),
					strerror(errno));
				fflush(log_fd);
				retval = 0;
			}
		}
	}
#ifdef DEBUG
	fprintf(log_fd, "%s AFTER: newtio\n", ficotime_now());
	fflush(log_fd);
	ficoserial_fprintf_termios_hex(*newtio, log_fd);
#endif //DEBUG
	return retval;
}

// Connect to serial port
bool ficoserial_init_dev(char *dev_str, char *type_str, char *baudrate_str, char *dataparitystop_str, bool flow_ctrl_hw, bool flow_ctrl_sw, struct termios *oldtio, struct termios *newtio, FILE *log_fd)
{
	bool retval = 1;
	
	int serialfd = -1;
	printf("%s\n", __func__);
	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (dev_str == NULL) {
		retval = 0;
	}
	else if (type_str == NULL){
		retval = 0;
	} 
	else if (baudrate_str == NULL) {
		retval = 0;
	}
	else if (dataparitystop_str == NULL) {
		retval = 0;
	}
	else if (oldtio == NULL) {
		retval = 0;
	}
	else if (newtio == NULL) {
		retval = 0;
	}
	else {
		// Open modem device for reading and writing and not as controlling tty
		// because we don't want to get killed if linenoise sends CTRL-C.
		serialfd = open(dev_str, O_RDWR | O_NOCTTY | O_NDELAY);
		if (serialfd == -1) {
			fprintf(log_fd, "%s Error: Unable to open %s device\n", ficotime_now(), dev_str);
			printf("%s Error: Unable to open %s device\n", ficotime_now(), dev_str);
			fflush(log_fd);
			retval = 0;
		}
		else {
			//
			// Block serial read behavior
			//
			printf("%s : Block serial read behavior\n", __func__);
			fcntl(serialfd, F_SETFL, 0);			
			//
			// Set serial port configuration options
			//
			if (!ficoserial_set_config(&serialfd, dev_str, type_str, baudrate_str, dataparitystop_str, flow_ctrl_hw, flow_ctrl_sw, oldtio, newtio, log_fd)) {
				fprintf(log_fd, "%s Error: Cannot set serial port configuration on %s\n", ficotime_now(), dev_str);
				printf("%s Error: Cannot set serial port configuration on %s\n", ficotime_now(), dev_str);
				fflush(log_fd);
				retval = 0;
			}
			else {
				printf("%s Set serial port configuration on %s\n", ficotime_now(), dev_str);
			}
			
			if (!(serialfd < 0)) {
				// Disconnect serial device
				if (!ficoserial_disconnect(&serialfd, oldtio, dev_str, log_fd)) {
					fprintf(log_fd, "\n%s Error: Cannot disconnect from %s\n\n", ficotime_now(), dev_str);
					printf("\n%s Error: Cannot disconnect from %s\n\n", ficotime_now(), dev_str);
					fflush(log_fd);
					retval = 0;
				}
				else {
					printf("\n%s Disconnected from %s\n\n", ficotime_now(), dev_str);
				}
			}
#ifdef DEBUG
			//
			// Print serial port settings
			//
			ficoserial_fprintf_settings(dev_str, type_str, dataparitystop_str, flow_ctrl_hw, flow_ctrl_sw, newtio, log_fd);
#endif	//DEBUG
		}
	}
	return retval;
}

// Connect to serial port
bool ficoserial_connect(int *serialfd, char *dev_str, struct termios *currenttio, FILE *log_fd)
{
	bool retval = 1;
	printf("%s\n", __func__);
	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (serialfd == NULL) {
		retval = 0;
	}
	else if (dev_str == NULL) {
		retval = 0;
	}
	else if (currenttio == NULL) {
		retval = 0;
	}
	else {
		if (!(*serialfd < 0)) {
			//
			// Disconnect serial device
			//
			if (!ficoserial_disconnect(serialfd, currenttio, dev_str, log_fd)) {
				fprintf(log_fd, "\n%s Error: Cannot disconnect from %s\n\n", ficotime_now(), dev_str);
				fflush(log_fd);
				retval = 0;
			}
		}
		if (retval) {
			// Open modem device for reading and writing and not as controlling tty
			// because we don't want to get killed if linenoise sends CTRL-C.
			*serialfd = open(dev_str, O_RDWR | O_NOCTTY | O_NDELAY);
			if (*serialfd == -1) {
				fprintf(log_fd, "%s Error: Unable to open %s device\n\n", ficotime_now(), dev_str);
				fflush(log_fd);
				retval = 0;
			}
			else {
				// Block serial read behavior
				fcntl(*serialfd, F_SETFL, 0);
			}
		}
	}
	return retval;
}

// Disconnect from serial port
bool ficoserial_disconnect(int *serialfd, struct termios *oldtio, char *dev_str, FILE *log_fd)
{
	bool retval = 1;
	printf("%s\n", __func__);
	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (dev_str == NULL) {
		retval = 0;
	}
	else if (serialfd == NULL) {
		retval = 0;
	}
	else if (!(*serialfd < 0)) {
		//
		// Set old configuration
		//
		tcsetattr(*serialfd, TCSANOW, oldtio);
		//
		// Close connection
		//
		if (close(*serialfd) < 0) {
			fprintf(log_fd, "\n%s Error: Cannot close serial file descriptor <%d>\n\n", ficotime_now(), *serialfd);
			fflush(log_fd);
			retval = 0;
		}
		else {
#ifdef DEBUG
			fprintf(log_fd, "%s Serial file descriptor <%d> has been closed\n",  ficotime_now(), *serialfd);
			fflush(log_fd);
#endif //DEBUG
			*serialfd = -1;
		}
	}
	return retval;
}

// Write 
bool ficoserial_write(int serialfd, u_int8_t *frame, u_int8_t frame_len, char *dev_str, FILE *log_fd)
{
	bool retval = 1;
	
	u_int8_t *tmp = frame;
	int tmp_len = -1; 
	int nbytes = 0;
	int offset  = 0;
	bool done = 0;
	

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (serialfd < 0) {
		retval = 0;
	}
	else if (dev_str == NULL) {
		retval = 0;
	}
	else if (frame == NULL) {
		retval = 0;
	}
	else if (!(frame_len > 0)) {
#ifdef DEBUG
		fprintf(log_fd, "%s Warning: %d byte length. Don't write\n", ficotime_now(), frame_len);
		fflush(log_fd);
#endif //DEBUG
		retval = 0;
	}
	else {
#ifdef DEBUG
		fprintf(log_fd, "%s Writting %d bytes on %s: ",
			ficotime_now(),
			frame_len,
			dev_str);
		fflush(log_fd);
		ficoprintf_uint8_hex(frame, frame_len, log_fd);
#endif	//DEBUG
		//
		// Disable data reception
		//
		tcflow(serialfd, TCIOFF);
		//
		// Clean serial drive's input queue
		//
		tcflush(serialfd, TCIFLUSH);
		//
		// Enable data transmission
		//
		tcflow(serialfd, TCOON);
		//
		// Clean serial driver's output queue before writing on it
		//
		tcflush(serialfd, TCOFLUSH);
		//
		// Write on serial port
		//
		do {
			offset += nbytes;
			tmp = (frame + offset);
			tmp_len = frame_len - offset;
			if (!(tmp_len > 0)) {
#ifdef DEBUG
				fprintf(log_fd, "%s Error: %d bytes remaining for writting. Don't write\n\n", ficotime_now(), (int)tmp_len);
				fflush(log_fd);
#endif //DEBUG
				retval = 0;
			}
			else if ((!(nbytes = write(serialfd, tmp, tmp_len)) > 0)) { 
				fprintf(log_fd, "\n%s Error: Cannot write on %s\n\n",
					ficotime_now(),
					dev_str);
				fflush(log_fd);
				retval = 0;
				done = 1;
			}
			else if ((offset + nbytes) < frame_len) {
				fprintf(log_fd, "%s Warning: Written %d bytes from %d bytes %s\n",
					ficotime_now(),
					(offset + nbytes),
					frame_len,
					dev_str);
				fflush(log_fd);
			}
			else {
#ifdef DEBUB
				fprintf(log_fd, "%s Written %d bytes from %d bytes %s\n",
					ficotime_now(),
					(offset + nbytes),
					frame_len,
					dev_str);
				fflush(log_fd);
#endif //DEBUG
				done = 1;
				//
				// Transmit all output queue
				//
				tcdrain(serialfd);
			}
		}
		while (done == 0);
	}
	return retval;
}

// Write string
bool ficoserial_write_str(int serialfd, char *frame_str, char *dev_str, FILE *log_fd)
{
	bool retval = 1;
	
	char *tmp_str = frame_str;
	size_t tmp_strlen = -1; 
	int nbytes = 0;
	int offset  = 0;
	bool done = 0;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (serialfd < 0) {
		retval = 0;
	}
	else if (dev_str == NULL) {
		retval = 0;
	}
	else if (frame_str == NULL) {
		retval = 0;
	}
	else if (!(strlen(frame_str) > 0)) {
#ifdef DEBUG
		fprintf(log_fd, "%s Warning: %d byte string length. Don't write\n", ficotime_now(), (int)strlen(frame_str));
		fflush(log_fd);
#endif //DEBUG
		retval = 0;
	}
	else {
#ifdef DEBUG
		fprintf(log_fd, "%s Writting \"%s\" (%d bytes) on %s...\n",
			ficotime_now(),
			frame_str,
			(int) strlen(frame_str),
			dev_str);
		fflush(log_fd);
#endif	//DEBUG
		//
		// Disable data reception
		//
		tcflow(serialfd, TCIOFF);
		//
		// Clean serial drive's input queue
		//
		tcflush(serialfd, TCIFLUSH);
		//
		// Enable data transmission
		//
		tcflow(serialfd, TCOON);
		//
		// Clean serial driver's output queue before writing on it
		//
		tcflush(serialfd, TCOFLUSH);
		//
		// Write on serial port
		//
		do {
			offset += nbytes;
			tmp_str = (frame_str + offset);
			tmp_strlen = strlen(tmp_str);
			if (!((tmp_strlen = strlen(tmp_str)) > 0)) {
#ifdef DEBUG
				fprintf(log_fd, "%s Warning: %d bytes remaining for writting. Don't write\n", ficotime_now(), (int)tmp_strlen);
				fflush(log_fd);
#endif //DEBUG
				retval = 0;
			}
			else if ((!(nbytes = write(serialfd, tmp_str, tmp_strlen)) > 0)) { 
				fprintf(log_fd, "\n%s Error: Cannot write \"%s\" (%d bytes) on %s\n\n",
					ficotime_now(),
					frame_str,
					(int)strlen(frame_str),
					dev_str);
				fflush(log_fd);
				retval = 0;
				done = 1;
			}
			else if (offset + nbytes < strlen(frame_str)) {
				fprintf(log_fd, "%s Warning: Written  %d bytes from %d bytes %s\n",
					ficotime_now(),
					(offset + nbytes),
					(int)strlen(frame_str),
					dev_str);
				fflush(log_fd);
			}
			else {
#ifdef DEBUB
				fprintf(log_fd, "%s Written  %d bytes from %d bytes %s\n",
					ficotime_now(),
					(offset + nbytes),
					strlen(frame_str),
					dev_str);
				fflush(log_fd);
#endif //DEBUG
				done = 1;
				//
				// Transmit all output queue
				//
				tcdrain(serialfd);
			}
		}
		while (done == 0);
	}
	return retval;
}

// Read nbytes
bool ficoserial_read_nbytes(int serialfd, u_int8_t **read_frame, u_int8_t read_frame_len, char *dev_str,  FILE *log_fd)
{
	bool retval = 1;
	
	u_int8_t buff[FICOSERIAL_RX_BUFFER_MAXSTRLEN];
	int nbytes = 0;
	u_int8_t  *tmp_buff = NULL;
	u_int offset = 0;
	bool done = 0;
	
	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (serialfd < 0) {
		retval = 0;
	}
	else if (read_frame == NULL) {
		retval = 0;
	}
	else if (dev_str == NULL) {
		retval = 0;
	}
	else {
#ifdef DEBUG
		fprintf(log_fd, "%s Reading line on %s...\n",
			ficotime_now(),
			dev_str);
		fflush(log_fd);
#endif	//DEBUG
		//
		// Clear return variable
		//
		if (*read_frame != NULL) {
			free(*read_frame);
			*read_frame = NULL;
		}
		//
		// Disable data transmission
		//
		tcflow(serialfd, TCOOFF);
		//
		// Enable data reception
		//
		tcflow(serialfd, TCION);
		//
		// Wait to read response
		//
		do {
			//
			// Clean buffer
			//	
			memset(buff, '\0', FICOSERIAL_RX_BUFFER_MAXSTRLEN); 
			//
			// Read serial port
			//
			if ((nbytes = read(serialfd, buff, FICOSERIAL_RX_BUFFER_MAXSTRLEN)) < 0) {
				if (errno == EAGAIN) {
					fprintf(log_fd, "%s Warning: Read response failed on %s\n",
						ficotime_now(),
						dev_str);
					fflush(log_fd);
					done = 0;
					retval = 0;
				} else {
					fprintf(log_fd, "\n%s Error: Read response failed (%d - %s)\n\n",
						ficotime_now(),
						errno,
						strerror(errno));
					fflush(log_fd);
					done = 1;
					retval = 0;
				}
			}
			else {
#ifdef DEBUG
				fprintf(log_fd, "%s Read %d bytes response: ",
					ficotime_now(),
					nbytes);
				fflush(log_fd);
				ficoprintf_uint8_hex(buff, nbytes, log_fd);
#endif	//DEBUG
				if (offset == 0) {
					//
					// Initialize read buffer
					//
#ifdef DEBUG
					fprintf(log_fd, "%s %d BYTES RECEIVED (offset: %d bytes) (malloc)\n", ficotime_now(), nbytes, offset);
					fflush(log_fd);
#endif //DEBUG					
					//
					// Copy all the buffer
					//
					if (((*read_frame) = (u_int8_t *)malloc(nbytes)) == NULL) {
#ifdef DEBUG
						fprintf(log_fd, "\n%s Error: Cannot copy from serial buffer\n\n", ficotime_now());
						fflush(log_fd);
#endif //DEBUB
						retval = 0;
						done = 1;
					}
					else {
						memset((*read_frame), 0, nbytes);
						memcpy((*read_frame), buff, nbytes);
						offset = nbytes;
						//
						// Check if read_frame_len bytes has been read
						//
						if (offset == read_frame_len) {
#ifdef DEBUG
							fprintf(log_fd, "Finish reading read_frame (%d nbytes VS %d offset VS %d read_len) : ",
								nbytes,
								offset,
								read_frame_len);
							fflush(log_fd);
							ficoprintf_uint8_hex((*read_frame), offset, log_fd); 
#endif //DEBUG
							done = 1;
						}
#ifdef DEBUG
						else {
							fprintf(log_fd, "Continue reading read_frame (%d nbytes VS %d offset VS %d read_len): ",
								nbytes,
								offset,
								read_frame_len);
							fflush(log_fd);
							ficoprintf_uint8_hex((*read_frame), offset, log_fd); 
						}
#endif //DEBUG
					}					
				}
				else {
					//
					// Initialize tmp buffer
					//
#ifdef DEBUG
					fprintf(log_fd, "%s %d BYTES RECEIVED (offset: %d bytes) (realloc)\n", ficotime_now(), nbytes, offset);
					fflush(log_fd);
#endif //DEBUG
					if ((tmp_buff = (u_int8_t *)malloc(offset)) == NULL) {
#ifdef DEBUG
						fprintf(log_fd, "\n%s Error: Cannot copy read_frame to tmp serial buffer\n\n", ficotime_now());
						fflush(log_fd);
#endif //DEBUB
						offset = 0;
						done = 1;
						retval = 0;
					}
					else {
						memset(tmp_buff, 0, nbytes);
						memcpy(tmp_buff, (*read_frame), offset);
						
						if ((offset + nbytes) > read_frame_len) {
							fprintf(log_fd, "%s Warning: %d + %d > %d bytes. Copy only %d bytes\n", ficotime_now(), offset, nbytes, read_frame_len, (read_frame_len - offset));
							fflush(log_fd);
							nbytes = read_frame_len - offset;
#ifdef DEBUG
							fprintf(log_fd, "%s Copy: ", ficotime_now());
							fflush(log_fd);
							ficoprintf_uint8_hex(buff, nbytes, log_fd);
#endif //DEBUG							
						}
						//
						// Copy to buffer
						//
						if ((*read_frame = (u_int8_t *)realloc(*read_frame, (offset + nbytes))) == NULL) {
#ifdef DEBUG
							fprintf(log_fd, "\n%s Error: Cannot copy from serial buffer\n\n", ficotime_now());
							fflush(log_fd);
#endif //DEBUB
							retval = 0;
							done = 1;
						}
						else {
							memset(*read_frame, 0, (offset + nbytes));
							memcpy(*read_frame, tmp_buff, offset);
							memcpy(*read_frame + offset, buff, nbytes);
							offset += nbytes;								
							//
							// Check if read_frame_len bytes has been read
							//
							if (offset == read_frame_len) {
#ifdef DEBUG
								fprintf(log_fd, "Finish reading read_frame (%d nbytes VS %d offset VS %d read_len) : ",
									nbytes,
									offset,
									read_frame_len);
								fflush(log_fd);
								ficoprintf_uint8_hex((*read_frame), offset, log_fd); 
#endif //DEBUG
								done = 1;
							}
#ifdef DEBUG
							else {
								fprintf(log_fd, "Continue reading read_frame (%d nbytes VS %d offset VS %d read_len): ",
									nbytes,
									offset,
									read_frame_len);
								fflush(log_fd);
								ficoprintf_uint8_hex((*read_frame), offset, log_fd); 
							}
#endif //DEBUG
						}
					}
				}
			}
				
			if (nbytes == 0) {
#ifdef DEBUG
				fprintf(log_fd, "%s Finish reading from %s (%d nbytes VS %d offset VS %d read_len):\n", ficotime_now(), dev_str, nbytes, offset, read_frame_len);
				fflush(log_fd);
#endif //DEBUG
				done = 1;
			}
//			else {
//				fprintf(stdout, "Continue reading: read_line_str =\"%s\" (%d VS %d || %d VS %d) || buff[%d]:%#x-\"%c\"\n",
//					*read_line_str,
//					(int)strlen(*read_line_str),
//					offset,
//					nbytes,
//					(int)strlen(buff),
//					(nbytes - 1),
//					buff[(nbytes - 1)],
//					(int)buff[(nbytes - 1)]);
//				fflush(stdout);
//			}
#ifdef DEBUG
			fprintf(log_fd, "%s Done: %d\n", ficotime_now(), done);
			fflush(log_fd);
#endif //DEBUG

		}
		while (done == 0);
		
		if (offset == 0) {
			fprintf(log_fd, "%s Warning: Nothing has been read from %s\n", ficotime_now(), dev_str);
			fflush(log_fd);
			retval = 0;
		}		
	}
	
	if (!retval) {
		if ((read_frame != NULL)
				&& (*read_frame != NULL)) {
			free(*read_frame);
			*read_frame = NULL;
		}
	}
#ifdef DEBUG
	else {
		fprintf(log_fd, "%s Read from %s (%d offset VS %d read_len): ", ficotime_now(), dev_str, offset, read_frame_len);
		fflush(log_fd);
		ficoprintf_uint8_hex(*read_frame, offset, log_fd);
	}
#endif //DEBUG
	if (tmp_buff != NULL) {
		free(tmp_buff);
		tmp_buff = NULL;
	}
	return retval;
}

// Check read 
bool ficoserial_read_check(int serialfd, u_int8_t *rep_frame, u_int8_t rep_frame_len, char *dev_str, FILE *log_fd)
{
	bool retval = 1;
	
	u_int8_t *read_frame = NULL;
	
	u_int8_t i = 0;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (serialfd < 0) {
		retval = 0;
	}
	else if (rep_frame == NULL) {
		retval = 0;
	}
	else if (dev_str == NULL) {
		retval = 0;
	}
	else if (!(rep_frame_len > 0)) {
		fprintf(log_fd, "%s Warning: %d byte string length. Don't read\n", ficotime_now(), rep_frame_len);
		fflush(log_fd);
		retval = 0;
	}
	else {
#ifdef DEBUG
		fprintf(log_fd, "%s Waiting %d bytes from %s: ",
			ficotime_now(),
			rep_frame_len,
			dev_str);
		fflush(log_fd);
		ficoprintf_uint8_hex(rep_frame, rep_frame_len, log_fd);
#endif	//DEBUG
		//
		// Read line string
		//
		if (!ficoserial_read_nbytes(serialfd, &read_frame, rep_frame_len, dev_str, log_fd)) {
			fprintf(log_fd, "\n%s Error: Cannot read %d bytes from %s\n\n", ficotime_now(), rep_frame_len, dev_str);
			fflush(log_fd);
			retval = 0;
		}
		else {
			for (i = 0; i < rep_frame_len; i++) {
#ifdef DEBUG
				fprintf(log_fd, "%s: read_frame[%d] = 0x%02x VS rep_frame[%d] = 0x%02x\n",
						ficotime_now(),
						i,
						*(read_frame + i),
						i,
						*(rep_frame + i));
				fflush(log_fd);
#endif //DEBUG 
				if (*(read_frame + i) != *(rep_frame + i)) {
					retval = 0;
					break;
				}
			}
#ifdef DEBUG
			fprintf(log_fd, "%s Response %s\n",
				ficotime_now(),
				(retval? "OK": "KO"));
			fflush(log_fd);
#endif //DEBUG
		}
	}
	if (read_frame != NULL) {
		free(read_frame);
		read_frame = NULL;
	}
	return retval;
}


// Read line
bool ficoserial_read_line_str(int serialfd, char **read_line_str, char end_of_line_char, char *dev_str,  FILE *log_fd)
{
	bool retval = 1;
	
	char buff[FICOSERIAL_RX_BUFFER_MAXSTRLEN];
	int nbytes = 0;
	char  *tmp_buff_str = NULL;
	u_int offset = 0;
	bool done = 0;
	
#ifdef DEBUG
	int i = 0;
#endif //DEBUG

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (serialfd < 0) {
		retval = 0;
	}
	else if (read_line_str == NULL) {
		retval = 0;
	}
	else if (dev_str == NULL) {
		retval = 0;
	}
	else {
#ifdef DEBUG
		fprintf(log_fd, "%s Reading line on %s...\n",
			ficotime_now(),
			dev_str);
		fflush(log_fd);
#endif	//DEBUG
		//
		// Clear return variable
		//
		ficostring_free(read_line_str);
		//
		// Disable data transmission
		//
		tcflow(serialfd, TCOOFF);
		//
		// Enable data reception
		//
		tcflow(serialfd, TCION);
		//
		// Wait to read response
		//
		do {
			//
			// Clean buffer
			//	
			memset(buff, '\0', FICOSERIAL_RX_BUFFER_MAXSTRLEN); 
			//
			// Read serial port
			//
			if ((nbytes = read(serialfd, buff, FICOSERIAL_RX_BUFFER_MAXSTRLEN)) < 0) {
				if (errno == EAGAIN) {
					fprintf(log_fd, "%s Warning: Read response failed on %s\n",
						ficotime_now(),
						dev_str);
					fflush(log_fd);
					done = 0;
					retval = 0;
				} else {
					fprintf(log_fd, "\n%s Error: Read response failed (%d - %s)\n\n",
						ficotime_now(),
						errno,
						strerror(errno));
					fflush(log_fd);
					done = 1;
					retval = 0;
				}
			}
			else {
#ifdef DEBUG
				fprintf(log_fd, "%s Read %d bytes response: \"%s\"",
					ficotime_now(),
					nbytes,
					buff);
				for (i = 0; i < nbytes; i++) {
					fprintf(log_fd, "0x%02x ", buff[i]);
				}
				fprintf(log_fd, "\n");
				fflush(log_fd);
#endif	//DEBUG
				if (offset == 0) {
					//
					// Initialize read line buffer
					//
					if (((int)buff[(nbytes - 1)]) == end_of_line_char) {
//#ifdef DEBUG
//						fprintf(log_fd, "%s LINE WITH END LINE CHAR RECEIVED (offset: %d bytes) (malloc)\n", ficotime_now(), offset);
//						fflush(log_fd);
//#endif //DEBUG
						//
						// Don't copy END LINE CHAR
						//
						if (((*read_line_str) = (char *)malloc(strlen(buff))) != NULL) {
							memset((*read_line_str), '\0', strlen(buff));
							memcpy((*read_line_str), buff, (strlen(buff)- 1));
							offset = strlen(buff) - 1;
#ifdef DEBUG
							fprintf(stdout, "Finish reading read_line_str =\"%s\" (%d VS %d || %d VS %d)\n",
								*read_line_str,
								(int)strlen(*read_line_str),
								offset,
								nbytes,
								(int)strlen(buff));
							fflush(stdout);
#endif //DEBUG							
						}
						else {
#ifdef DEBUG
							fprintf(log_fd, "\n%s Error: Cannot copy string from serial buffer\n\n", ficotime_now());
							fflush(log_fd);
#endif //DEBUB
							retval = 0;
						}
						done = 1;
					}
					else {
//#ifdef DEBUG
//						fprintf(log_fd, "%s No END LINE CHAR received (offset: %d bytes) (malloc)\n", ficotime_now(), offset);
//						fflush(log_fd);
//#endif //DEBUG
						//
						// Copy all buffer
						//					
						if (!ficostring_copy(read_line_str, buff)) {
							fprintf(log_fd, "\n%s Error: Cannot copy string from serial buffer\n\n", ficotime_now());
							fflush(log_fd);
							ficostring_free(read_line_str);
							done = 1;
							retval = 0;
						}
						else {
							offset += strlen(buff);
#ifdef DEBUG
							fprintf(stdout, "Continue reading read_line_str =\"%s\" (%d VS %d || %d VS %d)\n",
								*read_line_str,
								(int)strlen(*read_line_str),
								offset,
								nbytes,
								(int)strlen(buff));
							fflush(stdout);
#endif //DEBUG
						}
					}
				}
				else {
					//
					// Initialize tmp buffer
					//
					if (!ficostring_copy(&tmp_buff_str, *read_line_str)) {
						fprintf(log_fd, "\n%s Error: Cannot copy string to tmp serial string buffer\n\n", ficotime_now());
						fflush(log_fd);
						offset = 0;
						done = 1;
						retval = 0;
					}
					else {
//#ifdef DEBUG
//						fprintf(log_fd, "%s buff[(nbytes - 1)]  = %#x VS %#x VS %#x (offset: %d)\n",
//							ficotime_now(),
//							(int)buff[(nbytes - 1)],
//							FICOSERIAL_END_LINE,
//							FICOSERIAL_CR,
//							offset);
//						fflush(log_fd);
//#endif //DEBUG						
						//if (((int)buff[(nbytes - 1)]) == FICOSERIAL_END_LINE) {
						if (((int)buff[(nbytes - 1)]) == end_of_line_char) {
//#ifdef DEBUG
//							fprintf(log_fd, "%s LINE WITH END LINE CHAR RECEIVED (offset: %d bytes) (realloc)\n", ficotime_now(), offset);
//							fflush(log_fd);
//#endif //DEBUG
							//
							// Don't copy END LINE CHAR
							//
							if ((*read_line_str = (char *)realloc(*read_line_str, (strlen(tmp_buff_str) + strlen(buff)))) != NULL) {
								memset(*read_line_str, '\0', (strlen(tmp_buff_str) + strlen(buff)));
								memcpy(*read_line_str, tmp_buff_str, strlen(tmp_buff_str));
								offset = strlen(tmp_buff_str);
								memcpy(*read_line_str + offset, buff, (strlen(buff) - 1));
								offset += (strlen(buff) - 1);
#ifdef DEBUG
								fprintf(stdout, "Finish reading read_line_str =\"%s\" (%d VS %d || %d VS %d)\n",
									*read_line_str,
									(int)strlen(*read_line_str),
									offset,
									nbytes,
									(int)strlen(buff));
								fflush(stdout);
#endif //DEBUG	
							}
							else {
								retval = 0;
							}
							done = 1;							
						}
						else {
//#ifdef DEBUG
//							fprintf(log_fd, "%s No END LINE CHAR received (offset: %d bytes) (realloc)\n", ficotime_now(), offset);
//							fflush(log_fd);
//#endif //DEBUG
							//
							// Copy all buffer
							//
							if ((*read_line_str = (char *)realloc(*read_line_str, (strlen(tmp_buff_str) + strlen(buff) + 1))) != NULL) {
								memset(*read_line_str, '\0', (strlen(tmp_buff_str) + strlen(buff) + 1));
								memcpy(*read_line_str, tmp_buff_str, strlen(tmp_buff_str));
								offset = strlen(tmp_buff_str);
								memcpy(*read_line_str + offset, buff, strlen(buff));
								offset += strlen(buff);
#ifdef DEBUG
								fprintf(stdout, "Continue reading read_line_str =\"%s\" (%d VS %d || %d VS %d)\n",
									*read_line_str,
									(int)strlen(*read_line_str),
									offset,
									nbytes,
									(int)strlen(buff));
								fflush(stdout);
#endif //DEBUG
							}
							else {
								done = 1;
								retval = 0;
							}
						}
					}
				}
			}
				
			if (nbytes == 0) {
#ifdef DEBUG
				fprintf(log_fd, "%s Finish reading from %s (%d bytes)\n", ficotime_now(), dev_str, nbytes);
				fflush(log_fd);
#endif //DEBUG
				done = 1;
			}
//			else {
//				fprintf(stdout, "Continue reading: read_line_str =\"%s\" (%d VS %d || %d VS %d) || buff[%d]:%#x-\"%c\"\n",
//					*read_line_str,
//					(int)strlen(*read_line_str),
//					offset,
//					nbytes,
//					(int)strlen(buff),
//					(nbytes - 1),
//					buff[(nbytes - 1)],
//					(int)buff[(nbytes - 1)]);
//				fflush(stdout);
//			}
#ifdef DEBUG
			fprintf(log_fd, "%s Done: %d\n", ficotime_now(), done);
			fflush(log_fd);
#endif //DEBUG

		}
		while (done == 0);
		
		if (offset == 0) {
			fprintf(log_fd, "%s Warning: Nothing has been read from %s\n", ficotime_now(), dev_str);
			fflush(log_fd);
			retval = 0;
		}		
	}
	
	if (!retval) {
		ficostring_free(read_line_str);
	}
#ifdef DEBUG
	else {
		fprintf(log_fd, "%s Read string line from %s: \"%s\\n\"\n", ficotime_now(), dev_str, *read_line_str);
		fflush(log_fd);
	}
#endif //DEBUG
	ficostring_free(&tmp_buff_str);
	return retval;
}

// Check read string
bool ficoserial_read_check_str(int serialfd, char *rep_frame_str, char end_of_line, char *dev_str,  FILE *log_fd)
{
	bool retval = 1;
	
	char *read_line_str = NULL;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (serialfd < 0) {
		retval = 0;
	}
	else if (rep_frame_str == NULL) {
		retval = 0;
	}
	else if (dev_str == NULL) {
		retval = 0;
	}
	else if (!(strlen(rep_frame_str) > 0)) {
		fprintf(log_fd, "%s Warning: %d byte string length. Don't read\n", ficotime_now(), (int)strlen(rep_frame_str));
		fflush(log_fd);
		retval = 0;
	}
	else {
#ifdef DEBUG
		fprintf(log_fd, "%s Waiting \"%s\" (%d bytes) from %s...\n",
			ficotime_now(),
			rep_frame_str,
			(int) strlen(rep_frame_str),
			dev_str);
		fflush(log_fd);
#endif	//DEBUG
		//
		// Read line string
		//
		if (!ficoserial_read_line_str(serialfd, &read_line_str, end_of_line, dev_str, log_fd)) {
			fprintf(log_fd, "\n%s Error: Cannot read line string from %s\n\n", ficotime_now(), dev_str);
			fflush(log_fd);
			retval = 0;
		}
		else if (strcmp(read_line_str, rep_frame_str) != 0) {
			retval = 0;
		}
#ifdef DEBUG
		else {
			fprintf(log_fd, "%s Response \"%s\" checked\n",
				ficotime_now(),
				read_line_str);
			fflush(log_fd);
		}
#endif //DEBUG
	}
	ficostring_free(&read_line_str);
	return retval;
}
