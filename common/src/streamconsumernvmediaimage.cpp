/*****************************************************************************
 *
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 ****************************************************************************/

/*
 * capture2d.h
 *
 *  Last modification on: 17/08/2016
 *      Author: Miguel H. Rib <miguel.rib@ficosa.com>
 */


#include "streamconsumernvmediaimage.h"
#include <iostream>


StreamConsumerNvMediaImage::StreamConsumerNvMediaImage(EGLDisplay display, NvMediaDevice* device, EGLStream &stream)
    : display(display)
    , stream(stream.getID())
{
    consumer = NvMediaEglStreamConsumerCreate(
        device,
        display,
        stream.getID(),
        NvMediaSurfaceType_Image_RGBA);
       //NvMediaSurfaceType_R8G8B8A8_BottomOrigin);

    if(consumer == NULL)
    {
        std::cout << "" << " : Could not create NvMedia Image stream consumer!" << std::endl;
    }
}

StreamConsumerNvMediaImage::~StreamConsumerNvMediaImage()
{
    NvMediaEglStreamConsumerDestroy(consumer);
}

NvMediaImage* StreamConsumerNvMediaImage::acquire()
{
    NvMediaImage* result = NULL;
    NvMediaTime timeStamp;

    NvMediaStatus status = NvMediaEglStreamConsumerAcquireImage(consumer, &result, 1000, &timeStamp);
    if (status == NVMEDIA_STATUS_BAD_PARAMETER)
    {
        std::cout << "StreamConsumerNvMediaImage" << " : Cannot acquire image, status is: NVMEDIA_STATUS_BAD_PARAMETER" << std::endl;
    }else if (status == NVMEDIA_STATUS_TIMED_OUT)
    {
        std::cout << "StreamConsumerNvMediaImage" << " : Cannot acquire image, status is: NVMEDIA_STATUS_TIMED_OUT" << std::endl;
    }else if (status == NVMEDIA_STATUS_ERROR)
    {
        std::cout << "StreamConsumerNvMediaImage" << " : Cannot acquire image, status is: NVMEDIA_STATUS_ERROR" << std::endl;
    }else if (status != NVMEDIA_STATUS_OK)
    {
        std::cout << "StreamConsumerNvMediaImage" << " : Cannot acquire image, status is (unknown): " << status << std::endl;
    }

    return result;
}

NvMediaImage* StreamConsumerNvMediaImage::acquire(unsigned int timeoutms)
{
    NvMediaImage* result = nullptr;
    NvMediaTime timeStamp;

    NvMediaStatus status = NvMediaEglStreamConsumerAcquireImage(consumer, &result, timeoutms, &timeStamp);
    if (status == NVMEDIA_STATUS_BAD_PARAMETER)
    {
        std::cout << "StreamConsumerNvMediaImage" << " : Cannot acquire image, status is: NVMEDIA_STATUS_BAD_PARAMETER" << std::endl;
        result = nullptr;
    }else if (status == NVMEDIA_STATUS_TIMED_OUT)
    {
        std::cout << "StreamConsumerNvMediaImage" << " : Cannot acquire image, status is: NVMEDIA_STATUS_TIMED_OUT" << std::endl;
        result = nullptr;
    }else if (status == NVMEDIA_STATUS_ERROR)
    {
        std::cout << "StreamConsumerNvMediaImage" << " : Cannot acquire image, status is: NVMEDIA_STATUS_ERROR" << std::endl;
        result = nullptr;
    }else if (status != NVMEDIA_STATUS_OK)
    {
        std::cout << "StreamConsumerNvMediaImage" << " : Cannot acquire image, status is (unknown): " << status << std::endl;
        result = nullptr;
    }

    return result;
}

void StreamConsumerNvMediaImage::release(NvMediaImage* image)
{
    NvMediaEglStreamConsumerReleaseImage(consumer, image);
}

