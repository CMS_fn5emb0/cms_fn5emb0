/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "fbo.h"
#include <Logutils.hpp>
using namespace logutils;

//#######################################################################################
FBO::FBO()
{
    fbo = 0;
    tex = 0;

    width = 0;
    height = 0;
    format = 0;

    m_initialized = false;
}

FBO::~FBO()
{
    releaseFBO();
}

bool FBO::createFBO(unsigned int inputWidth, unsigned int inputHeight, GLuint inputFormat)
{
    if (m_initialized)
    {
        if (inputWidth != width || inputHeight != height || inputFormat != format)
            printf("Attempt to create an already valid FBO with different paramters. Please release it first!\n");
        return true;
    }

    bool success = false;

    if (inputFormat != GL_RGBA8 && inputFormat != GL_RGB8 && inputFormat != GL_R8 && inputFormat != GL_SRGB8_ALPHA8) return false;
    if (inputWidth <= 0 || inputHeight <= 0)             return false;

    GLuint dataFormat;

    if(inputFormat == GL_RGB8) dataFormat = GL_RGB;
    if(inputFormat == GL_RGBA8 || inputFormat == GL_SRGB8_ALPHA8) dataFormat = GL_RGBA;
    if(inputFormat == GL_R8)    dataFormat = GL_RED;

    // Allocate the output FBO
    //RGBA8 2D texture, 24 bit depth texture
    glGenFramebuffers(1, &fbo); 
    glGenTextures(1, &tex); 

    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, inputFormat, inputWidth, inputHeight, 0, dataFormat, GL_UNSIGNED_BYTE, NULL);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    //Attach 2D texture to this FBO
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    //-------------------------
    //Does the GPU support current FBO configuration?
    GLenum status;
    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    switch (status)
    {
    case GL_FRAMEBUFFER_COMPLETE:
        width = inputWidth;
        height = inputHeight;
        format = inputFormat;
        success = true;
        break;
    }

    // Unbind
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    if (success) {
        //printf("FBO create: Success - %d x %d - %d - fboID %d texID %d\n", width, height, format, fbo, tex);
        LOG_DBG_( "Function Name: %s, FBO create: Success - %d x %d - %d - fboID :  %d texID : %d\n", __func__,width, height, format, fbo, tex);
    }
    else {
        glDeleteTextures(1, &tex);
       // printf("FBO create: Fail- %d x %d - %d - fboID %d texID %d\n", inputWidth, inputHeight, inputFormat, fbo, tex);
        LOG_ERR_( "Function Name: %s, FBO create: Fail- %d x %d - %d - fboID:%d texID:%d\n", __func__,inputWidth, inputHeight, inputFormat, fbo, tex);
    }

    m_initialized = true;

    return success;
}

void FBO::releaseFBO()
{
    if (!m_initialized) return;
    glDeleteTextures(1, &tex);
    glDeleteFramebuffers(1, &fbo);

    fbo = 0;
    tex = 0;

    width = 0;
    height = 0;
    format = 0;

    m_initialized = false;
}

char* FBO::dumpFBOContent()
{
    if (!m_initialized) return NULL;

    char* buffer = NULL;

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    if (format == GL_RGBA8)
    {
        buffer = new char[width * height * 4];
        glPixelStorei(GL_PACK_ALIGNMENT, 4);
        glPixelStorei(GL_PACK_ROW_LENGTH, width);
        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    }   

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return buffer;
}

int FBO::dumpFBOContent(unsigned char* buffer, unsigned int bufferSize)
{
    if (!m_initialized) return 0;

    int writtenSize = 0;

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    if (format == GL_RGBA8 || format == GL_SRGB8_ALPHA8)
    {
        if (bufferSize != width * height * 4) return 0;
        glPixelStorei(GL_PACK_ALIGNMENT, 4);
        glPixelStorei(GL_PACK_ROW_LENGTH, width);
        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
        writtenSize = width * height * 4;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return writtenSize;
}

