/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "window_wayland.h"

#include <cstring>
#include <iostream>

WindowWayland::WindowWayland(int width, int height, bool robust)
: waylandInitialized(false)
, eglInitialized(false)
, robust(robust)
{
    createWindow(width, height);
    createEGLContext(robust);
}

WindowWayland::~WindowWayland()
{
    eglDestroyContext(display, context);
    eglDestroySurface(display, surface);
    eglTerminate(display);
    eglReleaseThread();

    wl_egl_window_destroy(wEglWin);
    wl_surface_destroy(wSurface);
    wl_shell_surface_destroy(wShellSurface);
    wl_shell_destroy(wShell);
    wl_compositor_destroy(wCompositor);
    wl_registry_destroy(wRegistry);
    wl_display_disconnect(wDisplay);
}

void WindowWayland::swapBuffers()
{
    if(!isInitialized())
    {
        return;
    }

    eglSwapBuffers(display, surface);
}

bool WindowWayland::isInitialized()
{
    return (waylandInitialized && eglInitialized);
}

void WindowWayland::resetContext()
{
    eglDestroyContext(display, context);
    
    EGLint contextAttribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 3,
        EGL_CONTEXT_OPENGL_ROBUST_ACCESS_EXT, robust ? EGL_TRUE : EGL_FALSE,
        EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_EXT, robust ? EGL_LOSE_CONTEXT_ON_RESET_EXT : EGL_NO_RESET_NOTIFICATION_EXT,
        EGL_NONE, EGL_NONE
    };
    
    context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs);
    if (context == EGL_NO_CONTEXT)
    {
        std::cout << "Could not create EGL context!" << std::endl;
        return;
    }
    
    // Make the context current
    EGLBoolean status = eglMakeCurrent(display, surface, surface, context);
    if (!status)
    {
        std::cout << "Could not activate context" << std::endl;
        return;
    }
}

void WindowWayland::makeCurrent()
{
    eglMakeCurrent(display, surface, surface, context);
}

void WindowWayland::registryHandleGlobal(void *data, struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version)
{
    WindowWayland *win = reinterpret_cast<WindowWayland*>(data);
    win->registryHandleGlobal(registry, name, interface, version);
}

void WindowWayland::registryHandleGlobal(struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version)
{
    if (strcmp(interface, "wl_compositor") == 0) 
    {
        wCompositor = reinterpret_cast<wl_compositor*>(wl_registry_bind(registry, name, &wl_compositor_interface, 1));
    }
    else if (strcmp(interface, "wl_shell") == 0) 
    {
        wShell = reinterpret_cast<wl_shell*>(wl_registry_bind(registry, name, &wl_shell_interface, 1));
    }
}

void WindowWayland::registryHandleGlobalRemove(void *data, struct wl_registry *registry, uint32_t name)
{
    WindowWayland *win = reinterpret_cast<WindowWayland*>(data);
    win->registryHandleGlobalRemove(registry, name);
}

void WindowWayland::registryHandleGlobalRemove(struct wl_registry *registry, uint32_t name)
{
    
}

void WindowWayland::handlePing(void *data, wl_shell_surface *shell_surface, uint32_t serial)
{
    WindowWayland *win = reinterpret_cast<WindowWayland*>(data);
    win->handlePing(shell_surface, serial);
}

void WindowWayland::handlePing(wl_shell_surface *shell_surface, uint32_t serial)
{
    wl_shell_surface_pong(shell_surface, serial);
}

void WindowWayland::handleConfigure(void *data, wl_shell_surface *shell_surface, uint32_t edges, int32_t width, int32_t height)
{
    WindowWayland *win = reinterpret_cast<WindowWayland*>(data);
    win->handleConfigure(shell_surface, edges, width, height);
}

void WindowWayland::handleConfigure(wl_shell_surface *shell_surface, uint32_t edges, int32_t width, int32_t height)
{
    wl_egl_window_resize(wEglWin, width, height, 0, 0);
}

void WindowWayland::handlePopupDone(void *data, wl_shell_surface *shell_surface)
{
    WindowWayland *win = reinterpret_cast<WindowWayland*>(data);
    win->handlePopupDone(shell_surface);
}

void WindowWayland::handlePopupDone(wl_shell_surface *shell_surface)
{
}

void WindowWayland::createWindow(int width, int height)
{
    wDisplay = wl_display_connect(nullptr);
    if(!wDisplay)
    {
        std::cout << "could not open display!" << std::endl;
        return;
    }
    wRegistry = wl_display_get_registry(wDisplay);
    wRegistryListener.global = registryHandleGlobal;
    wRegistryListener.global_remove = registryHandleGlobalRemove;
    wl_registry_add_listener(wRegistry, &wRegistryListener, this);
    
    wl_display_dispatch(wDisplay);
    wSurface = wl_compositor_create_surface(wCompositor);
    wEglWin = wl_egl_window_create(wSurface, width, height);
    if(wShell)
    {
        wShellSurface = wl_shell_get_shell_surface(wShell, wSurface);
        if(wShellSurface)
        {
            wShellSurfaceListener.configure = handleConfigure;
            wShellSurfaceListener.ping = handlePing;
            wShellSurfaceListener.popup_done = handlePopupDone;
            wl_shell_surface_add_listener(wShellSurface, &wShellSurfaceListener, this);
            wl_shell_surface_set_title(wShellSurface, "GL Playground");
            wl_shell_surface_set_toplevel(wShellSurface);
        }
    }
    wl_surface_set_opaque_region(wSurface, nullptr);
    
    waylandInitialized = true;
}

void WindowWayland::createEGLContext(bool robust)
{
    EGLint contextAttribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 3,
        EGL_CONTEXT_OPENGL_ROBUST_ACCESS_EXT, robust ? EGL_TRUE : EGL_FALSE,
        EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_EXT, robust ? EGL_LOSE_CONTEXT_ON_RESET_EXT : EGL_NO_RESET_NOTIFICATION_EXT,
        EGL_NONE, EGL_NONE
    };
    
    // Get Display
    display = eglGetDisplay((EGLNativeDisplayType)wDisplay);
    if (display == EGL_NO_DISPLAY)
    {
        std::cout << "Could not init EGL display" << std::endl;
        return;
    }
    
    // Initialize EGL
    EGLint majorVersion;
    EGLint minorVersion;
    EGLBoolean status = eglInitialize(display, &majorVersion, &minorVersion);
    
    if (!status)
    {
        std::cout << "Could not init EGL" << std::endl;
        return;
    }
    
    // Get configs
    EGLint numConfigs;
    status = eglGetConfigs(display, NULL, 0, &numConfigs);
    if (!status)
    {
        std::cout << "Could not get config count" << std::endl;
        return;
    }
    
    // Choose config
    EGLint attribList[] =
    {
       EGL_RENDERABLE_TYPE, EGL_OPENGL_ES3_BIT,
       EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
       EGL_RED_SIZE,       5,
       EGL_GREEN_SIZE,     6,
       EGL_BLUE_SIZE,      5,
//       EGL_ALPHA_SIZE,     8,
//       EGL_DEPTH_SIZE,     8,
//       EGL_STENCIL_SIZE,   8,
//       EGL_SAMPLE_BUFFERS, 1,
//       EGL_SAMPLES,        4,
       EGL_NONE
    };
    status = eglChooseConfig(display, attribList, &config, 1, &numConfigs);
    if (!status)
    {
        std::cout << "Failed to select config!" << std::endl;
        return;
    }
    
    eglBindAPI(EGL_OPENGL_ES_API);
    
    // Create a surface
    surface = eglCreateWindowSurface(display, config, (EGLNativeWindowType)wEglWin, NULL);
    if (surface == EGL_NO_SURFACE)
    {
        std::cout << "Could not create EGL surface!" << std::endl;
        return;
    }
    
    // Create a GL context
    context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs);
    if (context == EGL_NO_CONTEXT)
    {
        std::cout << "Could not create EGL context!" << std::endl;
        return;
    }
    
    // Make the context current
    status = eglMakeCurrent(display, surface, surface, context);
    if (!status)
    {
        std::cout << "Could not activate context" << std::endl;
        return;
    }

    status = eglSwapInterval(display, 1);
    if(!status)
    {
        std::cout << "Could not enable VSYNC!" << std::endl;
        return;
    }
    
    eglInitialized = true;
}
