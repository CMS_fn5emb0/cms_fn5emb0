/*****************************************************************************
 *
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 ****************************************************************************/

/*
 * capture2d.h
 *
 *  Last modification on: 17/08/2016
 *      Author: Miguel H. Rib <miguel.rib@ficosa.com>
 */


#include "window_egldevice.h"
#include <iostream>
#include <xf86drm.h>
#include <xf86drmMode.h>

PFNEGLGETPLATFORMDISPLAYEXTPROC eglGetPlatformDisplayEXT = nullptr;
PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC eglCreatePlatformWindowSurfaceEXT = nullptr;
PFNEGLCREATEPLATFORMPIXMAPSURFACEEXTPROC eglCreatePlatformPixmapSurfaceEXT = nullptr;
PFNEGLGETOUTPUTLAYERSEXTPROC eglGetOutputLayersEXT = nullptr;
PFNEGLQUERYDEVICESEXTPROC eglQueryDevicesEXT = nullptr;
PFNEGLSTREAMCONSUMEROUTPUTEXTPROC eglStreamConsumerOutputEXT = nullptr;

PFNEGLCREATESTREAMPRODUCERSURFACEKHRPROC eglCreateStreamProducerSurfaceKHR = nullptr;
PFNEGLCREATESTREAMKHRPROC eglCreateStreamKHR = nullptr;
PFNEGLQUERYDEVICESTRINGEXTPROC eglQueryDeviceStringEXT = nullptr;
PFNEGLDESTROYSTREAMKHRPROC eglDestroyStreamKHR = nullptr;


static void getEGLError(int line, const char *file, const char *function)
{
    EGLint error = eglGetError();
    if(error != EGL_SUCCESS)
    {
        std::cout << "window_egldevice : " << file << " in function << " << function << " in line " << line 
                  << ": eglError: 0x" << std::hex << error << std::dec << std::endl;
    }
}

#define eglError() getEGLError(__LINE__, __FILE__, __PRETTY_FUNCTION__);


WindowEGL::WindowEGL(int displayId, int windowId, int width, int height, bool robust)
: initialized(false)
, robust(robust)
{
    EGLint deviceCount = 4;
    EGLint foundDevices = 0;
    EGLDeviceEXT devices[deviceCount];
    
    // Get pointers of required EGL functions
    eglGetPlatformDisplayEXT = (PFNEGLGETPLATFORMDISPLAYEXTPROC)eglGetProcAddress("eglGetPlatformDisplayEXT");
    eglCreatePlatformWindowSurfaceEXT = (PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC) eglGetProcAddress("eglCreatePlatformWindowSurfaceEXT");
    eglCreatePlatformPixmapSurfaceEXT = (PFNEGLCREATEPLATFORMPIXMAPSURFACEEXTPROC)eglGetProcAddress("eglCreatePlatformPixmapSurfaceEXT");
    eglGetOutputLayersEXT = (PFNEGLGETOUTPUTLAYERSEXTPROC)eglGetProcAddress("eglGetOutputLayersEXT");
    eglQueryDevicesEXT = (PFNEGLQUERYDEVICESEXTPROC)eglGetProcAddress("eglQueryDevicesEXT");
    eglStreamConsumerOutputEXT = (PFNEGLSTREAMCONSUMEROUTPUTEXTPROC)eglGetProcAddress("eglStreamConsumerOutputEXT");
    
    eglCreateStreamProducerSurfaceKHR = (PFNEGLCREATESTREAMPRODUCERSURFACEKHRPROC)eglGetProcAddress("eglCreateStreamProducerSurfaceKHR");
    eglCreateStreamKHR = (PFNEGLCREATESTREAMKHRPROC)eglGetProcAddress("eglCreateStreamKHR");
    eglDestroyStreamKHR = (PFNEGLDESTROYSTREAMKHRPROC)eglGetProcAddress("eglDestroyStreamKHR");
    eglQueryDeviceStringEXT = (PFNEGLQUERYDEVICESTRINGEXTPROC)eglGetProcAddress("eglQueryDeviceStringEXT");

    EGLBoolean status = eglQueryDevicesEXT(deviceCount, devices, &foundDevices);
    if (status != EGL_TRUE) 
    {
        std::cout << "window_egldevice : " << "Failed to query devices (error: " << std::hex << eglGetError() << std::dec << std::endl;
        return;
    }
    std::cout << "window_egldevice : " << "found " << foundDevices << " devices" << std::endl;
    
    EGLAttrib layerAttribs[] = { 
        EGL_NONE, EGL_NONE,
        EGL_NONE, EGL_NONE,
    };
    // get device name to setup DRM, assume that devices[0] is the one to use
    const char *drmName = eglQueryDeviceStringEXT(devices[0], EGL_DRM_DEVICE_FILE_EXT);
    if (!drmName) 
    {
        std::cout << "window_egldevice : " << "Unable to query device string!" << std::endl;
        return;
    }
    
    int dispId = displayId;
    int dispWidth = width;
    int dispHeight = height;
    if(!initDrm(drmName, displayId, windowId, layerAttribs, dispWidth, dispHeight))	/*****/
    {
        std::cout << "window_egldevice : " << "Failed to init DRM" << std::endl;
        return;
    }
    std::cout << "window_egldevice : " << "DispSize: " << dispWidth << ", " << dispHeight << std::endl;

    EGLenum platform = EGL_PLATFORM_DEVICE_EXT;
    EGLint dispAttribs[] = {
        EGL_NONE, EGL_NONE
    };
    // create display on top of DRM display
    display = eglGetPlatformDisplayEXT(platform, (NativeDisplayType)devices[0], dispAttribs);
    if(display == EGL_NO_DISPLAY)
    {
        std::cout << "window_egldevice : " << "Failed to create EGL display" << std::endl;
        return;
    }
    
    // setup EGL like in windowing system
    status = eglInitialize(display, 0, 0);
    if(!status)
    {
        std::cout << "window_egldevice : " << "Could not init EGL!" << std::endl;
        return;
    }
    
    EGLint cfgAttribs[] = {
        EGL_SURFACE_TYPE,        EGL_STREAM_BIT_KHR,
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES3_BIT,
        EGL_RED_SIZE, 5,
        EGL_GREEN_SIZE, 6,
        EGL_BLUE_SIZE, 5,
        EGL_ALPHA_SIZE, 8,
        EGL_DEPTH_SIZE, 8,
        EGL_STENCIL_SIZE, 8,
        EGL_NONE, EGL_NONE
    };
    EGLint cfgCount = 0;
    
    status = eglChooseConfig(display, cfgAttribs, nullptr, 0, &cfgCount);
    if(!status || cfgCount == 0)
    {
        std::cout << "window_egldevice : " << "Found " << cfgCount << " configurations!" << std::endl;
        std::cout << "window_egldevice : " << "Could not read EGL config count! error: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        return;
    }
    
    EGLConfig *cfgs = new EGLConfig[cfgCount];
    status = eglChooseConfig(display, cfgAttribs, cfgs, cfgCount, &cfgCount);
    if(!status || cfgCount == 0)
    {
        std::cout << "window_egldevice : " << "Could not read EGL config! error: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        return;
    }
    eglBindAPI(EGL_OPENGL_ES_API);
    
    config = cfgs[0]; // just take the first one that suits our requirements
    delete[] cfgs;
    
    EGLint ctxAttribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 3,
        EGL_CONTEXT_OPENGL_ROBUST_ACCESS_EXT, robust ? EGL_TRUE : EGL_FALSE,
        EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_EXT, robust ? EGL_LOSE_CONTEXT_ON_RESET_EXT : EGL_NO_RESET_NOTIFICATION_EXT,
        EGL_NONE, EGL_NONE
    };
    context = eglCreateContext(display, config, nullptr, ctxAttribs);
    if(context == EGL_NO_CONTEXT)
    {
        std::cout << "window_egldevice : " << "Failed to create EGL context" << std::endl;
        return;
    }
    
    // create output layer on display
    EGLOutputLayerEXT layer;
    int n;
    if (!eglGetOutputLayersEXT(display, layerAttribs, &layer, 1, &n) || !n) 
    {
        std::cout << "window_egldevice : " << "Unable to get output layer" << std::endl;
        return;
    }

    // Create a stream and connect to the output
    EGLint stream_attr[] = { 
        EGL_STREAM_FIFO_LENGTH_KHR, 1, 
        EGL_NONE 
    };
    stream = eglCreateStreamKHR(display, stream_attr);
    if (stream == EGL_NO_STREAM_KHR) 
    {
        std::cout << "window_egldevice : " << "Unable to create stream" << std::endl;
        return;
    }
    
    if (!eglStreamConsumerOutputEXT(display, stream, layer)) 
    {
        std::cout << "window_egldevice : " << "Unable to connect output to stream" << std::endl;
        return;
    }
    
    EGLint srfAttribs[] = {
        EGL_WIDTH, dispWidth,
        EGL_HEIGHT, dispHeight,
        EGL_NONE, EGL_NONE
    };
    // create a surface as EGL stream producer
    surface = eglCreateStreamProducerSurfaceKHR(display, config, stream, srfAttribs);
    if(surface == EGL_NO_SURFACE)
    {
        std::cout << "window_egldevice : " << "Failed to create EGL surface, error: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        return;
    }
    
    status = eglMakeCurrent(display, surface, surface, context);
    if(!status)
    {
        std::cout << "window_egldevice : " << "Could not set EGL context!" << std::endl;
        return;
    }

    status = eglSwapInterval(display, 1);
    if(!status)
    {
        std::cout << "window_egldevice : " << "Could not enable VSYNC!" << std::endl;
        return;
    }

    eglError();
    initialized = true;
}

WindowEGL::~WindowEGL(void)
{
    eglDestroyStreamKHR(display, stream);
    eglDestroyContext(display, context);
    eglDestroySurface(display, surface);
    eglTerminate(display);
    eglReleaseThread();
}

void WindowEGL::swapBuffers(void)
{
    if(!isInitialized())
    {
        return;
    }

    eglSwapBuffers(display, surface);
}

bool WindowEGL::isInitialized(void)
{
    return initialized;
}

void WindowEGL::resetContext()
{
    // destroy old context and recreate with the same settings as before
    eglDestroyContext(display, context);
    
    EGLint contextAttribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 3,
        EGL_CONTEXT_OPENGL_ROBUST_ACCESS_EXT, robust ? EGL_TRUE : EGL_FALSE,
        EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_EXT, robust ? EGL_LOSE_CONTEXT_ON_RESET_EXT : EGL_NO_RESET_NOTIFICATION_EXT,
        EGL_NONE, EGL_NONE
    };
    
    context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs);
    if (context == EGL_NO_CONTEXT)
    {
        std::cout << "window_egldevice : " << "Could not create EGL context!" << std::endl;
        return;
    }
    
    // Make the context current
    EGLBoolean status = eglMakeCurrent(display, surface, surface, context);
    if (!status)
    {
        std::cout << "window_egldevice : " << "Could not activate context" << std::endl;
        return;
    }
}

void WindowEGL::makeCurrent()
{
    eglMakeCurrent(display, surface, surface, context);
}

bool WindowEGL::initDrm(const char *name, EGLAttrib* layerAttribs, int &dispWidth, int &dispHeight)
{
		WindowEGL::initDrm(name, 0, 0, layerAttribs, dispWidth, dispHeight);
}

bool WindowEGL::initDrm(const char *name, int displayId, int windowId, EGLAttrib* layerAttribs, int &dispWidth, int &dispHeight)
{
    // screen attached to which connector id?
    int conn = displayId;
    int crtc = -1, plane = -1;
    int xSurfSize = 0, ySurfSize = 0;
    int xOffset = 0, yOffset = 0;
    // auto detect resolution by setting modesize to 0
    //int xModeSize = dispWidth, yModeSize = dispHeight;
    int xModeSize = 0, yModeSize = 0;
    int bounce = 0;

    int drmFd;
    uint32_t drmConnId, drmEncId, drmCrtcId, drmPlaneId;
    uint32_t crtcMask;
    drmModeRes* drmResInfo = nullptr;
    drmModePlaneRes* drmPlaneResInfo = nullptr;
    drmModeCrtc* drmCrtcInfo = nullptr;
    drmModeConnector* drmConnInfo = nullptr;
    drmModeEncoder* drmEncInfo = nullptr;
    drmModePlane* drmPlaneInfo = nullptr;
    int drmModeIndex = 0;

    bool setMode  = false;

    drmFd = drmOpen(name, nullptr);
    if (drmFd == -1) 
    {
        std::cout << "window_egldevice : " << "Couldn't open device file " <<  name << std::endl;
        return false;
    }

    // Obtain DRM-KMS resources
    drmResInfo = drmModeGetResources(drmFd);
    if (!drmResInfo) 
    {
        std::cout << "window_egldevice : " << "Couldn't obtain DRM-KMS resources" << std::endl;
        return false;
    }

    // If a specific crtc was requested, make sure it exists
    if (crtc >= drmResInfo->count_crtcs) 
    {
        std::cout << "window_egldevice : " << "Requested crtc index (" << crtc << ") exceeds count (" << drmResInfo->count_crtcs << ")" << std::endl;
        return false;
    }
    crtcMask = (crtc >= 0) ? (1 << crtc) : ((1 << drmResInfo->count_crtcs) - 1);

    // If drawing to a plane is requested, obtain the plane info
    if (plane >= 0) 
    {
        drmPlaneResInfo = drmModeGetPlaneResources(drmFd);
        if (!drmPlaneResInfo) 
        {
            std::cout << "window_egldevice : " << "Unable to obtain plane resource list" << std::endl;
            return false;
        }
        if (plane >= drmPlaneResInfo->count_planes) 
        {
            std::cout << "window_egldevice : " << "Requested plane index (" << plane << ") exceeds count (" << drmPlaneResInfo->count_planes << ")" << std::endl;
            return false;
        }
        drmPlaneId = drmPlaneResInfo->planes[plane];
        drmPlaneInfo = drmModeGetPlane(drmFd, drmPlaneId);
        if (!drmPlaneInfo) 
        {
            std::cout << "window_egldevice : " << "Unable to obtain info for plane (" << drmPlaneId << ")"<< std::endl;
            return false;
        }
        crtcMask &= drmPlaneInfo->possible_crtcs;
        if (!crtcMask) 
        {
            std::cout << "window_egldevice : " << "Requested crtc and plane not compatible" << std::endl;
            return false;
        }
        std::cout << "window_egldevice : " << "Obtained plane information\n" << std::endl;
    }

    // Query info for requested connector
    if (conn >= drmResInfo->count_connectors) 
    {
        std::cout << "window_egldevice : " << "Requested connector index (" << conn << ") exceeds count (" << drmResInfo->count_connectors << ")" << std::endl;
        return false;
    }
    drmConnId = drmResInfo->connectors[conn];
    drmConnInfo = drmModeGetConnector(drmFd, drmConnId);
    if (!drmConnInfo) 
    {
        std::cout << "window_egldevice : " << "Unable to obtain info for connector (" << drmConnId << ")" << std::endl;
        return false;
    } 
    else if (drmConnInfo->connection != DRM_MODE_CONNECTED) 
    {
        std::cout << "window_egldevice : " << "Requested connnector is not connected (mode=" << drmConnInfo->connection << ")" << std::endl;
        return false;
    } 
    else if (drmConnInfo->count_modes <= 0) 
    {
        std::cout << "window_egldevice : " << "Requested connnector has no available modes" << std::endl;
        return false;
    }

    // If there is already an encoder attached to the connector, choose
    //   it unless not compatible with crtc/plane
    drmEncId = drmConnInfo->encoder_id;
    drmEncInfo = drmModeGetEncoder(drmFd, drmEncId);
    if (drmEncInfo) 
    {
        if (!(drmEncInfo->possible_crtcs & crtcMask)) 
        {
            drmModeFreeEncoder(drmEncInfo);
            drmEncInfo = nullptr;
        }
    }

    // If we didn't have a suitable encoder, find one
    if (!drmEncInfo) 
    {
        int i;
        for (i=0; i<drmConnInfo->count_encoders; ++i) 
        {
            drmEncId = drmConnInfo->encoders[i];
            drmEncInfo = drmModeGetEncoder(drmFd, drmEncId);
            if (drmEncInfo) 
            {
                if (crtcMask & drmEncInfo->possible_crtcs) 
                {
                    crtcMask &= drmEncInfo->possible_crtcs;
                    break;
                }
                drmModeFreeEncoder(drmEncInfo);
                drmEncInfo = nullptr;
            }
        }
        if (i == drmConnInfo->count_encoders) 
        {
            std::cout << "window_egldevice : " << "Unable to find suitable encoder" << std::endl;
            return false;
        }
    }

    // Select a suitable crtc. Give preference to any that's already
    //   attached to the encoder.
    for (int i=0; i<drmResInfo->count_crtcs; ++i) 
    {
        if (crtcMask & (1 << i)) 
        {
            drmCrtcId = drmResInfo->crtcs[i];
            if (drmResInfo->crtcs[i] == drmEncInfo->crtc_id) 
            {
                break;
            }
        }
    }

    // Query info for crtc
    drmCrtcInfo = drmModeGetCrtc(drmFd, drmCrtcId);
    if (!drmCrtcInfo) 
    {
        std::cout << "window_egldevice : " << "Unable to obtain info for crtc (" << drmCrtcId << ")" << std::endl;
        return false;
    }

    // If dimensions are specified and not using a plane, find closest mode
    if ((xModeSize || yModeSize) && (plane < 0)) 
    {
        // Find best fit among available modes
        int best_index = 0;
        int best_fit = 0x7fffffff;
        for (int i=0; i<drmConnInfo->count_modes; ++i) 
        {
            drmModeModeInfoPtr mode = drmConnInfo->modes + i;
            int fit = 0;

            if (xModeSize) 
            {
                fit += abs((int)mode->hdisplay - xModeSize) * (int)mode->vdisplay;
            }
            if (yModeSize) 
            {
                fit += abs((int)mode->vdisplay - yModeSize) * (int)mode->hdisplay;
            }

            if (fit < best_fit) 
            {
                best_index = i;
                best_fit = fit;
            }
            std::cout << "window_egldevice : " << i << ": " << (int)mode->hdisplay << " x " << (int)mode->vdisplay - yModeSize << std::endl; 
        }

        // Choose this size/mode
        drmModeIndex = best_index;
        xModeSize = (int)drmConnInfo->modes[best_index].hdisplay;
        yModeSize = (int)drmConnInfo->modes[best_index].vdisplay;
    }

    // We'll only set the mode if we have to.
    if ((drmConnInfo->encoder_id != drmEncId) 
            || (drmEncInfo->crtc_id != drmCrtcId) 
            || !drmCrtcInfo->mode_valid 
            || ((plane < 0) && xModeSize && (xModeSize!=(int)drmCrtcInfo->mode.hdisplay)) 
            || ((plane < 0) && yModeSize && (yModeSize!=(int)drmCrtcInfo->mode.vdisplay))) 
    {
        setMode = true;
    }

    // If dimensions haven't been specified, figure out good values to use
    if (!xModeSize || !yModeSize) 
    {
        // If mode requires reset, just pick the first one available
        //   from the connector
        if (setMode) 
        {
            xModeSize = (int)drmConnInfo->modes[0].hdisplay;
            yModeSize = (int)drmConnInfo->modes[0].vdisplay;
        }
        // Otherwise get it from the current crtc settings
        else 
        {
            xModeSize = (int)drmCrtcInfo->mode.hdisplay;
            yModeSize = (int)drmCrtcInfo->mode.vdisplay;
        }
    }

    // If surf size is unspecified, default to fullscreen normally
    // or to 1/4 fullscreen if in animated bounce mode.
    if (!xSurfSize || !ySurfSize) 
    {
        if (bounce) 
        {
            xSurfSize = xModeSize / 2;
            ySurfSize = yModeSize / 2;
        } 
        else 
        {
            xSurfSize = xModeSize;
            ySurfSize = yModeSize;
        }
    }

    // If necessary, set the mode
    if (setMode) 
    {
        drmModeSetCrtc(drmFd, drmCrtcId, -1, 0, 0, &drmConnId, 1, drmConnInfo->modes + drmModeIndex);
        std::cout << "window_egldevice : " << "Set mode" << std::endl;
    }

    // If plane is in use, set it
    if (plane >= 0) 
    {
        drmModeSetPlane(drmFd, drmPlaneId, drmCrtcId, -1, 0, xOffset, yOffset, xSurfSize, ySurfSize, 0, 0, xSurfSize << 16, ySurfSize << 16);
        std::cout << "window_egldevice : " << "Set plane configuration" << std::endl;
    }

    // Get the layer for this crtc/plane
    if (plane >= 0) 
    {
        layerAttribs[0] = EGL_DRM_PLANE_EXT;
        layerAttribs[1] = (EGLAttrib)drmPlaneId;
    } 
    else 
    {
        layerAttribs[0] = EGL_DRM_CRTC_EXT;
        layerAttribs[1] = (EGLAttrib)drmCrtcId;
    }
    dispWidth = xSurfSize;
    dispHeight = ySurfSize;
    
    return true;
}
