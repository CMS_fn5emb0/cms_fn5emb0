/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file FiniteStateMachineTemplate.cpp
*  @brief This file contains the implementation of the class
*  		  FiniteStateMachineTemplate
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "FiniteStateMachineTemplate.h"
#include <iostream>
FiniteStateMachineTemplate::FiniteStateMachineTemplate() {


	currentState = State::STOPPED;
	threadAction = new std::thread(&FiniteStateMachineTemplate::threadBehaviour,this);
	//videoBuffer = new boost::circular_buffer<NvMediaImage*>(BUFFER_SIZE);
	if(NvQueueCreate(&videoBuffer, BUFFER_SIZE, sizeof(NvMediaImage*)) != NVMEDIA_STATUS_OK) {
			fprintf(stderr, "Could not init Queue!\n");
	}

	nextStages = new std::list<FiniteStateMachineTemplate*>;
	nStages = 0;
	initialized = false;

}

FiniteStateMachineTemplate::~FiniteStateMachineTemplate() {
	threadAction->join();
	delete threadAction;
}


void FiniteStateMachineTemplate::enqueue(NvMediaImage* frame){

	if( NvQueuePut(videoBuffer, (void *)&frame, QUEUE_TIMEOUT) != NVMEDIA_STATUS_OK ) {
		fprintf(stdout, "%s: NvQueuePut failed. Frame (%p) dropped\n", __func__, frame);
		ImageBuffer *imBuffer = (ImageBuffer *)frame->tag;
		BufferPool_ReleaseBuffer(imBuffer->bufferPool, imBuffer);
	}

}

NvMediaImage* FiniteStateMachineTemplate::getFrame(){
	NvMediaImage* image;
	image = nullptr;
	if(NvQueueGet(videoBuffer, (void *)&image, QUEUE_TIMEOUT) != NVMEDIA_STATUS_OK) {
		//fprintf(stdout, "%s: NvQueueGet failed. \n", __func__, image);
	}

	return image;
}

void FiniteStateMachineTemplate::addNextStage(FiniteStateMachineTemplate* stage){

	nextStages->push_back(stage);
	nStages+=1;
}

void FiniteStateMachineTemplate::linkStages(NvMediaImage* frame){

	if (frame != nullptr){
		if(nStages>0){
			for (std::list<FiniteStateMachineTemplate*>::iterator it=nextStages->begin();
				 it != nextStages->end(); it++){
					if (*it != nullptr){
						(*it)->enqueue(frame);
					}
				}
		}else{
			ImageBuffer *imBuffer = (ImageBuffer *)frame->tag;
			BufferPool_ReleaseBuffer(imBuffer->bufferPool, imBuffer);
		}

	}else{
		//TODO[1] Error message
	}

}

void FiniteStateMachineTemplate::initMsgQueue(FiniteStateMachineTemplate* fsm) {

	fsm->outMsgQ = new std::queue <MSG>;
	initialized = true;
}

bool FiniteStateMachineTemplate::returnMsgQueueInitialized() {
	return initialized;
}

void FiniteStateMachineTemplate::linkQueues(FiniteStateMachineTemplate *fsm1, FiniteStateMachineTemplate *fsm2){
	//fsm1->inMsgQ = fsm2->outMsgQ;
	if (fsm2->returnMsgQueueInitialized()){
		fsm1->inMsgQ = fsm2->outMsgQ;
	}
	else{
		fsm2->initMsgQueue(fsm2);
		fsm1->inMsgQ = fsm2->outMsgQ;
	}
}

void FiniteStateMachineTemplate::threadBehaviour(){

	while(true){
		switch (currentState) {
			case State::TRANSITION_TO_INITIALIZED:
				internalChangeState(ST_Initializing());
				break;

			case State::INITIALIZED:
				internalChangeState(ST_Initialized());
				break;

			case State::TRANSITION_TO_RUN:
				internalChangeState(ST_Running());
				break;

			case State::RUN:
				internalChangeState(ST_Run());
				break;

			case State::TRANSITION_TO_STOPPED:
				internalChangeState(ST_Stopping());
				break;

			case State::STOPPED:
				internalChangeState(ST_Stopped());
				break;

			case State::TRANSITION_TO_FAILED:
				internalChangeState(ST_Failed());
				break;

			case State::FAILED:
				internalChangeState(ST_Failing());
				break;
			default:
				break;
		}
	}
}
FiniteStateMachineTemplate::TransitionMessage FiniteStateMachineTemplate::changeState(FiniteStateMachineTemplate::State newState){

		switch (newState) {
			case State::STOPPED:
				if (currentState == State::RUN){
					internalChangeState(State::TRANSITION_TO_STOPPED);
					return TransitionMessage::StateChanged;
				} else{
					return TransitionMessage::StateIgnored;
				}
				break;
			case State::INITIALIZED:
				if (currentState == State::STOPPED){
					internalChangeState(State::TRANSITION_TO_INITIALIZED);
					return TransitionMessage::StateChanged;
				} else{
					return TransitionMessage::StateIgnored;
				}
				break;
			case State::RUN:
				if (currentState == State::INITIALIZED){
					internalChangeState(State::TRANSITION_TO_RUN);
					return TransitionMessage::StateChanged;
				} else{
					return TransitionMessage::StateIgnored;
				}
				break;
			case State::FAILED:
				if (currentState == State::RUN){
					internalChangeState (State::TRANSITION_TO_FAILED);
					return TransitionMessage::StateChanged;
				} else {
					return TransitionMessage::StateIgnored;
				}
				break;
			default:
				return TransitionMessage::StateIgnored;
				break;
		}
		return TransitionMessage::StateIgnored;
}
FiniteStateMachineTemplate::TransitionMessage FiniteStateMachineTemplate::internalChangeState(FiniteStateMachineTemplate::State newState){

	switch (newState) {
		case State::STOPPED:
			if (currentState == State::TRANSITION_TO_STOPPED){
				currentState = newState;
				return TransitionMessage::StateChanged;
			}else return TransitionMessage::StateIgnored;

			break;
		case State::INITIALIZED:
			if (currentState == State::TRANSITION_TO_INITIALIZED){
				currentState = newState;
				return TransitionMessage::StateChanged;
			}else return TransitionMessage::StateIgnored;
			break;
		case State::RUN:
			if (currentState == State::TRANSITION_TO_RUN){
				currentState = newState;
				return TransitionMessage::StateChanged;
			}else return TransitionMessage::StateIgnored;
			break;
		case State::FAILED:
			if(currentState == State::TRANSITION_TO_FAILED){
				currentState = newState;
				return TransitionMessage::StateChanged;
			}else return TransitionMessage::StateIgnored;
			break;
		case State::TRANSITION_TO_STOPPED:
			if(currentState == State::RUN){
				currentState = newState;
				return TransitionMessage::StateChanged;
			}else return TransitionMessage::StateIgnored;
			break;
		case State::TRANSITION_TO_INITIALIZED:
			if(currentState == State::STOPPED){
				currentState = newState;
				return TransitionMessage::StateChanged;
			}else return TransitionMessage::StateIgnored;
			break;
		case State::TRANSITION_TO_RUN:
			if(currentState == State::INITIALIZED){
				currentState = newState;
				return TransitionMessage::StateChanged;
			}else return TransitionMessage::StateIgnored;
			break;
		case State::TRANSITION_TO_FAILED:
			currentState = newState;
			return TransitionMessage::StateChanged;
			break;
		default:
			return TransitionMessage::StateIgnored;
			break;
	}
	return TransitionMessage::StateIgnored;
}

FiniteStateMachineTemplate::State FiniteStateMachineTemplate::ST_Stopped(){
	return State::STOPPED;
}
FiniteStateMachineTemplate::State FiniteStateMachineTemplate::ST_Initialized(){
	return State::INITIALIZED;
}
FiniteStateMachineTemplate::State FiniteStateMachineTemplate::ST_Run(){
	return State::RUN;
}
FiniteStateMachineTemplate::State FiniteStateMachineTemplate::ST_Failed(){
	return State::FAILED;
}

FiniteStateMachineTemplate::State FiniteStateMachineTemplate::ST_Stopping(){
	return State::STOPPED;
}
FiniteStateMachineTemplate::State FiniteStateMachineTemplate::ST_Initializing(){
	return State::INITIALIZED;
};
FiniteStateMachineTemplate::State FiniteStateMachineTemplate::ST_Running(){
	return State::RUN;
};
FiniteStateMachineTemplate::State FiniteStateMachineTemplate::ST_Failing(){
	return State::FAILED;
};
