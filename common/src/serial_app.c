

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif // _GNU_SOURCE

//#define DEBUG_TRACE

#ifndef	_STDLIB_H
#include <stdlib.h>
#endif //_STDLIB_H
#ifndef	_STRING_H
#include <string.h>			// String management
#endif //_STRING_H
#ifndef	_SIGNAL_H
#include <signal.h>
#endif //_SIGNAL_H
#ifndef	_MATH_H
#include <math.h>			// Math library
#endif	//_MATH_H
#ifndef	_TERMIOS_H
#include <termios.h> 	// POSIX terminal control definitions
#endif //_TERMIOS_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "serial_app.h"


/*
CMS_LUMINANCE_CTRL_SERIAL_DEV = /dev/ttyUSB4
#CMS_LUMINANCE_CTRL_SERIAL_TYPE = USB-RS232
CMS_LUMINANCE_CTRL_SERIAL_TYPE = USB
#CMS_LUMINANCE_CTRL_SERIAL_BITRATE = 57600
CMS_LUMINANCE_CTRL_SERIAL_BITRATE = 115200
CMS_LUMINANCE_CTRL_SERIAL_DATA_PARITY_STOP = 8N1
CMS_LUMINANCE_CTRL_SERIAL_HW_FLOW_CTRL = no
CMS_LUMINANCE_CTRL_SERIAL_SW_FLOW_CTRL = no
CMS_LUMINANCE_CTRL_SERIAL_MAX_RTX = 0
*/


// Prinf serial port configuration
void serial_printf_settings(char *dev_str, char *type_str, char *dataparitystop_str, bool flow_ctrl_hw, bool flow_ctrl_sw, struct termios *newtio)
{
	speed_t baudrate = B0;
	int input_baudrate = -1;
	int output_baudrate = -1;

	//
	// Input baud rate
	//
	baudrate = cfgetispeed(newtio);
	switch (baudrate) {
	case B0:      input_baudrate = 0; break;
	case B50:     input_baudrate = 50; break;
	case B110:    input_baudrate = 110; break;
	case B134:    input_baudrate = 134; break;
	case B150:    input_baudrate = 150; break;
	case B200:    input_baudrate = 200; break;
	case B300:    input_baudrate = 300; break;
	case B600:    input_baudrate = 600; break;
	case B1200:   input_baudrate = 1200; break;
	case B1800:   input_baudrate = 1800; break;
	case B2400:   input_baudrate = 2400; break;
	case B4800:   input_baudrate = 4800; break;
	case B9600:   input_baudrate = 9600; break;
	case B19200:  input_baudrate = 19200; break;
	case B38400:  input_baudrate = 38400; break;
	case B57600:  input_baudrate = 57600; break;
	case B115200:  input_baudrate = 115200; break;
	case B230400:  input_baudrate = 230400; break;
	case B460800:  input_baudrate = 460800; break;
	case B500000:  input_baudrate = 500000; break;
	case B576000:  input_baudrate = 576000; break;
	case B921600:  input_baudrate = 921600; break;
	case B1000000:  input_baudrate = 1000000; break;
	case B1152000:  input_baudrate = 1152000; break;
	case B1500000:  input_baudrate = 1500000; break;
	case B2000000:  input_baudrate = 2000000; break;
	case B2500000:  input_baudrate = 2500000; break;
	case B3000000:  input_baudrate = 3000000; break;
	case B3500000:  input_baudrate = 3500000; break;
	case B4000000:  input_baudrate = 4000000; break;
	default:
		printf("\n%s Error: Invalid input baud rate\n", ficotime_now());
		break;
	}		

	//
	// Output baud rate
	//
	memset(&baudrate, 0, sizeof(speed_t)); 
	baudrate = cfgetospeed(newtio);
	switch (baudrate) {
	case B0:      output_baudrate = 0; break;
	case B50:     output_baudrate = 50; break;
	case B110:    output_baudrate = 110; break;
	case B134:    output_baudrate = 134; break;
	case B150:    output_baudrate = 150; break;
	case B200:    output_baudrate = 200; break;
	case B300:    output_baudrate = 300; break;
	case B600:    output_baudrate = 600; break;
	case B1200:   output_baudrate = 1200; break;
	case B1800:   output_baudrate = 1800; break;
	case B2400:   output_baudrate = 2400; break;
	case B4800:   output_baudrate = 4800; break;
	case B9600:   output_baudrate = 9600; break;
	case B19200:  output_baudrate = 19200; break;
	case B38400:  output_baudrate = 38400; break;
	case B57600:  output_baudrate = 57600; break;
	case B115200:  output_baudrate = 115200; break;
	case B230400:  output_baudrate = 230400; break;
	case B460800:  output_baudrate = 460800; break;
	case B500000:  output_baudrate = 500000; break;
	case B576000:  output_baudrate = 576000; break;
	case B921600:  output_baudrate = 921600; break;
	case B1000000:  output_baudrate = 1000000; break;
	case B1152000:  output_baudrate = 1152000; break;
	case B1500000:  output_baudrate = 1500000; break;
	case B2000000:  output_baudrate = 2000000; break;
	case B2500000:  output_baudrate = 2500000; break;
	case B3000000:  output_baudrate = 3000000; break;
	case B3500000:  output_baudrate = 3500000; break;
	case B4000000:  output_baudrate = 4000000; break;
	default:
		printf("\n%s Error: Invalid output baud rate\n", ficotime_now());
		break;
	}

#ifdef DEBUG_TRACE
	printf( "----------------------------------------------\n");
	printf( "%s SERIAL PORT INFO:\n", ficotime_now());
	printf( "\tDevice: %s\n", dev_str);
	printf( "\tConnection type: %s\n", type_str);
	printf( "\tData/Parity/Stop: %s\n", dataparitystop_str);
	printf( "\tInput speed: %d Bps\n", input_baudrate);
	printf( "\tOutput speed: %d Bps\n", output_baudrate);
	printf( "\tHW flow control: %s\n", flow_ctrl_hw == 1 ? "ON" : "OFF");
	printf( "\tSW flow control: %s\n", flow_ctrl_sw == 1 ? "ON" : "OFF");
	printf( "----------------------------------------------\n");
#endif
	return;
}



// Write string
bool serial_write_str(int serialfd, char *frame_str, char *dev_str)
{
	bool retval = 1;
	
	char *tmp_str = frame_str;
	size_t tmp_strlen = -1; 
	int nbytes = 0;
	int offset  = 0;
	bool done = 0;

	if (!(strlen(frame_str) > 0)) {

		printf("%s Warning: %d byte string length. Don't write\n", ficotime_now(), (int)strlen(frame_str));
	
	}
	else {

		printf("%s Writting \"%s\" (%d bytes) on %s...\n", ficotime_now(), frame_str, (int) strlen(frame_str), dev_str);

		// Disable data reception
		tcflow(serialfd, TCIOFF);
		// Clean serial drive's input queue
		tcflush(serialfd, TCIFLUSH);
		// Enable data transmission
		tcflow(serialfd, TCOON);
		// Clean serial driver's output queue before writing on it
		tcflush(serialfd, TCOFLUSH);
		// Write on serial port
		do {
			offset += nbytes;
			tmp_str = (frame_str + offset);
			tmp_strlen = strlen(tmp_str);
			if (!((tmp_strlen = strlen(tmp_str)) > 0)) {

				printf("%s Warning: %d bytes remaining for writting. Don't write\n", ficotime_now(), (int)tmp_strlen);
				retval = 0;
			}
			else if ((!(nbytes = write(serialfd, tmp_str, tmp_strlen)) > 0)) { 
				printf("\n%s Error: Cannot write \"%s\" (%d bytes) on %s\n\n",
					ficotime_now(),
					frame_str,
					(int)strlen(frame_str),
					dev_str);
				done = 1;
			}
			else if (offset + nbytes < strlen(frame_str)) {
				fprintf("%s Warning: Written  %d bytes from %d bytes %s\n",
					ficotime_now(),
					(offset + nbytes),
					(int)strlen(frame_str),
					dev_str);
			}
			else {

				fprintf("%s Written  %d bytes from %d bytes %s\n",
					ficotime_now(),
					(offset + nbytes),
					strlen(frame_str),
					dev_str);

				done = 1;
				// Transmit all output queue
				tcdrain(serialfd);
			}
		}
		while (done == 0);
	}
	return retval;
}




int ficosa_send_message(unsigned char * send_bytes, unsigned int length) 
{

        //printf("\n\n********************sendbytes***********************************\n\n");
        //printf("\n%x %x %x %x\n", send_bytes[0], send_bytes[1], send_bytes[2], send_bytes[3]);
        //printf("\n\n***************************************************************\n\n");
	//
	// SETTINGS SERIAL PORT
	//
	int nbytes;
	u_int8_t u8_backlight;	
	char write_frame_str[8];
	
	// Declarations
	char *dev_str;						// SERIAL device
	char *type_str;						// SERIAL type string
	char *baudrate_str;					// SERIAL baud rate
	char *dataparitystop_str;			// SERIAL data/parity/stop sting 
	int flow_ctrl_hw;					// SERIAL HW flow control flag
	int flow_ctrl_sw;					// SERIAL SW flow control flag
	u_int8_t max_rtx;					// SERIAL Max.retransmission number

	int serialfd;						// SERIAL file descriptor
	struct termios oldtio;				// SERIAL old serial configuration
	struct termios newtio;				// SERIAL new serial configuration

	// Initialize variables
	dev_str = "/dev/ttyUSB0";
	
	type_str = "USB-RS232";
	baudrate_str = "9600";
	dataparitystop_str = "8N1"; 
	flow_ctrl_hw = FICOSERIAL_FLOW_CTRL_OFF;
	flow_ctrl_sw = FICOSERIAL_FLOW_CTRL_OFF;
	max_rtx = 0;

	u8_backlight = 0;

	serialfd =-1;
	memset(&oldtio, 0, sizeof(struct termios));
	memset(&newtio, 0, sizeof(struct termios));

	nbytes = 0;

	//
	// CONNECT TO SERIAL PORT
	//
	
	
	// Open modem device for reading and writing and not as controlling tty
	// because we don't want to get killed if linenoise sends CTRL-C.
	serialfd = open(dev_str, O_RDWR | O_NOCTTY | O_NDELAY);
	if (serialfd == -1) {			
		printf("%s Error: Unable to open %s device\n", ficotime_now(), dev_str);
		return 0;
	} else {
#ifdef DEBUG_TRACE
		printf("%s Open %s device\n", ficotime_now(), dev_str);
#endif
	}	
	
#ifdef DEBUG_TRACE
	// Block serial read behavior
	printf("%s : Block serial read behavior\n", __func__);
#endif
	fcntl(serialfd, F_SETFL, 0);			
	
	// Set serial port configuration options

	// Clean struct for default port settings
#ifdef DEBUG_TRACE
	printf("%s : Clean struct for default port settings\n", __func__);
#endif
	memset(&oldtio, 0, sizeof(struct termios));
	memset(&newtio, 0, sizeof(struct termios));
	
	// Save current serial port settings
#ifdef DEBUG_TRACE
	printf("%s : Save current serial port settings\n", __func__);
#endif
	if (tcgetattr(serialfd, &oldtio) != 0) {
		if ((type_str  == FICOSERIAL_TYPE_USB) || (type_str == FICOSERIAL_TYPE_USB_RS232)) {
			printf("\n%s Error: Cannot save current serial port setting (%s)\n\n", ficotime_now(), strerror(errno));
		} else {
			printf("%s Warning: Cannot save current serial port setting (%s)\n", ficotime_now(), strerror(errno));
		}
	}

	//printf("%s BEFORE: oldtio\n", ficotime_now());
	//serial_printf_termios_hex(oldtio, log_fd);
	// Check serial connection type
	

	// Works for SERIAL_TYPE_USB_RS232:
#ifdef DEBUG_TRACE
	printf("%s Configuring USB-RS232 serial connection...\n", ficotime_now());
#endif

	newtio = oldtio;
	// Non - canonical
	newtio.c_cflag |= (CLOCAL | CREAD);
	newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
	// Raw output
	newtio.c_oflag &= ~OPOST;
	
	newtio.c_cflag &=  ~PARENB;            	// Make 8n1
	newtio.c_cflag &=  ~CSTOPB;
	newtio.c_cflag &=  ~CSIZE;
	newtio.c_cflag |=  CS8;

	newtio.c_iflag &= ~ CRTSCTS;			// Disable hardware control
	//newtio.c_iflag |= CRTSCTS;			// Enable hardware control
	
	newtio.c_iflag &= ~(IXON | IXOFF | IXANY);	// Disable software control
	//newtio.c_iflag |= (IXON | IXOFF | IXANY);	// Enable software control
	
	newtio.c_cc[VTIME] = 0;
	newtio.c_cc[VMIN] = 1;

	// Set speed
	cfsetispeed(&newtio, (speed_t)B9600);
	cfsetospeed(&newtio, (speed_t)B9600);
	
	// Clean the modem line and activate the settings for the port
	tcflush(serialfd, TCIOFLUSH);
	tcsetattr(serialfd, TCSANOW, &newtio);

	//
	// end FICOSERIAL_SET_CONFIG
	//
	serial_printf_settings(dev_str, type_str, dataparitystop_str, flow_ctrl_hw, flow_ctrl_sw, &newtio);
		
	
	//
	// SEND MESSAGE THROUGH SERIAL PORT
	//
	
	
	/*
		Byte 1: 0x55 --> PRV Roof
				0xAA --> PRV Door

		Byte 2: 0x00 --> Soiling OFF
				0x01 --> Soiling ON

		Byte 3: 0x00 --> Camara Limpia
				0x01 --> Camara Sucia

		Byte 4: XOR of previous 3 bytes
	*/
	
	
	// 1111 1110
	
	// 01010101
	// 00000001
	// 00000001
	// 01010100
		
	
	//unsigned char send_bytes4[] = { 0x55, 0x00, 0x01, 0x54}; // PRV_Roof | Soil_ON | Lens_Dirty | XOR_Value
	
	//write(serialfd, send_bytes4, 4);  //Send data
        //printf("\n\n********************sendbytes***********************************");
        //printf("\n%x %x %x %x\n", send_bytes[0], send_bytes[1], send_bytes[2], send_bytes[3]);
        //printf("\n\n***************************************************************");
	write(serialfd, send_bytes, length);  //Send data
	
	tcdrain(serialfd);
	 
	// PRV DOOR/ROOF
	/*
	snprintf(buf, sizeof(buf), "%d", prv_roof);
	nbytes = write( serialfd, buf, sizeof(buf) );
	printf("nbytes = %d\n", nbytes);
	
	// SOILING ON/OFF
	snprintf(buf, sizeof(buf), "%d", soild_on);
	nbytes = write( serialfd, buf, sizeof(buf) );
	printf("nbytes = %d\n", nbytes);
	// CLEAN/DIRTY
	snprintf(buf, sizeof(buf), "%d", soild_clean);
	nbytes = write( serialfd, buf, sizeof(buf) );
	printf("nbytes = %d\n", nbytes);
	// XOR
	snprintf(buf, sizeof(buf), "%d", soild_dirty);
	nbytes = write( serialfd, buf, sizeof(buf) );
	printf("nbytes = %d\n", nbytes);	
	*/
	//
	// DISCONNECT FROM SERIAL PORT
	//
	
	// Set old configuration
	tcsetattr(serialfd, TCSANOW, &oldtio);
	// Close connection
	if (close(serialfd) < 0) {
		printf("\n%s Error: Cannot close serial file descriptor <%d>\n\n", ficotime_now(), serialfd);
	}
	else {
#ifdef DEBUG_TRACE
		printf("%s Serial file descriptor <%d> has been closed\n",  ficotime_now(), serialfd);
#endif
		serialfd = -1;
	}
	
	return 0;
}
