/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file FiniteStateMachineTemplateWithAux.cpp
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "FiniteStateMachineTemplateWithAux.h"

FiniteStateMachineTemplateWithAux::FiniteStateMachineTemplateWithAux()
	:FiniteStateMachineTemplate(){
	if(NvQueueCreate(&auxVideoBuffer, BUFFER_SIZE, sizeof(NvMediaImage*)) != NVMEDIA_STATUS_OK) {
		fprintf(stderr, "Could not init Queue!\n");
	}
	nextAuxStages = new std::list<FiniteStateMachineTemplate*>;
	nAuxStages=0;
	trobat = false;
}
FiniteStateMachineTemplateWithAux::~FiniteStateMachineTemplateWithAux() {
	// TODO Auto-generated destructor stub
}

void FiniteStateMachineTemplateWithAux::auxEnqueue(NvMediaImage* frame){
	if( NvQueuePut(auxVideoBuffer, (void *)&frame, QUEUE_TIMEOUT) != NVMEDIA_STATUS_OK ) {
		fprintf(stdout, "%s: NvQueuePut failed. Frame (%p) dropped\n", __func__, frame);
	} else {
		fprintf(stdout, "%s: Enqueued frame (%p) \n", __func__, frame);
	}
}

NvMediaImage* FiniteStateMachineTemplateWithAux::getAuxFrame(){
	NvMediaImage* image;
	image = nullptr;

	if(NvQueueGet(auxVideoBuffer, (void *)&image, QUEUE_TIMEOUT) != NVMEDIA_STATUS_OK) {
		fprintf(stdout, "%s: NvQueueGet failed. \n", __func__, image);
	}

	return image;
}

void FiniteStateMachineTemplateWithAux::addNextAuxStage(FiniteStateMachineTemplate* stage){
	nextAuxStages->push_back(stage);
	nAuxStages+=1;
}

void FiniteStateMachineTemplateWithAux::linkAuxStages(NvMediaImage* frame){

	if (frame != nullptr){
		if(nAuxStages>0){
			for (std::list<FiniteStateMachineTemplate*>::iterator it=nextAuxStages->begin();
				 it != nextAuxStages->end(); it++){
					if (*it != nullptr){
						(*it)->enqueue(frame);
					}
				}
		}else{
			ImageBuffer *imBuffer = (ImageBuffer *)frame->tag;
			BufferPool_ReleaseBuffer(imBuffer->bufferPool, imBuffer);
		}


	}else{
	//TODO[1] Error message
	}
}
FiniteStateMachineTemplateWithAux::State FiniteStateMachineTemplateWithAux::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
FiniteStateMachineTemplateWithAux::State FiniteStateMachineTemplateWithAux::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
FiniteStateMachineTemplateWithAux::State FiniteStateMachineTemplateWithAux::ST_Run(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
FiniteStateMachineTemplateWithAux::State FiniteStateMachineTemplateWithAux::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

FiniteStateMachineTemplateWithAux::State FiniteStateMachineTemplateWithAux::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
FiniteStateMachineTemplateWithAux::State FiniteStateMachineTemplateWithAux::ST_Initializing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
FiniteStateMachineTemplateWithAux::State FiniteStateMachineTemplateWithAux::ST_Running(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
FiniteStateMachineTemplateWithAux::State FiniteStateMachineTemplateWithAux::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
