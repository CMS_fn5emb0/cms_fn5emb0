/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2015 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "eglstream.h"
#include <iostream>

static PFNEGLCREATESTREAMKHRPROC eglCreateStreamKHR = nullptr;
static PFNEGLDESTROYSTREAMKHRPROC eglDestroyStreamKHR = nullptr;
static PFNEGLSTREAMATTRIBKHRPROC eglStreamAttribKHR = nullptr;
static PFNEGLQUERYSTREAMKHRPROC eglQueryStreamKHR = nullptr;
static PFNEGLQUERYSTREAMU64KHRPROC eglQueryStreamu64KHR = nullptr;
static PFNEGLQUERYSTREAMTIMEKHRPROC eglQueryStreamTimeKHR = nullptr;
static PFNEGLGETPLATFORMDISPLAYEXTPROC eglGetPlatformDisplayEXT = nullptr;
static PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC eglCreatePlatformWindowSurfaceEXT = nullptr;
static PFNEGLCREATEPLATFORMPIXMAPSURFACEEXTPROC eglCreatePlatformPixmapSurfaceEXT = nullptr;
static PFNEGLGETOUTPUTLAYERSEXTPROC eglGetOutputLayersEXT = nullptr;
static PFNEGLQUERYDEVICESEXTPROC eglQueryDevicesEXT = nullptr;
static PFNEGLQUERYDEVICESTRINGEXTPROC eglQueryDeviceStringEXT = nullptr;
#include <Logutils.hpp>
using namespace logutils;

EGLStream::EGLStream(EGLDisplay display_, Mode mode, int queueLength)
: display(display_)
{
    // load required EGL extensions
    loadExtensions();
    
    if( display == EGL_NO_DISPLAY) 
    {
        EGLint deviceCount = 4;
        EGLint foundDevices = 0;
        EGLDeviceEXT devices[deviceCount];
        
        // Get pointers of required EGL functions
        EGLBoolean status = eglQueryDevicesEXT(deviceCount, devices, &foundDevices);
        if (status != EGL_TRUE) 
        {
           // std::cout << "Failed to query devices (error: " << std::hex << eglGetError() << std::dec << std::endl;
            LOG_ERR_( "Function Name: %s,Failed to query devices error %x %s %d:\n", __func__,std::hex,eglGetError(),std::dec);
            return;
        }
        
//        EGLAttrib layerAttribs[] = {
//            EGL_NONE, EGL_NONE,
//            EGL_NONE, EGL_NONE,
//        };
        // get device name to setup DRM, assume that devices[0] is the one to use
        const char *drmName = eglQueryDeviceStringEXT(devices[0], EGL_DRM_DEVICE_FILE_EXT);
        if (!drmName) 
        {
            LOG_ERR_( "Function Name: %s,Unable to query device string!\n", __func__);
            return;
        } else {
            LOG_DBG_( "Function Name: %s,Device Name: %s\n", __func__,drmName);

        }

        EGLenum platform = EGL_PLATFORM_DEVICE_EXT;
        EGLint dispAttribs[] = {
            EGL_NONE, EGL_NONE
        };
        // create display on top of DRM display
        display = eglGetPlatformDisplayEXT(platform, (NativeDisplayType)devices[0], dispAttribs);
        if(display == EGL_NO_DISPLAY)
        {
            LOG_ERR_( "Function Name: %s,Failed to create EGL display\n", __func__);
            return;
        }
        
        // setup EGL like in windowing system
        status = eglInitialize(display, 0, 0);
        if(!status)
        {
            LOG_ERR_( "Function Name: %s,Could not init EGL!\n", __func__);
            return;
        }

    }

    // create and configure stream either as FIFO or mailbox
    if(mode == MAILBOX)
    {
        EGLint streamAttr[] = { 
            EGL_NONE 
        };
        eglStream = eglCreateStreamKHR(display, streamAttr);
    }
    else //FIFO
    {
        EGLint streamAttr[] = { 
            EGL_STREAM_FIFO_LENGTH_KHR, queueLength,
            EGL_NONE
        };
        eglStream = eglCreateStreamKHR(display, streamAttr);
    }
    if(eglStream == EGL_NO_STREAM_KHR)
    {
        LOG_ERR_( "Function Name: %s,Failed to create EGLStream!\n", __func__);
        return;
    }
    
    // configure stream
    if(!eglStreamAttribKHR(display, eglStream, EGL_CONSUMER_LATENCY_USEC_KHR, 16000))
    {
        LOG_ERR_( "Function Name: %s,Failed to set EGL_CONSUMER_LATENCY_USEC_KHR\n", __func__);
    }
    if(!eglStreamAttribKHR(display, eglStream, EGL_CONSUMER_ACQUIRE_TIMEOUT_USEC_KHR, 16000))
    {
        LOG_ERR_( "Function Name: %s,Failed to set EGL_CONSUMER_ACQUIRE_TIMEOUT_USEC_KHR\n", __func__);
    }
}

EGLStream::~EGLStream()
{
    // release stream
    eglDestroyStreamKHR(display, eglStream);
    eglTerminate(display);
}

void EGLStream::loadExtensions()
{
    // query the driver for extensions if not yet done
    if(eglCreateStreamKHR == nullptr)
    {
        eglCreateStreamKHR = (PFNEGLCREATESTREAMKHRPROC)eglGetProcAddress("eglCreateStreamKHR");
        if(eglCreateStreamKHR == nullptr)
        {
            LOG_ERR_( "Function Name: %s,Failed to load eglCreateStreamKHR\n", __func__);
        }
    }
    if(eglDestroyStreamKHR == nullptr)
    {
        eglDestroyStreamKHR = (PFNEGLDESTROYSTREAMKHRPROC)eglGetProcAddress("eglDestroyStreamKHR");
        if(eglDestroyStreamKHR == nullptr)
        {
            LOG_ERR_( "Function Name: %s,Failed to load eglDestroyStreamKHR\n", __func__);
        }
    }
    if(eglStreamAttribKHR == nullptr)
    {
        eglStreamAttribKHR = (PFNEGLSTREAMATTRIBKHRPROC)eglGetProcAddress("eglStreamAttribKHR");
        if(eglStreamAttribKHR == nullptr)
        {
            LOG_ERR_( "Function Name: %s,Failed to load eglStreamAttribKHR\n", __func__);
        }
    }
    if(eglQueryStreamKHR == nullptr)
    {
        eglQueryStreamKHR = (PFNEGLQUERYSTREAMKHRPROC)eglGetProcAddress("eglQueryStreamKHR");
        if(eglQueryStreamKHR == nullptr)
        {
            LOG_ERR_( "Function Name: %s,Failed to load eglQueryStreamKHR\n", __func__);
        }
    }
    if(eglQueryStreamu64KHR == nullptr)
    {
        eglQueryStreamu64KHR = (PFNEGLQUERYSTREAMU64KHRPROC)eglGetProcAddress("eglQueryStreamu64KHR");
        if(eglQueryStreamu64KHR == nullptr)
        {
            LOG_ERR_( "Function Name: %s,Failed to load eglQueryStreamu64KHR\n", __func__);
        }
    }
    if(eglQueryStreamTimeKHR == nullptr)
    {
        eglQueryStreamTimeKHR = (PFNEGLQUERYSTREAMTIMEKHRPROC)eglGetProcAddress("eglQueryStreamTimeKHR");
        if(eglQueryStreamTimeKHR == nullptr)
        {
            LOG_ERR_( "Function Name: %s,Failed to load eglQueryStreamTimeKHR\n", __func__);
        }
    }   

    eglGetPlatformDisplayEXT = (PFNEGLGETPLATFORMDISPLAYEXTPROC)eglGetProcAddress("eglGetPlatformDisplayEXT");
    eglCreatePlatformWindowSurfaceEXT = (PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC) eglGetProcAddress("eglCreatePlatformWindowSurfaceEXT");
    eglCreatePlatformPixmapSurfaceEXT = (PFNEGLCREATEPLATFORMPIXMAPSURFACEEXTPROC)eglGetProcAddress("eglCreatePlatformPixmapSurfaceEXT");
    eglGetOutputLayersEXT = (PFNEGLGETOUTPUTLAYERSEXTPROC)eglGetProcAddress("eglGetOutputLayersEXT");
    eglQueryDevicesEXT = (PFNEGLQUERYDEVICESEXTPROC)eglGetProcAddress("eglQueryDevicesEXT");
    eglQueryDeviceStringEXT = (PFNEGLQUERYDEVICESTRINGEXTPROC)eglGetProcAddress("eglQueryDeviceStringEXT");
}

EGLStreamKHR EGLStream::getID()
{
    return eglStream;
}

EGLDisplay EGLStream::getEGLDisplay()
{
    return display;
}

EGLuint64KHR EGLStream::getFrameNumberConsumer()
{
    EGLuint64KHR frame;
    eglQueryStreamu64KHR(display, eglStream, EGL_CONSUMER_FRAME_KHR, &frame);
    return frame;
}

EGLuint64KHR EGLStream::getFrameNumberProducer()
{
    EGLuint64KHR frame;
    eglQueryStreamu64KHR(display, eglStream, EGL_PRODUCER_FRAME_KHR, &frame);
    return frame;
}

EGLTimeKHR EGLStream::getStreamTime()
{
    EGLTimeKHR time;
    eglQueryStreamTimeKHR(display, eglStream, EGL_STREAM_TIME_NOW_KHR, &time);
    return time;
}

EGLTimeKHR EGLStream::getConsumerTime()
{
    EGLTimeKHR time;
    eglQueryStreamTimeKHR(display, eglStream, EGL_STREAM_TIME_CONSUMER_KHR, &time);
    return time;
}

EGLTimeKHR EGLStream::getProducerTime()
{
    EGLTimeKHR time;
    eglQueryStreamTimeKHR(display, eglStream, EGL_STREAM_TIME_PRODUCER_KHR, &time);
    return time;
}
