/*****************************************************************************
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************************************/

/*
 * common_utils.c
 *
 *  Created on: 17/08/2015
 *      Author: Galder Unibaso <galder.unibaso@idneo.es>
 */

#ifndef __COMMON_UTILS_H__
#include "common_utils.h"
#endif // __COMMON_UTILS_H__

#ifndef	_STRING_H
#include <string.h>			// String management
#endif //_STRING_H
#ifndef	_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif //_SYS_IOCTL_H
#ifndef	_UNISTD_H
#include <unistd.h>
#endif //_UNISTD_H
#ifndef	_ERRNO_H
#include <errno.h>
#endif //_ERRNO_H
#ifndef	_CTYPE_H
#include <ctype.h>			// Character handling library
#endif //_CTYPE_H
#ifndef	_MATH_H
#include <math.h>			// Math library
#endif	//_MATH_H
//#ifndef	_TIME_H
//#include <time.h>			// Time library
//#endif 	//_TIME_H
#ifndef	_SYS_TIMERFD_H
#include <sys/timerfd.h>	// Timerfd library
#endif //_SYS_TIMERFD_H

//----------------------------------------------------------------------------
// GLOBAL VARIABLES
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// FUNCTIONS
//----------------------------------------------------------------------------

// Print u_int8_t hexadecimal
void ficoprintf_uint8_hex(u_int8_t *frame, u_int8_t frame_len, FILE *log_fd)
{
	u_int8_t i = 0;
	
	if (frame == NULL) {
		return;
	}
	else if (!(frame_len > 0)) {
		return;
	}
	else if (log_fd == NULL) {
		log_fd = stdout;
	}
	
	for (i = 0; i < frame_len; i++) {
		if (i == (frame_len - 1)) {
			fprintf(log_fd, "0x%02x\n", *(frame + i));
		}
		else {
			fprintf(log_fd, "0x%02x-", *(frame + i));
		}
		fflush(log_fd);
	}
	return;
}

// Print uint hexadecimal
void ficoprintf_uint_hex(u_int *frame, u_int frame_len, FILE *log_fd)
{
	u_int i = 0;
	
	if (frame == NULL) {
		return;
	}
	else if (!(frame_len > 0)) {
		return;
	}
	else if (log_fd == NULL) {
		log_fd = stdout;
	}
	
	for (i = 0; i < frame_len; i++) {
		if (i == (frame_len - 1)) {
			fprintf(log_fd, "0x%08x\n", *(frame + i));
		}
		else {
			fprintf(log_fd, "0x%08x-", *(frame + i));
		}
		fflush(log_fd);
	}
	return;
}

// Get now local time string HH:mm:ss
char *ficotime_now(void)
{
	time_t now_time = time(NULL);
	int i = 0;

	strtok(asctime(localtime(&now_time))," ");
	for (i=0; i<2; i++) {
		strtok(NULL, " ");
	}
	return (strtok(NULL," "));
}

// Get now local time string HH:mm:ss.mmm
bool ficotime_now_ms(char **now_time_str)
{
	struct timespec now_time_spec;
	char tmp_str[FICOTIME_MS_MAXSTRLEN];
	
	int i = 0;
	bool retval = 1;
	
	if (now_time_str == NULL) {
		retval = 0;
	}
	else {
		memset(tmp_str, '\0', FICOTIME_MS_MAXSTRLEN);
		clock_gettime(CLOCK_REALTIME, &now_time_spec);
		strtok(asctime(localtime(&(now_time_spec.tv_sec)))," ");
		for (i=0; i<2; i++) {
			strtok(NULL, " ");
		}
		// Get now local time string HH:mm:ss.ms
		snprintf(tmp_str, FICOTIME_MS_MAXSTRLEN, "%s.%03d", strtok(NULL," "), (int)round(now_time_spec.tv_nsec/1.0e6));
		if (!ficostring_copy(now_time_str, tmp_str)) {
			retval = 0;
		}
	}
	return (retval);
}

// Get now local time string HH:mm:ss.uuuuuu
bool ficotime_now_us(char **now_time_str)
{
	struct timespec now_time_spec;
	char tmp_str[FICOTIME_US_MAXSTRLEN];
	
	int i = 0;
	bool retval = 1;
	
	if (now_time_str == NULL) {
		retval = 0;
	}
	else {
		memset(tmp_str, '\0', FICOTIME_US_MAXSTRLEN);
		clock_gettime(CLOCK_REALTIME, &now_time_spec);
		strtok(asctime(localtime(&(now_time_spec.tv_sec)))," ");
		for (i=0; i<2; i++) {
			strtok(NULL, " ");
		}
		// Get now local time string HH:mm:ss.uuuuuu
		snprintf(tmp_str, FICOTIME_US_MAXSTRLEN, "%s.%06d", strtok(NULL," "), (int)round(now_time_spec.tv_nsec/1.0e3));
		if (!ficostring_copy(now_time_str, tmp_str)) {
			retval = 0;
		}
	}
	return (retval);
}

// Get now local time string HH:mm:ss.nnnnnnnnn
bool ficotime_now_ns(char **now_time_str)
{
	struct timespec now_time_spec;
	char tmp_str[FICOTIME_NS_MAXSTRLEN];
	
	int i = 0;
	bool retval = 1;
	
	if (now_time_str == NULL) {
		retval = 0;
	}
	else {
		memset(tmp_str, '\0', FICOTIME_NS_MAXSTRLEN);
		clock_gettime(CLOCK_REALTIME, &now_time_spec);
		strtok(asctime(localtime(&(now_time_spec.tv_sec)))," ");
		for (i=0; i<2; i++) {
			strtok(NULL, " ");
		}
		// Get now local time string HH:mm:ss.uuuuuu
		snprintf(tmp_str, FICOTIME_NS_MAXSTRLEN, "%s.%ld", strtok(NULL," "), now_time_spec.tv_nsec);
		if (!ficostring_copy(now_time_str, tmp_str)) {
			retval = 0;
		} 
	}
	return (retval);
}

// String free
void ficostring_free(char **str)
{
	if (str == NULL) {
		return;
	}
	else if (*str != NULL) {
		free(*str);
		(*str) = NULL;
	}
	return;
}

bool ficostring_copy(char **to, char *from)
{
	bool retval = 1;

	if ((to == NULL)
			|| (from == NULL)) {
		retval = 0;
	}
	else {
		ficostring_free(to);
		if (((*to) = (char *)malloc(strlen(from) + 1)) == NULL) {
			retval = 0;
		}
		else {
			memset((*to), '\0', (strlen(from) + 1));
			strcpy((*to), from);
		}
	}
	return retval;
}

// Parse to upper string
bool ficoparse_toupper(char **input)
{
	char *p = NULL;
	bool retval = 1;
	printf("%s\n", __func__);
	if ((input == NULL) || (*input == NULL)) {
		retval = 0;
	}
	else {
		
		for (p = *input; *p != '\0'; ++p) {
			printf("****\n");
			*p = toupper(*p);			
		}
	}
	printf("%s : retVal=%d\n", __func__, retval);
	return retval;
}


// Parse to lower string
bool ficoparse_tolower(char **input)
{
	char *p = NULL;
	bool retval = 1;

	if ((input == NULL) || (*input == NULL)) {
		retval = 0;
	}
	else {
		for (p = *input; *p != '\0'; ++p) {
			*p = tolower(*p);
		}
	}
	return retval;
}

// Parse configuration parameter and value
bool ficoparse_conf_param_value(char *ps_name, char *param, char *value, const char **params, char ***values, bool **is_set_values, bool **is_available_params, int max_param_num, FILE *log_fd)
{
	bool found = 0;
	int i = 0;

	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (ps_name == NULL) {
		retval = 0;
	}
	else if (param == NULL)
	{
		retval = 0;
	}
	else if (value == NULL)
	{
		retval = 0;
	}
	else if ((params == NULL)
			|| (*params == NULL)) {
		retval = 0;
	}
	else if ((values == NULL)
			|| (*values == NULL)) {
		retval = 0;
	}
	else if ((is_set_values == NULL)
			|| (*is_set_values == NULL)) {
		retval = 0;
	}
	else if ((is_available_params == NULL)
			|| (*is_available_params == NULL)) {
		retval = 0;
	}
//	else if (!ficoparse_tolower(&param)) {
//		retval = 0;
//	}
//	else if (!ficoparse_tolower(&value)) {
//		retval = 0;
//	}
	else {
		i = 0;
		while ((!found)
				&& (i < max_param_num)) {
//			if (!ficoparse_tolower(&params[i])) {
//				retval = 0;
//			}
//			else if (strcmp(param, params[i]) == 0)
			if ((*((*is_available_params)+i))
					&& (strcmp(param, params[i]) == 0)) {
				found = 1;
//				if (!ficoparse_tolower(((*values)+i))) {
//					retval = 0;
//				}
//				else if (strcmp(value, *((*values)+i)) != 0) {
				if (strcmp(value, *((*values)+i)) != 0) {
					if (strlen(*((*values)+i)) != strlen(value)) {
						(*((*values)+i)) = (char *)realloc((*((*values)+i)), strlen(value)+1);
						if (*((*values)+i) == NULL) {
							memset(*((*values)+i), '\0', strlen(value)+1);
							free(*((*values)+i));
							*((*values)+i) = NULL;
							*((*is_set_values)+i) = 0;
							retval = 0;
						}
						else {
							memset(*((*values)+i), '\0', strlen(value)+1);
							strcpy(*((*values)+i), value);
							*((*is_set_values)+i) = 1;
						}
					}
					else {
						strcpy(*((*values)+i), value);
						*((*is_set_values)+i) = 1;
					}
				}
				else {
					*((*is_set_values)+i) = 1;
				}
			}
			else if (!(*((*is_available_params)+i))
					&& (strcmp(param, params[i]) == 0)) {
				found = 1;
				fprintf(log_fd, "\n[%s] Warning: [%s-%s] is not available for this version. Ignore it\n",
						ps_name, param, value);
				fflush(log_fd);
			}
			else {
				i++;
			}
		}

		if (!found) {
			fprintf(log_fd, "\n[%s] Error: Not valid configuration parameter [%s-%s] for this version. Ignore it\n\n",
					ps_name, param, value);
			fflush(log_fd);
		}
#ifdef DEBUG
		else {
			fprintf(stdout, "[%s-%s]: %s\n", params[i], (*((*values)+i)), (((*((*is_available_params)+i)) && (retval == 1))?"OK":"KO"));
			fflush(stdout);
		}
#endif //DEBUG
	}
#ifdef DEBUG
	if (!retval) {
		fprintf(log_fd, "\n[%s] Error: Cannot parse configuration parameter [%s-%s] for this version\n\n",
				ps_name, param, value);
		fflush(log_fd);
	}
#endif //DEBUG
	return retval;
}

// Set non-blocking property to a socket
int ficosocket_set_nonblocking(int sockfd)
{
	int flags;

	if (sockfd < 0) {
		return 0;
	}
#ifdef O_NONBLOCK
	if ((flags = fcntl(sockfd, F_GETFL, 0)) == -1) {
		flags = 0;
	}
	return (fcntl(sockfd, F_SETFL, flags | O_NONBLOCK));
#else
	flags = 1;
	// FIXME WARNING Not found header file for FIOBIO definition
	//return (ioctl(sockfd, FIOBIO, &flags));
	return (ioctl(sockfd, FIONBIO, &flags));
#endif //O_NONBLOCK
}

// Close socket
bool ficosocket_close(int *sockfd, FILE *log_fd)
{
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	// Check socket file descriptor
	if (sockfd == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s Error: Invalid socket address\n\n", ficotime_now());
		fflush(log_fd);
#endif //DEBUG
		retval = 0;
	}
	else if (*sockfd < 0) {
		fprintf(log_fd, "\n%s Error: Socket <%d> is not initialized or it has been already closed\n\n", ficotime_now(), *sockfd);
		fflush(log_fd);
		retval = 0;
	}
	else {
		// Close connection
		if (close(*sockfd) < 0) {
			fprintf(log_fd, "\n%s Error: Cannot close socket <%d>\n\n", ficotime_now(), *sockfd);
			fflush(log_fd);
			retval = 0;
		}
		else {
#ifdef DEBUG
			fprintf(log_fd, "%s Socket <%d> has been closed\n", ficotime_now(), *sockfd);
			fflush(log_fd);
#endif //DEBUG
			*sockfd = -1;
		}
	}
	return retval;
}

// Check if file_str file exists
bool ficofile_exists(char *file_str)
{
	FILE *fd = NULL;

	bool retval = 0;

	if ((fd = fopen(file_str, "r")) != NULL) {
		fclose(fd);
		retval = 1;
	}
	return retval;
}

// Close fd file descriptor
bool ficofile_close(FILE **fd)
{
	bool retval = 1;
	if ((*fd) != NULL) {
		if (fclose(*fd) == EOF) {
			retval = 0;
		}
		else {
			(*fd) = NULL;
		}
	}
	return retval;
}

// Open file_str file to read
FILE *ficofile_open_read(char *file_str)
{
	FILE *fd = NULL;

	if (ficofile_exists(file_str) < 1) {
		return NULL;
	}
	fd = fopen(file_str, "r");
	return fd;
}

// Open file_str file to write
FILE *ficofile_open_write(char *file_str)
{
	return (fopen(file_str, "w"));
}

// Gets param from line with delim (ex: "param delim value")
char *ficoline_get_param_with_delim(char *line, char *delim)
{
	char *param = NULL;
	char *temp = NULL;

	if ((line == NULL)
			|| (delim == NULL)) {
		return NULL;
	}

	param = strtok(line, delim);
	temp = strtok(param, FICOLINE_SPACE_DELIMITER);
	while ((temp != NULL) && (strcmp(param, temp) != 0)) {
		param = temp;
		temp = strtok(NULL, FICOLINE_SPACE_DELIMITER);
	}
	return param;
}

// Gets value from line with delim (ex: "param delim value")
char *ficoline_get_value_with_delim(char *line, char *delim)
{
	char *value;
	char *temp;

	if ((line == NULL)
			|| (delim == NULL)) {
		return NULL;
	}

	strtok(line, delim);
	value = strtok(NULL, FICOLINE_RETURN_DELIMITER);
	temp = strtok(value, FICOLINE_SPACE_DELIMITER);
	while ((temp != NULL) && (strcmp(value, temp) != 0)) {
		value = temp;
		temp = strtok(value, FICOLINE_SPACE_DELIMITER);

	}
	return value;
}

// Try to lock a mutex
bool ficopthread_trylock_mutex(pthread_mutex_t *mutex)
{
	bool retval = 1;

	// Check address
	if (mutex == NULL) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid mutex address\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Try to block mutex
	else if (pthread_mutex_trylock(mutex) != 0) {
#ifdef DEBUG
		fprintf(stderr, "[%s] Warning: mutex not locked\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	return retval;
}

// Block to trying to lock a mutex
bool ficopthread_trylock_mutex_block(pthread_mutex_t *mutex)
{
	bool retval = 1;

	// Check address
	if (mutex == NULL) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid mutex address\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval =  0;
	}
	else {
		// Wait until mutex it locked
		while (pthread_mutex_trylock(mutex) != 0) {
//#ifdef DEBUG
//			fprintf(stderr, "[%s] Warning: mutex is already locked. Wait\n", __func__);
//			fflush(stderr);
//#endif	//DEBUG
		}
		// Mutex locked
		retval = 1;
	}
	return retval;
}

// Unlock a mutex
bool ficopthread_unlock_mutex(pthread_mutex_t *mutex)
{
	bool retval = 1;

	// Check address
	if (mutex == NULL) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid mutex address\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Try to unlock mutex
	else if (pthread_mutex_unlock(mutex) != 0) {
		// Mutex not unlocked
#ifdef DEBUG
		fprintf(stderr, "[%s] Warning: Mutex not unlocked\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	return retval;
}

// Unlock a mutex (blocking)
bool ficopthread_unlock_mutex_block(pthread_mutex_t *mutex)
{
	bool retval = 1;

	// Check address
	if (mutex == NULL) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid mutex address\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Try to unlock mutex
	else {
		// Wait until mutex it locked
		while (pthread_mutex_unlock(mutex) != 0) {
//#ifdef DEBUG
//			fprintf(stderr, "[%s] Warning: Mutex not unlocked\n", __func__);
//			fflush(stderr);
//#endif //DEBUG
		}
	}
	return retval;
}

// Init mutexed bool value (blocking)
bool ficopthread_init_mutexed_bool_block(bool value, bool *mutexed_var, pthread_mutex_t *mutex)
{
	bool retval = 1;

	// Check variable address
	if (mutexed_var == NULL) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid mutexed bool variable address\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Check value
	else if ( (value != 0)
		&& (value != 1) ) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid value \"%d\" for mutexed bool variable\n\n", __func__, value);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	else {
		// Assign value
		(*mutexed_var) = value;

		// Init mutex
		if (pthread_mutex_init(mutex, NULL) != 0)
		{
#ifdef DEBUG
			fprintf(stderr,
					"\n%s Error: Cannot initialize mutexed bool variable\n\n",
					__func__);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
	}
	return retval;
}

// Set mutexed bool value (blocking)
bool ficopthread_set_mutexed_bool_block(bool value, bool *mutexed_var, pthread_mutex_t *mutex)
{
	bool retval = 1;

	// Check variable address
	if (mutexed_var == NULL) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid mutexed bool variable address\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Check value
	else if ( (value != 0)
		&& (value != 1) ) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid value \"%d\" for mutexed bool variable\n\n", __func__, value);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Block until lock mutex
	else if (!ficopthread_trylock_mutex_block(mutex)) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Cannot lock-block mutexed bool variable\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	else {
		// Assign value
		(*mutexed_var) = value;

		// Block until unlock mutex
		if (!ficopthread_unlock_mutex_block(mutex)) {
#ifdef DEBUG
			fprintf(stderr, "\n[%s] Error: Cannot unlock-block mutexed bool variable\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
	}
	return retval;
}

// Set mutexed bool value (unblocking)
bool ficopthread_set_mutexed_bool_unblock(bool value, bool *mutexed_var, pthread_mutex_t *mutex)
{
	bool retval = 1;

	// Check variable address
	if (mutexed_var == NULL) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid mutexed bool variable address\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Check value
	else if ( (value != 0)
		&& (value != 1) ) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid value \"%d\" for mutexed bool variable\n\n", __func__, value);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Try to lock mutex without blocking
	else if (!ficopthread_trylock_mutex(mutex)) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Cannot lock-unblock mutexed bool variable\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	else {
		// Assign value
		(*mutexed_var) = value;

		// Unlock mutex without blocking
		if (!ficopthread_unlock_mutex(mutex)) {
#ifdef DEBUG
			fprintf(stderr, "\n[%s] Error: Cannot unlock-unblock mutexed bool variable\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
	}
	return retval;
}

// Get mutexed bool value (blocking)
bool ficopthread_get_mutexed_bool_block(bool *value, bool mutexed_var, pthread_mutex_t *mutex)
{
	bool retval = 1;

//	// Check value address
	if (value == NULL) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Invalid value variable address\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	// Block until lock mutex
	else if (!ficopthread_trylock_mutex_block(mutex)) {
#ifdef DEBUG
		fprintf(stderr, "\n[%s] Error: Cannot lock mutexed bool variable\n\n", __func__);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	else {
		// Read variable
		if ( (mutexed_var != 0)
			&& (mutexed_var != 1) ) {
#ifdef DEBUG
			fprintf(stderr, "\n[%s] Error: Invalid mutexed bool variable value \"%d\"\n\n", __func__, mutexed_var);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
		else {
			(*value) = mutexed_var;
		}

		// Block until unlock mutex
		if (!ficopthread_unlock_mutex_block(mutex)) {
#ifdef DEBUG
			fprintf(stderr, "\n[%s] Error: Cannot unlock mutexed bool variable\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
	}
	return retval;
}

// Wait pthread finish
bool ficopthread_wait_finish(pthread_t thread_id, void **thread_retval)
{
	int join_retval = -1;
	bool retval = 1;
	if ((join_retval = pthread_join(thread_id, thread_retval)) != 0) {
		switch(join_retval) {
		case EDEADLK:
			// A deadlock was detected (e.g., two threads tried to join with
	        // each other); or thread specifies the calling thread.
#ifdef DEBUG
			fprintf(stderr,"\n[%s] Error: A deadlock was detected in thread id %d\n\n", __func__, (int)thread_id);
			fflush(stderr);
#endif //DEBUG
			*thread_retval = NULL;
			retval = 0;
			break;
		case EINVAL:
			// Thread is not a joinable thread
			// or Another thread is already waiting to join with this thread.
#ifdef DEBUG
			fprintf(stderr,"\n[%s] Error: Thread id %d is not joinable\n\n", __func__, (int)thread_id);
			fflush(stderr);
#endif //DEBUG
			*thread_retval = NULL;
			retval = 0;
			break;
		case ESRCH:
			// No thread with the ID thread could be found.
#ifdef DEBUG
			fprintf(stderr,"[%s] Warning: No thread with the thread id %d could be found\n", __func__, (int)thread_id);
			fflush(stderr);
#endif //DEBUG
			*thread_retval = NULL;
			break;
		default:
#ifdef DEBUG
			fprintf(stderr,"\n[%s] Error: Unknown error joining thread id %d\n\n", __func__, (int)thread_id);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
			*thread_retval = NULL;
			break;
		}
	}
	return retval;
}

/*
// Free one way list element
void ficolist_oneway_list_element_free(ficolist_oneway_element_t **list_element)
{
	if ((list_element == NULL)
			|| (*list_element == NULL)) {
		return;
	}
	free((*list_element)->element);
	(*list_element)->element = NULL;
	(*list_element)->next = NULL;
	free(*list_element);
	*list_element = NULL;
	return;
}

// Clean start mutexed oneway list
bool ficolist_mutexed_oneway_list_clean_start(ficolist_mutexed_oneway_t **list, FILE *log_fd)
{
	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (list == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid list address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		return 0;
	}
	else if (*list != NULL) {
		// Clean list
#ifdef DEBUG
		fprintf(log_fd, "%s: Warning: Cleaning uncleared mutexed list address\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		*list = NULL;
	}
	return 1;
}

// Initializes mutexed oneway list
bool ficolist_mutexed_oneway_list_init(ficolist_mutexed_oneway_t **list, FILE *log_fd)
{
	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (list == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid list address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		return 0;
	}
	else if (*list != NULL) {
		// Clean list
#ifdef DEBUG
		fprintf(log_fd, "%s: Warning: Cleaning uncleared mutexed list address\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		*list = NULL;
	}
	(*list) = (ficolist_mutexed_oneway_t *)malloc(sizeof(ficolist_mutexed_oneway_t));
	if (*list != NULL) {
		(*list)->num = 0;
		(*list)->head = NULL;
		(*list)->tail = NULL;
		if (pthread_mutex_init(&((*list)->mutex), NULL) != 0) {
			return 0;
		}
		return 1;
	}
	return 0;
}

// Delete head list element from oneway list
bool ficolist_mutexed_oneway_list_extract_head(void **element, ficolist_mutexed_oneway_t **list, FILE *log_fd)
{
	ficolist_oneway_element_t *temp = NULL;
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if ((list == NULL)
			|| (*list == NULL)) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid list address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		return 0;
	}
	else if (element == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid element address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		return 0;
	}
	else {
		// Block until lock mutex
		if (!ficopthread_trylock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
			fprintf(stderr, "\n%s: Error: Cannot lock mutex\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			return 0;
		}

		// Check if it the list is empty
		if (((*list)->num == 0)
				&& ((*list)->head == NULL)
				&& ((*list)->tail == NULL)) {
			//
			// Nothing to do
			//
			*element = NULL;
			retval = 0;
		}
		else {
			temp = (*list)->head;
			*element = temp->element;
			// Check if it is the only element
			if (((*list)->num == 1)
					&& ((*list)->tail == (*list)->head)) {
				(*list)->head = NULL;
				(*list)->tail = NULL;
			}
			else {
				(*list)->head = (*list)->head->next;
			}
			(*list)->num--;

			// Delete temp
			temp->next = NULL;
			free(temp);
			temp = NULL;
		}

		// Block until unlock mutex
		if (!ficopthread_unlock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
			fprintf(stderr, "\n%s: Error: Cannot unlock mutex\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
		return retval;
	}
}

// Check if oneway list is empty
int ficolist_mutexed_oneway_list_check_empty(ficolist_mutexed_oneway_t *list, FILE *log_fd)
{
	int retval = -1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if (list == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid list address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		return -1;
	}
	else {
		// Block until lock mutex
		if (!ficopthread_trylock_mutex_block(&(list->mutex))) {
#ifdef DEBUG
			fprintf(stderr, "\n%s: Error: Cannot lock mutex\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			return 0;
		}

		if ((list->num == 0)
				&& (list->head == NULL)
						&& (list->tail == NULL)) {
			retval = 1;
		}
		else if ((list->num != 0)
				&& (list->head != NULL)
						&& (list->tail != NULL)) {
			retval = 0;
		}
		else {
#ifdef DEBUG
			fprintf(log_fd, "\n%s Error: List is corrupted\n\n", __func__);
			fflush(log_fd);
#endif //DEBUG
			retval = -1;
		}

		// Block until unlock mutex
		if (!ficopthread_unlock_mutex_block(&(list->mutex))) {
#ifdef DEBUG
			fprintf(stderr, "\n%s: Error: Cannot unlock mutex\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
		return retval;
	}
}

// Append element to oneway list
bool ficolist_mutexed_oneway_list_append(void *element, ficolist_mutexed_oneway_t **list, FILE *log_fd)
{
	ficolist_oneway_element_t *temp = NULL;
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if ((list == NULL)
			|| (*list == NULL)) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid list address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		retval = 0;
	}
	else if (element == NULL) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid element address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		retval = 0;
	}
	else {
		// Allocate memory for twoway list element
		temp = (ficolist_oneway_element_t *)malloc(sizeof(ficolist_oneway_element_t));
		if (temp != NULL) {
			// Fill oneway list element
			temp->element = element;

			// Block until lock mutex
			if (!ficopthread_trylock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
				fprintf(stderr, "\n%s: Error: Cannot lock mutex\n\n", __func__);
				fflush(stderr);
#endif //DEBUG
				retval = 0;
			}
			else {
				// Check if it the list is empty
				if (((*list)->num == 0)
						&& ((*list)->head == NULL)
						&& ((*list)->tail == NULL)) {
					// Append in the head
					(*list)->head = temp;
				}
				else {
					(*list)->tail->next = temp;
				}
				temp->next = NULL;
				(*list)->tail = temp;
				(*list)->num++;

				// Block until unlock mutex
				if (!ficopthread_unlock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
					fprintf(stderr, "\n%s: Error: Cannot unlock mutex\n\n", __func__);
					fflush(stderr);
#endif //DEBUG
					retval = 0;
				}
			}
		}
		else {
			retval = 0;
		}
	}
	if (!retval) {
		if (temp != NULL) {
			free(temp);
			temp = NULL;
		}
	}
	return retval;
}

// Delete head list element from oneway list
bool ficolist_mutexed_oneway_list_delete_head(ficolist_mutexed_oneway_t **list, FILE *log_fd)
{
	ficolist_oneway_element_t *temp = NULL;
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if ((list == NULL)
			|| (*list == NULL)) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid list address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		return 0;
	}
	else {
		// Block until lock mutex
		if (!ficopthread_trylock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
			fprintf(stderr, "\n%s: Error: Cannot lock mutex\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			return 0;
		}

		// Check if it the list is empty
		if (((*list)->num == 0)
				&& ((*list)->head == NULL)
				&& ((*list)->tail == NULL)) {
			//
			// Nothing to do
			//
		}
		else {
			temp = (*list)->head;
			// Check if it is the only element
			if (((*list)->num == 1)
					&& ((*list)->tail == (*list)->head)) {
				(*list)->head = NULL;
				(*list)->tail = NULL;
			}
			else {
				(*list)->head = (*list)->head->next;
			}
			(*list)->num--;

			// Delete list element
			ficolist_oneway_list_element_free(&temp);
#ifdef DEBUG
			fprintf(stderr, "[%s] List element deleted\n", __func__);
			fflush(stderr);
#endif //DEBUG
		}

		// Block until unlock mutex
		if (!ficopthread_unlock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
			fprintf(stderr, "\n%s: Error: Cannot unlock mutex\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
		return retval;
	}
}

// Clean oneway list
bool ficolist_mutexed_oneway_list_clean(ficolist_mutexed_oneway_t **list, FILE *log_fd)
{
	bool lock_state = 1;
	bool retval = 1;

	if (log_fd == NULL) {
		log_fd = stderr;
	}

	if ((list == NULL)
			|| (*list == NULL)) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid list address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		return 0;
	}
	else {
		// Block until lock mutex
		if (!(lock_state = ficopthread_trylock_mutex_block(&((*list)->mutex)))) {
#ifdef DEBUG
			fprintf(stderr, "\n%s: Error: Cannot lock mutex\n\n", __func__);
			fflush(stderr);
#endif //DEBUG
			return 0;
		}

		// Check if it the list is empty
		if (((*list)->num == 0)
				&& ((*list)->head == NULL)
				&& ((*list)->tail == NULL)) {
			//
			// Nothing to do
			//
		}
		else {
			while (retval
					&& ((*list)->num > 0)
					&& ((*list)->head != NULL)) {
				// Block until unlock mutex
				if (!ficopthread_unlock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
					fprintf(stderr, "\n%s: Error: Cannot unlock mutex\n\n", __func__);
					fflush(stderr);
#endif //DEBUG
					retval = 0;
				}
				else {
					lock_state = 0;
					ficolist_mutexed_oneway_list_delete_head(list, log_fd);
					// Block until lock mutex
					if (!(lock_state = ficopthread_trylock_mutex_block(&((*list)->mutex)))) {
#ifdef DEBUG
						fprintf(stderr, "\n%s: Error: Cannot lock mutex\n\n", __func__);
						fflush(stderr);
#endif //DEBUG
						retval = 0;
					}
				}
			}
		}

		if (lock_state) {
			// Block until unlock mutex
			if (!ficopthread_unlock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
				fprintf(stderr, "\n%s: Error: Cannot unlock mutex\n\n", __func__);
				fflush(stderr);
#endif //DEBUG
				retval = 0;
			}
			else {
				lock_state = 0;
			}
		}
		return retval;
	}
}

// Free oneway list
bool ficolist_mutexed_oneway_list_free(ficolist_mutexed_oneway_t **list, FILE *log_fd)
{
	bool retval = 1;

	if ((list == NULL)
			|| (*list == NULL)) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Invalid list address\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		retval = 0;
	}
	else if (!ficolist_mutexed_oneway_list_clean(list, log_fd)) {
#ifdef DEBUG
		fprintf(log_fd, "\n%s: Error: Cannot clean list\n\n", __func__);
		fflush(log_fd);
#endif //DEBUG
		retval = 0;
	}
	else {
		if ((list != NULL)
				&& (*list == NULL)) {
			//
			// Nothing to do
			//
		}
		else if ((list != NULL)
				&& (*list != NULL)) {
			// Block until lock mutex
			if (!ficopthread_trylock_mutex_block(&((*list)->mutex))) {
#ifdef DEBUG
				fprintf(stderr, "\n%s: Error: Cannot lock mutex\n\n", __func__);
				fflush(stderr);
#endif //DEBUG
				retval = 0;
			}
			else {
				free(*list);
				(*list) = NULL;
				retval = 1;
			}
		}
		else {
#ifdef DEBUG
			fprintf(log_fd, "\n%s: Error: List is corrupted after be cleaned\n\n", __func__);
			fflush(log_fd);
#endif //DEBUG
			retval = 0;
		}
	}	
	return retval;
}
*/

// Create timer
bool ficotimerfd_create(int *timer_fd)
{
	bool retval = 1;

	if (timer_fd == NULL) {
		retval = 0;
	}
	else {
		if (!(*timer_fd < 0)) {
			retval = ficotimerfd_destroy(timer_fd);
		}
		if (retval) {
			// Create the timer
			if ((*timer_fd = timerfd_create (CLOCK_MONOTONIC, 0)) < 0) {
				retval  = 0;
			}
		}
	}
	return retval;
}

// Destroy timer
bool ficotimerfd_destroy(int *timer_fd)
{
	bool retval = 0;

	if (timer_fd == NULL) {
		retval = 0;
	}
	else if (*timer_fd < 0) {
#ifdef DEBUG
		fprintf(stderr, "%s Warning: Timer_fd <%d> is not initialized or it has been already closed\n", ficotime_now(), *timer_fd);
		fflush(stderr);
#endif //DEBUG
		retval = 0;
	}
	else {
		if (!ficotimerfd_stop(*timer_fd)) {
#ifdef DEBUG
			fprintf(stderr, "\n%s Error: Cannot stop timer_fd <%d>. Force close\n\n", ficotime_now(), *timer_fd);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
		if (close(*timer_fd) < 0) {
#ifdef DEBUG
			fprintf(stderr, "\n%s Error: Cannot close timer_fd <%d>\n\n", ficotime_now(), *timer_fd);
			fflush(stderr);
#endif //DEBUG
			retval = 0;
		}
	}
	if (retval) {
#ifdef DEBUG
		fprintf(stderr, "%s Timer_fd <%d> has been closed\n",ficotime_now(), *timer_fd);
		fflush(stderr);
#endif //DEBUG
		*timer_fd = -1;
	}
	return retval;
}

// Start timer
bool ficotimerfd_start(time_t sec, long nsec, time_t ival_sec, long ival_nsec, int timer_fd)
{
	int ret = -1;
	struct itimerspec itval;

	bool retval = 1;

	if (timer_fd < 0) {
		retval = 0;
	}
	else {
		itval.it_interval.tv_nsec = ival_nsec;
		itval.it_interval.tv_sec = ival_sec;
		itval.it_value.tv_nsec = nsec;
		itval.it_value.tv_sec = sec;
		if ((ret = timerfd_settime (timer_fd, 0, &itval, NULL)) < 0) {
			retval = 0;
		}
	}
	return retval;
}

// Wait timer
bool ficotimerfd_wait(int timer_fd)
{
	unsigned long long missed;
	int ret = -1;
	bool retval = 1;
	char error_str[FICOLINE_MAXSTRLEN];

	if (timer_fd < 0) {
		retval = 0;
	}
	// Wait for the next timer event. If we have missed any the
	// number is written to "missed"
	else if ((ret = read (timer_fd, &missed, sizeof (missed))) == -1) {
		sprintf(error_str, "[%s] Error: read %d timer", __func__, timer_fd);
		perror (error_str);

		retval = 0;
	}
	// "missed" should always be >= 1, but just to be sure, check it is not 0 anyway
	//if (missed > 0)
	//	info->wakeups_missed += (missed - 1);

	return retval;
}

// Stop timer
bool ficotimerfd_stop(int timer_fd)
{
	int ret = -1;
	struct itimerspec itval;

	bool retval = 1;

	if (timer_fd < 0) {
		retval = 0;
	}
	else {
		itval.it_interval.tv_nsec = 0;
		itval.it_interval.tv_sec = 0;
		itval.it_value.tv_nsec = 0;
		itval.it_value.tv_sec = 0;
		if ((ret = timerfd_settime (timer_fd, 0, &itval, NULL)) < 0) {
			retval = 0;
		}
	}
	return retval;
}
