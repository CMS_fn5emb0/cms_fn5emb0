/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright © 2014-2015 NVIDIA Corporation. All rights reserved.
// 
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "util_GL.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

//#######################################################################################
void checkGLerror(char* file, int line)
{
    GLint error = glGetError();
    if (error)
    {
        const char* errorString = 0;
        switch (error)
        {
        case GL_INVALID_ENUM: errorString = "GL_INVALID_ENUM"; break;
        case GL_INVALID_FRAMEBUFFER_OPERATION: errorString = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
        case GL_INVALID_VALUE: errorString = "GL_INVALID_VALUE"; break;
        case GL_INVALID_OPERATION: errorString = "GL_INVALID_OPERATION"; break;
        default: errorString = "unknown error"; break;
        }
        printf("File %s, Line %d: glError \"%s\" (0x%x)\n", file, line, errorString, error);
        assert(false);
    }
}

//#######################################################################################
// Returns a string containing the text in a vertex/fragment shader source file.
//
static char *shaderLoadSource(const char *filePath)
{
    const size_t blockSize = 2048;
    FILE *fp;
    char buf[blockSize];
    char *source = NULL;
    size_t tmp, sourceLength = 0;

    /* open file */
    fp = fopen(filePath, "r");
    if (!fp) {
        fprintf(stderr, "shaderLoadSource(): Unable to open %s for reading\n", filePath);
        return NULL;
    }

    /* read the entire file into a string */
    while ((tmp = fread(buf, 1, blockSize, fp)) > 0) {
        char *newSource = (char*)malloc(sourceLength + tmp + 1);
        if (!newSource) {
            fprintf(stderr, "shaderLoadSource(): malloc failed\n");
            if (source)
                free(source);
            return NULL;
        }

        if (source) {
            memcpy(newSource, source, sourceLength);
            free(source);
        }
        memcpy(newSource + sourceLength, buf, tmp);

        source = newSource;
        sourceLength += tmp;
    }

    /* close the file and null terminate the string */
    fclose(fp);
    if (source)
        source[sourceLength] = '\0';

    return source;
}

//#######################################################################################
static GLuint shaderCompileFromMemory(GLenum type, const char *source)
{
    GLuint shader;
    GLint length, result;

    /* create shader object, set the source, and compile */
    shader = glCreateShader(type); CHECK_GL_ERROR;
    length = strlen(source);
    glShaderSource(shader, 1, (const char **)&source, &length); CHECK_GL_ERROR;
    glCompileShader(shader); CHECK_GL_ERROR;


    /* make sure the compilation was successful */
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result); CHECK_GL_ERROR;
    if (result == GL_FALSE) {
        char *log;

        /* get the shader info log */
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length); CHECK_GL_ERROR;
        log = (char*)malloc(length);
        glGetShaderInfoLog(shader, length, &result, log);  CHECK_GL_ERROR;

        /* print an error message and the info log */
        printf("shaderCompileFromFile(): Unable to compile shader, error = %s, source = \"%s\"\n", log, source);
        free(log);

        glDeleteShader(shader); CHECK_GL_ERROR;
        return 0;
    }

    return shader;
}

//#######################################################################################
// Returns a shader object containing a shader compiled from the given GLSL shader file.
//
static GLuint shaderCompileFromFile(GLenum type, const char *filePath)
{
    char *source;
    GLuint shader;

    /* get shader source */
    source = shaderLoadSource(filePath);
    if (!source)
        return 0;

    shader = shaderCompileFromMemory(type, source);
    free(source);

    return shader;
}

//#######################################################################################
// Compiles and attaches a shader of the given type to the given program object.

void shaderAttachFromFile(GLuint program, GLenum type, const char *filePath)
{
    /* compile the shader */
    GLuint shader = shaderCompileFromFile(type, filePath);
    if (shader != 0) {
        /* attach the shader to the program */
        glAttachShader(program, shader); CHECK_GL_ERROR;

        /* delete the shader - it won't actually be
         * destroyed until the program that it's attached
         * to has been destroyed */
        glDeleteShader(shader); CHECK_GL_ERROR;
    }
}

//#######################################################################################
// Compiles and attaches a shader of the given type to the given program object.

void shaderAttachFromMemory(GLuint program, GLenum type, const char *source)
{
    /* compile the shader */
    GLuint shader = shaderCompileFromMemory(type, source);
    if (shader != 0) {
        /* attach the shader to the program */
        glAttachShader(program, shader); CHECK_GL_ERROR;

        /* delete the shader - it won't actually be
         * destroyed until the program that it's attached
         * to has been destroyed */
        glDeleteShader(shader); CHECK_GL_ERROR;
    }
}

//#######################################################################################
// Loads a vertex and pixel shader from files, compiles and attaches a newly 
// created program. Returns the program id.

GLuint buildAndLinkProgramFromFiles(const char *VSfilePath, const char *PSfilePath)
{

    GLint result;
    /* create program object and attach shaders */

    GLuint g_program = glCreateProgram(); CHECK_GL_ERROR;
    shaderAttachFromFile(g_program, GL_VERTEX_SHADER, VSfilePath);
    shaderAttachFromFile(g_program, GL_FRAGMENT_SHADER, PSfilePath);

    /* link the program and make sure that there were no errors */
    glLinkProgram(g_program); CHECK_GL_ERROR;
    glGetProgramiv(g_program, GL_LINK_STATUS, &result); CHECK_GL_ERROR;
    if (result == GL_FALSE) {
        GLint length;
        char* log;

        /* get the program info log */
        glGetProgramiv(g_program, GL_INFO_LOG_LENGTH, &length); CHECK_GL_ERROR;
        log = (char*)malloc(length);
        glGetProgramInfoLog(g_program, length, &result, log); CHECK_GL_ERROR;

        /* print an error message and the info log */
        fprintf(stderr, "sceneInit(): Program linking failed: %s\n", log);
        free(log);

        /* delete the program */
        glDeleteProgram(g_program); CHECK_GL_ERROR;
        return  0;
    }

    return g_program;
}

//#######################################################################################
// Loads a vertex and pixel shader from files, compiles and attaches a newly 
// created program. Returns the program id.

GLuint buildAndLinkProgramFromMemory(const char *vsSource, const char *psSource)
{
    GLint result;

    /* create program object and attach shaders */
    GLuint g_program = glCreateProgram(); CHECK_GL_ERROR;
    shaderAttachFromMemory(g_program, GL_VERTEX_SHADER, vsSource);
    shaderAttachFromMemory(g_program, GL_FRAGMENT_SHADER, psSource);

    /* link the program and make sure that there were no errors */
    glLinkProgram(g_program); CHECK_GL_ERROR;
    glGetProgramiv(g_program, GL_LINK_STATUS, &result); CHECK_GL_ERROR;
    if (result == GL_FALSE) {
        GLint length;
        char* log;

        /* get the program info log */
        glGetProgramiv(g_program, GL_INFO_LOG_LENGTH, &length); CHECK_GL_ERROR;
        log = (char*)malloc(length);
        glGetProgramInfoLog(g_program, length, &result, log); CHECK_GL_ERROR;

        /* print an error message and the info log */
        printf("sceneInit(): Program linking failed: %s\n", log);
        free(log);

        /* delete the program */
        glDeleteProgram(g_program); CHECK_GL_ERROR;
        return  0;
    }

    return g_program;
}
