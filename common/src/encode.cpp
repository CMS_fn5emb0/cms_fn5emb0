#include "encode.h"
#include <iostream>
#include <cstring>

#include <signal.h>
#include <stdio.h>
#include <execinfo.h>
#include <stdlib.h>
#include <unistd.h>

#define NVMEDIA_CHECK(nvmedia_call) {NvMediaStatus x = nvmedia_call; if( x != NVMEDIA_STATUS_OK ) { printf("File %s, Line %d: %d\n", __FILE__ , __LINE__ , x ); exit(0); }};

Encode::Encode(NvMediaDevice *device, int width, int height, unsigned int bitrate, unsigned int framerate)
: conversion(nullptr)
, bitrate(bitrate)
, width(width)
, height(height)
{
    // this creates and configure the H264 encoder
    // the provided settings are optimized for image quality
    // other use-cases will need different parameters

    // configure parameters that are required at initialization
    NvMediaEncodeInitializeParamsH264 initParams;
    initParams.encodeWidth = width;
    initParams.encodeHeight = height;
    initParams.enableLimitedRGB = NVMEDIA_FALSE;
    initParams.frameRateNum = framerate;
    initParams.frameRateDen = 1;
    initParams.profile =  NVMEDIA_ENCODE_PROFILE_AUTOSELECT;
    initParams.level = NVMEDIA_ENCODE_LEVEL_AUTOSELECT;
    initParams.maxNumRefFrames = 13;
    initParams.rotation = NVMEDIA_ENCODE_ROTATION_NONE;
    initParams.mirroring = NVMEDIA_ENCODE_MIRRORING_NONE;
    initParams.enableExternalMEHints = NVMEDIA_FALSE;
    encoder = NvMediaIEPCreate(device, NVMEDIA_IMAGE_ENCODE_H264, &initParams, NvMediaSurfaceType_Image_YUV_420, 1, 8, nullptr);
    if(!encoder)
    {
        std::cout << "Could not create encoder!" << std::endl;
        return;
    }
    
    // set h264 configuration
    memset(&vuiParams, 0, sizeof(NvMediaEncodeConfigH264VUIParams));
    memset(&encodeConfig, 0, sizeof(NvMediaEncodeConfigH264));
    encodeConfig.features = 0;
    encodeConfig.gopLength = 16;
    encodeConfig.rcParams.rateControlMode = NVMEDIA_ENCODE_PARAMS_RC_CBR;
    encodeConfig.rcParams.numBFrames = 0;
    encodeConfig.rcParams.params.cbr.averageBitRate = bitrate; // 1MB/s ~ 8Mbit/s
    encodeConfig.rcParams.params.cbr.vbvBufferSize = 0;
    encodeConfig.rcParams.params.cbr.vbvInitialDelay = 0;
    encodeConfig.repeatSPSPPS = NVMEDIA_ENCODE_SPSPPS_REPEAT_INTRA_FRAMES;
    encodeConfig.idrPeriod = 16;
    encodeConfig.numSliceCountMinus1 = 0;
    encodeConfig.disableDeblockingFilterIDC = 0;
    encodeConfig.adaptiveTransformMode = NVMEDIA_ENCODE_H264_ADAPTIVE_TRANSFORM_DISABLE;
    encodeConfig.bdirectMode = NVMEDIA_ENCODE_H264_BDIRECT_MODE_DISABLE;
    encodeConfig.entropyCodingMode = NVMEDIA_ENCODE_H264_ENTROPY_CODING_MODE_CABAC;
    encodeConfig.intraRefreshPeriod = 0;
    encodeConfig.intraRefreshCnt = 0;
    encodeConfig.maxSliceSizeInBytes = bitrate / framerate;
    encodeConfig.h264VUIParameters = &vuiParams;
    encodeConfig.motionPredictionExclusionFlags = 0;
    NvMediaIEPSetConfiguration(encoder, &encodeConfig);
    
    // create a buffer to convert color/image format if required
    NvMediaImageAdvancedConfig advConfig;
    memset(&advConfig, 0, sizeof(advConfig));
    conversion = NvMediaImageCreate(device, NvMediaSurfaceType_Image_YUV_420, NVMEDIA_IMAGE_CLASS_SINGLE_IMAGE, 1, 
            width, height, NVMEDIA_IMAGE_ATTRIBUTE_UNMAPPED | NVMEDIA_IMAGE_ATTRIBUTE_SEMI_PLANAR | NVMEDIA_IMAGE_ATTRIBUTE_EXTRA_LINES, &advConfig);
    // create 2d engine instance for format conversion
    engine2d = NvMedia2DCreate(device);

    // create chunk
    chunk = new DataChunk;

    chunk->size      = 1024 * 1024 * sizeof(char);   // Bitrate is 1MB/s, this size shoudl be plenty
    chunk->validData = 0;
    chunk->data      = new char[chunk->size + 256]; // additional 256 byte for some headers
}

Encode::~Encode()
{
    // release buffer and hw engines
    NvMedia2DDestroy(engine2d);
    NvMediaIEPDestroy(encoder);
    NvMediaImageDestroy(conversion);

    delete [] chunk->data;
    delete chunk;
}

void Encode::encode(NvMediaImage *frame, int offset)
{
    NvMediaImage *toEnc = frame;

    // check if image format is supported by encoder
    // if not convert to a support image format (via 2D engine)

    if(frame->type != NvMediaSurfaceType_Image_YUV_420)
    {


        memset(&blitParams, 0, sizeof(blitParams));

        NvMediaRect src;
        src.x0 = offset * width;
        src.x1 = src.x0 + width;
        src.y0 = 0;
        src.y1 = height;

        NvMedia2DBlit(engine2d, conversion, nullptr, frame, &src, &blitParams);
        toEnc = conversion;



    }

    // set frame configuration and encode image
    picParams.pictureType = NVMEDIA_ENCODE_PIC_TYPE_AUTOSELECT;
    picParams.encodePicFlags = NVMEDIA_ENCODE_PIC_FLAG_OUTPUT_SPSPPS;
    picParams.nextBFrames = 0;
    picParams.rcParams.rateControlMode = NVMEDIA_ENCODE_PARAMS_RC_CBR;
    picParams.rcParams.numBFrames = 0;
    picParams.rcParams.params.cbr.averageBitRate = bitrate; // 1MB/s ~ 8Mbit/s
    picParams.rcParams.params.cbr.vbvBufferSize = 0;
    picParams.rcParams.params.cbr.vbvInitialDelay = 0;
    picParams.seiPayloadArrayCnt = 0;
    picParams.seiPayloadArray = nullptr;
    picParams.meExternalHints = nullptr;
    NvMediaIEPFeedFrame(encoder, toEnc, nullptr, &picParams);
}

Encode::DataChunk* Encode::getData()
{
    // query encoder if data is available
    unsigned int size;
        
    NvMediaBool encodeDoneFlag = NVMEDIA_FALSE;
    while(!encodeDoneFlag) 
    {

        //NvMediaStatus status = NvMediaIEPBitsAvailable(encoder, &size, NVMEDIA_ENCODE_BLOCKING_TYPE_IF_PENDING, NVMEDIA_VIDEO_ENCODER_TIMEOUT_INFINITE);
    	NvMediaStatus status = NVMEDIA_STATUS_OK;
    	switch(status)
        {
            case NVMEDIA_STATUS_OK:
                if(size > 0)
                {
                    /*if(chunk->size < size)
                        std::cout << "Encoder ERROR: Internal buffer too small" << std::endl;*/
                               
                    chunk->validData = 0;
                    // get data from encoder
                    int status = NvMediaIEPGetBits(encoder, &chunk->validData, chunk->data);
                    if(status != NVMEDIA_STATUS_OK)
                    {
                        returnData(chunk);
                        return nullptr;
                    }
                    else
                        return chunk;
                }
                encodeDoneFlag = 1;
                break;
            case  NVMEDIA_STATUS_PENDING:
                std::cout << "Status - pending" << std::endl;
                break;
            default: 
                return nullptr;
        }
    }
}

void Encode::returnData(DataChunk *data)
{
    // free use of data buffer

    data->validData = 0;
}

