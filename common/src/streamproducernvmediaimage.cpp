/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright (c) 2014-2015 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "streamproducernvmediaimage.h"
#include <iostream>
#include <Logutils.hpp>
using namespace logutils;

StreamProducerNvMediaImage::StreamProducerNvMediaImage(EGLDisplay display,
		NvMediaDevice* device, EGLStream& stream, NvMediaSurfaceType type,
		int width, int height) {
	producer = NvMediaEglStreamProducerCreate(device, display, stream.getID(),
			type, width, height);
	if (producer == nullptr) {
		LOG_ERR_(
				"Function Name:%s:Could not create NvMedia Image stream producer!\n", __func__);
	}
}

StreamProducerNvMediaImage::~StreamProducerNvMediaImage() {
	NvMediaEglStreamProducerDestroy(producer);
}

bool StreamProducerNvMediaImage::postFrame(NvMediaImage *frame) {
	NvMediaStatus status = NvMediaEglStreamProducerPostImage(producer, frame,
			NULL);
	if (status != NVMEDIA_STATUS_OK) {
		if (status == NVMEDIA_STATUS_TIMED_OUT)
			LOG_ERR_(
					"Function Name:%s:Error posting frame: 0x %x %d %d  \n", __func__, std::hex, status, std::dec);
		// std::cout << "Error posting frame: 0x" << std::hex << status << std::dec << std::endl;
		return false;
	}

	return true;
}

NvMediaImage *StreamProducerNvMediaImage::getFrame(unsigned int timeoutms) {
	NvMediaImage* frame = nullptr;
	NvMediaStatus status = NvMediaEglStreamProducerGetImage(producer, &frame,
			timeoutms);
	if (status != NVMEDIA_STATUS_OK) {
		if (status != NVMEDIA_STATUS_TIMED_OUT)
			//std::cout << "Error getting frame: 0x" << std::hex << status<< std::dec << std::endl;
			LOG_ERR_(
					"Function Name:%s:Error getting frame: 0x %x %d %d  \n", __func__, std::hex, status, std::dec);
		frame = nullptr;
	}

	return frame;
}

