/*****************************************************************************
 *
 * Copyright (C) 2015 FICOSA - ADAS Business Unit (Spain)
 *
 ****************************************************************************/

/*
 * capture2d.h
 *
 *  Last modification on: 17/08/2016
 *      Author: Miguel H. Rib <miguel.rib@ficosa.com>
 */


#include "window_x11.h"
#include "window_wayland.h"
#include "window_egldevice.h"


AbstractWindow* AbstractWindow::create(WindowManager wm, int width, int height, bool robust) {
	create(wm, 0, 0, width, height, robust);

    return 0;
}

AbstractWindow* AbstractWindow::create(WindowManager wm, int displayId, int windowId, int width, int height, bool robust) {
    switch( wm ) {
        case X11 : return new WindowX11(width, height, robust);
        case WAYLAND : return new WindowWayland(width, height, robust);
        case EGLOUTPUT : return new WindowEGL(displayId, windowId, width, height, robust);
    }

    return 0;
}
