/**
* @file    serialmessage.cpp
*
* @brief   This file has the source code which will provide message string to
*          pass on to the CAN device in order to show the current status of camera
*
* @copyright FICOSA ADAS 
*
*/

// include files
#include "serialmessage.h"
#include "serial_app.h"

//namespaces
using namespace SerialMsgHandler;

// extern from serial port folder
//extern int ficosa_send_message(unsigned char * send_bytes, unsigned int length);

/**
* @brief constructor initilises all the members to the default values
*
*/
CSerialMsgHandler::CSerialMsgHandler()
{
    _msgstring << "";
    _prvPos = PRV_ROOF;
    _soilState = SOIL_ON;
    _camType = CAM_LIMPIA;
}

/**
* @brief  This function will set all the member variables to the values 
*         as per the current situation of camera at one time, but this
*         function is not yet used
*
* @param  prvpos      This is camera mounting position either Roof or Door
* @param  soilstate   This will intimate the soil state present on camera zero/one
* @param  camType     This will gives the camera state clean or dirty
*
* @return nothing
*/

void CSerialMsgHandler::setSerialMsgHandlerParams( unsigned int prvpos, bool  soilstate, unsigned int camType)
{
    cout << "Set Paramerter for serial message handler" << endl;

    if(PRV_ROOF == prvpos)
       _prvPos = 0x55;
    else
       _prvPos = 0xAA;

    if(SOIL_ON == soilstate)
       _soilState = 0x01;
    else
       _soilState = 0x00;

    if(CAM_LIMPIA == camType)
       _camType = 0x00;
    else
       _camType = 0x01;

}

/**
* @brief  This function will set prv position of camera
*
* @param  prvpos This is camera mounting position either Roof or Door
*
* @return nothing
*/

void CSerialMsgHandler::setPRVposition(unsigned int prvpos)
{
    if(PRV_ROOF == prvpos)
       _prvPos = 0x55;
    else
       _prvPos = 0xAA;
}

/**
* @brief  This function will set soil state 
*
* @param  soilstate This will intimate the soil state present on camera zero/one
*
* @return nothing
*/

void CSerialMsgHandler::setSoilStatus( bool soilstate)
{
    // set the soil state
    if(SOIL_ON == soilstate)
       _soilState = 0x01;
    else
       _soilState = 0x00;
}

/**
* @brief  This function will set camera type  
*
* @param  camType  This will gives the camera state clean or dirty
*
* @return nothing
*/

void CSerialMsgHandler::setCameraType(unsigned int camType)
{
    // set camera type clean/dirty
    if(CAM_LIMPIA == camType)
       _camType = 0x00;
    else
       _camType = 0x01;
}

/**
* @brief  This function will send message string to CAN device 
*
* @return nothing
*/

void CSerialMsgHandler::sendMessage()
{
    // this is the fourth parameter that needs to be send to
    // can device
    unsigned int xorValue = (_prvPos ^ _soilState ^ _camType);
    bool status = 0;
    // loop to push the vales of diff parameter to string that
    // needs to be pushed to the CAN device
    for(unsigned int idx=0; idx < 4; idx++)
    {
       switch(idx) {
         case PVR_POS:
              _sendBytes[idx] = _prvPos;
              _msgstring << _prvPos;    //prv position Roof/Door
              break;
         case CAMTYPE:
              _sendBytes[idx] = _camType;
              _msgstring << _camType;   //cam status dirty/clean
              break;
         case SOILSTATE:
              _sendBytes[idx] = _soilState;
              //printf("\nSoil State = %x\n", _sendBytes[2]); 
              _msgstring << _soilState; //soild state present/not present
              break;
         case XOR:
              _sendBytes[idx] = xorValue;
              _msgstring << xorValue;   // xor of all above three param values
              break;
         default:
              break;
       }

    }

    // message send to can
    status = ficosa_send_message(_sendBytes, 4);

    // printf message on console
    //printMessage();

    // clear screen once the message is sent
    _msgstring.str(""); 
}

/**
* @brief  This function will print debug message string that has been 
*         send to CAN device 
*
* @return nothing
*/

void CSerialMsgHandler::printMessage()
{
    static unsigned int idxCount = 0;
    
    //todo : print the messages on console
    if(idxCount%10 == 0)
    {
        cout << "----------Message to CAN ------------------ " << endl;  
        //cout << _msgstring.str().c_str() << endl;  
        //cout << "PRV Position ="<<_prvPos << "\tSoil state=" << _soilState << "\t Camera state=" << _camType << endl;
        printf("\n%x %x %x %x\n", _sendBytes[0], _sendBytes[1], _sendBytes[2], _sendBytes[3]);  
        cout << "\n------------------------------------------- " << endl;
    }

    idxCount++;
}

// end of file
