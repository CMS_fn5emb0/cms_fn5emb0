/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file SnapshotFiniteStateMachine.h
*  @brief This file contains the definition of the class
*  		  SnapshotFiniteStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 17/06/2016
*******************************************************************/

#ifndef SnapshotFiniteStateMachine_H_
#define SnapshotFiniteStateMachine_H_
#include "FiniteStateMachineTemplate.h"
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include <vector>
#include "ImageWritter.h"
/** @class SnapshotFiniteStateMachine
 *  @details This class incorporates the functionalities to
 *  		 do snapshots
 */

class SnapshotFiniteStateMachine: public FiniteStateMachineTemplate {
public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	SnapshotFiniteStateMachine(std::string _snapshotName, std::string formatSnapshot);

	/** @brief Free the resources generated for the object
	 */
	virtual ~SnapshotFiniteStateMachine();

	void takeSnapshot(std::string snapshotName);
	void takeSnapshot(std::string snapshotName, NvMediaImage*& frame);
	void takeSnapshot(std::string snapshotName, bool _syncSnapShot,NvMediaImage *frame);

private:

	bool snapshotActivated;
	bool syncSnapShot;

	std::vector<int> compression_params;
	std::string snapshotName;
	std::string formatSnapshot;
	ImageWritter *imageWritter;
	NvMediaImage *syncFrame;

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state fail to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* SNAPSHOTFINITESTATEMACHINE_H_ */
