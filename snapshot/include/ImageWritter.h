/*
 * ImageWritter.h
 *
 *  Created on: Oct 4, 2016
 *      Author: fn5amm0
 */

#ifndef IMAGEWRITTER_H_
#define IMAGEWRITTER_H_

#include <vector>
#include <queue>
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include <pthread.h>
#include <semaphore.h>

#define MAX_IMAGE_TO_WRITE 16

struct Image{
	std::string name;
	cv::Mat data;
};

struct SnapshotBuffer{
	Image *image[MAX_IMAGE_TO_WRITE];
	int in; // first empty slot
	int out; // first full slot
	sem_t full; // number  of full slots
	int empty; // number of empty slots
	pthread_mutex_t inMutex;
	pthread_mutex_t outMutex;
	pthread_mutex_t emptyMutex;
};

class ImageWritter {
public:
	ImageWritter(std::vector<int> _compression_params);
	virtual ~ImageWritter();
	bool putImageToBuffer(std::string &_name, cv::Mat &_data);

private:
	static void*  writeImageThread(void *arg) { return ((ImageWritter*)arg)->writeImage(); }
	void* writeImage(void);
	pthread_t imageWritterThread;
	std::vector<int> compression_params;
	SnapshotBuffer *imageBuffer;

};
#endif /* IMAGEWRITTER_H_ */
