/*
 * ImageWritter.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: fn5amm0
 */

#include "ImageWritter.h"

ImageWritter::ImageWritter(std::vector<int> _compression_params){
	// TODO Auto-generated constructor stub

	compression_params = _compression_params;
	// init buffer


	imageBuffer = new SnapshotBuffer();
	for (int i = 0; i < MAX_IMAGE_TO_WRITE; i++){

			imageBuffer->image[i]= new Image();
		}

	imageBuffer->in=0;
	imageBuffer->out=0;

	sem_init(&imageBuffer->full, 0, 0);
	imageBuffer->empty = MAX_IMAGE_TO_WRITE;

	// init thread

	pthread_create(&imageWritterThread, nullptr, ImageWritter::writeImageThread, this);

}

ImageWritter::~ImageWritter() {
	// TODO Auto-generated destructor stub
}

bool ImageWritter::putImageToBuffer(std::string &_name, cv::Mat &_data){

	if (imageBuffer->empty<=0) return false;
	else{

		imageBuffer->image[imageBuffer->in]->name = _name;
		imageBuffer->image[imageBuffer->in]->data = _data;

		pthread_mutex_lock(&imageBuffer->inMutex);
		if (imageBuffer->in < MAX_IMAGE_TO_WRITE-1) imageBuffer->in = imageBuffer->in+1;
		else imageBuffer->in = 0;
		pthread_mutex_unlock(&imageBuffer->inMutex);

		pthread_mutex_lock(&imageBuffer->emptyMutex);
		imageBuffer->empty = imageBuffer->empty-1;
		pthread_mutex_unlock(&imageBuffer->emptyMutex);
		sem_post(&imageBuffer->full);
		return true;
	}


}

void* ImageWritter::writeImage(void){

	while(true){
		sem_wait(&imageBuffer->full);;

		cv::imwrite(imageBuffer->image[imageBuffer->out]->name, imageBuffer->image[imageBuffer->out]->data, compression_params);

		pthread_mutex_lock(&imageBuffer->outMutex);
		if (imageBuffer->out < MAX_IMAGE_TO_WRITE-1) imageBuffer->out = imageBuffer->out+1;
		else imageBuffer->out = 0;
		pthread_mutex_unlock(&imageBuffer->outMutex);

		pthread_mutex_lock(&imageBuffer->emptyMutex);
		if (imageBuffer->empty < MAX_IMAGE_TO_WRITE) imageBuffer->empty = imageBuffer->empty+1;
		pthread_mutex_unlock(&imageBuffer->emptyMutex);

	}
}
