/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file SnapshotFiniteStateMachine.cpp
*  @brief This file contains the definition of the class
*  		  SnapshotFiniteStateMachine
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 16/06/2016
*******************************************************************/

#include "SnapshotFiniteStateMachine.h"

#include "sys/statvfs.h"
#include <unistd.h>
#include "surf_utils.h"
SnapshotFiniteStateMachine::SnapshotFiniteStateMachine(std::string _snapshotName, std::string _formatSnapshot)
	:FiniteStateMachineTemplate(){

	snapshotActivated = false;

	snapshotName = _snapshotName;
	formatSnapshot = _formatSnapshot;
	syncFrame = nullptr;
	syncSnapShot = false;
	if(formatSnapshot == "ppm"){
			compression_params.push_back(CV_IMWRITE_PXM_BINARY);
			compression_params.push_back(1);
	}else if(formatSnapshot == "png"){
		compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
		compression_params.push_back(3);
	}else if(formatSnapshot == "jpeg"){
		compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
		compression_params.push_back(95);
	}else{
		std::cout<<"invalid format"<<std::endl;
	}
	imageWritter = new ImageWritter(compression_params);
}
SnapshotFiniteStateMachine::~SnapshotFiniteStateMachine() {
	// TODO Auto-generated destructor stub
}


void SnapshotFiniteStateMachine::takeSnapshot(std::string _snapshotName){
	snapshotName = _snapshotName;
	snapshotActivated = true;

}

void SnapshotFiniteStateMachine::takeSnapshot(std::string _snapshotName, NvMediaImage*& frame){

	snapshotName = _snapshotName;

	//while(getFrame() == nullptr){}

	//frame = getFrame();
	snapshotActivated = true;

}

void SnapshotFiniteStateMachine::takeSnapshot(std::string _snapshotName, bool _syncSnapShot,NvMediaImage *frame){
	snapshotName = _snapshotName;
	syncFrame = frame;
	syncSnapShot=_syncSnapShot;
	snapshotActivated = true;
}

SnapshotFiniteStateMachine::State SnapshotFiniteStateMachine::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
SnapshotFiniteStateMachine::State SnapshotFiniteStateMachine::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
SnapshotFiniteStateMachine::State SnapshotFiniteStateMachine::ST_Run(){
NvMediaImage *frame = nullptr;
frame = getFrame();
if (frame != nullptr){
	if (snapshotActivated){
		struct statvfs info;
		statvfs ("./", &info);
		unsigned long long freeSpace = (unsigned long long)info.f_bsize * (unsigned long long)info.f_bfree;
			if (freeSpace > MINIUM_SPACE_FREE){
				// map captured frame to CPU space
				NvMediaImageSurfaceMap mapping;
				//lock the Nvmedia image
				NvMediaStatus status = NvMediaImageLock(frame, NVMEDIA_IMAGE_ACCESS_READ, &mapping);
				cv::Mat snapshot = cv::Mat(mapping.height,mapping.width,CV_8UC4);
				snapshot.data = reinterpret_cast<unsigned char*>(mapping.surface[0].mapping);
				NvMediaImageUnlock(frame);
				cv::cvtColor(snapshot, snapshot , cv::COLOR_RGB2BGR);
				//cv::imwrite(snapshotName, snapshot,compression_params);

				if (!imageWritter->putImageToBuffer(snapshotName, snapshot)){
					fprintf(stdout, "%s Line:%d : For safety reasons frame(%p) was discarted\n", __FILE__, __LINE__,frame);
				}

				snapshotActivated = false;
			}else{
				//TODO[1] ELSE FREE SPACE
			}
		}else{
			// TODO[1] ELSE SNAPSHOT NOT ACTIVATED
		}
	linkStages(frame);
	}else{
		// TODO[1] ELSE FRAME NULL
	}
	return State::RUN;
}
SnapshotFiniteStateMachine::State SnapshotFiniteStateMachine::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

SnapshotFiniteStateMachine::State SnapshotFiniteStateMachine::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
SnapshotFiniteStateMachine::State SnapshotFiniteStateMachine::ST_Initializing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
SnapshotFiniteStateMachine::State SnapshotFiniteStateMachine::ST_Running(){
	return State::RUN;
}
SnapshotFiniteStateMachine::State SnapshotFiniteStateMachine::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
