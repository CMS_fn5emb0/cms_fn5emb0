/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright © 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "overlayblend.h"
#include "overlay.h"
#include <string.h>
#include <GLES2/gl2ext.h>
#include <iostream>
#include <png.h>
//#include "X62ph1_Traction_bmp.h"
//#include "X82_SD_bmp.h"

using namespace std;

///#define VERTICAL_SPLIT
#define SCALE_SEP

//#######################################################################################
// Initialize the class and required variables
OverlayBlend::OverlayBlend() {
	memset(&m_programMesh, 0, sizeof(PROGRAM_TEXQUAD));

	m_initialized = false;

}

//#######################################################################################
// destroy the class and variables
OverlayBlend::~OverlayBlend() {
	release();
}

//#######################################################################################
// contruct a program and initialize all the required varibles
void OverlayBlend::initialize(OVLCMSPARAMETERS parameters, int sizeTexture) {

	_sizeTexture = sizeTexture;

	const char* filepath[_sizeTexture];

	m_texture = new GLuint[_sizeTexture];

	for(int i = 0; i < _sizeTexture; i++){
		filepath[i]= parameters.FILE_PATH.front();
		parameters.FILE_PATH.pop_front();
	}

	if (m_initialized)
		return;

	BMPData bmpdata;

	ovlimg = new OVLIMG[_sizeTexture];

	for(int i = 0; i < _sizeTexture; i++){
		convertPNG2BMP(filepath[i], &bmpdata);
		ovlimg[i].img = bmpdata.bmpimg;
		ovlimg[i].width = bmpdata.width;
		ovlimg[i].height = bmpdata.height;

	}

	m_rectifiedFBO.createFBO(parameters.DST_WIDTH,
			parameters.DST_HEIGHT, GL_RGBA8);
	CHECK_GL_ERROR;

	m_programMesh.program = buildAndLinkProgramFromMemory(
			get2DTexQuadVS().c_str(), get2DTexQuadPSH().c_str());
	CHECK_GL_ERROR;

	m_programMesh.aPosition = glGetAttribLocation(m_programMesh.program,
			"position");
	CHECK_GL_ERROR;
	m_programMesh.aTexcoord = glGetAttribLocation(m_programMesh.program,
			"texcoord");
	CHECK_GL_ERROR;
	m_programMesh.uTex0 = glGetUniformLocation(m_programMesh.program, "tex");
	CHECK_GL_ERROR;
	m_programMesh.myUniformLocation = glGetUniformLocation(
			m_programMesh.program, "myUniform");
	m_programMesh.myUniformLocationh = glGetUniformLocation(
			m_programMesh.program, "myUniformh");
	m_programMesh.uTex1 = glGetUniformLocation(m_programMesh.program, "tex1");
	CHECK_GL_ERROR;

	glGenTextures(_sizeTexture, m_texture);

	for(int i = 0; i < _sizeTexture; i++){
		glBindTexture(GL_TEXTURE_2D, m_texture[i]);
		CHECK_GL_ERROR;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ovlimg[i].width, ovlimg[i].height, 0, GL_RGBA,
					GL_UNSIGNED_BYTE, ovlimg[i].img);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		CHECK_GL_ERROR;
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		CHECK_GL_ERROR;
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		CHECK_GL_ERROR;
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		CHECK_GL_ERROR;
	}

	// Passthrough quad
	generatePassthroughMesh();

	m_initialized = true;
}

//#######################################################################################
// release all buffers, textures and programs
void OverlayBlend::release() {
	if (!m_initialized)
		return;

	glUseProgram(0);
	glDeleteProgram(m_programMesh.program);

	m_rectifiedFBO.releaseFBO();

	glDeleteTextures(_sizeTexture, m_texture);

	delete pngimg;

	delete[] ovlimg;

	m_initialized = false;
}

//#######################################################################################
void OverlayBlend::generatePassthroughMesh() {
	GLfloat coords[3][4];

	// Verts will be x,y and s,t
	// Vertices will be in the -1 to 1 range
	// X is horizontal, Y is vertical
	coords[0][0] = -1.0f;
	coords[0][1] = -1.0f;
	coords[1][0] = 3.0f;
	coords[1][1] = -1.0f;
	coords[2][0] = -1.0f;
	coords[2][1] = 3.0f;

	coords[0][2] = 0.0;
	coords[0][3] = 1.0f;
	coords[1][2] = 2.0f;
	coords[1][3] = 1.0f;
	coords[2][2] = 0.0f;
	coords[2][3] = -1.0f;

	// Create and bind the VAO
	glGenVertexArrays(1, &m_quadVAO);
	CHECK_GL_ERROR;
	glBindVertexArray(m_quadVAO);
	CHECK_GL_ERROR;

	// Create and fill the VBO
	glGenBuffers(1, &m_quadVBO);
	CHECK_GL_ERROR;
	glBindBuffer(GL_ARRAY_BUFFER, m_quadVBO);
	CHECK_GL_ERROR;
	glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(GLfloat), coords,
			GL_STATIC_DRAW);
	CHECK_GL_ERROR;
	// set up vertex attributes
	GLuint positionLoc = glGetAttribLocation(m_programMesh.program, "position");
	CHECK_GL_ERROR;
	glEnableVertexAttribArray(positionLoc);
	CHECK_GL_ERROR;
	glVertexAttribPointer(positionLoc, 2, GL_FLOAT, GL_FALSE,
			4 * sizeof(GLfloat), 0);
	CHECK_GL_ERROR;

	GLuint texcoordLoc = glGetAttribLocation(m_programMesh.program, "texcoord");
	CHECK_GL_ERROR;
	glEnableVertexAttribArray(texcoordLoc);
	CHECK_GL_ERROR;
	glVertexAttribPointer(texcoordLoc, 2, GL_FLOAT, GL_FALSE,
			4 * sizeof(GLfloat), (GLvoid*) (2 * sizeof(GLfloat)));
	CHECK_GL_ERROR;

	glBindVertexArray(0);
	CHECK_GL_ERROR;
}

//#######################################################################################
void OverlayBlend::renderImage(const GLuint inputTexture) {

	// Draw rectified image
	glDisable(GL_DEPTH_TEST);

	glBindFramebuffer(GL_FRAMEBUFFER, m_rectifiedFBO.fbo);
	CHECK_GL_ERROR;

	glViewport(0, 0, m_rectifiedFBO.width, m_rectifiedFBO.height);
	CHECK_GL_ERROR;
	glScissor(0, 0, m_rectifiedFBO.width, m_rectifiedFBO.height);
	CHECK_GL_ERROR;
	glEnable(GL_SCISSOR_TEST);
	CHECK_GL_ERROR;

	glClearColor(1.0, 0.0, 0.0, 0.0);
	;
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(m_programMesh.program);
	CHECK_GL_ERROR;

	// input image
	glActiveTexture(GL_TEXTURE1);
	CHECK_GL_ERROR;
	glBindTexture(GL_TEXTURE_EXTERNAL_OES, inputTexture);
	CHECK_GL_ERROR;
	glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_S,
			GL_CLAMP_TO_EDGE);
	CHECK_GL_ERROR;
	glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_T,
			GL_CLAMP_TO_EDGE);
	CHECK_GL_ERROR;
	glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	CHECK_GL_ERROR;
	glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	CHECK_GL_ERROR;

	{
		glBindVertexArray(m_quadVAO);
		CHECK_GL_ERROR;
		glDrawArrays(GL_TRIANGLES, 0, 3);
		CHECK_GL_ERROR;
		glBindVertexArray(0);
		CHECK_GL_ERROR;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	CHECK_GL_ERROR;

}

//#######################################################################################
void OverlayBlend::renderOverlay(unsigned int textureId) {

	// Draw texture image
	glUseProgram(m_programMesh.program);
	CHECK_GL_ERROR;

	glUniform1i(m_programMesh.uTex0, 1);
	CHECK_GL_ERROR;
	glUniform1i(m_programMesh.uTex1, 0);
	CHECK_GL_ERROR;
	glUniform1i(m_programMesh.uTex2, 0);
		CHECK_GL_ERROR;

	glUniform1f(m_programMesh.myUniformLocation, 1.0f);

	glActiveTexture(GL_TEXTURE0);
	CHECK_GL_ERROR;
	glBindTexture(GL_TEXTURE_2D, m_texture[textureId]);
	CHECK_GL_ERROR;

}


//#######################################################################################
std::string OverlayBlend::get2DTexQuadVS() {
	std::string shaderProgram = STRINGIFY(

			\n#version 300 es\n

			layout(location = 0) in vec2 position;
			layout(location = 1) in vec2 texcoord;

			out vec2 TexCoord0;

			void main()
			{
				TexCoord0 = vec2(texcoord.x, texcoord.y);
				vec2 pos = vec2(position.x, position.y);
				gl_Position = vec4( pos.x, pos.y, 0.0, 1.0);
			}

	);
	return shaderProgram;
}

std::string OverlayBlend::get2DTexQuadPSH() {
	std::string shaderProgram = STRINGIFY(

			\n#version 300 es\n
			\n#extension GL_NV_EGL_stream_consumer_external: enable\n
			\n#extension GL_OES_EGL_image_external : enable\n

			precision highp float;

			uniform samplerExternalOES tex;
			uniform sampler2D tex1;
			uniform float myUniform;
			uniform float myUniformh;

			in vec2 TexCoord0;

			layout(location = 0) out vec4 fragColor;

			void main()
			{
				vec4 texel = texture2D(tex, TexCoord0.st);
				vec4 texel1 = texture2D(tex1, TexCoord0.st);
				if(TexCoord0.t > myUniform)
				texel = vec4 (0.0, 0.0, 0.0, texel.a);
			fragColor = ((texel1) + (texel));
		}
)	;
	return shaderProgram;
}

void OverlayBlend::convertPNG2BMP(const char *filename, BMPData* bmpdata) {

	int width, height;
	png_byte color_type;
	png_byte bit_depth;
	png_bytep *row_pointers;

	FILE *fp = fopen(filename, "rb");

	png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL,
			NULL);
	if (!png)
		abort();

	png_infop info = png_create_info_struct(png);
	if (!info)
		abort();

	if (setjmp(png_jmpbuf(png)))
		abort();

	png_init_io(png, fp);

	png_read_info(png, info);

	width = png_get_image_width(png, info);
	height = png_get_image_height(png, info);
	color_type = png_get_color_type(png, info);
	bit_depth = png_get_bit_depth(png, info);
	// Read any color_type into 8bit depth, RGBA format.
	// See http://www.libpng.org/pub/png/libpng-manual.txt

	if (bit_depth == 16)
		png_set_strip_16(png);

	if (color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_palette_to_rgb(png);

	// PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
	if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		png_set_expand_gray_1_2_4_to_8(png);

	if (png_get_valid(png, info, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png);

	// These color_type don't have an alpha channel then fill it with 0xff.
	if (color_type == PNG_COLOR_TYPE_RGB || color_type == PNG_COLOR_TYPE_GRAY
			|| color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

	if (color_type == PNG_COLOR_TYPE_GRAY
			|| color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png);

	png_read_update_info(png, info);

	row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
	for (int y = 0; y < height; y++) {
		row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png, info));
	}

	png_read_image(png, row_pointers);

	fclose(fp);

	pngimg = new unsigned char[height * width * 4];
	int rowindex = 0;
	for (int y = 0; y < height; y++) {
		png_bytep row = row_pointers[y];
		for (int x = 0; x < width; x++) {
			png_bytep px = &(row[x * 4]);
			for (int z = 0; z < 4; z++) {
				pngimg[rowindex] = px[z];
				rowindex++;
			}
		}
	}
	bmpdata->bmpimg = pngimg;
	bmpdata->height = height;
	bmpdata->width = width;
}
