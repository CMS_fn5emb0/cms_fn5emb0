#include "overlay.h"

#include "gpuprogram.h"
#include "shader.h"
#include "vbo.h"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <execinfo.h>
#include <stdlib.h>
#include <unistd.h>

#include <cstring>

Overlay::Overlay(NvMediaDevice *device, EGLDisplay display, AbstractWindow* win,
		int cameras, OVLCMSPARAMETERS gparameters, const char* vShader, const char* fShader, int sizeTexture) :
		blender(nullptr) {
	this->cameras = cameras;

	this->gparameters = gparameters;

	this->_vShader = vShader;
	this->_fShader = fShader;

	// give away rendering context
	// rendering thread will take over and perform rendering into that context
	eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

	streamsCam2GL = new EGLStream*[cameras];
	streamsGL2Cam = new EGLStream*[cameras];
	for (int i = 0; i < cameras; ++i) {
		streamsGL2Cam[i] = new EGLStream(display, EGLStream::FIFO, 8);
		streamsCam2GL[i] = new EGLStream(display, EGLStream::FIFO, 8);
	}

	consumersNM = new StreamConsumerNvMediaImage*[cameras];
	for (int i = 0; i < cameras; ++i) {
		consumersNM[i] = new StreamConsumerNvMediaImage(display, device,
				*streamsGL2Cam[i]);
	}

	consumersGL = new StreamConsumerOpenGL*[cameras];
	for (int i = 0; i < cameras; ++i) {
		win->makeCurrent();
		consumersGL[i] = new StreamConsumerOpenGL(display, *streamsCam2GL[i]);
	}


	eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

	glInit(win, gparameters, sizeTexture);

	producersNM = new StreamProducerNvMediaImage*[cameras];
	for (int i = 0; i < cameras; ++i) {
		producersNM[i] = new StreamProducerNvMediaImage(display, device,
				*(getCam2GlStream())[i], NvMediaSurfaceType_Image_YUV_422,
				gparameters.DST_WIDTH, gparameters.DST_HEIGHT);
	}

	// OpenGL -> NvMediaImage
	producers = new StreamProducerOpenGL*[cameras];
	for (int i = 0; i < cameras; ++i) {
		win->makeCurrent();
		producers[i] = new StreamProducerOpenGL(display, win->getEGLConfig(),
				*streamsGL2Cam[i], gparameters.DST_WIDTH,
				gparameters.DST_HEIGHT);
	}

	eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

}
StreamProducerNvMediaImage** Overlay::getNvidiaProducer() {
	return producersNM;
}

StreamProducerOpenGL** Overlay::getOpenGlProducer() {
	return producers;
}

NvMediaImage* Overlay::getImage() {
	return consumersNM[0]->acquire();
}

NvMediaImage* Overlay::getFrame() {
	return producersNM[0]->getFrame(0);
}

const GLuint Overlay::getTexid() {
	return consumersGL[0]->getTexID();
}

bool Overlay::postImage(NvMediaImage* image) {
	return producersNM[0]->postFrame(image);
}

void Overlay::releaseImage(NvMediaImage* image) {
	return consumersNM[0]->release(image);
}

void Overlay::postTexture() {
	producers[0]->postTexture();
}

void Overlay::addOverlay(unsigned int textureId){

	blender->renderOverlay(textureId);

}

void Overlay::renderInputImage() {

	consumersGL[0]->acquireAndBind(0);

	blender->renderImage(consumersGL[0]->getTexID());
	consumersGL[0]->releaseAndUnbind(0);

	// render result image into EGL surface
	producers[0]->enableCurrent();
	{
		renderImage2EglSurface(&gparameters);
	}

	producers[0]->postTexture();
	producers[0]->disableCurrent();
}


Overlay::~Overlay() {
	delete blender;

	for (int i = 0; i < cameras; ++i) {
		delete consumersNM[i];
	}
	delete[] consumersNM;
	for (int i = 0; i < cameras; ++i) {
		delete producers[i];
	}
	delete[] producers;
	delete producers;

	for (int i = 0; i < cameras; ++i) {
		delete producersNM[i];
	}
	delete[] producersNM;
	delete producersNM;

	for (int i = 0; i < cameras; ++i) {
		delete consumersGL[i];
	}
	delete[] consumersGL;
	for (int i = 0; i < cameras; i++) {
		delete streamsCam2GL[i];
		delete streamsGL2Cam[i];
	}
	delete[] streamsCam2GL;
	delete[] streamsGL2Cam;
}

void Overlay::glInit(AbstractWindow* win, OVLCMSPARAMETERS cmsParameters, int sizeTexture) {

	// -------------------------------------------
	// This thread will do all rendering routines.
	// Ensure that this thread is marked as a current
	// rendering thread to window.
	// -------------------------------------------
	win->makeCurrent();

	// -------------------------------------------
	// initialize OpenGL (ES) for rendering
	// setup GL states as required
	// -------------------------------------------
	glViewport(0, 0, cmsParameters.DST_WIDTH, cmsParameters.DST_HEIGHT);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


	// create some quad to render to screen
	GLfloat vQuadVertices[] = { -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, -1.0f,
			0.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
			1.0f, 0.0f, };

	// upload quads to a vertex buffer object
	int quadStripe[] = { 3, 2 };
	quaVBO = new VBO(vQuadVertices, 4, quadStripe, 2, GL_STATIC_DRAW);

	// create a gpu program with a simple vertex and fragment shader
	Shader texturedvs(GL_VERTEX_SHADER, _vShader, Shader::File);
	Shader texturedfs(GL_FRAGMENT_SHADER, _fShader, Shader::File);

	texturedpro = new GPUProgram(texturedvs, texturedfs);

	index = texturedpro->getUniformLocation("tex");

	locations[0] = texturedpro->getAttribLocation("vPosition");
	locations[1] = texturedpro->getAttribLocation("vTexCoord");
	quaVBO->setPointerAndBind(locations, 2);

	// Initialize the blender
	blender = new OverlayBlend();
	blender->initialize(cmsParameters, sizeTexture);

	eglMakeCurrent(win->getEGLDisplay(), EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

}

EGLStream** Overlay::getCam2GlStream() {
	return streamsCam2GL;
}

void Overlay::renderImage2EglSurface(OVLCMSPARAMETERS* cmsParameters) {
	glClear(GL_COLOR_BUFFER_BIT);

	// Display the output
	glViewport(0, 0, cmsParameters->DST_WIDTH, cmsParameters->DST_HEIGHT);

	texturedpro->bind();
	glUniform1i(index, 0);
	quaVBO->setPointerAndBind(locations, 2);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, blender->getOutputTexture());
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindTexture(GL_TEXTURE_2D, 0);
	texturedpro->unbind();
	quaVBO->unsetPointerAndUnbind(locations, 2);

}

void Overlay::glDisplay(OVLCMSPARAMETERS* cmsParameters) {
	glClear(GL_COLOR_BUFFER_BIT);

	// Display the output
	glViewport(0, 0, cmsParameters->DST_WIDTH, cmsParameters->DST_HEIGHT);

	texturedpro->bind();
	glUniform1i(index, 0);
	quaVBO->setPointerAndBind(locations, 2);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, blender->getOutputTexture());
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindTexture(GL_TEXTURE_2D, 0);

	texturedpro->unbind();
	quaVBO->unsetPointerAndUnbind(locations, 2);

}
