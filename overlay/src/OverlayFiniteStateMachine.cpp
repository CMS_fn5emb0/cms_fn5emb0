/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file OverlayFiniteStateMachine.cpp
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#include "OverlayFiniteStateMachine.h"
OverlayFiniteStateMachine::OverlayFiniteStateMachine(NvMediaDevice *_device, int _width, int _height,int cameraCount ,std::list<const char*> filePath, bool overlay, AbstractWindow* _winOvl, const char* vShader, const char* fShader)
	:FiniteStateMachineTemplate(){

	sizeTexture = filePath.size();
	textureId = -1;
	lastTextureId = -2;

	device = _device;
	width = _width;
	height = _height;
	winOvl = _winOvl;
	EGLDisplay display = winOvl->getEGLDisplay();
	engine2d = NvMedia2DCreate(device);

	//init ovlparameters
	for(int i = 0; i < sizeTexture; i++){
		gparametersovl.FILE_PATH.push_back(filePath.front());
		filePath.pop_front();
	}

	// SRC DATA
	gparametersovl.SRC_WIDTH = 1280;
	gparametersovl.SRC_HEIGHT = 480;

	// DST DATA
	gparametersovl.DST_WIDTH = 1280;
	gparametersovl.DST_HEIGHT = 480;

	ovl = new Overlay(device, display, _winOvl, cameraCount, gparametersovl, vShader, fShader, sizeTexture);

	memset(&blitParams, 0, sizeof(blitParams));

}
OverlayFiniteStateMachine::~OverlayFiniteStateMachine() {
	// TODO Auto-generated destructor stub
}

void OverlayFiniteStateMachine::setTextureId(){

	if(!inMsgQ->empty()){
		if(lastTextureId != reinterpret_cast<int>(inMsgQ->front().msg)){
			textureId = reinterpret_cast<int>(inMsgQ->front().msg);
			//std::cout << "lastTexture: " << lastTextureId << "currentTexture: " << textureId << std::endl;
			lastTextureId = textureId;
		} else{
		}
	}else{
	}
}

OverlayFiniteStateMachine::State OverlayFiniteStateMachine::ST_Stopped(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
OverlayFiniteStateMachine::State OverlayFiniteStateMachine::ST_Initialized(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
OverlayFiniteStateMachine::State OverlayFiniteStateMachine::ST_Run(){
	NvMediaImage *frame;
	frame = getFrame();

	if (frame != nullptr){

		winOvl->makeCurrent();

		ovl->postImage(frame);

		ovl->renderInputImage();

		if(textureId > -1)
			ovl->addOverlay(textureId);

		rgbaImage = ovl->getImage();

		//NvMedia2DBlit(engine2d, frame, nullptr, rgbaImage, nullptr, &blitParams);
		NvMediaStatus status = NvMedia2DBlit(engine2d, frame, nullptr, rgbaImage, nullptr,  &blitParams);
		if (status != NVMEDIA_STATUS_OK) {
			fprintf(stdout, "Function Name:%s: File:%s: NvMedia2DBlit failed ,status:%d\n", __func__, __FILE__, status);
		}

		if(rgbaImage) ovl->releaseImage(rgbaImage);
		ovl->getFrame();
		winOvl->swapBuffers();

		setTextureId();
		linkStages(frame);
	}
	else{
	}

	return State::RUN;

}
OverlayFiniteStateMachine::State OverlayFiniteStateMachine::ST_Failed(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}

OverlayFiniteStateMachine::State OverlayFiniteStateMachine::ST_Stopping(){
	//TODO[1] TO BE IMPLEMENTED
	return State::STOPPED;
}
OverlayFiniteStateMachine::State OverlayFiniteStateMachine::ST_Initializing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::INITIALIZED;
}
OverlayFiniteStateMachine::State OverlayFiniteStateMachine::ST_Running(){
	//TODO[1] TO BE IMPLEMENTED
	return State::RUN;
}
OverlayFiniteStateMachine::State OverlayFiniteStateMachine::ST_Failing(){
	//TODO[1] TO BE IMPLEMENTED
	return State::FAILED;
}
