/////////////////////////////////////////////////////////////////////////////////////////
// This code contains NVIDIA Confidential Information and is disclosed 
// under the Mutual Non-Disclosure Agreement. 
// 
// Notice 
// ALL NVIDIA DESIGN SPECIFICATIONS AND CODE ("MATERIALS") ARE PROVIDED "AS IS" NVIDIA MAKES 
// NO REPRESENTATIONS, WARRANTIES, EXPRESSED, IMPLIED, STATUTORY, OR OTHERWISE WITH RESPECT TO 
// THE MATERIALS, AND EXPRESSLY DISCLAIMS ANY IMPLIED WARRANTIES OF NONINFRINGEMENT, 
// MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. 
// 
// NVIDIA Corporation assumes no responsibility for the consequences of use of such 
// information or for any infringement of patents or other rights of third parties that may 
// result from its use. No license is granted by implication or otherwise under any patent 
// or patent rights of NVIDIA Corporation. No third party distribution is allowed unless 
// expressly authorized by NVIDIA.  Details are subject to change without notice. 
// This code supersedes and replaces all information previously supplied. 
// NVIDIA Corporation products are not authorized for use as critical 
// components in life support devices or systems without express written approval of 
// NVIDIA Corporation. 
// 
// Copyright © 2014-2016 NVIDIA Corporation. All rights reserved.
//
// NVIDIA Corporation and its licensors retain all intellectual property and proprietary
// rights in and to this software and related documentation and any modifications thereto.
// Any use, reproduction, disclosure or distribution of this software and related
// documentation without an express license agreement from NVIDIA Corporation is
// strictly prohibited.
//
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef __OVERLAYBLEND_H_
#define __OVERLAYBLEND_H_

#include <string>
#include "fbo.h"
#include "util_GL.h"
#include "util_Matrix.h"
#include "streamconsumeropengl.h"
#include "streamproduceropengl.h"
#include <list>


typedef struct
{

	std::list<const char*> FILE_PATH;
    int   SRC_WIDTH;
    int   SRC_HEIGHT;

    int   DST_WIDTH;
    int   DST_HEIGHT;

}OVLCMSPARAMETERS;

typedef struct
{
    unsigned char* bmpimg;
    GLsizei width;
    GLsizei height;
}BMPData;

typedef struct
{
    unsigned char* img;
    GLsizei width;
    GLsizei height;
}OVLIMG;

//#######################################################################################
class OverlayBlend
{
public:

	/** @breif This method Initialize the class and required variables.
	 *
	 */
    OverlayBlend();

    /** @breif This method destroy the class and variables.
	 *
	 */
    ~OverlayBlend();

    /** @breif This method create a program and initialize all the required variables.
	 *
	 */
    void initialize(OVLCMSPARAMETERS parameters, int sizeTexture);

    /** @breif This method release all buffers, textures and programs.
	 *
	 */
    void release();
    
    /** @breif This method render input frame.
     * @var inputTexture: input texture unit.
	 *
	 */
    void renderImage(const GLuint inputTexture);

    /** @brief This method render image texture.
	 * @param textureId: id of the texture to render.
	 *
	 * @return void
	 */
    void renderOverlay(unsigned int textureId);

    /** @brief This method return the output image texture.
	 *
	 * @return output texture
	 */
    GLuint getOutputTexture() { return m_rectifiedFBO.tex; };

private:
    void generatePassthroughMesh();


    void convertPNG2BMP( const char *filename,BMPData *bmpdata);
private:

    typedef struct{ GLuint program; GLuint aPosition; GLuint aTexcoord; GLuint uTex0; GLint myUniformLocation; GLint myUniformLocationh; GLuint uTex1; GLuint uTex2;}PROGRAM_TEXQUAD;


    // Vertex shaders
    std::string get2DTexQuadVS();

    std::string get2DTexQuadPSH();

    PROGRAM_TEXQUAD m_programMesh;

private:

	/** @var ovlimg: OVLIMG object
	 *
	 */
    OVLIMG*  ovlimg;

    /** @var ovlimg: FBO object
	 *
	 */
    FBO m_rectifiedFBO;

    // Passthrough quad
    GLuint  m_quadVBO;
    GLuint  m_quadVAO;

    /** @var m_texture: array in which the generated texture names are stored
     *
     */
    GLuint* m_texture;

    bool m_initialized;

    /** @var _sizeTexture: number of textures.
	 *
	 */
    int _sizeTexture;

    unsigned char* pngimg;
};

#endif
