
#ifndef OVERLAY_H
#define OVERLAY_H

#include "window.h"
#include "eglstream.h"
#include "streamconsumernvmediaimage.h"
#include "streamconsumeropengl.h"
#include "streamproduceropengl.h"
#include "streamproducernvmediaimage.h"
#include "overlayblend.h"
#include "gpuprogram.h"
#include "vbo.h"
#include <nvmedia.h>


class Overlay
{
public:

		/** @brief Parameterized constructor.
		 *
		 */
	    Overlay(NvMediaDevice *device, EGLDisplay display, AbstractWindow* win, int cameras, OVLCMSPARAMETERS gparameters, const char* vShader, const char* fShader, int sizeTexture);

	    /** @brief Overlay Class Destructor.
		 *
		 */
	    ~Overlay();

	    EGLStream** getCam2GlStream(/*EGLStream** cam2gl*/);

	    /** @brief This method initialize openGL (ES) for rendering and setup GL state as requeired.
		 * @param window:
		 * @param gparameters: information of the image to reder.
		 * @param sizeTexture: number of textures to render.
		 *
		 * @return void
		 */
	    void glInit(AbstractWindow* window, OVLCMSPARAMETERS gparameters, int sizeTexture);

	    /** @brief This method render image to eglsurface.
		 * @param gparameters: information of the image to render.
		 *
		 * @return void
		 */
	    void renderImage2EglSurface(OVLCMSPARAMETERS* cmsParameters);

	    /** @brief This method display the output image.
		 * @param gparameters: information of the image to render
		 *
		 * @return void
		 */
	    void glDisplay(OVLCMSPARAMETERS* cmsParameters);

	    /** @brief This method return Nvmedia stream producer.
		 *
		 * @return NvMediaImage stream producer
		 */
	    StreamProducerNvMediaImage** getNvidiaProducer();

	    /** @brief This method return OPENGL stream producer.
		 *
		 * @return OPENGL stream producer
		 */
	    StreamProducerOpenGL** getOpenGlProducer();

	    /** @brief This method acquire image from EGL stream.
	     *
		 * @return consumer acquire image.
		 */
	    NvMediaImage* getImage();

	    /** @brief This method return frame from the stream.
	     *
	     * @return frame from the stream.
	     */
	    NvMediaImage* getFrame();

	    /** @brief This method post producer NvMediaImage frame to stream.
		 * @var image: frame to post.
		 *
		 * @return true if the frame was posted correctly or false if not.
		 */
	    bool postImage(NvMediaImage* image);

	    /** @brief This method realease acquired frames.
	     * @var image: frame to realease.
	     *
	     * @return void
	     */
	    void releaseImage(NvMediaImage* image);

	    /** @brief This method return a image texture id.
		 *
		 * @return a texture id.
		 */
	    const GLuint getTexid();

	    /** @brief This method post texture to the stream.
	     *
	     * @return void
	     */
	    void postTexture();

	    /** @brief This method render input image.
		 *
		 * @return void
		 */
	    void renderInputImage();

	    /** @brief This method render a texture.
		 * @param textureId: variable to select a texture.
		 *
		 * @return void
		 */
	    void addOverlay(unsigned int textureId);

private:

	    /** @var gparameters: OVLCMSPARAMETERS object.
		 *
		 */
	    OVLCMSPARAMETERS gparameters;

	    /** @var consumersNM: StreamConsumerNvMediaImage object.
		 *
		 */
		StreamConsumerNvMediaImage** consumersNM;

		/** @var consumersGL: StreamConsumerOpenGL object.
		 *
		 */
		StreamConsumerOpenGL** consumersGL;

		/** @var producers: StreamProducerOpenGL object.
		 *
		 */
		StreamProducerOpenGL** producers;

		/** @var producersNM: StreamProducerNvMediaImage object.
		 *
		 */
		StreamProducerNvMediaImage** producersNM;

		/** @var streamsCam2GL: EGLStream object.
		 *
		 */
		EGLStream** streamsCam2GL;

		/** @var streamsGL2Cam: EGLStream object.
		 *
		 */
		EGLStream** streamsGL2Cam;

		/** @var _vShader: Path of vertex shader.
		 *
		 */
		const char* _vShader;

		/** @var _fShader: Path of fragment shader.
		 *
		 */
		const char* _fShader;

		/** @var blender: OverlayBlend object.
		 *
		 */
		OverlayBlend* blender;

		/** @var cameras: number o cameras.
		 *
		 */
		int cameras;

		//openGL Parameters
		GLuint locations[2];
		GLuint index;

		/** @var quaVBO: VBO class object.
		 *
		 */
		VBO* quaVBO;

		/** @var texturedpro: GPUProgram class object.
		 *
		 */
		GPUProgram* texturedpro;

};


#endif
