/************  Copyright 2016-2026 FICOSA ADAS  ***************//**
*  @par Language: MISRA C++ 2008 & C++11
*******************************************************************
*  @file OverlayFiniteStateMachine.h
*  @brief Template to create new modules
*  @author  Albert Mosella Montoro
*  @version 1.0
*  @date 15/06/2016
*******************************************************************/

#ifndef OVERLAYFINITESTATEMACHINE_H_
#define OVERLAYFINITESTATEMACHINE_H_
#include "FiniteStateMachineTemplate.h"
#include "overlay.h"
#include "overlayblend.h"
#include "nvmedia_2d.h"

/** @class OverlayFiniteStateMachine
 *  @details This class incorporates the functionalities to
 *  		 release memory of the capture module
 */

class OverlayFiniteStateMachine: public FiniteStateMachineTemplate {
public:

	/** @brief Default constructor initialize the parameters with default values.
	 *
	 */
	OverlayFiniteStateMachine(NvMediaDevice *_device, int _width, int _height,int cameraCount , std::list<const char*> filePath, bool overlay, AbstractWindow* _winOvl, const char* vShader, const char* fShader);

	/** @brief Free the resources generated for the object
	 */
	virtual ~OverlayFiniteStateMachine();
	/** @brief This method manages the messages that comes from Handle Module.
	 * Sets the image overlayed according the ID that comes from Handle
	 *
	 * @return void
	 */
	void setTextureId();

private:

	/** @var device: NvMediaDevice object.
	 *
	 */
	NvMediaDevice *device;

	/** @var rgbImage: NvMediaImage object.
	 *
	 */
	NvMediaImage *rgbaImage;

	/** @var ovlImage: NvMediaImage object.
	 *
	 */
	NvMediaImage *ovlImage;

	/** @var gparametersovl: OVLCMSPARAMETERS object.
	 *
	 */
	OVLCMSPARAMETERS gparametersovl;

	/** @var ovl: Overlay Class object.
	 *
	 */
	Overlay* ovl;

	/** @var engine2d: NvMedia2D object.
		 *
		 */
	NvMedia2D *engine2d;

	/** @var sizeTexture: number of textures.
	 *
	 */
	int sizeTexture;

	/** @var width:
	 *
	 */
	int width;

	/** @var height:
	 *
	 */
	int height;
	/** @var textureId: Id of the current texture stored in the queue of overlay
	 *
	 */
	int textureId;
	/** @var lastTextureId: Id of the last texture stored in the queue of overlay
		 *
		 */
	int lastTextureId;

	/** @var blitParams: NvMedia2DBlitParameters object.
	 *
	 */
	NvMedia2DBlitParameters blitParams;
	AbstractWindow* winOvl;
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state stopped, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopped();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state initialized, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Initialized();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state run, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Run();
	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state failed, after execution
	 * 		   returns the current state.
	 *
	 * @return State
	*/
	State ST_Failed();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Stopping();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to initialize,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Initializing();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state transition to run,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Running();

	/** @brief This method is the override of the parent class. Its used to
	 * 		   describe the behavior of the state fail to stop,
	 * 		   after execution returns the current state.
	 *
	 * @return State
	*/
	State ST_Failing();
};

#endif /* OVERLAYFINITESTATEMACHINE_H_ */
